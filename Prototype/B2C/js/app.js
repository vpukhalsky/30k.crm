'use strict';

angular.module('30k.b2c', [
    
    'mm.foundation',
    'ui.router',
    'ngAnimate',
    'duScroll',
    'angulartics', 
    'angulartics.google.analytics',
    'angulartics.debug', //remove on production as well as the script in HTML footer
    
    '30k.service',
    '30k.tooltip',
    '30k.error',
    '30k.alerts',
    '30k.user.filters',
    '30k.focus'
    
])

.controller('ModalController', ['$scope', '$modalInstance', function($scope, $modalInstance){
                
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
}])

.service('googleAuthService', ['$q', function($q){
    
    var self = this;
    
    var promises = {
        init: null,
        api: null
    };
    
    var options = {
        scope: 'profile email',
        fetch_basic_profile: true
    };
    
    function initApi(){
        
        if(!promises.api){
            
            promises.api = $q.defer();
            /*
            window.onGoogleApiLoad = function(){
                promises.api.resolve(gapi);
            }.bind(self);
            */
            if(gapi) promises.api.resolve(gapi);
            else promises.api.reject();
            
        }
        
        return promises.api.promise;
    }
    
    function init(){
      
        if(!promises.init){
            
            promises.init = $q.defer();
            
            initApi().then(function(gapi){
                
                gapi.load('auth2', function(){
                    
                    var auth2 = gapi.auth2.init({
                        //client id set up in html head as meta param
                    });
                    
                    promises.init.resolve({ gapi: gapi, auth2: auth2 });
                    
                }.bind(self));
                
            }, function(){
                
                promises.init.reject();
                
            });
            
        }
        
        return promises.init.promise;
        
    }
    
    function addClickListener(elementId){
        
        var q = $q.defer();
        
        init().then(function(api){
            
            var element = document.getElementById(elementId);
            
            api.auth2.attachClickHandler(element, options, function(googleUser){
                
                var response = googleUser.getAuthResponse();
                q.resolve(response.access_token);
                
            }, function(error){
                
                console.log(error);
                q.reject(error);
                
            });
            
        }, function(){
            
            console.warn('Google Auth2 could not be initialized');
            q.reject();
            
        });
        
        return q.promise;
        
    }
    
    return {
        init: init,
        addClickListener: addClickListener
    };
    
}])

.service('etcService', ['$modal', 'collectionService', '$filter', '$q', function($modal, collectionService, $filter, $q){
    
    var chromeExtensionIds = {
        development: 'iabpdmlbcbmiagljjjmpgbidjknfjoig',
        testing: 'lkkpbdnodfgbepjchnpbebimibcpinbi',
        production: 'efpemjkcjaedejkodfdapnddegflnkkh'
    };
    
    function logIn(hideSignUp){
        
        $modal.open({
            windowClass: 'small login-modal',
            templateUrl: 'views/login/login.html?v=5',
            controller: 'LoginController',
            resolve: {
                hideSignUp: function(){ return hideSignUp; }
            }
        });
        
    }
    
    function forgotPassword(returnToLogin){
        
        $modal.open({
            windowClass: 'tiny forgot-pass-modal',
            templateUrl: 'views/login/forgot-password.html?v=1',
            controller: 'ForgotPasswordController',
            resolve: {
                returnToLogin: function(){ return returnToLogin; }
            }
        });
        
    }
    
    function getFfps(text){
        
        return collectionService.getCollection('home/get-ffps.json', 'sffp', { text: text });
        
    }
    
    function getAirports(text){
        
        return collectionService.getCollection('signup/get-airports.json', 'airport', { text: text });
        
    }
    
    function getAirlines(text){
        
        return collectionService.getCollection('home/get-airlines.json', 'airline', { text: text }, true, 'calc');
        
    }
    
    function getShortItinerary(itinerary, airports){
        
        var middle = null;
        var legs = itinerary.legs;
        var lastLegFlights = legs[legs.length - 1].flights;
        var findFilter = $filter('find');
        
        var first = {
            code: legs[0].flights[0].departure,
            direct: true
        };

        var last = {
            code: legs[legs.length - 1].flights[lastLegFlights.length - 1].arrival
        };
        
        if(legs.length <= 1 && legs[0].flights.length > 1){
            
            var flight = Math.floor(legs[0].flights.length / 2);
            middle = {
                code: legs[0].flights[flight].departure,
                direct: true
            };
            
            if(legs[0].flights.length > 3) first.direct = false;
            if(legs[0].flights.length > 2) middle.direct = false;
            
        }else if(legs.length > 1){
            
            var leg = Math.floor(legs.length / 2);
    
            middle = {
                code: legs[leg].flights[legs[leg].flights.length - 1].departure,
                direct: true
            };
            
            if(legs.length != 2 || legs[0].flights.length != 1 || legs[1].flights.length != 1){
                first.direct = false;
            }
            
            if(legs.length > 2){
                middle.direct = false;
            }
            
        }
        
        var flights = [first];
        if(middle) flights.push(middle);
        flights.push(last);
        
        flights.forEach(function(f){
            f.name = findFilter(airports, f.code, 'code').name;
        });
        
        return flights;
    }
    
    function showFullItinerary(itinerary, airports, airlines){
        
        $modal.open({
            windowClass: 'small full-itinerary-modal',
            templateUrl: 'views/home/full-itinerary.html?v=1',
            controller: 'ItineraryModalController',
            resolve: {
                itinerary: function(){ return itinerary; },
                airports: function(){ return airports; },
                airlines: function(){ return airlines; }
            }
        });
        
    }
    
    function updateChromeExtension(action, data){
       
        var q = $q.defer();
       
        if(!window.chrome || !angular.isObject(chrome) || !angular.isObject(chrome.runtime)){
            q.reject();
            return q.promise;
        }
    
        var message = { action: action };
        if(data) message.data = data;
       
        for(var i in chromeExtensionIds){
           
           if(!chromeExtensionIds.hasOwnProperty(i)) continue; 
           var id = chromeExtensionIds[i];
           
           chrome.runtime.sendMessage(id, message, function(response){
               if(q && response !== undefined) {
                   q.resolve(response);
                   q = null;
               }
           });
           
        }
        
        return q.promise;
       
    }
    
    return {
        logIn: logIn,
        getFfps: getFfps,
        getAirports: getAirports,
        getAirlines: getAirlines,
        getShortItinerary: getShortItinerary,
        showFullItinerary: showFullItinerary,
        updateChromeExtension: updateChromeExtension,
        forgotPassword: forgotPassword
    };
    
}])

.controller('AccrualDetailsModalController', ['$scope', '$modalInstance', 'ffp', 'awardGoal', 'fare', 'accrualId', 'findFilter', function($scope, $modalInstance, ffp, awardGoal, fare, accrualId, findFilter){
    
    $scope.ffp = ffp;
    $scope.awardGoal = awardGoal;
    $scope.fare = fare;
    $scope.accrual = findFilter(ffp.accruals, accrualId);
    
    $scope.canShowProgress = (ffp.connected || ffp.manualInput) 
        && (
            ($scope.accrual.status && angular.isObject(ffp.statusGoal))
            || (!$scope.accrual.status && angular.isObject(awardGoal) && angular.isObject(ffp.awardGoal))
        );
        
    $scope.getCurrentMiles = function(){
        
        return $scope.accrual.value;
        
    };
    
    function getThreshold(){
        
        var t = null;
        
        if($scope.accrual.status){
            
            t = $scope.ffp.statusGoal.thresholds;
            
        }else{
            
            t = $scope.ffp.awardGoal.thresholds;
            
        }
        
        var ta = findFilter(t, accrualId, 'accrual');
        
        return ta;
        
    }
    
    $scope.getThreshold = function(){
        
        var ta = getThreshold();
        
        return ta.value;
        
    };
    
    $scope.getRemainingMiles = function(){
        
        var f = $scope.getCurrentMiles();
        if(fare) f = $scope.getTotalMiles();
        var left = $scope.getThreshold() - f;
        if(left < 0) left = 0;
        return left;
        
    };
    
    $scope.getTotalMiles = function(){
        
        var a = findFilter(fare.accruals, accrualId, 'accrualId');
        return $scope.getCurrentMiles() + a.value;
        
    };
    
    $scope.getNextStatus = function(){
        
        return ffp.statusGoal.name;  
        
    };
    
    $scope.cancel = function(){
        
        $modalInstance.dismiss('cancel');
        
    };
    
    $scope.hasConditions = function(){
        
        if(!$scope.accrual.status) return false;
        
        var t = getThreshold();
        
        return angular.isObject(t) && angular.isArray(t.conditions) && t.conditions.length > 0;
          
    };
    
    $scope.getConditions = function(){
        
        var t = getThreshold();
        return t.conditions;
        
    };
    
}])

.controller('MainController', ['$scope', 'etcService', 'kkService', '$timeout', function($scope, etcService, kkService, $timeout){
    
    var confMsgDelay = 3000;
    
    $scope.sendingConfirmation = false;
    $scope.sentConfirmation = false;
    
    if(location.hash == '#/forgot-password'){
        etcService.forgotPassword(false);
    }
    
    $scope.logIn = function(hideSignUp){
        
        etcService.logIn(hideSignUp);
        
    };
    
    $scope.resendConfirmation = function(){
        
        $scope.sendingConfirmation = true;
        
        kkService.sendPost('dashboard/account-confirmation.json', {}).then(function(response){
            
            $scope.sentConfirmation = true;
            $scope.sendingConfirmation = false;
            
            $timeout(function(){
                $scope.sentConfirmation = false;
            }, confMsgDelay);
            
        }, function(){
            
            $scope.sendingConfirmation = false;
            
        });
          
    };
    
}])

.directive('kkTest', [function(){
    
    return {
        restrict: 'E',
        scope: {
            tests: '='
        },
        template: '<div ng-include="template"></div>',
        controller: function($scope, $element){
            
            var testParamName = 'ga_ab_test';
            
            $scope.template = '';
            
            function parseSearchParams(){
                
                var params = {};
                
                if(window.location.search.length <= 1) return params;
                
                var search = window.location.search.substring(1);
                var pairs = search.split('&');
                
                pairs.forEach(function(pair){
                    
                    var p = pair.split('=');
                    
                    if(p.length > 1){
                        
                        var key = decodeURIComponent(p[0]);
                        var value = decodeURIComponent(p[1]);
                        params[key] = value;
                        
                    }
                    
                });
                
                return params;
                
            }
            
            function getTestParam(){
                
                var params = parseSearchParams();
                var param = 'default';
                if(angular.isString(params[testParamName])) param = params[testParamName];
                return param;
                
            }
            
            function init(){
                
                var tests = {};
                
                if(angular.isString($scope.tests)){
                    tests.default = $scope.tests;
                }else if(angular.isObject($scope.tests)){
                    tests = $scope.tests;
                }
                
                if(!angular.isString(tests.default)){
                    console.log('Invalid kk-test configuration. No default template url is defined.');
                    return false;
                }
                
                var test = getTestParam();
                $scope.template = tests[test];
                
            }
            
            init();
            
        }
    };
    
}]);

