'use strict';

/* Focuses specific field automatically */

angular.module('30k.focus', [])

.service('kkFocusService', ['$rootScope', '$timeout', function($rootScope, $timeout){
    
    var delay = 200;
    
    function focus(id){
        
        blur();
        
        $timeout(function(){
            $rootScope.$broadcast('kk-focus', { id: id });
        }, delay + 1);
        
    }
    
    function blur(){
        
        $timeout(function(){
            $rootScope.$broadcast('kk-blur');
        });
        
    }
    
    return {
        focus: focus,
        blur: blur
    };
    
}])

.directive('kkFocus', ['kkFocusService', function(kkFocusService){
    
    return {
        controller: ["$scope", "$element", "$attrs", function($scope, $element, $attrs){
            
            $scope.$on('kk-focus', function(event, args){
                
                if($attrs.kkFocus == args.id && $($element).not(':focus').size() > 0){
                    
                    $scope.$apply(function(){
                
                        $($element).focus();
                    });
                    
                }
                
            });
            
            $scope.$on('kk-blur', function(event){
                
                $scope.$apply(function(){
                    $($element).blur();
                });
                
            });
            
        }]
    };
    
}]);

