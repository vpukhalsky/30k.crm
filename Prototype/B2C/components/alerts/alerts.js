'use strict';

/* Success and error alerts module */

angular.module('30k.alerts', [
    'mm.foundation'
])

.directive('kkAlerts', ['$rootScope', '$location', '$anchorScroll', 'alertService', function($rootScope, $location, $anchorScroll, alertService){
    
    return{
        restrict: 'A',
        scope: {
            key: '@?'
        },
        template: '<alert ng-repeat="alert in alerts" type="alert.type" close="closeAlert($index)" class="slideInDown fadeOut">{{alert.msg}}</alert>',
        
        controller: ["$scope", "$element", function($scope, $element){
            
            var alertScrollOffset = 100;
            
            if(!angular.isString($scope.key)){
                $scope.key = 'global';
            }
            
            $scope.alerts = alertService.getAlerts($scope.key);

            $scope.$watchCollection('alerts', function(newValue, oldValue){
              
              if(newValue.length > oldValue.length){
                scrollToAlert();
              }
              
            });
            
            function scrollToAlert(){
                
                var top = $(window).scrollTop();
                var height = $(window).height();
                var offset = $($element).offset();
                
                if(offset.top < top + alertScrollOffset || offset.top > top + height - alertScrollOffset){
                
                    var scrollOffset = offset.top - alertScrollOffset;
                    if(scrollOffset < 0) scrollOffset = 0;
                    $('html,body').animate({ scrollTop: (scrollOffset + 'px') }, 300);
                    
                }
                
            }
            
            $scope.addAlert = function(alert){
                addAlert(alert);
            };
            
            function addAlert(alert){
                $scope.alerts.unshift(alert);
            }
            
            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };
    
        }]
    };
    
}])

.service('alertService', ['$rootScope', '$modal', '$timeout', 'indexOfFilter', function($rootScope, $modal, $timeout, indexOfFilter){
    
    var alerts = { global: [] };
    var id = 0;
    var hideDelay = 3000;
    
    function getAlerts(key){
        
        if(!angular.isDefined(key)) key = 'global';
        else if(!angular.isArray(alerts[key])) alerts[key] = [];
        return alerts[key];
        
    }
    
    function show(message, type, key){
        
        id++;

        var a = getAlerts(key);
        if(!type) type = 'success';
        a.unshift({ type: type, msg: message, id: id});
        
        var popId = id;
        
        $timeout(function(){
            hide(key, popId);
        }, hideDelay);
        
    }
    
    function hide(key, id){
        
        var a = getAlerts(key);
        var index = indexOfFilter(a, id);
        if(index >= 0) a.splice(index, 1);
        
    }
    
    function popup(title, message, callback){
        
        $modal.open({
            template: '<div class="panel"><h3>{{title}}</h3><p>{{message}}</p><button ng-click="cancel()" class="secondary small">OK</button><a class="close-reveal-modal" ng-click="cancel()">&#215;</a></div>',
            windowClass: 'tiny',
            resolve: {
                title: function(){
                    return title;
                },
                message: function(){
                    return message;
                }
            },
            controller: function($scope, $modalInstance, title, message){
                
                $scope.title = title;
                $scope.message = message;
                
                $scope.cancel = function(){
                    $modalInstance.dismiss('cancel');
                };
                
            }
        }).result.then(function(){
            if(callback) callback();
        });
    }
    
    return {
        show: show,
        popup: popup,
        getAlerts: getAlerts
    };
    
}]);
