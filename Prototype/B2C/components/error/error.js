'use strict';

/* Form inputs error messages module */

angular.module('30k.error', [])

.directive('kkError', ['$timeout', function($timeout){
    
    return {
        
        scope: {
            field: '=',
            type: '@?'
        },
        
        template: '<small class="error kk-error" ng-cloak ng-show="show()" ng-init="onInit()" ng-class="{ \'hingeInFromTop hingeOutFromTop\' : initialized }" ng-transclude></small>',
        replace: true,
        restrict: 'AE',
        transclude: true,
        
        controller: ["$scope", "$element", function($scope, $element){
            
            var submited = false;
            
            $scope.initialized = false;
            
            $scope.onInit = function(){
                
                $timeout(function(){
                    $scope.initialized = true;
                }, 200);
                
            };
            
            $scope.show = function(){
                
                if(!$scope.initialized) { return false; }
                
                if($scope.field.$invalid && ($scope.field.$dirty || submited)){
                    
                    if(angular.isString($scope.type) && !$scope.field.$error[$scope.type]){
                        return false;
                    }

                    return true;
                }

                return false;
            };
            
            function onFormSubmit(){
                submited = true;
            }
            
            var form = $element.closest('form');
            form.on('submit', onFormSubmit);
            
            $scope.$on('$destroy', function(){
                form.off('submit', onFormSubmit);
            });
            
            $scope.$on('kk-error', function(event){
                onFormSubmit();
            });
        }]
    };
}]);