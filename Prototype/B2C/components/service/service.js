'use strict';

angular.module('30k.service', [
  '30k.alerts'
])

.service('kkService', ['$http', '$q', '$rootScope', 'alertService', function($http, $q, $rootScope, alertService){
    
    var jsonPath = 'json/';
    var globalMessage = 'An unknown server error occurred.';
    
    function _handleError(response, alertKey){
        
        if(response.config.loader && $rootScope.loading > 0) $rootScope.loading--;
        
        var message = globalMessage;
 
        if (!response.data.message){
            
            _showAlert('An unknown server error occured. Please try again or refresh page.', 'alert', alertKey);
            
        }else{
            
            _showAlert(response.data.message, 'alert', alertKey);
            message = response.data.message;
        }
        
        return $q.reject(message);

    }
         
    function _handleSuccess(response, alertKey){
        
        if(response.config.loader && $rootScope.loading > 0) $rootScope.loading--;
        
        if(!angular.isObject(response.data) || !response.data.success){
            
            var message = globalMessage;
            
            if(response.data.message){
                message = response.data.message;
                _showAlert(response.data.message, 'alert', alertKey);
            }
            
            return $q.reject(message);
            
        }else{
            if(response.data.message){
                _showAlert(response.data.message, 'success', alertKey);
            }
        }
        
        return response.data.result;
        
    }
    
    function _showAlert(msg, type, alertKey){
        
        if(type === undefined) type = 'alert';
        alertService.show(msg, type, alertKey);
        
    }
    
    function sendPost(urlSlug, data, loader, alertKey){
        
        if(loader === undefined) loader = true;
        
        if(loader) $rootScope.loading++;
        
        var request = $http({
            method: 'post',
            url: jsonPath + urlSlug,
            data: data,
            loader: loader
        });
    
        return resolve(request, alertKey);
        
    }
    
    function resolve(request, alertKey){
        return request.then(
            function(response){ return _handleSuccess(response, alertKey); }, 
            function(response){ return _handleError(response, alertKey); }
        );
    }
    
    function _prepareGetUrl(url, params){
        
        if(params && angular.isObject(params)){
            for(var i in params){
                url = url.replace(':' + i, params[i]);
            }
        }
        
        return url;
    }
    
    function sendGet(urlSlug, params, loader, alertKey){
        
        if(loader === undefined) loader = true;
        
        if(loader) $rootScope.loading++;
        
        var url = _prepareGetUrl(urlSlug, params);
        
        var request = $http({
            method: 'get',
            url: jsonPath + url,
            loader: loader
        });
        
        return resolve(request, alertKey);
    }
    
    function promise(data){
        
        var d = $q.defer();
        d.resolve(data);
        return d.promise;
        
    }
    
    return {
        sendPost: sendPost,
        sendGet: sendGet,
        promise: promise
    };
    
}])

.service('collectionService', ['kkService', '$filter', '$modal', function(kkService, $filter, $modal){
    
    var collections = {};
    var meta = {};
    
    function getConfig(url, type, params, loader, alertKey){
        
        if(!angular.isObject(meta[type])) meta[type] = {};
        
        if(angular.isDefined(meta[type].config)){
            return kkService.promise(meta[type].config);
        }
        
        var promise = null;
        
        if(params){
            promise = kkService.sendPost(url, params, loader, alertKey);
        }else{
            promise = kkService.sendGet(url, {}, loader, alertKey);
        }
        
        return promise.then(function(response){
            meta[type].config = response;
            return meta[type].config;
        });
        
    }
    
    function _openDeleteItemsModal(url, type, items, templateUrl){
        
        if(!templateUrl) templateUrl = 'components/service/delete-items-modal.html';
        
        return $modal.open({
            templateUrl: templateUrl,
            controller: 'DeleteItemsModalController',
            windowClass: 'tiny delete-items-modal',
            resolve: {
                items: function(){
                    return items;
                },
                type: function(){
                    return type;
                },
                url: function(){
                    return url;
                }
            }
        });
        
    }
    
    function deleteItem(url, type, item, templateUrl, loader, alertKey){
        
        var modalInstance = _openDeleteItemsModal(url, type, [item], templateUrl);
        
        return modalInstance.result.then(function(result){
            if(result.remove == true) return _deleteItem(result.url, result.type, result.items[0].id, loader, alertKey);
            return false;
        });
        
    }
    
    function deleteItems(url, type, items, templateUrl, loader, alertKey){
        
        var modalInstance = _openDeleteItemsModal(url, type, items, templateUrl);
        
        return modalInstance.result.then(function(result){
            var itemIds = result.items.map(function(item){ return item.id });
            if(result.remove == true) return _deleteItems(result.url, result.type, itemIds, loader, alertKey);
            return false;
        });
        
    }
    
    function _deleteItem(url, type, id, loader, alertKey){
        
        return kkService.sendPost(url, { id: id }, loader, alertKey).then(function(response){
            
            _deleteItemFromCollection(type, response.item);

            return response;
        });
        
    }
    
    function _deleteItems(url, type, ids, loader, alertKey){
        
        return kkService.sendPost(url, { ids: ids }, loader, alertKey).then(function(response){
            
            response.items.forEach(function(item){
                _deleteItemFromCollection(type, item);
            });
            
            return response;
        });
        
    }
    
    function _deleteItemFromCollection(type, item){
        
        var collection = collections[type];
        
        var index = $filter('indexOf')(collection, item.id);
        if(index >= 0) collection.splice(index, 1);
        
        if(angular.isObject(meta[type]) && angular.isArray(meta[type].fullItems)){
            var i = meta[type].fullItems.indexOf(item.id);
            if(i >= 0) meta[type].fullItems.splice(i, 1);
        }
        
    }
    
    function getCollection(url, type, params, loader, alertKey){
        
        if(!angular.isDefined(params)) params = {};
        if(loader === undefined) loader = true;
        
        if(angular.isArray(collections[type]) && meta[type].paged === false) return kkService.promise(collections[type]);
        else{
            return kkService.sendPost(url, params, loader, alertKey).then(function(response){
                
                collections[type] = response.items;
                
                if(!angular.isObject(meta[type])) meta[type] = {};
                meta[type].paged = false;
                meta[type].fullItems = [];
                
                return collections[type];
                
            });
        }
        
    }
    
    function getPagedCollection(url, type, params, loader, alertKey){
        
        var key = JSON.stringify(params);
        if(loader === undefined) loader = true;
        
        if(angular.isArray(collections[type]) && angular.isObject(meta[type]) && meta[type].paged && meta[type].lastPagedKey == key){
            return kkService.promise(meta[type].lastPagedResponse);
        }
        
        return kkService.sendPost(url, params, loader, alertKey).then(function(response){
            collections[type] = response.items;
            
            if(!angular.isObject(meta[type])) meta[type] = {};
            
            meta[type].paged = true;
            meta[type].lastPagedKey = key;
            meta[type].lastPagedResponse = response;
            
            return response;
        });
        
    }
    
    function getFullItem(url, type, value, paramName, loader, alertKey){
        
        if(!angular.isDefined(paramName)) paramName = 'id';
        
        if(angular.isObject(meta[type]) 
            && angular.isArray(meta[type].fullItems) 
            && meta[type].fullItems.indexOf(value) >= 0
        ){
            var item = $filter('find')(collections[type], value, paramName);
            if(item) return kkService.promise(item);
        }
        
        var params = {};
        params[paramName] = value;
        
        return kkService.sendPost(url, params, loader, alertKey).then(function(response){
            
            if(!angular.isArray(collections[type])) collections[type] = [];
            var item = _updateItem(type, response.item);
            
            if(!angular.isObject(meta[type])) meta[type] = {};
            if(!angular.isArray(meta[type].fullItems)) meta[type].fullItems = [];
            meta[type].fullItems.push(value);
            
            return item;
        });
    
    }
    
    function getItem(url, type, value, paramName){
        
        if(!angular.isDefined(paramName)) paramName = 'id';
        
        if(angular.isArray(collections[type])){
            var item = $filter('find')(collections[type], value, paramName);
            if(item) return kkService.promise(item);
        }
        
        var params = {};
        params[paramName] = value;
        
        return kkService.sendPost(url, params, loader, alertKey).then(function(response){
            return response.item;
        });
    }
    
    function _updateItem(type, item){
        
        var collection = collections[type];
        
        var r = $filter('find')(collection, item.id);
        
        if(r) angular.extend(r, item);
        else{
            r = item; 
            if(angular.isArray(collection)) collection.push(r);
        }
        
        return r;
    }
    
    function saveItem(url, type, item, loader, alertKey){
        
        return kkService.sendPost(url, item, loader, alertKey).then(function(response){
            
            angular.extend(item, response.item);
            response.item = _updateItem(type, item);
            return response;
            
        });
        
    }
    
    function saveItems(url, type, items, loader, alertKey){
        
        return kkService.sendPost(url, items, loader, alertKey).then(function(response){
            
            var find = $filter('find');
            
            response.items.forEach(function(item){
                
                var i = find(items, item.id);
                
                if(i) {
                    angular.extend(i, item);
                }else{
                    i = item;
                }
                
                item = _updateItem(type, i);
            });
            
            return response;
            
        });
        
    }
    
    function cloneItem(url, type, item, loader, alertKey){
        
        var clone = angular.copy(item);
        delete clone.id;
        
        return saveItem(url, type, clone, loader, alertKey).then(function(response){
            
            if(collections[type]) _replaceClone(collections[type], item.id, response.item);
            return response;
            
        });
        
    }
    
    function _replaceClone(collection, originalId, clone){
        
        var indexOf = $filter('indexOf');
        var cloneIndex = indexOf(collection, clone.id);
        collection.splice(cloneIndex, 1);
        var originalIndex = indexOf(collection, originalId);
        collection.splice(originalIndex + 1, 0, clone);
        
    }
    
    function cloneItems(url, type, items, loader, alertKey){
        
        var ids = [];
        
        var clones = angular.copy(items);
        
        clones.forEach(function(clone){
            ids.push(clone.id);
            delete clone.id;
        });
        
        return saveItems(url, type, clones, loader, alertKey).then(function(response){
            if(collections[type]){
                for(var i = 0 ; i < response.items.length ; i++){
                    if(ids[i]) _replaceClone(collections[type], ids[i], response.items[i]);
                }
            }
            
            return response;
            
        });
        
    }
    
    function flushCollection(type){
        
        if(collections[type]) delete collections[type];
        if(meta[type]) delete meta[type];
        
    }

    return {
        getCollection: getCollection,
        getPagedCollection: getPagedCollection,
        deleteItem: deleteItem,
        deleteItems: deleteItems,
        getItem: getItem,
        saveItem: saveItem,
        saveItems: saveItems,
        getFullItem: getFullItem,
        getConfig: getConfig,
        cloneItem: cloneItem,
        cloneItems: cloneItems,
        flushCollection: flushCollection
    };
    
}])

.controller('DeleteItemsModalController', ['$scope', '$modalInstance', 'items', 'type', 'url', function($scope, $modalInstance, items, type, url){
    
    $scope.items = items;
    $scope.type = type;
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.confirm = function(){
        $modalInstance.close({
            remove: true,
            items: items,
            type: type,
            url: url
        });
    };
    
}]);