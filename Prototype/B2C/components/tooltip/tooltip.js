'use strict';

/* Simple tooltip with HTML content */

angular.module('30k.tooltip', [
  'ngSanitize'
])

.directive('kkTooltip', ['$compile', '$timeout', '$templateCache', function($compile, $timeout, $templateCache){
  
  return {
    restrict: 'A',
    scope: {
      kkTooltip: '@',
      tooltipData: '=?',
      tooltipIf: '=?',
      tooltipToggle: '=?'
    },
    controller: ["$scope", "$element", function($scope, $element){
      
      var idPrefix = 'kk-tooltip-';
      var counter = getCounter();
      var tip = null;
      var template = '<div ng-attr-id="{{id}}" class="kk-tooltip" ng-class="pos" ng-style="style"><span class="tip" ng-style="tipStyle"></span><span ng-include="template"></span></div>';
      var defaultContentTemplate = '<span ng-bind-html="kkTooltip"></span>';
      var tipOffset = {
        top: -14,
        left: 5
      };
      var globalOffset = {
        x: 6,
        y: 6
      };
      var timeout = false;
      var delay = 100;
      var toggleOpened = false;
      
      $scope.$watch('tooltipIf', function(newValue){
        
        if(newValue) angular.element($element).addClass('has-tooltip');
        else angular.element($element).removeClass('has-tooltip');
        
      });
      
      $scope.data = $scope.tooltipData;
      $scope.template = 'kk-tooltip-default-content-template.html';
      $scope.id = null;
      
      if(!angular.isDefined($scope.data)){
        
        $templateCache.put($scope.template, defaultContentTemplate);
        if(!angular.isDefined($scope.tooltipIf)) $scope.tooltipIf = $scope.kkTooltip;
        
      }else{
        
        if(!angular.isDefined($scope.tooltipIf)) $scope.tooltipIf = $scope.data;
        $scope.template = $scope.kkTooltip;
        
      }
      
      $scope.style = {
        top: 0,
        left: 0,
        visibility: 'hidden'
      };
      
      $scope.tipStyle = {
        top: tipOffset.top + 'px',
        left: tipOffset.left + 'px'
      };
      
      $scope.pos = 'bottom';
      
      function getCounter(){
        var body = $('body');
        return body.data('kk-tooltip-counter');
      }
      
      function setCounter(val){
        var body = $('body');
        return body.data('kk-tooltip-counter', val);
      }
      
      function createTooltip(){
        var el = angular.element(template);
        tip = $compile(el)($scope, function(tip, $scope){
          $('body').append(tip);
        });
      }
      
      function show(){
        
        if(timeout) $timeout.cancel(timeout);
        
        timeout = $timeout(function(){
          
          var wnd = $(window);
          var offset = $element.offset();
          
          var wndRect = {
            top: wnd.scrollTop(),
            left: 0,
            width: wnd.width(),
            height: wnd.height()
          };
          
          var relTargetRect = {
            top: offset.top - wndRect.top,
            left: offset.left - wndRect.left,
            width: $element.outerWidth(),
            height: $element.outerHeight()
          };
          
          var rect = {
            width: tip.outerWidth(),
            height: tip.outerHeight()
          };
          
          var tipRect = {
            width: Math.abs(tipOffset.left / 2),
            height: Math.abs(tipOffset.top / 2)
          };
          
          if(relTargetRect.top + relTargetRect.height + globalOffset.y + tipRect.height + rect.height < wndRect.height){
            
            $scope.pos = 'bottom';
            rect.top = relTargetRect.top + relTargetRect.height + globalOffset.y + tipRect.height;
            tipRect.top = tipOffset.top;
            
          }else if(relTargetRect.top - rect.height - tipRect.height - globalOffset.y > 0){
            
            $scope.pos = 'top';
            rect.top = relTargetRect.top - rect.height - tipRect.height - globalOffset.y;
            tipRect.top = rect.height;
            
          }else if(relTargetRect.left + relTargetRect.width + globalOffset.x + tipRect.width + rect.width < wndRect.width){
            
            $scope.pos = 'right';
            rect.left = relTargetRect.left + relTargetRect.width + globalOffset.x + tipRect.height;
            tipRect.left = - 2 * tipRect.height;
            
          }else{
            
            $scope.pos = 'left';
            rect.left = relTargetRect.left - globalOffset.x - tipRect.width - rect.width;
            tipRect.left = rect.width;
            
          }
          
          if($scope.pos == 'top' || $scope.pos == 'bottom'){
            
            if(relTargetRect.left + relTargetRect.width < wndRect.width){
              
              rect.left = relTargetRect.left + globalOffset.x;
              tipRect.left = tipOffset.left;
              
            }else{
              
              rect.left = wndRect.width - rect.width - globalOffset.x;
              tipRect.left = relTargetRect.left - rect.left + tipOffset.left;
              
            }
            
          }else{
            
            if(relTargetRect.top + rect.height - globalOffset.y < wndRect.height){
              
              rect.top = relTargetRect.top - globalOffset.y;
              tipRect.top = tipOffset.top;
              
            }else{

              rect.top = wndRect.height - rect.height - globalOffset.y;
              tipRect.top = relTargetRect.top - rect.top + Math.abs(tipOffset.top);
              
            }
            
          }
          
          $scope.style.left = (wndRect.left + rect.left) + 'px';
          $scope.style.top = (wndRect.top + rect.top) + 'px';
          $scope.style.visibility = 'visible';
          $scope.style.display = 'inline-block';
          $scope.tipStyle.left = tipRect.left + 'px';
          $scope.tipStyle.top = tipRect.top + 'px';
          
          timeout = false;
          
        }, delay);
        
        $('body').on('mousemove', onMouseMove);
        
      }
      
      function hide(){
        
        if(timeout) $timeout.cancel(timeout);
        
        timeout = $timeout(function(){
          $scope.style.visibility = 'hidden';
          timeout = false;
        }, delay);
        
        $('body').off('mousemove', onMouseMove);
      }
      
      function createAndShow(){
        
        if($scope.tooltipIf){
          if(!tip) createTooltip();
          show();
        }
        
      }
      
      function onMouseEnter(e){ 
        
        if(!toggleOpened) createAndShow();
        
      }
      
      function onMouseLeave(e){
        
        if(!toggleOpened) hide();
        
      }
      
      function onMouseMove(e){
        
        if(toggleOpened) return;
        
        var x = e.pageX;
        var y = e.pageY;
        var offset = $element.offset();
        var width = $element.outerWidth();
        var height = $element.outerHeight();
        
        if((x < offset.left || x > offset.left + width) && (y < offset.top || y > offset.top + height)){
        
          hide();
        }
      }
      
      function onClick(e){
        
        if(!$scope.tooltipToggle) return false;
        
        if(!toggleOpened) createAndShow();
        else hide();
        
        toggleOpened = !toggleOpened;
        
      }
      
      if(counter === null || counter === undefined){
        counter = 0;
      }else{
        counter++;
      }
      
      setCounter(counter);
      
      $scope.id = idPrefix + counter;
      
      if(angular.isArray($scope.kkTooltip)){
        $scope.kkTooltip = $scope.kkTooltip.join('<br />');
      }
      
      $element.on('mouseenter', onMouseEnter);
      $element.on('mouseleave', onMouseLeave);
      $element.on('click', onClick);
      
      $scope.$on('$destroy', function(){
        $element.off('mouseenter', onMouseEnter);
        $element.off('mouseleave', onMouseLeave);
        $element.off('click', onClick);
      });
      
    }]
    
  };
  
}]);
