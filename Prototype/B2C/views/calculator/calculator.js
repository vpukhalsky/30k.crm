'use strict';

angular.module('30k.b2c')

.controller('CalculatorController', ['$scope', 'alertService', '$modal', '$filter', 'collectionService', '$state', '$timeout', 'etcService', '$analytics', 'kkFocusService', '$document', 'kkService', function($scope, alertService, $modal, $filter, collectionService, $state, $timeout, etcService, $analytics, kkFocusService, $document, kkService){
    
    $scope.config = null;
    
    $scope.ffpLoading = false;
    $scope.loadingResult = false;
    
    $scope.calc = {
        mode: 'one-way',
        fares: []
    };
    
    $scope.fareColors = [];
    var colorDir = true;
    
    $scope.flightNoPattern = /^[a-zA-Z][a-zA-Z0-9][0-9]{1,4}$/;
    $scope.bookingCodePattern = /^[a-zA-Z]$/;
    
    $scope.results = false;
    $scope.sampleProgress = 0;
    
    var configUrl = 'calculator/config.json';
    var calculateUrl = 'calculator/calculate.json';
    
    $scope.initCalc = function(isHome){
        
        if(isHome){
            configUrl = 'home/config.json';
            calculateUrl = 'home/calculate.json';
        }
        
        init();
        
    };
    
    $scope.reset = function(){
        
        $scope.results = false;
        $scope.calc = {
            mode: $scope.config.initialMode,
            fares: []
        };
        $scope.addFare();
          
    };
    
    $scope.formatFfpInput = function(ffp){
        
        if(!ffp) return null;
        return ffp.name;
        
    };
    
    $scope.formatAirlineInput = function(airline){
        
        if(!airline) return null;
        return airline.name;
        
    };
    
    $scope.getFfps = function(value){
        
        return etcService.getFfps(value);
        
    };
    
    $scope.getAirlines = function(value){
        
        return etcService.getAirlines(value);
        
    };
    
    $scope.selectSampleFfp = function(ffp){
        $scope.calc.ffp = ffp;
    };
    
    $scope.trySaveFfp = function(){
        
        $modal.open({
            windowClass: 'save-ffp-modal tiny',
            templateUrl: 'views/home/save-ffp-modal.html?v=1',
            controller: 'ModalController'
        });
        
    };
    
    $scope.tryMulticity = function(){
        
        $modal.open({
            windowClass: 'multi-city-modal tiny',
            templateUrl: 'views/home/multi-city-modal.html?v=1',
            controller: 'ModalController'
        });
        
    };
    
    function getNewFareColor(){
        
        var color = 0;
        
        if(colorDir){
            
            for(var i = 0 ; i < $scope.config.maxFaresToCompare ; i++){
                if($scope.fareColors.indexOf(i) < 0){
                    color = i;
                    break;
                }
            }
            
        }else{
            
            for(var i = $scope.config.maxFaresToCompare - 1 ; i > 0 ; i--){
                if($scope.fareColors.indexOf(i) < 0){
                    color = i;
                    break;
                }
            }
            
        }
        
        colorDir = !colorDir;
        
        return color;
    }
    
    $scope.addFare = function(){
        
        $scope.calc.fares.forEach(function(fare){
            fare.active = false;
        });
        
        var color = getNewFareColor();
        $scope.fareColors.push(color);
        
        $scope.calc.fares.push({
            active: true,
            color: color,
            legs: [{
                flights: [{}],
            },{
                flights: [{}]
            }]
        });
        
        if($scope.calc.fares.length > 1){
            
            var fareIndex = $scope.calc.fares.length - 1;
            kkFocusService.focus('airline-' + fareIndex + '-0-0');
            
            $analytics.eventTrack('click', {
                'category': 'calculator',
                'label': 'add-fare'
            });
            
        }
        
    };
    
    $scope.getLegs = function(fare){
        
        var limit = $filter('limitTo');
        if($scope.calc.mode == 'one-way') return limit(fare.legs, 1);
        else if($scope.calc.mode == 'round-trip') return limit(fare.legs, 2);
        else return fare.legs;
        
    };
    
    $scope.removeLeg = function(fare, index){
    
        fare.legs.splice(index, 1);
      
    };
    
    $scope.openFlightNoHint = function(){
        
        $modal.open({
            windowClass: 'medium booking-code-hint',
            templateUrl: 'views/calculator/flight-no-hint.html',
            controller: 'CalculatorHintModalController',
            resolve: {
                samples: function(){ return $scope.config.flightNoSamples; }
            }
        });
        
    };
    
    $scope.openBookingCodeHint = function(){
        
        $modal.open({
            windowClass: 'medium booking-code-hint',
            templateUrl: 'views/calculator/booking-code-hint.html',
            controller: 'CalculatorHintModalController',
            resolve: {
                samples: function(){ return $scope.config.bookingCodeSamples; }
            }
        });
        
    };
    
    $scope.removeFlight = function(leg, index){
        
        leg.flights.splice(index, 1);
        
    };
    
    $scope.addFlight = function(leg, fareIndex, legIndex){
        
        var flight = {};
        
        if(leg.flights.length > 0){
            if(angular.isObject(leg.flights[leg.flights.length - 1].airline)){
                flight.airline = angular.copy(leg.flights[leg.flights.length - 1].airline);    
            }
        }
        
        leg.flights.push(flight);
        
        var flightIndex = leg.flights.length - 1;
        var prefix = flight.airline ? 'flight' : 'airline';
        kkFocusService.focus(prefix + '-' + fareIndex + '-' + legIndex + '-' + flightIndex);

    };
    
    $scope.addLeg = function(fare, fareIndex){
        
        var flight = {};
        
        if(fare.legs.length > 0){
            
            var flights = fare.legs[fare.legs.length - 1].flights;
            
            if(flights.length > 0){
                var lastFlight = flights[flights.length - 1];
                if(angular.isObject(lastFlight.airline)){
                    flight.airline = angular.copy(lastFlight.airline);
                }
            }
        }
        
        fare.legs.push({
            flights: [flight]
        });
        
        var prefix = flight.airline ? 'flight' : 'airline';
        var legIndex = fare.legs.length - 1;
        kkFocusService.focus(prefix + '-' + fareIndex + '-' + legIndex + '-0');
        
    };
    
    function selectInvalidFare(data){
        
        var invalidFare = -1;
        
        for(var i = 0 ; i < data.fares.length ; i++){
            
            var fare = data.fares[i];
            var valid = true;
            
            fare.legs.forEach(function(leg){
                
                leg.flights.forEach(function(flight){
                    
                    if(!angular.isString(flight.flight) || !flight.flight.match($scope.flightNoPattern)
                        || !angular.isString(flight.bookingCode) || !flight.bookingCode.match($scope.bookingCodePattern)
                    ){
                        valid = false;
                        return false;
                    }
                     
                });
                
                if(!valid) return false;
                
            });
            
            if(!valid){
                invalidFare = i;
                break;
            }
            
        }
        
        if(invalidFare >= 0){
         
            for(var i = 0 ; i < $scope.calc.fares.legnth ; i++){
                
                var fare = $scope.calc.fares[i];
                
                if(invalidFare == i) fare.active = true;
                else fare.active = false;
                
            }
            
        }
        
    }
    
    $scope.submitCalcForm = function(form){
        
        var data = toSave($scope.calc);
        
        if(data.fares.length > 1){
            
            $analytics.eventTrack('submit', {
                category: 'calculator',
                label: 'comparison'
            });
            
        }
        
        $analytics.eventTrack('submit', {
            category: 'calculator',
            label: data.mode
        });
        
        if(!form.$valid){
            
            $analytics.eventTrack('submit', {
                category: 'calculator',
                label: 'front-end-error'
            });
            
            selectInvalidFare(data);
            
        }else{
            submitCalc(data);
        }
        
    };
    
    function toSave(data){
        
        var temp = angular.copy(data);
        
        if(angular.isObject(temp.ffp)){
            temp.ffp = temp.ffp.id;
        }
        
        temp.fares.forEach(function(fare){
            
            if(temp.mode == 'one-way'){
                fare.legs = fare.legs.splice(0, 1);
            }else if(temp.mode == 'round-trip'){
                fare.legs = fare.legs.splice(0, 2);
            }
            
            fare.legs.forEach(function(leg){
                
                leg.flights.forEach(function(flight){
                    
                    if(angular.isObject(flight.airline)){
                        flight.flight = flight.airline.code + flight.flight;
                        delete flight.airline;
                    }
                    
                });
                
            });
            
        });
        
        return temp;
    }
    
    function submitCalc(data){
        
        $scope.loadingResult = true;
            
        kkService.sendPost(calculateUrl, data, true, 'calc').then(function(result){ 
            
            angular.extend($scope.calc, result.item);
            $scope.loadingResult = false;
            
            var fares = $scope.calc.result.fares;
            
            fares.forEach(function(fare){
                fare.shortItinerary = etcService.getShortItinerary(fare.itinerary, $scope.calc.result.airports);
                var best = $filter('filter')(fare.ffps, { best: true });
                if(best.length > 0) fare.ffp = best[0].id;
                else fare.ffp = fare.ffps[0].id;
            });
            
            showResults();
            
            $analytics.eventTrack('submit', {
                category: 'calculator', 
                label: 'success'
            });
            
        }, function(){
            
            $scope.loadingResult = false;
            
            $analytics.eventTrack('submit', {
                category: 'calculator', 
                label: 'back-end-error'
            });
            
        });
        
    }
    
    function showResults(){
        
        $scope.results = true;
        
        var calcForm = $('.calc-form');
        
        if(calcForm.size() > 0){
            $timeout(function(){
               $document.scrollToElementAnimated(calcForm, 140);
            });
        }
        
        $timeout(function(){
            $scope.sampleProgress = 90;
        }, 500);
        
    }
    
    $scope.hideResults = function(){
        
        $scope.results = false; 
        
    };
    
    function setupFare(fare){
        
        if(!fare){
            fare = $scope.calc.result.fares[0];
        }
        
        return fare;
    }
    
    $scope.getFfpResult = function(fare){
        
        fare = setupFare(fare);
        return $filter('find')(fare.ffps, getFfpResultId(fare));
        
    };
    
    function getFfpResultId(fare){
        
        fare = setupFare(fare);
        var ffp = 0;
        if(fare.ffp) ffp = fare.ffp;
        else ffp = $scope.calc.ffp.id;
        return ffp;
        
    }
    
    $scope.getFareFfp = function(fare){
        
        return $filter('find')($scope.calc.result.userFfps, getFfpResultId(fare));
        
    };
    
    $scope.getFareAccrual = function(id, fare){
        fare = setupFare(fare);
        return $filter('find')($scope.getFareFfp(fare).accruals, id);
        
    };
    
    $scope.getAirport = function(code){
        
        return $filter('find')($scope.calc.result.airports, code, 'code');
          
    };
    
    $scope.showAccrualDetails = function($event, fare, accrualId){
        
        $event.stopPropagation();
        
        $modal.open({
            windowClass: 'tiny accrual-details-modal',
            controller: 'AccrualDetailsModalController',
            templateUrl: 'views/calculator/accrual-details-modal.html?v=1',
            resolve: {
                ffp: function(){
                    return $scope.getFareFfp(fare);
                },
                awardGoal: function(){
                    return $scope.config.awardGoal;
                },
                fare: function(){
                    return $scope.getFfpResult(fare);
                },
                accrualId: function(){ return accrualId; }
            }
        });
        
    };
    
    $scope.showFullItinerary = function(fare){
        
        etcService.showFullItinerary(fare.itinerary, $scope.calc.result.airports, $scope.calc.result.airlines);
        
    };
    
    $scope.getFareFirstFlightAirline = function(fare){
        
        var a = fare.itinerary.legs[0].flights[0].airline;
        var airline = $filter('find')($scope.calc.result.airlines, a, 'code');
        return airline;
        
    };
    
    $scope.cloneFare = function(fare){
        
        fare.active = false;
        var clone = angular.copy(fare);
        clone.active = true;
        var color = getNewFareColor();
        $scope.fareColors.push(color);
        clone.color = color;
        $scope.calc.fares.push(clone);
        
    };
    
    $scope.removeFare = function(index){
        
        var colorIndex = $scope.fareColors.indexOf($scope.calc.fares[index].color);
        $scope.fareColors.splice(colorIndex, 1);
        $scope.calc.fares.splice(index, 1);
        
        var active = 0;
        
        if($scope.calc.fares.length > index - 1){
            active = index - 1;
        }
        
        $scope.calc.fares[active].active = true;
        
    };
    
    $scope.showAwardTooltip = function(){
    
        $modal.open({
            windowClass: 'small award-miles-tooltip',
            controller: function($scope, $modalInstance){
                $scope.cancel = function(){ $modalInstance.dismiss('cancel'); };
            },
            templateUrl: 'views/calculator/award-miles-tooltip.html?v=1'
        });
      
    };
    
    $scope.showStatusTooltip = function(){
    
        $modal.open({
            windowClass: 'small status-miles-tooltip',
            controller: function($scope, $modalInstance){
                $scope.cancel = function(){ $modalInstance.dismiss('cancel'); };
            }, 
            templateUrl: 'views/calculator/status-miles-tooltip.html?v=1'
        });
      
    };
    
    $scope.saveFfp = function(ffp){
        
        showEditFfpModal(ffp).then(function(f){
            
            $scope.config.userFfps.push(f);
            delete $scope.calc.ffp;
            
        });
        
    };
    
    $scope.editFfp = function(ffp){
        
        showEditFfpModal(angular.copy(ffp)).then(function(f){
            
            $scope.hideResults();
            submitCalc();
            
        });
        
    };
    
    function showEditFfpModal(ffp, userFfp){
        
        return $modal.open({
           windowClass: 'small ffp-edit-modal',
           controller: 'FfpEditModalController',
           templateUrl: 'views/dashboard/edit-ffp-modal.html?v=1',
           backdrop: 'static',
           resolve: {
               ffp: function(){ return ffp; },
               userFfp: function(){ return userFfp; }
           }
        }).result;
        
    }
    
    $scope.toggleDetails = function(fare){
        fare.details = !fare.details;
    };
    
    $scope.onFaresInit = function(){
        kkFocusService.focus('airline-0-0-0');
    };
    
    $scope.onAirlineSelected = function(flightObject, airline, fare, leg, flight){
        flightObject.airline = airline;
        kkFocusService.focus('flight-' + fare + '-' + leg + '-' + flight);
    };
    
    $scope.onFfpSelect = function(){
        
        $analytics.eventTrack('submit', {
            category: 'calculator', 
            label: 'ffp'
        });
        
    };
    
    $scope.onFfpSelectInit = function(){
        
        kkFocusService.focus('ffp-select');
        
        $analytics.eventTrack('init', {
            category: 'calculator', 
            label: 'ffp'
        });
        
    };
    
    $scope.onFareResultInit = function(){
        
        $analytics.eventTrack('init', {
            category: 'calculator', 
            label: 'fare'
        });
        
    };
    
    $scope.hasLastStatus = function(ffpId){
        
        var find = $filter('find');
        var ffp = find($scope.calc.result.userFfps, ffpId);  
        return angular.isObject(ffp.statusGoal) && ffp.statusGoal.isMax === true;
        
    };
    
    $scope.isAwardAccrual = function(accrualId, fare){
        
        return !$scope.getFareAccrual(accrualId, fare).status;
        
    };
    
    $scope.hasConditions = function(ffpId, accrualId){
        
        var find = $filter('find');
        var ffp = find($scope.calc.result.userFfps, ffpId);
        
        if(!angular.isObject(ffp.statusGoal) || !angular.isArray(ffp.statusGoal.thresholds) || ffp.statusGoal.thresholds.length <= 0){
            return false;
        }
        
        var threshold = find(ffp.statusGoal.thresholds, accrualId, 'accrual');
        
        return angular.isObject(threshold) && angular.isArray(threshold.conditions) && threshold.conditions.length > 0;
        
    };
    
    function init(){
        
        collectionService.getConfig(configUrl, 'calc').then(function(config){
            
            $scope.config = config;
            $scope.calc.mode = $scope.config.initialMode;
            $scope.addFare();
            
        });
        
    }
    
}])

.controller('CalculatorHintModalController', ['$scope', '$modalInstance', 'samples', function($scope, $modalInstance, samples){
    
    $scope.samples = samples;
    $scope.sample = null;
     
    if(angular.isArray(samples) && samples.length > 0) $scope.sample = samples[0];
    
    $scope.selectSample = function(sample){
        $scope.sample = sample;
    };
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
}])

.controller('ItineraryModalController', ['$scope', '$modalInstance', 'itinerary', 'airports', 'airlines', 'findFilter', function($scope, $modalInstance, itinerary, airports, airlines, findFilter){
    
    $scope.itinerary = itinerary;
    $scope.airports = airports;
    $scope.airlines = airlines;
    
    $scope.getAirportName = function(code){
        return findFilter($scope.airports, code, 'code').name;
    };
    
    $scope.cancel =  function(){
        $modalInstance.dismiss('cancel');
    };
    
}]);

