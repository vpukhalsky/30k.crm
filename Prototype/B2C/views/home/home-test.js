'use strict';

angular.module('30k.b2c')

.constant('homeBeConfig', {
    twitterMessage: 'Add miles to your flight results. {{counter}} frequent flyers will benefit. Check www.30k.com',
    twitterUrlTemplate: 'https://twitter.com/intent/tweet?screen_name={{website}}&text={{message}}',
    twitterWebsites: [{
        id: 'KAYAK', 
        name: 'Kayak'
    },{
        id: 'Skyscanner',
        name: 'Skyscanner'
    },{
        id: 'priceline',
        name: 'Priceline'
    },{
        id: 'Orbitz',
        name: 'Orbitz'
    },{
        id: 'Expedia',
        name: 'Expedia'
    },{
        id: 'Googletravel',
        name: 'Google'
    },{
        id: 'tripit',
        name: 'Tripit'
    },{
        id: 'concur',
        name: 'Concur'
    },{
        id: 'travel_jp',
        name: 'Travel.jp'
    },{
        id: 'Momondo',
        name: 'Momondo'
    }],
    defaultTwitterWebsite: 'KAYAK',
    cursorInterval: 500,
    extensionUrl: 'https://chrome.google.com/webstore/detail/efpemjkcjaedejkodfdapnddegflnkkh',
    mobileWidth: 640,
    chromeUrl: 'https://www.google.com/chrome/'
})

.controller('HomeInfoController', ['$scope', '$interval', 'homeBeConfig', 'findFilter', 'numberFilter', function($scope, $interval, config, findFilter, numberFilter){
    
    $scope.counter = 0;
    $scope.showCursor = true;
    $scope.selected = { website: config.defaultTwitterWebsite };
    $scope.websites = config.twitterWebsites;
    $scope.message = '';
    $scope.twitterUrl = '';
    
    $interval(function(){
        $scope.showCursor = !$scope.showCursor;
    }, config.cursorInterval);
    
    $scope.onWebsiteChange = function(){
        updateTwitterUrl();
    };
    
    $scope.getWebsiteName = function(){
        
        var website = findFilter($scope.websites, $scope.selected.website);
        if(!website) return '';
        return '@' + website.name.toLowerCase();
        
    };
    
    $scope.initCounter = function(value, interval){
        
        $scope.counter = value;
        
        $interval(function(){
            
            $scope.counter++;
            updateTwitterMessage();
            
        }, interval);
        
    };
    
    function updateTwitterMessage(){
        $scope.message = config.twitterMessage.replace('{{counter}}', numberFilter($scope.counter, 0));
        updateTwitterUrl();
    }
    
    function updateTwitterUrl(){
        
        $scope.twitterUrl = encodeURI(config.twitterUrlTemplate
            .replace('{{website}}', $scope.selected.website)
            .replace('{{message}}', $scope.message));
        
    }
    
    updateTwitterMessage();
    
}])

.controller('HomeHeaderController', ['$scope', '$modal', 'homeBeConfig', '$analytics', 'etcService', function($scope, $modal, config, $analytics, etcService){
    
    var isInstalled = false;
    $scope.params = {};
    
    etcService.updateChromeExtension('is-installed').then(function(response){
       
        if(response) isInstalled = true;
       
    });
    
    function parseTestParams(){
        
        if(window.location.search.length <= 1) return;
        
        var search = window.location.search.substring(1);
        var pairs = search.split('&');
        
        pairs.forEach(function(pair){
            
            var p = pair.split('=');
            
            if(p.length > 1){
                
                var key = decodeURIComponent(p[0]);
                var value = decodeURIComponent(p[1]);
                $scope.params[key] = value;
                if(key == 'prc') $scope.params[key] = parseInt(value, 10);
                
            }
            
        });
        
    }
    
    $scope.isChrome = function(){
        return angular.isObject(window.chrome);
    };
    
    $scope.isInstalled = function(){
        return isInstalled || ($scope.isChrome() && window.chrome.app && window.chrome.app.isInstalled);
    };
    
    $scope.isMobile = function(){
        return $(window).width() < config.mobileWidth;
    };
    
    $scope.install = function(source){
        
        $analytics.eventTrack('click', {
            category: 'install-extension', 
            label: (source ? source : 'unknown'),
            value: 1
        });
        
        if($scope.params.pmt){
             
            showSurveyModal().then(function(){
                install();
            });
            
        }else{
            
            install();
            
        }
        
    };
    
    function install(){
        
        if(!$scope.isMobile()){
           
            if($scope.isChrome()){
                
                if(!$scope.isInstalled()){
                    installInline();
                }
                
            }else{
                
                showChromeModal();
                
            }
        }else{
            
            openGoogleStore();
            
        }
        
    }
    
    function showSurveyModal(){
        
        return $modal.open({
            templateUrl: 'views/home/survey-modal.html?v5',
            windowClass: 'small survey',
            controller: ['$scope', '$modalInstance', 'homeBeConfig', 'kkService', '$timeout', 'params', function($scope, $modalInstance, config, kkService, $timeout, params){
                
                $scope.sites = config.twitterWebsites;
                $scope.loading = false;
                $scope.submitted = false;
                
                $scope.data = {
                    travel: null
                };
                
                $scope.close = function(){
                    $modalInstance.dismiss();
                };
                
                $scope.submit = function(){
                    
                    $scope.loading = true;
                    
                    kkService.sendPost('home/survey.json?v', $scope.data, true, 'survey').then(function(response){
                        
                        $scope.loading = false;
                        $scope.submitted = true;
                        
                    }, function(){
                        
                        $scope.loading = false;
                        
                    });
                    
                };
                
                $scope.install = function(){
                    $modalInstance.close(true);
                };
                
            }],
            resolve: {
                params: function(){ return $scope.params; }
            }
        }).result;
        
    }
    
    function openGoogleStore(){
        
        $analytics.eventTrack('click', {
            category: 'install-extension-result', 
            label: 'store',
            value: 1
        });
        
        var win = window.open(config.extensionUrl, '_blank');
        win.focus();
        
    }
    
    function installInline(){
        
        chrome.webstore.install(config.extensionUrl, function(){
            
            isInstalled = true;
            
            $analytics.eventTrack('click', {
                category: 'install-extension-result', 
                label: 'inline',
                value: 1
            });
            
        }, function(error, errorCode){
            
            $analytics.eventTrack('click', {
                category: 'install-extension-result', 
                label: 'inline',
                value: 0
            });
            
            $analytics.eventTrack('click', {
                category: 'install-extension-failure-reason', 
                label: errorCode,
                value: 1
            });
            
            openGoogleStore();
            
        });
        
    }
    
    function showChromeModal(){
        
        $modal.open({
            templateUrl: 'views/home/chrome-alert-modal.html?v2',
            windowClass: 'tiny chrome-alert',
            controller: ['$scope', '$modalInstance', 'homeBeConfig', function($scope, $modalInstance, config){
                
                $scope.chromeUrl = config.chromeUrl;
                
                $scope.close = function(){
                    $modalInstance.dismiss();
                };
                
                $scope.install = function(){
                    $modalInstance.close(true);
                };
                
            }]
        }).result.then(function(){
            openGoogleStore();
        }, function(){
            onInstallCancel();
        });
        
    }
    
    function onInstallCancel(){
        
        $analytics.eventTrack('click', {
            category: 'install-extension-result', 
            label: 'cancel',
            value: 0
        });
        
    }
    
    $scope.showVideoModal = function(){
        
        var openTime = new Date();
        
        var modal = $modal.open({
           templateUrl: 'views/home/video-modal.html?v=5',
           windowClass: 'medium video-modal', 
           controller: function($scope, $modalInstance){
               
               $scope.close = function(){
                   $modalInstance.dismiss(); 
               };
           
           }
        });
        
        modal.opened.then(function(){
            
            openTime = new Date();
            
            $analytics.eventTrack('click', {
                category: 'extension', 
                label: 'video-open',
                value: 1
            });
            
        });
        
        modal.result.finally(function(){
            
            var now = new Date();
            var sec = Math.round((now.getTime() - openTime.getTime()) / 1000);
            
            $analytics.eventTrack('click', {
                category: 'extension', 
                label: 'video-close',
                value: sec
            });
            
        });
        
    };
    
    parseTestParams();
    
}]);