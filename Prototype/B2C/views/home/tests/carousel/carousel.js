'use strict';

angular.module('30k.b2c')

.controller('HomeCarousel', ['$scope', '$interval', function($scope, $interval){
    
    var maxSlides = 4;
    var timeout = 5000;
    
    $scope.slide = 0;
    $interval(tick, timeout);
    
    function tick(){
        
        if($scope.slide + 1 < maxSlides) $scope.slide++;
        else $scope.slide = 0;
        
    }
    
}]);