'use strict';

angular.module('30k.b2c')

.controller('HomePricing', ['$scope', '$modal', '$analytics', 'homeBeConfig', function($scope, $modal, $analytics, config){
    
    $scope.showPricing = function(){
        
        $analytics.eventTrack('click', {
            category: 'pricing', 
            label: 'open',
            value: 1
        });
        
        $modal.open({
           templateUrl: 'views/home/tests/price/pricing.html',
           windowClass: 'small pricing-modal',
           controller: ['$scope', '$modalInstance', function($scope, $modalInstance){
                
                $scope.install = function(){
                    $modalInstance.close(true);
                };
                
                $scope.close = function(){
                    $modalInstance.dismiss();
                };
               
           }]
        }).result.then(function(){
           
            $analytics.eventTrack('click', {
                category: 'install-extension', 
                label: 'pricing',
                value: 1
            });
            
            var win = window.open(config.extensionUrl, '_blank');
            win.focus();
            
        }, function(){
            
            // do nothing
            
        });
          
    };
    
}]);