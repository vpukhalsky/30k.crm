'use strict';

angular.module('30k.b2c')

.controller('SignUpController', ['$scope', 'etcService', 'kkService', 'googleAuthService', function($scope, etcService, kkService, googleAuthService){
    
    $scope.user = {};
    $scope.loading = false;
    
    var googleAuthBtnId = 'google-auth2-btn';
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        $scope.loading = true;
        
        kkService.sendPost('signup/sign-up.json', $scope.user, true, 'signup').then(function(result){
            
            $scope.loading = false;
            window.location = result.redirect;
            
        }, function(){
            
            $scope.loading = false;
            
        });
        
    };
    
    function signInWithGoogle(accessToken){
        
        $scope.loading = true;
        
        kkService.sendPost('signup/sign-up.json', {
            vendor: 'google',
            access_token: accessToken
        }, true, 'signup').then(function(result){
            
            $scope.loading = false;
            window.location = result.redirect;
            
        }, function(){
            
            $scope.loading = false;
            
        });
        
    }
    
    $scope.getAirports = function(text){
        return etcService.getAirports(text);
    };
    
    $scope.formatAirport = function(airport){
        if(!airport) return null;
        return airport.name;
    };
    
    function init(){
        
        googleAuthService.addClickListener(googleAuthBtnId).then(signInWithGoogle);
        
    }
    
    init();
    
}]);