'use strict';

angular.module('30k.b2c')

.controller('SettingsController', ['$scope', 'kkService', 'alertService', 'etcService', '$timeout', '$analytics', function($scope, kkService, alertService, etcService, $timeout, $analytics){
    
    $scope.config = null;
    $scope.sections = null;
    $scope.account = {
        deleted: false,
        message: ''
    };
    
    $scope.expand = function(section){
        
        section.expanded = true;
        
    };
    
    $scope.formatAirport = function(airport){
        return airport.name;
    };
    
    $scope.getAirports = function(text){
        return etcService.getAirports(text);
    };
    
    $scope.submit = function(form, data){
        
        if(!form.$valid) return;
        
        data.loading = true;
        save(data);
        
    };
    
    function save(data){
        
        kkService.sendPost('settings/save-settings.json', data).then(function(saved){
            
            angular.extend(data, saved);
            
            if(angular.isObject(data.account) && data.account.deleted) onAccountDeleted(data.account);
            
        }).finally(function(){
            
            data.loading = false;
            data.expanded = false;
            
        });
        
    }
    
    function onAccountDeleted(data){
        
        $analytics.eventTrack('click', {
            category: 'settings', 
            label: 'account-delete',
            value: 1
        });
        
        $scope.account.deleted = true;
        if(angular.isString(data.message)) $scope.account.message = data.message;
        
        if(!data.redirectUrl) return false;
        
        var delay = angular.isNumber(data.redirectDelay) ? data.redirectDelay : 5000;
        
        $timeout(function(){
            
            window.location = data.redirectUrl;
            
        }, delay);
        
    }
    
    $scope.getPasswordIndicator = function(){
        
        var val = $scope.sections.password.newPassword;
        var result = 0;
        if(!angular.isString(val)) return result;
        
        if(val.length > 8) result++;
        if(val.match(/\d+/)) result++;
        if(val.match(/[A-Z]+/)) result++;
        if(val.match(/[\!\$\*\+]/)) result++;
        
        return result;
    };
    
    $scope.resetPassword = function(){
        
        $scope.resetLoading = true;
        
        kkService.sendGet('settings/reset-password.json').then(function(result){
            
            alertService.show(result.msg, result.type);
            $scope.resetLoading = false;
            
        },function(){
            
            $scope.resetLoading = false;
            
        });
        
    };
    
    function init(){
        
        $scope.loadingConfig = true;
        
        kkService.sendGet('settings/config.json').then(function(config){
            
            $scope.sections = config.settings;
            $scope.config = {
                cabins: config.cabins,
                awardTypes: config.awardTypes
            };
            $scope.loadingConfig = false;
            
        }, function(){
            
            $scope.loadingConfig = false;
            
        });
        
    }
    
    init();
    
}]);