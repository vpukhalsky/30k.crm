'use strict';

angular.module('30k.b2c')

.controller('FaqController', ['$scope', '$timeout', '$document', '$window', function($scope, $timeout, $document, $window){
    
    var scrollOffset = 150;
    var scrollDuration = 300;
    
    $scope.isOpen = {};
    
    $timeout(function(){
        setupWatchers();
        openSection();
    }, 50);
    
    $scope.initSection = function(id){
        $scope.isOpen[id] = false;
    };
    
    function openSection(){
        
        if(angular.isString($window.location.hash) && $window.location.hash.length > 1){
            var hash = $window.location.hash.substring(2);
            if(angular.isDefined($scope.isOpen[hash])) $scope.isOpen[hash] = true;
        }
        
    }
    
    function setupWatchers(){
        
        $scope.$watchCollection('isOpen', function(newCollection, oldCollection){
            for(var i in newCollection){
                if(!newCollection.hasOwnProperty(i)) continue;
                if(newCollection[i] !== oldCollection[i]){
                    onToggle(i, newCollection[i]);
                    break;
                }
            }
        });
        
    }
    
    function onToggle(faqId, open){
    
        var el = $('#' + faqId);
        if(el.size() > 0) $document.scrollToElement(el, scrollOffset, scrollDuration);

    }
    
}]);