'use strict';

angular.module('30k.b2c')

.controller('FfpCollectionController', ['$scope', '$filter', '$modal', 'collectionService', '$analytics', 'etcService', function($scope, $filter, $modal, collectionService, $analytics, etcService){
    
    $scope.ffpLimit = 3;
    $scope.more = false;
    $scope.loadingFfps = false;
    $scope.ffps = null;
    $scope.awardGoal = null;
    
    $scope.getFfps = function(){
        
        var ffps = $scope.ffps;
        
        if(!$scope.more){
            var limit = $filter('limitTo');
            ffps = limit(ffps, $scope.ffpLimit); 
        }
        
        return ffps;
        
    };
    
    $scope.showMore = function(val){
        $scope.more = val;
    };
    
    $scope.toggleDetails = function(ffp){
        ffp.details = !ffp.details;  
    };
    
    $scope.editFfp = function(ffp){
        
        $modal.open({
           windowClass: 'small ffp-edit-modal',
           controller: 'FfpEditModalController',
           templateUrl: 'views/dashboard/edit-ffp-modal.html?v=1',
           backdrop: 'static', 
           resolve: {
               ffp: function(){ return { id: ffp.id, name: ffp.name } },
               userFfp: function(){ return ffp; }
           }
        });
        
    };
    
    $scope.editFfpStatus = function(ffp){
    
        $modal.open({
            windowClass: 'medium',
            controller: 'FfpStatusModalController',
            templateUrl: 'views/signup-ffp/edit-ffp-status-modal.html?v=1',
            resolve: {
                ffp: function(){ return ffp; },
                userFfp: function(){ return ffp; }
            }
        });
      
    };
    
    $scope.deleteFfp = function(ffp){
        
        ffp.loading = true;
        
        collectionService.deleteItem('dashboard/delete-ffp.json', 'ffp', ffp, 'views/dashboard/delete-ffp-modal.html', true, 'ffps').then(function(){
            
            etcService.updateChromeExtension('update-memberships');
            
        }, function(){
            if(ffp) ffp.loading = false;
        });
        
    };
    
    $scope.updateFfp = function(ffp){
        
        ffp.loading = true;
        
        collectionService.saveItem('dashboard/update-ffp.json', 'ffp', { id: ffp.id }, true, 'ffps').then(function(){
            
            ffp.loading = false;
            etcService.updateChromeExtension('update-memberships');
            
        }, function(){
            
            ffp.loading = false;
            
        });
        
    };
    
    $scope.showAccrualDetails = function($event, ffp, accrualId){
        
        $event.stopPropagation();
        
        $modal.open({
            windowClass: 'tiny accrual-details-modal',
            controller: 'AccrualDetailsModalController',
            templateUrl: 'views/calculator/accrual-details-modal.html?v=1',
            resolve: {
                ffp: function(){
                    return ffp;
                },
                awardGoal: function(){
                    return $scope.awardGoal;
                },
                fare: function(){
                    return null;
                },
                accrualId: function(){ return accrualId; }
            }
        });
        
    };
    
    $scope.hasConditions = function(ffp, accrualId){
        
        if(!angular.isObject(ffp.statusGoal) || !angular.isArray(ffp.statusGoal.thresholds) || ffp.statusGoal.thresholds.length <= 0){
            return false;
        }
        
        var find = $filter('find');
        
        var threshold = find(ffp.statusGoal.thresholds, accrualId, 'accrual');
        
        return angular.isObject(threshold) && angular.isArray(threshold.conditions) && threshold.conditions.length > 0;
        
    };
    
    function initAnalytics(){
        
        $analytics.eventTrack('init', {
            category: 'ffp', 
            label: 'init',
            value: $scope.ffps.length
        });
        
        var fresh = 0;
        
        $scope.ffps.forEach(function(ffp){
            if(!ffp.manualInput && !ffp.connected){
                fresh++;
            }
        });
        
        $analytics.eventTrack('init', {
            category: 'ffp', 
            label: 'init-new',
            value: fresh
        });
        
    }
    
    function init(){
        
        $scope.loadingFfps = true;
        
        collectionService.getConfig('dashboard/config.json', 'ffp', {}, true, 'ffps').then(function(result){
            
            $scope.awardGoal = result.awardGoal;
            $scope.ffpLimit = result.ffpLimit;
            
        });
        
        collectionService.getCollection('dashboard/get-ffps.json', 'ffp', {}, true, 'ffps').then(function(result){

            $scope.ffps = result;
            $scope.loadingFfps = false;
            
            initAnalytics();
            
        }, function(){
            
            $scope.loadingFfps = false;
            
        });
        
        etcService.updateChromeExtension('visit-dashboard');
        
    }
    
    init();
    
}])

.controller('CalculatorCollectionController', ['$scope', 'collectionService', 'etcService', function($scope, collectionService, etcService){
    
    $scope.items = null;
    
    $scope.showFullItinerary = function(item){
        etcService.showFullItinerary(item.result.fares[0].itinerary, item.result.airports, item.result.airlines);
    };
    
    function setupItems(){
        
        $scope.items.forEach(function(item){
            item.shortItinerary = etcService.getShortItinerary(item.result.fares[0].itinerary, item.result.airports);
        });
        
    }
    
    function init(){
        
        $scope.loading = true;
        
        collectionService.getCollection('dashboard/get-calculations.json', 'calc', {}).then(function(result){
            
            $scope.items = result;
            setupItems();
            $scope.loading = false;
            
        }, function(){
            
            $scope.loading = false;
            
        });
    }
    
    init();
    
}])

.directive('kkAwardProgress', ['$modal', '$filter', function($modal, $filter){
    
    return {
        templateUrl: 'views/dashboard/progress.html',
        scope: {
            awardGoal: '=?',
            ffp: '=',
            fare: '=?'
        },
        controller: ["$scope", "$element", function($scope, $element){
            
            var find = $filter('find');
            
            function getValueFromFare(accrualId){
                
                var value = 0;
                if(!$scope.fare) return value;
                
                var accrual = find($scope.fare.accruals, accrualId, 'accrualId');
                if(accrual){
                    value = accrual.value;
                }
                
                return value;
                
            }
            
            function getProgressAccrualId(){
                
                var best = null;
                var type = $scope.awardGoal ? 'awardGoal' : 'statusGoal';
                
                $scope.ffp[type].thresholds.forEach(function(t){
                    
                    var accrual = find($scope.ffp.accruals, t.accrual);
                    
                    if(accrual){
                        
                        var pct = (accrual.value + getValueFromFare(accrual.id)) / t.value;
                        if(!angular.isObject(best) || best.value < pct){
                            
                            best = {
                                id: accrual.id,
                                value: pct
                            };
                            
                        }
                        
                    }
                    
                });
                
                if(best) return best.id;
                else return null;
                
            }
            
            $scope.getRemainingMiles = function(){
                
                var type = $scope.awardGoal ? 'awardGoal' : 'statusGoal';
                var accrualId = getProgressAccrualId($scope.ffp, true);
                var accrual = find($scope.ffp.accruals, accrualId);
                var t = find($scope.ffp[type].thresholds, accrualId, 'accrual');
                
                var left = t.value - accrual.value - getValueFromFare(accrualId);
                if(left < 0) left = 0;
                
                return left;
                
            };
            
            $scope.getCurrentMiles = function(){
               
                var accrualId = getProgressAccrualId();
                var accrual = find($scope.ffp.accruals, accrualId);
                return accrual.value;
                
            };
            
            $scope.getTotalMiles = function(){
                
                return $scope.getCurrentMiles() + $scope.getFareMiles();
                
            };
            
            $scope.getFareMiles = function(){
                
                var accrualId = getProgressAccrualId();
                return getValueFromFare(accrualId);
                
            };
            
            $scope.getCurrentProgress = function(){
                
                var p = $scope.getCurrentMiles() / $scope.getThreshold();
                if(p > 1) p = 1;
                
                return p;
                
            };
            
            $scope.getTotalProgress = function(){
                
                var accrualId = getProgressAccrualId();
                var p = ($scope.getCurrentMiles() + getValueFromFare(accrualId)) / $scope.getThreshold();
                if(p > 1) p = 1;
                
                return p;
                  
            };
            
            $scope.getRemainingProgress = function(){
                
                var p = $scope.getRemainingMiles() / $scope.getThreshold();
                if(p > 1) p = 1;
                return p;
                
            };
            
            function getThreshold(){
                
                var type = $scope.awardGoal ? 'awardGoal' : 'statusGoal';
                var accrualId = getProgressAccrualId();
                return find($scope.ffp[type].thresholds, accrualId, 'accrual');
                
            }
            
            $scope.hasLastStatus = function(){
                return !$scope.awardGoal && $scope.ffp.statusGoal.isMax === true;
            };
            
            $scope.getThreshold = function(){
                
                var t = getThreshold();
                return t.value;
                
            };
            
            $scope.getAccrualName = function(){
                
                var id = getProgressAccrualId();
                var accrual = find($scope.ffp.accruals, id);
                return accrual.name;
                
            };
            
            $scope.getNextStatus = function(){
                
                return $scope.ffp.statusGoal.name;
                
            };
            
            $scope.showTooltip = function(){
                
                if($scope.awardGoal){
                    showAwardTooltip();
                }else{
                    showStatusTooltip();
                }
              
            };
            
            $scope.hasConditions = function(){
                
                if(angular.isObject($scope.awardGoal)) return false;
                var threshold = getThreshold();
                return angular.isObject(threshold) && angular.isArray(threshold.conditions) && threshold.conditions.length > 0;
                
            };
            
            $scope.hasThresholds = function(){
                
                var type = $scope.awardGoal ? 'awardGoal' : 'statusGoal';
                return angular.isArray($scope.ffp[type].thresholds) && $scope.ffp[type].thresholds.length > 0;
                
            };
            
            $scope.getConditions = function(){
                
                var threshold = getThreshold();
                return threshold.conditions;
                
            };
            
            function showAwardTooltip(){
                
                $modal.open({
                    windowClass: 'tiny award-miles-tooltip',
                    controller: ["$scope", "$modalInstance", function ($scope, $modalInstance) {
                        $scope.cancel = function(){ $modalInstance.dismiss('cancel'); };
                    }],
                    scope: $scope,
                    templateUrl: 'views/calculator/award-miles-tooltip.html?v=1'
                });
                
            }
            
            function showStatusTooltip(){
                
                $modal.open({
                    windowClass: 'tiny status-miles-tooltip',
                    controller: ["$scope", "$modalInstance", function ($scope, $modalInstance) {
                        $scope.cancel = function(){ $modalInstance.dismiss('cancel'); };
                    }],
                    scope: $scope,
                    templateUrl: 'views/calculator/status-miles-tooltip.html?v=1'
                });
                
            }
        }]
    };
    
}]);