'use strict';

angular.module('30k.b2c')

.controller('PasswordRecoveryController', ['$scope', 'kkService', function($scope, kkService){
    
    var tokenParam = 'token';
    
    $scope.loading = false;
    $scope.sent = false;
    $scope.formData = {};
    
    $scope.reset = function(){
        $scope.sent = false;
        $scope.form = {};
    };
    
    $scope.updatePasswordMatch = function(form){
        
        var valid = $scope.formData.password == $scope.formData.passwordRepeat;
        form.newPasswordRepeat.$setValidity('equal', valid);
        
    };
    
    function toSubmit(data){
        
        var token = '';
        
        if(window.location.search.length > 0){
            
            var search = window.location.search.substring(1);
            var params = search.split('&');
            
            params.forEach(function(param){
                
                var keyValue = param.split('=');
                
                if(keyValue.length > 1 && keyValue[0] == tokenParam){
                    token = keyValue[1];
                }
                
            });
            
        }
        
        var result = {
            password: data.password,
            token: token
        };
        
        return result;
    }
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        $scope.loading = true;
        
        kkService.sendPost('password-recovery/submit.json', toSubmit($scope.formData), true, 'password').then(function(result){
            
            $scope.sent = true;
            $scope.loading = false;
            
        }, function(){
            
            $scope.loading = false;
            
        });
        
    };
    
}]);