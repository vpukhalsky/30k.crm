'use strict';

angular.module('30k.b2c')

.controller('LoginController', ['$scope', '$modalInstance', 'kkService', 'etcService', 'hideSignUp', 'googleAuthService', function($scope, $modalInstance, kkService, etcService, hideSignUp, googleAuthService){
    
    $scope.login = {};
    
    $scope.hideSignUp = hideSignUp;
    
    var googleAuthBtnId = 'google-auth2-btn';
    
    $scope.showForgotPasswordModal = function(){
        
        $scope.cancel();
        etcService.forgotPassword(true);
        
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        $scope.loading = true;
        
        kkService.sendPost('login/submit.json', $scope.login, true, 'login').then(function(result){
            
            $scope.loading = false;
            window.location = result.redirect;
            
        }, function(){
            $scope.loading = false;
        });
        
    };
    
    function signInWithGoogle(accessToken){
        
        $scope.loading = true;
        
        kkService.sendPost('login/submit.json', {
            vendor: 'google',
            access_token: accessToken
        }, true, 'login').then(function(result){
            
            $scope.loading = false;
            window.location = result.redirect;
            
        }, function(){
            
            $scope.loading = false;
            
        });
        
    }
    
    $scope.cancel = function(){
        
        $modalInstance.dismiss('cancel');
        
    };
    
    function init(){
        
        googleAuthService.addClickListener(googleAuthBtnId).then(signInWithGoogle);
        
    }
    
    init();
    
}])

.controller('ForgotPasswordController', ['$scope', '$modalInstance', 'kkService', 'etcService', 'returnToLogin', function($scope, $modalInstance, kkService, etcService, returnToLogin){
    
    $scope.data = {};
    $scope.loading = false;
    $scope.sent = false;
    
    $scope.cancel = function(forceClose){
        
        $modalInstance.dismiss('cancel');
        if(returnToLogin && !forceClose) etcService.logIn();
        
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        $scope.loading = true;
        
        kkService.sendPost('login/forgot-pass.json', $scope.data.email, true, 'pass').then(function(response){
            
            $scope.loading = false;
            $scope.sent = response.emailSent;
            
        }, function(){
            
            $scope.loading = false;
            
        });
        
    };
       
}]);