'use strict';

angular.module('30k.b2c')

.controller('ContactController', ['$scope', 'kkService', function($scope, kkService){
    
    $scope.loading = false;
    $scope.sent = false;
    $scope.form = {};
    
    $scope.reset = function(){
        $scope.sent = false;
        $scope.form = {};
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        $scope.loading = true;
        
        kkService.sendPost('contact/submit.json', $scope.form, true, 'contact').then(function(result){
            
            $scope.sent = true;
            $scope.loading = false;
            
        }, function(){
            
            $scope.loading = false;
            
        });
        
    };
    
}]);