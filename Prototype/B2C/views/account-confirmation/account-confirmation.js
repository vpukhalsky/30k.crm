'use strict';

angular.module('30k.b2c')

.controller('AccountConfirmationController', ['$scope', '$interval', function($scope, $interval){
    
    var timeout = 5000;
    
    $scope.redirectUrl = 'calculator.html';
    
    $scope.timer = timeout / 1000;
    $scope.flown = false;
    
    var interval = $interval(function(){
        
        $scope.timer--;
        
        if($scope.timer <= 0){
            
            $scope.timer = 0;
            $scope.redirect();
            
        }
        
    }, 1000);
    
    $scope.redirect = function(){
        
        if(interval) $interval.cancel(interval);
        
        $scope.flown = true;
        
        interval = $interval(function(){
            
            $interval.cancel(interval);
            window.location = $scope.redirectUrl;
            
        }, 1000);
        
    };
    
}]);