'use strict';

angular.module('30k.b2c')

.controller('FfpController', ['$scope', '$filter', '$modal', 'alertService', 'collectionService', 'etcService', function($scope, $filter, $modal, alertService, collectionService, etcService){
    
    $scope.selectedFfp = null;
    $scope.ffps = null;
    $scope.config = null;
    
    var ffpsLimit = 8;
    var selectedFfps = [];
    
    $scope.getFfps = function(text){
        return collectionService.getCollection('signup-ffp/get-autocomplete-ffps.json', 'affp', { text: text });
    };
    
    $scope.getConfigFfps = function(){
        return $filter('limitTo')($scope.config.ffps, ffpsLimit).concat(selectedFfps);
    };
    
    $scope.hasPreSelected = function(){
        
        if(!$scope.config || !$scope.ffps) return false;
        
        var find = $filter('find');
        var ps = find($scope.config.ffps, true, 'preSelected');
        if(ps && !find($scope.ffps, ps.id)) return true;
        return false;
          
    };
    
    $scope.formatFfp = function(ffp){
        if(!ffp) return null;
        return ffp.name;
    };
    
    $scope.selectFfp = function(ffp){
        
        var find = $filter('find');
        
        if(angular.isObject(ffp)){
            showEditFfpStatusModal(ffp, find($scope.ffps, ffp.id));
        }
        
    };
    
    function showEditFfpStatusModal(ffp, userFfp){
        
        var find = $filter('find');
        
        $modal.open({
            windowClass: 'large ffp-status-edit-modal',
            controller: 'FfpStatusModalController',
            templateUrl: 'views/signup-ffp/edit-ffp-status-modal.html?v=1',
            resolve: {
                ffp: function(){ return ffp; },
                userFfp: function(){ return userFfp; }
            }
        }).result.then(function(f){
            
            if(userFfp) userFfp.status = f.status;
            else {
                $scope.ffps.push(f);
                
                if(!find($scope.config.ffps, f.id) && !find(selectedFfps, f.id)){
                    selectedFfps.push(f);
                }
                
                $scope.selectedFfp = null;
                
            }
            
        });
        
    }
    
    $scope.submit = function(){
        
        if($scope.ffps.length > 0) window.location = 'calculator.html';
        else{
            alertService.show($scope.config.noFfpSelectedAlert, 'alert');
        }
        
    };
    
    function init(){
        
        collectionService.getConfig('signup-ffp/config.json', 'sftp').then(function(config){
            
            $scope.config = config;
            
        });
        
        collectionService.getCollection('signup-ffp/get-ffps.json', 'ffp', {}).then(function(result){

            $scope.ffps = result; 
            
        });
        
        etcService.updateChromeExtension('visit-dashboard');
        
    }
    
    init();
    
}])

.controller('FfpStatusModalController', ['$scope', '$modalInstance', 'ffp', 'userFfp', '$timeout', 'collectionService', '$analytics', 'etcService', function($scope, $modalInstance, ffp, userFfp, $timeout, collectionService, $analytics, etcService){
    
    var afterSelectTimeout = 800;
    var closing = false;
    var closingTimeout = null;
    
    $scope.ffp = ffp;
    
    $scope.selected = {
        id: ffp.defaultStatus
    };
    
    $scope.loading = false;
    
    if(userFfp && userFfp.status){
        $scope.selected.id = userFfp.status;
    }
    
    $scope.cancel = function(){
        if(!closing) $modalInstance.dismiss('cancel');
    };
    
    $scope.getButtonGroupClass = function(){
        return ['even-' + $scope.ffp.statuses.length];
    };
    
    $scope.select = function(status){
        
        $scope.selected.id = status.id;
        closing = true;
        
        if(closingTimeout){
            $timeout.cancel(closingTimeout);
        }
        
        closingTimeout = $timeout(function(){
            
            var result = angular.copy(ffp);
            result.status = $scope.selected.id;
            save(result);
            
        }, afterSelectTimeout);
        
    };
    
    function save(result){
        
        $scope.loading = true;
        
        collectionService.saveItem('signup-ffp/save-ffp-status.json', 'ffp', result, true, 'ffpedit').then(function(response){
            
            $scope.loading = false;
            $modalInstance.close(response.item);
            etcService.updateChromeExtension('update-memberships');
            
        }, function(){
            
            $scope.loading = false;
            
        });
        
        $analytics.eventTrack('submit', {
            category: 'ffp', 
            label: 'status'
        });
        
    }
    
    function init(){
        
        $analytics.eventTrack('init', {
            category: 'ffp', 
            label: 'status-selection'
        });
        
    }
    
    init();
    
}])

.controller('FfpEditModalController', ['$scope', '$modalInstance', 'ffp', 'userFfp', 'findFilter', 'kkService', 'collectionService', '$analytics', 'etcService', function($scope, $modalInstance, ffp, userFfp, findFilter, kkService, collectionService, $analytics, etcService){
    
    $scope.ffp = ffp;
    
    $scope.tabs = {
        manualMode: false,
        autoMode: true
    };
    
    $scope.result = null;
    
    $scope.isNew = !angular.isObject(userFfp);
    $scope.userFfp = userFfp ? angular.copy(userFfp) : { accruals: [] };
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.selectAuto = function(){
        $scope.tabs.manualMode = false;
        $scope.tabs.autoMode = true;
    };
    
    $scope.close = function(){
        
        $modalInstance.close($scope.result);
        
    };
    
    $scope.submit = function(form){
        
        if(isNew()){
            $analytics.eventTrack('submit', {
                category: 'ffp',
                label: ($scope.tabs.autoMode ? 'connect' : 'manual-input')
            });
        }
        
        if(!form.$valid) return;
        
        var ffp = angular.copy($scope.userFfp);
        ffp.connect = $scope.tabs.autoMode;
        
        $scope.loadingResults = true;
        
        collectionService.saveItem('signup-ffp/save-ffp.json', 'ffp', ffp, true, 'ffpedit').then(function(result){
            
            $scope.result = result.item;
            $scope.loadingResults = false;
            
            if(isNew()){
                $analytics.eventTrack('submit', {
                    category: 'ffp',
                    label: ($scope.tabs.autoMode ? 'connect-success' : 'manual-input-success')
                });
            }
            
            etcService.updateChromeExtension('update-memberships');
            
        }, function(){
            
            $scope.loadingResults = false;
            
        });
        
    };
    
    function initUserFfp(){
        
        $scope.ffp.accruals.forEach(function(accrual){
            if(!findFilter($scope.userFfp.accruals, accrual.id)){
                $scope.userFfp.accruals.push({
                    id: accrual.id
                });
            }
        });
        
    }
    
    function setupFfp(){
        
        kkService.sendPost('signup-ffp/get-ffp.json', ffp.id, true, 'ffpedit').then(function(result){
            
            angular.extend($scope.ffp, result.item);
            afterInit();
            
        }, function(){
            
            $scope.ffpLoading = true;
            
        });
        
    }
    
    function afterInit(){
        
        $scope.ffpLoading = false;
        initUserFfp();
        initAnalytics();
        
    }
    
    function isNew(){
        return !$scope.userFfp.connected && !$scope.userFfp.manualInput;
    }
    
    function initAnalytics(){
        
        if(isNew()){
            
            $analytics.eventTrack('init', {
                category: 'ffp',
                label: 'edit-new'
            });
            
        }
        
    }
    
    function init(){
        
        $scope.ffpLoading = true;
        
        if(!angular.isArray(ffp.accruals) || !angular.isArray(ffp.statuses)){

            setupFfp();
            
        }else{
            
            afterInit();
            
        }
        
    }
    
    init();
    
}]);