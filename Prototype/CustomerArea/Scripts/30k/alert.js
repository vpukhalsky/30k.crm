/**
 * Alert class
 */ 

_3.Alert = KClass.extend({
    
    /**
     * Time in sec. for which alert is displayed
     */ 
    deleteAfter: 10,
    
    observed: {
        
        /**
         * Message to display in alert
         */ 
        Message: '',
        
        /**
         * CSS class for alert box
         */ 
        css: 'alert',
    
    },
    
    init: function(data){
        
        this.parent(data);
        
        //scroll to alert
        window.scrollTo(0, 0);
        
        //remove after @see deleteAfter sec
        setTimeout($.proxy(this.remove, this), this.deleteAfter * 1000);
    },
    
    remove: function(alert){
        if(!alert) alert = this;
        _3.view.removeAlert(alert);
    }
});