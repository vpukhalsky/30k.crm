/**
 * FFP View Model class
 */ 
_3.FFPViewModel = KClass.extend({
    
    /**
     * Knockout.js data-view-view-data binding enabled
     */ 
    isViewModel: true,
   
    /**
     * Enter key code (to save rule in quick mode)
     */ 
    ENTER_KEY: 13,
    
    /**
     * Scroll to specified object eg. when new added
     */ 
    scrollTo: null,
    
    /**
     * Object containing last scroll position for all the views
     */ 
    scrollPos: {},

    /*
    Id of the timeout handler set.
    */
    timeoutId: 0,
    
    idleTimeout: 120 * 60 * 1000,
    
    /*Id of the program rules been reviewed for last time*/
    lastViewedRulesProgramId: 0,
    
    observed: {
        
        /**
         * Which section is currently visible for user?
         * @type string
         */ 
        selectedMainNav: 'home',
        
        /**
         * Is initial data retrived from server?
         * @see ffp-all.json
         */ 
        isLoaded: false,
        
        /**
         * Is any pending AJAX request enabled?
         */ 
        isLoading: false,
        
        /**
         * Array of available accruals to set in FFPs
         * @see ffp-all.json
         * @type KeyValue
         */ 
        accruals: [],
       
        /**
         * Array of FFPs
         * @see ffp.js
         * @type FFP
         */ 
        testFfps: [],
        
        /**
         * Currently edited Test
         * @type Test
         */
        editedTest: null,

        /**
         * Have tests been loaded?
         * @type bool
         */
        areTestsLoaded: false,

        /**
         * Array of alerts displayed on top of screen eg. in case of server errors
         * @see alert.js
         * @type Alert
         */ 
        alerts: [],
        
        showValidation: false
    },
    
    computed: {
        
        
    },
    
    mapped: {
        accruals: _3.FFPAccrual,
        editedTest: _3.Test,
        testFfps: _3.FFP
    },
    
    /**
     * Static array of view objects used to display main menu
     * @type Object
     */ 
    mainNav: [],
    
    /**
     * Constructor
     * @param data - config object overwritting default class properties
     */ 
    init: function(data){
        
        this.initFoundation();
        
        //build navigation array
        this.initNav();
        
        this.parent(data);
        
        $(window).on('scroll', $.proxy(this.onScroll, this));

        $(window).on('popstate', $.proxy(this.onpopstate, this));
        
        this.navigate();
        
        $(document).bind('click', $.proxy(function () {
            this.timeoutId = this.resetIdleTimeout(this.timeoutId);
        }, this));

        this.timeoutId = this.setIdleTimeout();
    },
    

    onpopstate: function (event) {  
        if (event && event.originalEvent && event.originalEvent.state) {
            this.navigate(event.originalEvent.state);
        }
    },

    /**
     * Init Foundation scripts 
     */ 
    initFoundation: function(){
        $(document).foundation();  
    },
    
    /**
     * Set array of main menu items
     */ 
    initNav: function(){
        this.mainNav = [
            {
                name: 'Home',
                id: 'home',
                url: _3.utils.getViewUrl('home.html'),
                title: 'Customer Area / Home',
                callback: $.proxy(this.navigate, this)
            },
            {
                name: 'Calculator',
                id: 'test-edit',
                url: _3.utils.getJsonUrl('calculator.json'),
                title: 'Customer Area / Earnings calculator',
                requestMethod: _3.utils.sendGetJson,
                callback: $.proxy(this.navigate, this),
                responseCallback: $.proxy(this.calcResponseReceived, this)
            },
            {
                name: 'Statistics',
                id: 'stats',
                url: 'stats',
                title: 'Customer Area / Statistics',
                callback: $.proxy(this.navigate, this)
            }
        ];
    },
    
    navigate: function(item) {
        if (item != null) {
            history.pushState({ name: item.name, id: item.id, url: item.url }, null, '/#/' + item.id);
            this.showView(item);
        } else if (window.location.href.indexOf('/#/') != -1) {
            var id = window.location.href.substring(window.location.href.indexOf('/#/') + 3);
            for (var i = 0; i < this.mainNav.length; i++) {
                if (this.mainNav[i].id == id) {
                    this.navigate(this.mainNav[i]);
                    return;
                }
            }
        }else {
            this.navigate(this.mainNav[0]);
        }
    },

    showView: function(item) {
        var section = $('section#' + item.id);
        
        if (section.length == 0) this.loadView(item, $.proxy(this.onShowViewSuccess, this, item));
        else this.onShowViewSuccess(item);
    },
    
    loadView: function(item, callback) {
        this.isLoading(true);

        var method = item.requestMethod ? item.requestMethod : _3.utils.sendGet;

        method(item.url, $.proxy(function (response) {
            if (response) {
                
                if (item.responseCallback) {
                    item.responseCallback(response, item);
                } else {
                    this.populateContent(response);
                }
                
                if ($.isFunction(callback)) callback();
            } else {
                this.addAlert(response.status);
            }
            this.isLoading(false);
        }, this));
    },
    
    populateContent: function(content) {
        var target = $("div#content");

        var loadedEl = $(content);

        ko.applyBindings(this, loadedEl[0]);

        target.html(loadedEl);
    },
    
    onShowViewSuccess: function(item) {
        this.selectedMainNav(item.id);
        document.title = item.title;
    },
    
    calcResponseReceived: function(response, item) {
        if (response.Success) {

            var ffps = response.Value.FFPs;

            for (var i = 0 ; i < ffps.length ; i++) {
                for (var j = 0 ; j < ffps[i].ConfiguredAccruals.length ; j++) {
                    ffps[i].ConfiguredAccruals[j].Enabled = true;
                }
            }

            this.update('testFfps', response.Value.FFPs);
            this.update('accruals', response.Value.Accruals);
            this.populateContent(response.Value.Html);
            
            _3.airportsAutocomplete.initAirportLookups();

        } else {
            _3.view.addAlert(response.Status.Message);
        }
    },
    
    /**
     * Sets single Test in 'edit' mode
     * @param test
     * @see tests
     */ 
    setEditedTest: function(test){
        if(test) this.editedTest(test); //set defined current Test
        else if(test === null || test === false) this.editedTest(null); //no Test is edited
        else {
            //create new Test and open to edit
            
            this.update('editedTest', { isNew: false });
        }
    },
    
    /**
     * Opens Test grid section and drops edited Test
     */ 
    cancelTestEdit: function(){
        this.setEditedTest(null);
    },

    /**
     * Add subscribers to knockout.js observabled properties
     */ 
    initSubscribed: function(){
        
        //makes sure after AJAX request is finished to init datepickers
        this.isLoading.subscribe($.proxy(this.onLoadingFinished, this));
        
        this.selectedMainNav.subscribe($.proxy(this.onViewChange, this));
    },
    
    /**
     * Re-initialize datapickers if needed
     */
    onLoadingFinished: function () {
        if (this.isLoading() == false) {
            this.initDatepickers();
        }
    },
    
    /**
     * Inits datepicker widget
     * @see foundation-datepicker.js
     */
    initDatepickers: function(){
        var view = this.selectedMainNav();
        if(view == 'rule-edit' || view == 'test-edit'){
            
            //init only when FFP rule edit or test edit section is opened
            
            $('.date-input').fdatepicker({
                format: 'mm/dd/yyyy',
                weekStart: 1
            });   
        }
    },
    
    /**
     * Helper method used in FFP rule to retrive corresponding FFP
     * @param string ffpCode
     * @return FFP
     */
    getFfpByCode: function (ffpCode) {
        if (typeof ffpCode != 'string') return null;

        var ffps = this.testFfps();
        for (var i = 0 ; i < ffps.length ; i++) {
            if (ffps[i].Code().toLowerCase() == ffpCode.toLowerCase()) return ffps[i];
        }
        return null;
    },
    
    /**
     * After HTML <section> containing view has been changed
     */ 
    onViewChange: function(){
        this.showValidation(false);  
        this.initDatepickers();
        this.scroll();
    },
    
       
    /**
     * Data-binding helper method
     * @see programs.html
     * @param FFPAccrual accrual
     * @return string
     */ 
    getAccrualName: function(accrual){

        var a = this.accruals();
        for(var i = 0 ; i < a.length ; i++){
            if (a[i].AccrualType() == accrual.AccrualType()) {
                return a[i].Name();
            }
        }
        //return accrual.Alias();
        return '';
    },
    
    /**
     * Data-binding helper method
     * @see programs.html
     * @param FFPAccrual accrual
     * @return string
     */
    getAccrualNameForHeader: function(accrual) {
        return accrual.Alias();
    },
    
        /**
     * Data-binding helper method
     * @see programs.html
     * @param string type
     * @return string
     */ 
    getAccrualAlias: function(type){
        var accrual = this.getAccrualFromType(type);
        if(accrual) return accrual.Alias();
        return '';
    },
    
    /**
     * Data-binding helper method
     * @see programs.html
     * @param string type
     * @return boolean
     */ 
    getAccrualEnabled: function(type){
        var accrual = this.getAccrualFromType(type);
        if(accrual) return accrual.Enabled();
        return '';
    },
    
    /**
     * Returns currently edited FFP's configured accrual of specified type
     * @param string type
     * @return FFPAccrual
     */ 
    getAccrualFromType: function(type){
        var ca = this.editedFfp().ConfiguredAccruals();
        
        for(var i = 0 ; i < ca.length ; i++){
            if(ca[i].AccrualType() == type){
                return ca[i];
            }
        }
        
        return null;
    },
    
    /**
     * Displays error alert on top of window
     */ 
    addAlert: function(message){
        this.alerts.push(new _3.Alert({ message: message }));
    },
    
    /**
     * Displays green notification on top of window
     */ 
    addNotification: function(message){
        this.alerts.push(new _3.Alert({ message: message, css: 'success' }));
    },
    
    /**
     * User action to close alert
     */ 
    removeAlert: function(alert){
        this.alerts.remove(alert);
    },
    
    /**
     * Show validation alerts and focus on first invalid field
     */ 
    validate: function(){
        this.showValidation(true);
        
        var invalid = $('.validated-field.error:visible').first();
        if(invalid && invalid.size() > 0) invalid.find('input, textarea, select').first().focus();
    },
    
    /**
    * Updates tabindex attributes fields included in rule edit view
    */
    updateRuleTabIndex: function(){
        var first = $('#rule-fare-family');
        if(first.size() <= 0) return;
        
        var ti = parseInt(first.attr('tabindex'));
        
        var gridRows = $('#rule-creator-grid tbody tr');
        var mr = $('#marketing-rules').find('input[type="checkbox"]');
        var or = $('#operating-rules').find('input[type="checkbox"]');
        
        //vertical order
        gridRows.each($.proxy(function(rowIndex, row){
            var inputs = $(row).find('input[type="text"],input[type="checkbox"]');
            inputs.each($.proxy(function(inputIndex, input){
                var index = ti + inputIndex * gridRows.size() + rowIndex;
                $(input).attr('tabindex', index);
            }, this));
        }, this));
        
        var gridInputs = $('#rule-creator-grid').find('input[type="text"],input[type="checkbox"]');
        ti += gridInputs.size();
        
        //horizontal order
        //ti = this.incrementTabIndex(gridInputs, ti);
        
        ti = this.incrementTabIndex(mr, ti);
        $('#rule-marketing-specific').attr('tabindex', ti++);
        
        ti = this.incrementTabIndex(or, ti);
        $('#rule-operating-specific').attr('tabindex', ti++);
        
        $('#rule-flight-limitations').attr('tabindex', ti++);
        $('#rule-flight-exclusions').attr('tabindex', ti++);
        
        $('#rule-geo').attr('tabindex', ti++);
        $('#rule-geo-cc').attr('tabindex', ti++);
        
        $('#rule-valid-from').attr('tabindex', ti++);
        $('#rule-valid-until').attr('tabindex', ti++);
        
        $('#rule-flight-from').attr('tabindex', ti++);
        $('#rule-flight-until').attr('tabindex', ti++);
        
        ti = this.incrementTabIndex($('#rule-status-tiers').find('input, select, button'), ti);
        
        $('#rule-comments').attr('tabindex', ti++);
        
        $('#submit-edited-rule').attr('tabindex', ti++);
        $('#cancel-edited-rule').attr('tabindex', ti++);
        $('#apply-edited-rule').attr('tabindex', ti++);
    },
    
    /**
     * Increment tabindex attribute of elements starting with tabIndex value
     * @param elements - jQueryinput elements
     * @param tabIndex - @type int, starting index
     * @return last used tabIndex + 1
     */ 
    incrementTabIndex: function(elements, tabIndex){
        for(var i = 0 ; i < elements.size() ; i++){
            elements.eq(i).attr('tabindex', tabIndex++);
        }
        return tabIndex;
    },
    
    /**
     * Scroll to object set in @scrollTo
     */ 
    scroll: function(){
        
        var el = null;
        var pos = -1;
        
        if(this.scrollTo && this.scrollTo instanceof _3.Rule){
            el = $('[data-rule-id="' + this.scrollTo.Id + '"]');
        }
        
        var view = this.selectedMainNav();
        
        if(el && el.size() > 0) pos = parseInt(el.offset().top);
        else if (this.scrollPos[view] && (view == 'rule-all' || view == 'test-all')) {
            pos = this.scrollPos[this.selectedMainNav()]; 
        }else{
            pos = 0;
        }
            
        if(pos >= 0){
            $('html, body').animate({
                scrollTop: pos
            }, 50);   
        }
        
        this.scrollTo = null;
    },
    
    /**
     * Responses to scroll event,
     * saves scroll position according to the current view
     */ 
    onScroll: function(e){
        if(this.isLoaded() && !this.isLoading()){
            this.scrollPos[this.selectedMainNav()] = $(window).scrollTop();
        }
    },
    
    /**
     * Data-binding helper method
     * @see programs.html
     * @param FFPAccrual accrual
     * @return string
     */
    getTierAliasName: function (tier) {

        var t = this.tierAliases();
        for (var i = 0 ; i < t.length ; i++) {
            if (t[i].Id == tier()) {
                return t[i].Name;
            }
        }
        //return accrual.Alias();
        return '';
    },

    /*
    Sets idle timeout to automatically log user off.
    */
    setIdleTimeout: function() {
        return window.setTimeout(this.logout, this.idleTimeout);
    },

    /*
    Sets idle timeout to automatically log user off.
    */
    resetIdleTimeout: function (timeoutId) {
        window.clearTimeout(timeoutId);
        return this.setIdleTimeout();
    },
    
    /*
    Redirects user to the logout action.
    */
    logout: function() {
        window.location.href = "/Account/Logoff";
    }
});

//set as globally accessable
_3.view = new _3.FFPViewModel();
