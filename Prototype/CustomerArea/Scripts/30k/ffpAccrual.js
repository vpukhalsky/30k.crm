/**
 * FFP Accrual class
 */ 

_3.FFPAccrual = KClass.extend({
    
    Id: 0,
    
    observed: {
        
        /**
         * eg. 'Rdm' - works as an unique identifier for any type of miles
         * @required
         */
        AccrualType: '',
        
        /**
         * User-defined alias of accrual specific for particuar FFP
         * eg. 'Cool Status Miles'
         */ 
        Alias: '',
        
        /**
         * Defined in FFP edit view
         * @type int
         */ 
        EarningBase: null,
        
        /**
         * Is particular accrual type enabled for the FFP?
         */ 
        Enabled: false,
        
        /**
         * Common name for accrual type
         */ 
        Name: '',
    },
    
    computed: {
        elementId: function(){
            return 'program-accrual-' + this.AccrualType().toLowerCase();
        },
        
        /**
         * FFP form edit validation helper
         */ 
        isValid: function(){
            return (this.Alias().length > 0 && this.EarningBase() && this.Enabled()) || !this.Enabled();
        }
    },
    
    filtered: [
        'isValid'
    ],
    
    init: function(data){
        this.parent(data);
        
        if(this.EarningBase() && this.Alias()) this.Enabled(true);
    }
});
