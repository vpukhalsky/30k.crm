﻿/*
Departure-arrival airports autocomplete box.
*/
_3.Airports = Class.extend({
    

    /**
     * Constructor
     * @param data - config object overwritting default class properties
     */ 
    init: function (data) {
        this.initAirportLookups();
    },
    
    /**
    * Inits autocomplete on airport input fields
    */
    initAirportLookups: function (scope) {
        if (!scope) scope = $('body');
        scope.find('input.airport').autocomplete({
            autoFocus: true,
            source: function (request, response) {

                $(this.element).addClass('loader');

                $.ajax({
                    url: "/Flights/AirportLookup",
                    dataType: "json",
                    data: {
                        value: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.DisplayValue,
                                //value: item.DispayCodeValue
                                value: item.Code
                            };
                        }));
                    }
                });
            },
            minLength: 3,
            delay: 100,
            select: function (event, ui) {
                var currentValue = ui.item.value;
                var airportCode = currentValue.toString().substring(currentValue.length, currentValue.length - 5).replace('(', '').replace(')', '');
                var currentAutocomplete = $(event.target);
                currentAutocomplete.attr("code", airportCode);
                currentAutocomplete.attr("lastDisplayValue", currentValue);
                currentAutocomplete.val(currentValue);
                currentAutocomplete.trigger('airport.changed');
            },
            change: function (event, ui) {
                var currentAutocomplete = $(event.target);
                var previousKey = currentAutocomplete.attr("lastDisplayValue");
                var currentKey = currentAutocomplete.val();
                if (previousKey != currentKey) {
                    currentAutocomplete.attr("lastDisplayValue", "");
                    currentAutocomplete.attr("code", "");
                }
            },
            response: function (event, ui) {

                var currentAutocomplete = $(event.target);

                currentAutocomplete.removeClass('loader');

                var currentlyFocused = $(document.activeElement).attr("id");

                var id = currentAutocomplete.attr("id");

                if (currentlyFocused != id && ui.content != null && ui.content.length > 0) {
                    var currentValue = ui.content[0].value;
                    var airportCode = currentValue.toString().substring(currentValue.length, currentValue.length - 5).replace('(', '').replace(')', '');
                    currentAutocomplete.attr("code", airportCode);
                    currentAutocomplete.attr("lastDisplayValue", currentValue);
                    currentAutocomplete.val(currentValue);
                }
            }
        });

        scope.find('input.airport').each($.proxy(function (index, el) {
            $(el).data("ui-autocomplete")._renderItem = function (ul, item) {
                var term = this.term;
                var tt = this.term.lastIndexOf(" ");

                if (tt != -1) {
                    term = this.term.substring(0, tt);
                }

                var re = new RegExp(term, 'gi');
                var t = item.label.replace(re, "<span style='font-weight:bold;color:Black;'>" + "$&" + "</span>");
                return $("<li></li>").data("item.autocomplete", item).append("<a>" + t + "</a>").appendTo(ul);
            };
        }, this));
    }
});

//set as globally accessable
_3.airportsAutocomplete = new _3.Airports();