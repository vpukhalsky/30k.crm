/**
 * Tag Input class
 */ 

_3.TagInput = KClass.extend({
    
    observed: {
        /**
         * Is inputed tag valid?
         */ 
        isValid: true,
        
        /**
         * Error message to show if invalid
         */ 
        error: ''
    },
    
    /**
     * Array of submited tags
     */ 
    tags: [],
    
    /**
     * Key codes used to submit input
     */ 
    inputFinishCodes: {
        comma: 188,
        enter: 13,
        space: 32
    },
    
    /**
     * Config array - which keys submit user data?
     */ 
    stopKeys: ['comma', 'enter', 'space'],
    
    /**
     * Url for AJAX request validating submited tag.
     * Left empty mean no validation on server side.
     */ 
    validationUrl: null,
    
    /**
     * Method to overload eg. to create only uppercase tags
     * @param string tag
     * @return string tag
     */ 
    beforeAdd: function(tag){
        return tag;
    },
    
    /**
     * Add tag to collection
     * @param string tag
     * 
     */
    add: function(tag){
        tag = this.beforeAdd(tag);
        
        if(this.tags.indexOf(tag) == -1){
            
            //do not allow duplicates
            this.tags.push(tag);
        }
    },
    
    /**
     * Remove tag from collection
     */ 
    remove: function(tag){
        this.tags.remove(tag);
    },
    
    /**
     * Validate tag if neccessary
     */ 
    validate: function(input){
        var value = input.val();
        
        if(this.validationUrl){
            _3.view.isLoading(true);

            var valueToSend = JSON.stringify({ "Value": value });

            _3.utils.sendPost(this.validationUrl, valueToSend, $.proxy(function (response) {
                _3.view.isLoading(false);

                if (response.Success) {
                    this.validationSuccess(value, input);
                } else {
                    this.isValid(false);
                    if (response.Status && response.Status.Message) {
                        this.error(response.Status.Message);
                    } else {
                        this.error('Unable to add the tag due to server validation fail.');
                    }
                }
            }, this));

        } else {
            this.validationSuccess(value, input);
        }
    },
    
    /**
     * When field is valid
     */ 
    validationSuccess: function(value, input){
        this.isValid(true);
        this.add(value);
        input.val('');
    },
    
    /**
     * Action invoked when user presses key within input
     */ 
    onKeypress: function(data, event){
        var input = $(event.currentTarget);
        
        if(this.isInputFinishCode(event)){
            var maxlength = input.attr('maxlength');

            if((maxlength && input.val().length == parseInt(maxlength)) || !maxlength){
                this.validate(input);
            }
            
            event.preventDefault();
            
            return false;
        }else{
            return true;
        }
    },
    
    /**
     * Check for stop key to submit new tag
     */ 
    isInputFinishCode: function(event){
        for(var i in this.inputFinishCodes){
            if(this.stopKeys.indexOf(i) != -1 
                && (this.inputFinishCodes[i] == event.keyCode || this.inputFinishCodes[i] == event.charCode)
            ){
                return true;
            }
        }
        
        return false;
    }
});
