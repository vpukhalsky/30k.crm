/**
 * Utils class
 */ 

//30K class namespace
_3 = {};

_3.Utils = Class.extend({
    

    /**
     * Path to JSON folder
     * for prototype use only
     */
    jsonPath: '/json',

    viewPath: '/views',
    
    /**
     * Returns correct url of requested JSON file
     * @param string file
     * @return string URL
     */
    getJsonUrl: function (file) {
        var parts = location.href.split('/');
        parts.splice(parts.length - 1, 1);

        return parts.join('/') + this.jsonPath + '/' + file;
    },

    /**
     * Returns correct url of requested JSON file
     * @param string file
     * @return string URL
     */
    getViewUrl: function (file) {
        var parts = location.href.split('/');
        parts.splice(parts.length - 1, 1);

        return parts.join('/') + this.viewPath + '/' + file;
    },
    
    toUpperCase: function(s) {
        return s.toUpperCase();
    },
    
    /**
     * Simple filter to capitalize first letter of every word in string
     */
    toCapitalCase: function(s) {
        var words = s.split(' ');

        for (var i = 0; i < words.length; i++) {
            words[i] = words[i].substr(0, 1).toUpperCase() + words[i].substr(1);
        }

        return words.join(' ');
    },

    getAbbr: function(phrase) {
        var result = '';
        var words = phrase.split(' ');

        for (var i = 0; i < words.length; i++) {
            result += words[i].charAt(0).toUpperCase();
        }

        return result;
    },
    
    /*
    * Encapsulates $.get request adding additional callbacks
    */
    sendGet: function(url, success) {
        //$.get(url, { "_": $.now() }, success).fail(this.ajaxRequestErrorCallback);
        $.get(url, success).fail(this.ajaxRequestErrorCallback);
    },
    
    /*
    * Encapsulates $.getJSON request adding additional callbacks
    */
    sendGetJson: function (url, success) {
        $.getJSON(url, success).fail(this.ajaxRequestErrorCallback);
    },
    
    /**
    * Encapsulates $.ajax HTTP PUT request adding additional callbacks
    */
    sendPut: function(url, data, success){
        $.ajax({
            url: url,
            success: success,
            type: 'PUT',
            error: this.ajaxRequestErrorCallback,
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8'
        });
    },
    
    /**
    * Encapsulates $.ajax HTTP DELETE request adding additional callbacks
    */
    sendDelete: function(url, success){
        $.ajax({
            url: url,
            success: success,
            type: 'DELETE',
            error: this.ajaxRequestErrorCallback
        });
    },

    /*
    * Encapsulates $.post request adding additional callbacks
    */
    sendPost: function (url, data, success) {
        $.ajax({
            url: url,
            success: success,
            type: 'POST',
            error: this.ajaxRequestErrorCallback,
            data: data,
            dataType: 'json',
            /*contentType: 'application/json; charset=utf-8'*/
        });
        //$.post(url, data, success).fail(this.ajaxRequestErrorCallback);
    },

    /*
    * Handler for the ajax request error callback
    */
    ajaxRequestErrorCallback: function (response, textStatus, error) {
        /// <param name="response" type="jQueryXmlHttpRequest">
        /// </param>
        /// <param name="textStatus" type="String">
        /// </param>
        /// <param name="error" type="Error">
        /// </param>
        if (response && response.status == "401") {
            //if (!this.redirectedToLogin) {
            //    _3.view.addAlert("Your session expired on server. You will be automatically redirected to the login page in few seconds...");
            //}
            //this.redirectedToLogin = true;
            //window.setTimeout(_3.view.logout, 1500);
            _3.view.logout();
        } else {
            _3.view.isLoading(false);
            _3.view.addAlert(error);
        }
    },
    
    strToHash: function(str){
        if (str.length % 32 > 0) str += Array(33 - str.length % 32).join("z");
        var hash = '', bytes = [], i = j = k = a = 0, dict = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','1','2','3','4','5','6','7','8','9'];
        for (i = 0; i < str.length; i++ ) {
            ch = str.charCodeAt(i);
            bytes[j++] = (ch < 127) ? ch & 0xFF : 127;
        }
        var chunk_len = Math.ceil(bytes.length / 32);   
        for (i=0; i<bytes.length; i++) {
            j += bytes[i];
            k++;
            if ((k == chunk_len) || (i == bytes.length-1)) {
                a = Math.floor( j / k );
                if (a < 32)
                    hash += '0';
                else if (a > 126)
                    hash += 'z';
                else
                    hash += dict[  Math.floor( (a-32) / 2.76) ];
                j = k = 0;
            }
        }
        return hash;
    }
});

$.extend(_3, {
    utils: new _3.Utils()
});

