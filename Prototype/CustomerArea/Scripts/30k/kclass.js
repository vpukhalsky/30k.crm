/**
 * Knockout data-binding model class
 */ 
 
var KClass = Class.extend({
    
    /**
     * View Model works as major model for entire view.
     * Only one can be defined on one document.
     */ 
    isViewModel: false,
        
    /**
     * Object containing observed properties
     * together with their initial values
     * 
     * eg. 
     * 
     * observed{
     *  property: 'initialValue',
     *  another: 1
     * }
     */ 
    observed: {},
    
    /**
     * Object containing computed properties
     * and corresponding functions or names of methods
     * of that class used as these.
     * 
     * eg.
     * 
     * computed{
     *  myValue: function(a, b){ return a+b; },
     *  foo: 'methodName'
     * }
     */ 
    computed: {},
    
    /**
     * Object containing mapping config
     * for observed properties, that either should be
     * wrapped with class or properties, which are arrays
     * of objects of that class.
     * 
     * eg.
     * 
     * observed: { foo: 1, bar: ['a','z'] }
     * 
     * mapped: {
     *  foo: FooClass,
     *  bar: BarClass
     * }
     */ 
    mapped: {},
    
    /**
     * Array of property names to filter in saveFilter method,
     * whilist saving object as JSON
     */ 
    filtered: [],
    
    filteredByDefault: [
        'isViewModel',
        'observed',
        'computed',
        'mapped',
        'filtered',
        'filteredByDefault'
    ],
    
    /**
     * Filters properties in saved object converted to JSON
     */ 
    jsonFilter: function(key, value){
        if(($.isArray(this.filtered) && this.filtered.indexOf(key) != -1)
            || ($.isArray(this.filteredByDefault) && this.filteredByDefault.indexOf(key) != -1)) return undefined;
        
        return value;
    },
    
    /**
     * Setup observed properties
     */ 
    initObserved: function(){
        for(var i in this.observed){
            
            //property is an array
            if($.isArray(this.observed[i])) this[i] = ko.observableArray(this.toClass(i, this.observed[i]));
            
            //any other type
            else this[i] = ko.observable(this.toClass(i, this.observed[i]));
        }
    },
    
    /**
     * Setup computed properties
     */ 
    initComputed: function(){
        for(var i in this.computed){
            if($.isFunction(this.computed[i])){
                
                //computed property has anonimous function set
                this[i] = ko.computed($.proxy(this.computed[i], this));
                
            }else{
                
                //computed property will invoke method named this.computed[i]
                this[i] = ko.computed($.proxy(this[this.computed[i]], this));   
            }
        }
    },
    
    /**
     * Extends object with data containing new values for properties
     * @param Object data
     */ 
    ext: function(data){
        if(data){
            for(var i in data){
                if(this.observed[i] !== undefined){
                    
                    //update using proper class
                    this[i](this.toClass(i, data[i]));
                }
                else if(i != 'computed' && i != 'observed'){
                    
                    //update regular JS object property
                    this[i] = this.toClass(i, data[i]);
                }
            }
        }
    },
    
    /**
     * Wrapps data speicfied in value using correspoding class mapped through key param
     * @param string key
     * @param Object value
     */ 
    toClass: function(key, value){
        var result = value;
        
        if(this.mapped[key]){
            //key exists in mapped config object
            
            //class name
            var type = this.mapped[key];
            
            if($.isArray(value)){
                //split value into separate array elements
                
                result = [];
                
                for(var i = 0 ; i < value.length ; i++){
                    
                    //wrap into class
                    result.push(new type(value[i]));
                }
            }else{
                
                //wrap into class
                result = new type(value);
            }
        }
        
        return result;
    },
    
    /**
     * Update particular observed property with new value
     * mapping correct class if set in mapped
     * 
     * @param string property 
     * @param Object value
     */ 
    update: function(property, value){
        this[property](this.toClass(property, value));
    },
        
    /**
     * Constructor
     * Overwrites properties in object including observed ones
     * 
     * @param Object data
     */ 
    init: function(data){
        this.initObserved();
        this.initSubscribed();
        
        this.ext(data);
        
        this.initComputed();
        
        //should be executed once per document
        if(this.isViewModel) ko.applyBindings(this);
    },
    
    /**
     * Clone recursivelly class property
     * @param Object property
     * @param boolean noComputed - optional, true to exclude computed properties
     */ 
    cloneRecursive: function(property, noComputed){
        if($.isArray(property)){
            
            //clone every element separatelly
            for(var j = 0 ; j < property.length ; j++){
                
                //clone KClass objects recursivelly
                if(property[j] instanceof KClass) property[j] = property[j].clone(noComputed);
            }
        }else if(property instanceof KClass){
            property = property.clone(noComputed);
        }
        
        return property;
    },
    
    /**
     * Clone object recursivelly 
     * @param boolean noComputed - optional, true to exclude computed properties
     */ 
    clone: function(noComputed){
        var clone = {};
        
        for(var i in this.observed) clone[i] = this[i]();
        
        if(!noComputed){
            for(var i in this.computed) clone[i] = this[i]();
        }
        
        //copy regular JS properties as well
        for(var i in this){
            
            //exclude functions and internal KClass properties
            if(!$.isFunction(this[i]) && i != 'computed' && i != 'mapped' && i != 'observed' && i != 'isViewModel' && i != 'parent'){
                clone[i] = this[i];
            }
        }
        
        for(var i in clone){
            clone[i] = this.cloneRecursive(clone[i], noComputed);
        }
        
        return clone;
    },
    
    /**
     * Method to overload in order to place subscribers over observed properties
     * 
     * eg.
     * 
     * observed: { foo: 1, bar: 'a' },
     * 
     * initSubscribers: function(){
     *  this.foo.subscribe(function(){ console.log('foo updated'); } );
     * }
     */ 
    initSubscribed: function(){
        
    }
});
