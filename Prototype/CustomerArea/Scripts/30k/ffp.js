/**
 * FFP class
 * 
 */ 
_3.FFP = KClass.extend({
    
    /**
     * Program unique id
     * @type int
     */ 
    Id: null,
    
    observed: {
        
        /**
         * True if FFP has neever been submited to server
         */ 
        isNew: false,
        
        Name: '',
        
        Code: '',
        
        Enabled: true,
        
        /**
         * Array of accruals both enabled and disabled for this FFP
         * @type FFPAccrual
         */ 
        ConfiguredAccruals: [],
    },
    
    computed: {
        
    },
    
    mapped: {
        //property -> class
        ConfiguredAccruals: _3.FFPAccrual,
    },
    
    /**
     * Properties not included in save FPP JSON
     */ 
    filtered: [
        'observed',
        'computed',
        'mapped',
        'filtered'
    ]
});
