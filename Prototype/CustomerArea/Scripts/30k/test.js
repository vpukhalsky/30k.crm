/**
 * Test class
 */ 
_3.Test = KClass.extend({
    
    /**
     * Test unique id
     * @type int
     */ 
    Id: null,
    
    /**
     * Hash based on editable values.
     * Used to check if test has been changed after it has been loaded from server
     * @type string
     */ 
    lastSaveHash: '',
 
    observed: {
        
        /**
         * True if Test has never been submited to server
         */ 
        isNew: false,
        
        /**
         * Has been run after being loaded
         */
        testRun: true,
        
        /**
         * Ffp Code
         * @type string
         */ 
        FfpCode: '',
        
        /**
         * Array of airport codes
         * @type string
         */ 
        Itinerary: [],
        
        /**
         * Array of airline codes, one per segment
         * @type string
         */ 
        MkAirlines: [],
        
        /**
         * Array of airline codes, one per segment
         * @type string
         */ 
        OpAirlines: [],
        
        /**
         * Array of fare codes, one per segment
         * @type string
         */ 
        FareCodes: [],
        
        /**
         * Array of flight numbers, one per segment
         * @type string
         */ 
        FlightNums: [],
        
        /**
         * Date in format: mm/dd/yyyy
         * @type string
         */ 
        FlightDate: '',
        
        /**
         * Date in format: mm/dd/yyyy
         * @type string
         */ 
        PurchDate: '',
        
        /**
         * Test objective
         * @type string
         */ 
        TestObj: '',
        
        CurrencyCode: null,
        
        BasePrice: null,
        
        Taxes: null,
        
        CxSurcharges: null,
        
        GovtFees: null,
        
        /**
         * Array of test blocks object
         * @type Object
         */ 
        CalcBlocks: [],
        
        /**
         * Array of test values grouped by accrual type
         * @type TestAccrual
         */ 
        BasicAccruals: [],
        
        /**
         * Array of computed test values for specific status
         * @type Object
         */ 
        BasicStatuses: [],
        
        /*
        * Array of itinerary segments.
        */
        BasicSegments: [],
        
        /**
         * Program Configured Accruals
         * @type FFPAccrual
         */ 
        FfpAccruals: [],
        
        /**
         * Initial departure airport code
         * @type string
         */ 
        DepartureAirport: '',
        
        /**
         * Itinerary segments to organize UI
         * @type TestSegment
         */ 
        Segments: [],
        
    },
        
    computed: {
        
        isDepartureAirportValid: function(){
            return this.DepartureAirport().length == 3;  
        },
        
        isBasicTestValid: function(){
            var ba = this.BasicAccruals();
            var fa = this.FfpAccruals();
            
            for (var i = 0 ; i < ba.length ; i++) {
                for (var y = 0; y < fa.length; y++) {
                    if (fa[y].AccrualType() == ba[i].AccrualType() && !ba[i].isValid()) {
                        return false;
                    }
                }
            }
            
            return true;
        },
        
        /**
         * Is Test valid to save?
         */ 
        isValid: function(){
            var segments = this.Segments();
            for(var i = 0 ; i < segments.length ; i++){
                if(!segments[i].isValid()) return false;
            }
            
            var valid = 
                this.isDepartureAirportValid()
                && this.isBasicTestValid();
            
            return valid;
        },
        
        /**
         * Do expected values match computed?
         */ 
        Matched: function(){
            var ba = this.BasicAccruals();
            
            for (var i = 0; i < ba.length; i++) {
                
                if(!ba[i].isCalculated()) continue;

                if(!ba[i].Matched()) return false;
            }
            
            return true;
        },
        
        /**
         * Has basic test been calculated already?
         */ 
        isBasicCalculated: function(){
            var ba = this.BasicAccruals();
            var a = this.FfpAccruals();
            for (var i = 0; i < ba.length; i++) {
                for (var y = 0; y < a.length; y++) {
                    if (a[y].AccrualType() == ba[i].AccrualType() && ba[i].isCalculated()) {
                        return true;
                    }
                }
                //if (a.indexOf(ba[i].AccrualType()) != -1 && ba[i].isCalculated()) {
                //    return true;
                //}
            }
            return false;
        },
        
        hasBasicSegmentsCalculated: function() {
            var ba = this.BasicAccruals();
            for (var i = 0; i < ba.length; i++) {
                if (ba[i].Segments() && ba[i].Segments().length > 0) {
                    return true;
                };
            }
            return false;
        },
        
        hasBasicStatusesCalculated: function () {
            var statuses = this.BasicStatuses();
            
            for (var i = 0; i < statuses.length; i++) {
                if (statuses[i].Status) return true;
            }

            return false;
        },
        
        totalDistance: function() {
            var dist = 0;

            for (var i = 0; i < this.Segments().length; i++) {
                dist += this.Segments()[i].Distance();
            }

            return dist;
        }
    },
    
    mapped: {
        BasicAccruals: _3.TestAccrual,
        FfpAccruals: _3.FFPAccrual,
        Segments: _3.TestSegments
    },
    
    /**
     * Properties not included in save Test JSON
     */ 
    filtered: [
        'observed',
        'computed',
        'mapped',
        'filtered',
        'IsValid',
        'BasicAccruals',
        'MinimumAccruals',
        'FfpAccruals',
        'Matched',
        'Segments',
        'DepartureAirport',
        'BasicStatuses',
        'isBasicCalculated',
        'hasMinimumMilesTest',
        'lastSaveHash',
        'isDepartureAirportValid',
        'isBasicTestValid',
        'isValid',
        'testRun',
        'hasBasicSegmentsCalculated',
        'hasBasicStatusesCalculated',
        'allTestsSelected'
    ],
    
    /**
     * Inits knockout.js subscribers on observed properties
     * @see KClass
     */ 
    initSubscribed: function(){
        
        //if FFP is changed...
        this.FfpCode.subscribe($.proxy(this.updateFfpAccruals, this));
    },
    
        /**
     * Save corresponding FFP's accrual types
     */ 
    updateFfpAccruals: function(){
        var ffp = this.getFfp();
        if(!ffp) return;
        
        var code = this.FfpCode();
        
        if (code.length > 0) {

            this.testRun(false);

            this.BasicAccruals([]);

            this.BasicSegments([]);

            this.FfpAccruals([]);
            
            var ffp = this.getFfp();
            
            var ca = ffp.ConfiguredAccruals();
            var accruals = _3.view.accruals();
            
            var temp = [];
            for(var i = 0 ; i < ca.length ; i++){
                if(ca[i].Enabled()) temp.push(ca[i]);
            }
            
            //keep order the same as in ffpViewModel.accruals
            for (var i = 0 ; i < accruals.length ; i++) {
                for (var y = 0; y < temp.length; y++) {
                    if (temp[y].AccrualType() == accruals[i].AccrualType()) {
                        this.FfpAccruals.push(temp[y]);
                    }
                }
            }
        }
    },
    
    onSegmentChange: function() {
        this.testRun(false);
        this.BasicAccruals([]);
    },
    
    /**
     * Shorthand method
     */ 
    getFfp: function(){
        return _3.view.getFfpByCode(this.FfpCode());
    },

    /**
     * Create hash value based on editable fields
     * @return String hash
     */ 
    toHash: function(){
        var data = this.DepartureAirport() 
            + this.PurchDate() + this.FlightDate()
            + this.FfpCode() + this.CurrencyCode() + this.BasePrice() + this.Taxes()
            + this.CxSurcharges() + this.GovtFees();
        
        var ba = this.BasicAccruals();
        var s = this.Segments();
        
        for(var i = 0 ; i < ba.length ; i++) data += ba[i].Expected();
        
        for(var i = 0 ; i < s.length ; i++) data += s[i].Airport() + s[i].MkAirline() + s[i].OpAirline() + s[i].FareCode() + s[i].FlightNum();
        
        return _3.utils.strToHash(data);
    },
    
    /**
     * Extend test with @param obj
     */ 
    ext: function(obj){
        this.parent(obj);
        this.setupSegments();
        this.setupAccruals();
        this.lastSaveHash = this.toHash();
    },
    
    /**
     * Move values from object models to simple arrays and CalcBlocks object
     */ 
    beforeSave: function(){
        var segments = this.Segments();
        this.Itinerary([this.DepartureAirport().toUpperCase()]);
        this.MkAirlines([]);
        this.OpAirlines([]);
        this.FareCodes([]);
        this.FlightNums([]);
        
        for(var i = 0 ; i < segments.length ; i++){
            var s = segments[i];
            this.Itinerary.push(s.Airport().toUpperCase());
            this.MkAirlines.push(s.MkAirline().toUpperCase());
            this.OpAirlines.push(s.OpAirline().toUpperCase());
            this.FareCodes.push(s.FareCode().toUpperCase());
            this.FlightNums.push(s.FlightNum() ? s.FlightNum() : '');
        }
        
        var cb = this.CalcBlocks();
        var ba = this.BasicAccruals();
        var basicBlock = cb[0];
        
        for(var i = 0 ; i < ba.length ; i++){
            var accrual = ba[i];
            basicBlock.Expected[accrual.AccrualType()] = accrual.Expected();
            accrual.reset();
        }
    },
    
    /**
     * Save data from CalcBlocks as separate objects
     * grouped by accrual type.
     * @see TestAccrual
     */ 
    setupAccruals: function(){
        
        if(!_3.view) return;
        
        var cb = this.CalcBlocks();
        
        var types = _3.view.accruals();
        
        this.BasicAccruals([]);
        this.BasicSegments([]);
        
        if(this.isNew()){
            
            for (var i = 0 ; i < types.length ; i++) {
                
                var type = types[i].AccrualType();
                
                this.BasicAccruals.push(new _3.TestAccrual({
                    AccrualType: type
                }));
            }
            
            this.CalcBlocks([{
                Distance: 0,
                Expected: {}
            }]);
        }else{
        
            var fa = this.FfpAccruals();

            for(var i = 0 ; i < cb.length ; i++){
                
                var block = cb[i];
                
                for (var j = 0 ; j < types.length ; j++) {
                    
                    var type = types[j].AccrualType();

                    var faMatched = false;

                    for (var y = 0; y < fa.length; y++) {
                        if (fa[y].AccrualType() == type) {
                            faMatched = true;
                            break;
                        }
                    }
                    
                    if (!faMatched) {
                        continue;
                    }

                    var segments = [];
                    var statuses = [];
                    
                    var accrualConfig = {
                        AccrualType: type,
                        Expected: block.Expected[type],
                    };
                    
                    if(block.Calculated){
                        
                        var ps = block.Calculated.FromFare.PerSegment;
                        
                        for (var k = 0; k < ps.length; k++) {
                            segments.push(ps[k][type]);
                            this.Segments()[k].Distance(ps[k].Distance);
                            this.BasicSegments.push(ps[k].Segment);
                        }
                        
                        var blockType = block.Calculated.FromFare.Total[type];   
                        
                        accrualConfig.TotalFromFare = (blockType !== null && blockType !== undefined) ? blockType.Value : null;
                        accrualConfig.Matched = (blockType !== null && blockType !== undefined) ? blockType.Matched : true;
                        accrualConfig.Segments = segments;
                    
                    }
    
                    var accrual = new _3.TestAccrual(accrualConfig);
                    
                    this.BasicAccruals.push(accrual);
                }
                
                if(block.Calculated){
                    
                    //add statuses
                    var fs = block.Calculated.FromStatus;
                    
                    for(var k = 0 ; k < fs.length ; k++) statuses.push(fs[k]);
                    
                    this.BasicStatuses(statuses);
                }
                
            }
        }
        
    },
    
    /**
     * Save values in form easy to read
     * by knockout (grid with computed values)
     */ 
    setupSegments: function(){
        var it = this.Itinerary();
        
        if(it.length < 1){
            this.Segments([new _3.TestSegment()]);
        }else{
            var ma = this.MkAirlines();
            var oa = this.OpAirlines();
            var fc = this.FareCodes();
            var fn = this.FlightNums();
            
            this.Segments([]);
            
            this.DepartureAirport(it[0]);
            
            for(var i = 0 ; i < it.length - 1 ; i++){
                
                this.Segments.push(
                    new _3.TestSegment({
                        Airport: it[i+1],
                        MkAirline: ma[i],
                        OpAirline: oa[i],
                        FareCode: fc[i],
                        FlightNum: fn[i]
                    })
                );
                
            }   
        }
    },
    
    /**
     * Run saved test by sending request to server
     */ 
    run: function (callback) {
        
        if (!this.isValid()) {
            _3.view.validate(true);
            return;
        }
        
        _3.view.isLoading(true);
        
        var url = _3.routes.CALCULATOR_CALCULATE;

        this.beforeSave();
        var data = ko.toJSON(this, this.jsonFilter);

        var wrappedObject = { Value: data };

        _3.utils.sendPost(url, wrappedObject, $.proxy(function (response) {
            if(response.Success) {

                this.testRun(true);

                //overwrite properties with new data
                this.ext(response.Value);

                _3.airportsAutocomplete.initAirportLookups();

                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Status.Message);
            }
            
            _3.view.isLoading(false);
        }, this)); 
        
    },
    
    clear: function () {
        _3.view.setEditedTest();
        _3.view.initDatepickers();
        _3.airportsAutocomplete.initAirportLookups();
    },
    
    /**
     * Delete segment from collection
     */ 
    removeSegment: function(segment){
        this.Segments.remove(segment);
    },
    
    /**
     * Add segment to collection
     */ 
    addSegment: function(){
        var fareCode = '';
        var segments = this.Segments();
        
        if(segments.length > 0){
            fareCode = segments[segments.length - 1].FareCode();
        }
        
        this.Segments.push(new _3.TestSegment({
            FareCode: fareCode
        }));
        
        _3.airportsAutocomplete.initAirportLookups();
    },
    
    /**
     * Fare code in segment changed
     */
    onFareCodeChange: function(segment){
        if(!this.isNew()) return;
        
        var segments = this.Segments();
        var found = false;
        
        for(var i = 0 ; i < segments.length ; i++){
            if(!found && segments[i] == segment){
                found = true;
                continue;
            }else if(found && segments[i].FareCode() == segments[i].observed.FareCode){
                segments[i].FareCode(segment.FareCode());
            }
        }
    }
});
