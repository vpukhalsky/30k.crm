'use strict';


// Declare app level module which depends on filters, and services
angular.module('30k_crm', [
  'ngRoute',
  'mm.foundation',
  'datePicker',
  'googlechart',
  '30k_crm.filters',
  '30k_crm.services',
  '30k_crm.directives',
  '30k_crm.controllers'
])

.config(['$routeProvider', 'routes', function($routeProvider, routes) {
  
  for(var r in routes){
    $routeProvider.when(routes[r].path, { templateUrl: routes[r].templateUrl, controller: routes[r].controller });
  }
  
  $routeProvider.otherwise({redirectTo: routes.customers.path});
  
}]);
