'use strict';

/* Services */

angular.module('30k_crm.services', [])

.value('version', '1.0')

.constant('routes', {
    customersByMonth: {
        path: '/customers/:year/:month',
        templateUrl: 'partials/customers.html', 
        controller: 'CustomersController'
    },
    customers: {
        path: '/customers',
        templateUrl: 'partials/customers.html', 
        controller: 'CustomersController'
    },
    customerEdit: {
        path: '/customer/:id/edit',
        templateUrl: 'partials/customer-edit.html',
        controller: 'CustomerEditController'
    },
    customerNew: {
        path: '/customer/new',
        templateUrl: 'partials/customer-edit.html',
        controller: 'CustomerEditController'
    }
})

.value('config', {
    customer: null
})

.service('kkService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){
    
    var jsonPath = 'json/';
    
    function _handleError(response){
        
        $rootScope.loading--;
        var message = 'An unknown server error occurred.';
 
        if (!response.data.message){
            
            _showAlert('An unknown server error occured. Please try again or refresh page.');
            
        }else{
            
            _showAlert(response.data.message);
            message = response.data.message;
        }
        
        return $q.reject(message);

    }
         
    function _handleSuccess(response){
        
        $rootScope.loading--;
        
        if(!angular.isObject(response.data) || !response.data.success){
            
            if(response.data.message){
                _showAlert(response.data.message);
            }
            
        }else{
            if(response.data.message){
                _showAlert(response.data.message, 'success');
            }
        }
        
        return response.data.result;
        
    }
    
    function _showAlert(msg, type){
        
        if(type === undefined) type = 'alert';

        $rootScope.$emit('kk-alert', { alert: { type: type, msg: msg }});
    }
    
    function sendPost(urlSlug, data){
        
        $rootScope.loading++;
        
        var request = $http({
            method: 'post',
            url: jsonPath + urlSlug,
            data: data
        });
    
        return request.then(_handleSuccess, _handleError);
        
    }
    
    function _prepareGetUrl(url, params){
        
        if(params && angular.isObject(params)){
            for(var i in params){
                url = url.replace(':' + i, params[i]);
            }
        }
        
        return url;
    }
    
    function sendGet(urlSlug, params){
        
        $rootScope.loading++;
        
        var url = _prepareGetUrl(urlSlug, params);
        
        var request = $http({
            method: 'get',
            url: jsonPath + url
        });
        
        return request.then(_handleSuccess, _handleError);
    }
    
    return {
        sendPost: sendPost,
        sendGet: sendGet
    };
    
}])

.service('customerService', ['kkService', '$q', 'config', function(kkService, $q, config){
    
    var collection = {};
    var customers = {};
    var stats = {};
    
    function getConfig(){
        
        if(angular.isObject(config.customer)){
            var deferred = $q.defer();
            deferred.resolve(config.customer);
            return deferred.promise;
        }else{
            return kkService.sendGet('customers/config.json').then(function(response){
                config.customer = response;
                return config.customer;
            });
        }
        
    }
    
    function _generateKey(month, year){
        if(month === undefined || year === undefined){
            var today = new Date();
            month = today.getMonth();
            year = today.getFullYear();
        }
        
        return month + '_' + year;
    }
    
    function getCollection(month, year){
        
        var key = _generateKey(month, year);
        
        if(!angular.isObject(collection[key])){
            
            return kkService.sendPost('customers/list.json', {
                month: month,
                year: year
            }).then(function(response){
                
              var key = _generateKey(response.month, response.year);
              collection[key] = response.customers;
              return response.customers;
              
            }, function(response){
              //TODO
            });   
            
        }else{
            
            var deferred = $q.defer();
            deferred.resolve(collection[key]);
            return deferred.promise;
            
        }
    }
    
    function _generateCustomerKey(id){
        return 'cust_' + id;
    }
    
    function getOne(id){
        var key = _generateCustomerKey(id);
        
        if(angular.isObject(customers[key])){
            
            var deferred = $q.defer();
            deferred.resolve(customers[key]);
            return deferred.promise;
            
        }else{
            
            return kkService.sendPost('customers/single.json', {
                id: id
            }).then(function(response){
                
              var key = _generateCustomerKey(response.customer.id);
              customers[key] = response.customer;
              return response.customer;
              
            }, function(response){
              //TODO
            }); 
            
        }
    }
    
    var toSave = null;
    
    function saveCallback(response){ 
        var key = _generateCustomerKey(response.customer.id);
        if(customers[key] === undefined){
            //new customer - invalidate cache
            collection = {};
        }
        customers[key] = angular.extend(toSave, response.customer);
        return customers[key];
    }
    
    function save(customer){
        toSave = customer;
        return kkService.sendPost('customers/save.json', customer).then(saveCallback, saveCallback);
    }
    
    function generateApiPass(customer){
        return kkService.sendPost('customers/apiPass.json', {
            id: customer.id
        }).then(function(response){
            if(customer.id == response.customer.id) customer = angular.extend(customer, response.customer);
            return customer;
        }, function(response){
            if(customer.id == response.customer.id) customer = angular.extend(customer, response.customer);
            return customer;
        });
    }
    
    function _generateStatsKey(customer, period){
        return customer.id + '_' 
            + period.start.getFullYear() + '_' 
            + period.start.getMonth() + '_' 
            + period.start.getDate() + '_' 
            + period.end.getFullYear() + '_' 
            + period.end.getMonth() + '_' 
            + period.end.getDate();
    }
    
    function getStats(customer, period){
        
        var key = _generateStatsKey(customer, period);
        
        if(angular.isObject(stats[key])){
            
            var deferred = $q.defer();
            deferred.resolve(stats[key]);
            return deferred.promise;
            
        }else{
            
            return kkService.sendPost('customers/stats.json',{
                id: customer.id,
                start: period.start,
                end: period.end
            }).then(function(response){
                
                return _addStats(response);
                
            }, function(){
                //TODO
            }); 
            
        }
    }
    
    function _prepareGraphsData(data){
        
        for(var i = 0 ; i < data.length ; i++){
            var date = new Date(data[i].c[0].v);
            var parts = date.toDateString().split(' ');
            data[i].c[0].v = parts[1] + ' ' + parts[3];
        }
        return data;
        
    }
    
    function _addStats(response){
        
        var period = {
            start: new Date(response.start),
            end: new Date(response.end)
        };
                
        var key = _generateStatsKey(response.customer, period);
        
        for(var i in response.data.graphs){
            response.data.graphs[i] = _prepareGraphsData(response.data.graphs[i]);
        }
        
        var total = 0;
        var items = response.data.billings.items;
        
        for(var i = 0 ; i < items.length ; i++){
            total += items[i].subtotal;
        }
        
        response.data.billings.total = total;
        
        stats[key] = response.data;
        
        return stats[key];
    }
    
    return {
        getCollection: getCollection,
        getOne: getOne,
        save: save,
        generateApiPass: generateApiPass,
        getStats: getStats,
        getConfig: getConfig
    };
}]);