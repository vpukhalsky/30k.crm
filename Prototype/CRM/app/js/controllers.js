'use strict';

/* Controllers */

angular.module('30k_crm.controllers', [])

.controller('MainController', ['$scope', 'customerService', '$interval', function($scope, customerService, $interval) {
  
  $scope.config = null;
  $scope.timeLeftToLogout = '0s';
  
  var idleInterval = null;
  var idleCounter = 0;
  
  function logout(){
    document.location = $scope.config.session.logoutUrl;
  }
  
  function updateIdleInterval(){
    
    idleCounter++;
    
    var remain = $scope.config.session.idleTimeout - idleCounter;
    if(remain < 0) remain = 0;
    
    var h = Math.floor(remain / 3600);
    var m = Math.floor((remain - h * 3600) / 60);
    var s = remain % 60;
    
    $scope.timeLeftToLogout = '';
    if(h > 0) $scope.timeLeftToLogout = h + 'h';
    if(m > 0) $scope.timeLeftToLogout += m + 'm';
    $scope.timeLeftToLogout += s + 's';
    
    if(remain <= 0) logout();
  }
  
  function updateIdleTimeout(){
    idleCounter = 0;
  }
  
  function initIdleTimeout(){
    
    idleInterval = $interval(updateIdleInterval, 1000);
    angular.element(document).on('click', updateIdleTimeout);
    
    $scope.$on('$destroy', function(){
      if(idleInterval) $timeout.cancel(idleInterval);
      angular.element(document).off('click', updateIdleTimeout);
    });
    
  }
  
  customerService.getConfig().then(function(c){
    
    $scope.config = angular.copy(c);
    initIdleTimeout();
    
  });
  
}])

.controller('CustomersController', ['$scope', 'customerService', '$routeParams', '$location', 'routes', function($scope, customerService, $routeParams, $location, routes) {
  
  $scope.protocol = 'http://';
  
  $scope.customers = [];
  
  $scope.period = new Date();
  
  $scope.onDatepickerInit = function(scope){
    scope.$on('setDate', function(e, date, view){
      
      var today = new Date();
      if(date.valueOf() > today.valueOf()) return;
      
      var url = routes.customersByMonth.path
        .replace(':month', date.getMonth()+1)
        .replace(':year', date.getFullYear());
        
      $location.path(url);
      
    });
  };
  
  $scope.getTotal = function(field){
    var total = 0;
    for(var i = 0 ; i < $scope.customers.length ; i++){
      total += $scope.customers[i][field];
    }
    return total;
  };
  
  function load(month, year){
    
    customerService.getCollection(month, year).then(function(customers){
      
      $scope.period = new Date(parseInt(year), parseInt(month) + 1, 0, 0, 0, 0);
      $scope.customers = customers;
      
    }, function(){
      //TODO
    });
    
  }
  
  function init(){
    var month = $scope.period.getMonth();
    var year = $scope.period.getFullYear();
    
    if($routeParams.year && $routeParams.month){
      month = $routeParams.month - 1;
      year = $routeParams.year;
    }
    
    load(month, year);
  }

  init();
  
}])

.controller('CustomerEditController', ['$scope', 'kkService', 'customerService', '$routeParams', '$location', 'routes', '$modal', '$timeout', '$window', function($scope, kkService, customerService, $routeParams, $location, routes, $modal, $timeout, $window) {
  
  $scope.customer = {};
  $scope.config = null;
  
  $scope.periodPickerClick = function($event){
    
    $timeout(function(){
      
      var picker = document.getElementById('period-picker');
      var matchLeft = picker.style.left.match(/\d+\.?\d*/);
      if(!angular.isArray(matchLeft) || matchLeft.length < 1) return;
      var left = parseFloat(matchLeft[0]);
      var width = picker.offsetWidth;
      var maxWidth = $window.innerWidth;
      var offset = 30;
      var diff = left + width - maxWidth + offset;
      
      if(diff > 0){
        left -= diff;
        angular.element(picker).css('left', left + 'px');
      }
      
    }, 200);
    
  };
  
  $scope.paymentTerm = 0;
  
  $scope.paymentTermChange = function(){
    
    if(this.customer.setDefaultPaymentTerms){
      
        this.customer.paymentTerm = this.paymentTerm;
        
    }
    
  };
  
  $scope.graphsHorizontal = true;
  
  $scope.setGraphsLayout = function(layout){
    
    $scope.graphsHorizontal = layout;
    $timeout(function(){
      angular.element($window).triggerHandler('resize');  
    }, 0);
    
  };
  
  $scope.apiPassInputType = 'password';
  $scope.cacPassInputType = 'password';
  $scope.create = false;
  
  $scope.statsPeriod = {
    start: new Date(),
    end: new Date(),
    range: 0
  };
  
  $scope.monthRange = 0;
  $scope.yearRange = 0;
  
  $scope.selectedPrePaid = [];
  
  $scope.statsSelected = false;
  
  $scope.billings = {};
  
  var revenueChartLabel = 'Revenue ({{CODE}})';
  
  $scope.$watch('config.currency.code', function(newValue, oldValue){
    $scope.revenueChart.data.cols[1].label = revenueChartLabel.replace('{{CODE}}', newValue);
  });
  
  $scope.revenueChart = {
    type: 'ColumnChart',
    options: {
      title: 'Revenue in selected period',
      colors: ['#5da423']
    },
    data: {
      cols: [{
        id: 'd', label: 'Date', type: 'string'
      },{
        id: 's', label: revenueChartLabel, type: 'number'
      }],
      rows: []
    }
  };
  
  $scope.searchesChart = {
    type: 'ColumnChart',
    options: {
      title: 'Number of searches in selected period',
      colors: ['#2ba6cb']
    },
    data: {
      cols: [{
        id: 'd', label: 'Date', type: 'string'
      },{
        id: 's', label: 'Searches', type: 'number'
      }],
      rows: []
    }
  };
  
  var json = '';
  var statsTimeoutPromise = false;
  var forceValidation = false;
  
  $scope.onStatisticsTabSelect = function(){
    
    $scope.statsSelected = true;
    loadStats();
    
  };
  
  function loadStats(){
    
    if(!$scope.statsSelected) return;
    
    if(statsTimeoutPromise){
      $timeout.cancel(statsTimeoutPromise);
      statsTimeoutPromise = null;
    }
    
    statsTimeoutPromise = $timeout(function(){
      
      var period = angular.copy($scope.statsPeriod);
      period.start.setDate(period.start.getDate() + 1);
      period.end.setDate(period.end.getDate() + 1);
      
      customerService.getStats($scope.customer, period).then(function(stats){
        
        $scope.searchesChart.data.rows = stats.graphs.searches;
        $scope.revenueChart.data.rows = stats.graphs.revenue;
        $scope.billings = stats.billings;
        
      }, function(){
        //TODO
      });
      
    }, 500);
  }
  
  $scope.isInvalid = function(field){
    return (!field.$valid && field.$dirty) || (forceValidation && !field.$valid);
  };
  
  $scope.save = function(form){
    
    forceValidation = true;
    
    if($scope.saveDisabled(form)) return false;  
    
    customerService.save($scope.customer).then(function(customer){
      
      setCustomer(customer);
      forceValidation = false;
      
      $location.path(routes.customerEdit.path.replace(':id', customer.id));
      
    }, function(customer){
      
      forceValidation = false;
      setCustomer(customer);
      
    });
  };
  
  $scope.showApiPassword = function(){
    if($scope.apiPassInputType == 'password'){
      $scope.apiPassInputType = 'text';
    }else{
      $scope.apiPassInputType = 'password';
    }
  };
  
  $scope.showCacPassword = function(){
    if($scope.cacPassInputType == 'password'){
      $scope.cacPassInputType = 'text';
    }else{
      $scope.cacPassInputType = 'password';
    }
  };
  
  $scope.generateApiPass = function(){
    customerService.generateApiPass($scope.customer).then(function(customer){
       $scope.customer = customer;
    }, function(response){
      //TODO
    });
  };
  
  $scope.saveDisabled = function(form){
    return json == angular.toJson($scope.customer) || !form.$valid;
  };
  
  $scope.addPriceTier = function(){
    var tier = angular.copy($scope.config.defaultCustomer.priceTiers[0]);
    
    if($scope.customer.priceTiers.length > 0){
      var upper = $scope.customer.priceTiers[$scope.customer.priceTiers.length - 1];
      tier.from = upper.to + 1;
      tier.to = tier.from + 1000;
      tier.price = upper.price;
    }
    
    $scope.customer.priceTiers.push(tier);
  };
  
  $scope.removePriceTier = function(index){
    $scope.customer.priceTiers.splice(index, 1);
  };
  
  $scope.togglePrePaid = function(prePaid){
    var index = $scope.selectedPrePaid.indexOf(prePaid);
    if(index < 0) $scope.selectedPrePaid.push(prePaid);
    else $scope.selectedPrePaid.splice(index, 1);
  };
  
  $scope.addPrePaid = function(){
    editPrePaid();
  };
  
  $scope.editPrePaid = function(){
    editPrePaid($scope.selectedPrePaid[0]);
  };
  
  function editPrePaid(prePaid){
    var block = {};
    var index = -1;
    
    if(angular.isObject(prePaid)){
      
      block = angular.copy(prePaid);
      index = $scope.customer.prePaidBlocks.indexOf(prePaid);
      if(!(block.expireDate instanceof Date)) block.expireDate = new Date(block.expireDate);
      
    }else{
      
      block = angular.copy($scope.config.defaultPrePaidBlock);
      var now = new Date();
      block.createdDate = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
      block.expireDate = new Date(now.getFullYear() + 1, now.getMonth(), now.getDate(), 0, 0, 0);
      
    }
    
    var modalInstance = $modal.open({
      templateUrl: 'partials/pre-paid-edit.html',
      controller: 'PrePaidEditController',
      windowClass: 'tiny',
      resolve: {
        prePaid: function(){
          return block;
        },
        index: function(){
          return index;
        },
        config: function(){
          return $scope.config;
        }
      }
    });
    
    modalInstance.result.then(function(result){
      
      if(result.index >= 0){
        $scope.customer.prePaidBlocks[result.index] = result.prePaid;  
      }else{
        $scope.customer.prePaidBlocks.push(result.prePaid);
      }
      
    }, function(){
      //TODO
    });
    
  }
  
  $scope.removePrePaid = function(){
    for(var i = 0 ; i < $scope.selectedPrePaid.length ; i++){
      var index = $scope.customer.prePaidBlocks.indexOf($scope.selectedPrePaid[i]);
      $scope.customer.prePaidBlocks.splice(index, 1);
    }
    $scope.selectedPrePaid = [];
  };
  
  $scope.enablePrePaid = function(){
    for(var i = 0 ; i < $scope.selectedPrePaid.length ; i++){
      $scope.selectedPrePaid.enabled = true;
      var index = $scope.customer.prePaidBlocks.indexOf($scope.selectedPrePaid[i]);
      $scope.customer.prePaidBlocks[index].enabled = true;
    }
  };
  
  $scope.disablePrePaid = function(){
    for(var i = 0 ; i < $scope.selectedPrePaid.length ; i++){
      $scope.selectedPrePaid.enabled = false;
      var index = $scope.customer.prePaidBlocks.indexOf($scope.selectedPrePaid[i]);
      $scope.customer.prePaidBlocks[index].enabled = false;
    }
  };
  
  function setCustomer(customer){
    $scope.customer = angular.copy(customer);
    $scope.customer.setDefaultPaymentTerms = false;
    json = angular.toJson($scope.customer);
    $scope.statsSelected = false;
  }
  
  function load(id){
    customerService.getOne(id).then(function(customer){
      
      setCustomer(customer);
      
    }, function(){
      //TODO
    });
  }
  
  function loadConfig(){
    
    customerService.getConfig().then(function(config){
      $scope.config = angular.copy(config);
      onConfigLoaded();
    });
  }
  
  function getMonthPeriod(){
    var now = new Date();
    var end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
    var start = new Date(end.getFullYear(), end.getMonth() - 1, end.getDate(), 0, 0, 0);
    end.setMilliseconds(0);
    start.setMilliseconds(0);
    
    return {
      start: start,
      end: end,
      range: end.valueOf() - start.valueOf()
    };
    
  }
  
  $scope.setMonthPeriod = function(){
    $scope.statsPeriod = getMonthPeriod();
  };
  
  $scope.setYearPeriod = function(){
    $scope.statsPeriod = getYearPeriod();
  };
  
  $scope.$watch('statsPeriod.start', function(newValue, oldValue){
    $scope.statsPeriod.range = $scope.statsPeriod.end.valueOf() - newValue.valueOf();
    loadStats();
  });
  
  $scope.$watch('statsPeriod.end', function(newValue, oldValue){
    $scope.statsPeriod.range = newValue.valueOf() - $scope.statsPeriod.start.valueOf();
    loadStats();
  });
  
  $scope.isMonthSelected = function(){
    return $scope.statsPeriod.range == $scope.monthRange;
  };
  
  $scope.isYearSelected = function(){
    return $scope.statsPeriod.range == $scope.yearRange;
  };
  
  $scope.isOtherPeriodSelected = function(){
    return !$scope.isMonthSelected() && !$scope.isYearSelected();
  };
  
  $scope.preventDefault = function($event){
    $event.preventDefault(); 
    $event.stopPropagation();
  };
  
  function getYearPeriod(){
    var now = new Date();
    var end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
    var start = new Date(end.getFullYear() - 1, end.getMonth(), end.getDate(), 0, 0, 0);
    end.setMilliseconds(0);
    start.setMilliseconds(0);
    
    return {
      start: start,
      end: end,
      range: end.valueOf() - start.valueOf()
    };
    
  }
  
  $scope.generateInvoice = function(){
    
    alert('TODO: Generate invoice');
    
  };
  
  function init(){
    var id = $routeParams.id;
    
    $scope.passInputType = 'password';
    $scope.statsPeriod = getMonthPeriod();
    $scope.monthRange = getMonthPeriod().range;
    $scope.yearRange = getYearPeriod().range;
    
    if(id !== undefined && id.match(/^\d+$/)){
      
      $scope.create = false;
      load(parseInt(id));
      
    }else{
      
      $scope.create = true;
      setCustomer($scope.customer);
      
    }
    
    loadConfig();
  }
  
  function onConfigLoaded(){
    
    $scope.paymentTerm = $scope.config.paymentTerms[0].id;
    if($scope.create) $scope.customer = angular.copy($scope.config.defaultCustomer);
    
  }
  
  init();
  
}])

.controller('PrePaidEditController', ['$scope', '$modalInstance', 'prePaid', 'index', 'customerService', 'config', function($scope, $modalInstance, prePaid, index, customerService, config) {
  
  $scope.prePaid = prePaid;
  $scope.index = index;
  $scope.config = config;
  
  var json = angular.toJson(prePaid);
  
  $scope.save = function () {
    
    $modalInstance.close({
      prePaid: $scope.prePaid,
      index: $scope.index
    });
    
  };
  
  $scope.isInvalid = function(field){
    return !field.$valid && field.$dirty;
  };
  
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  
  $scope.saveDisabled = function(form){
    return (json == angular.toJson($scope.prePaid) && index >= 0) || !form.$valid;
  };
  
  loadConfig();
  
}]);