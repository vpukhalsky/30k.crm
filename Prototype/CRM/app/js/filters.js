'use strict';

/* Filters */

angular.module('30k_crm.filters', [])

.filter('domain', function() {
  
  return function(text) {
      return text.replace(/^https?\:\/\/w{0,3}\.?/, '');
  };
  
});
