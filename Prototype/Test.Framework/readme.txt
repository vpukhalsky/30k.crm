1.
[GET]
/TestRules/

The response (described by Response.PageData.json) first of all it will contain properties �inherited�  from the Response.Default.json and will also contain the Value property containing existing testing rules (every of those described by the TestRule.json) as well as pages infrastructure required data (like programs configuration).

2.
[PUT]
/TestRules/

Sends testing rule model (described with TestRule.json) using PUT http verb to the server. If Id of the rule isn�t yet set the rule considered to be a new one, otherwise existing record will be matched by given Id and updated.
The response (described by Response.Default.json) provides information about performed action on the server � whether or not it succeeded, and message to be displayed to the client.

3.
[GET]
/TestRules/Clone/{Id}

Forces server to create a clone of the existing testing rule referenced by {Id} parameter.
The response (described by Response.CloneRule.json) will contain properties �inherited�  from the Response.Default.json and also contain the Value property containing cloned testing rule (described by the TestRule.json) if action succeeded on the server.

4.
[DELETE]
/TestRules/{Id}

Forces server to delete testing rule with given {Id}.
The response (described by Response.Default.json) provides information about performed action on the server � whether or not it succeeded, and message to be displayed to the client.
