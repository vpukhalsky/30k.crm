/**
 * Test View Model class
 * Main class of 30k Testing Framework
 * @author Artur Polkowski
 * 
 */ 
_3.TestViewModel = KClass.extend({
    
    /**
     * Knockout.js data-view-view-data binding enabled
     */ 
    isViewModel: true,
    
    observed: {
        
        /**
         * Which section is currently visible for user?
         * @type string
         */ 
        selectedMainNav: 'test-all',
        
        /**
         * Is initial data retrived from server?
         * @see test-all.json
         */ 
        isLoaded: false,
        
        /**
         * Is any pending AJAX request enabled?
         */ 
        isLoading: false,
        
        /**
         * Array of Tests
         * @see test.js
         * @type Test
         */ 
        tests: [],
        
        /**
         * Currently edited Test
         * @type Test
         */ 
        editedTest: null,
        
        /**
         * Array of alerts displayed on top of screen eg. in case of server errors
         * @see alert.js
         * @type Alert
         */ 
        alerts: [],
        
        showValidation: false
    },
    
    mapped: {
        editedTest: _3.Test,
        tests: _3.Test,
        alerts: _3.Alert
    },
    
    /**
     * Static array of view objects used to display main menu
     * @type Object
     */ 
    mainNav: [],
    
    /**
     * Constructor
     * @param data - config object overwritting default class properties
     */ 
    init: function(data){
        
        //init Foundation scripts first
        $(document).foundation();
        
        //build navigation array
        this.initNav();
        
        this.parent(data);
        
        //no edited Test
        this.setEditedTest(null);
        
        //pull data from server
        this.load();
    },
    
    /**
     * Set array of main menu items
     */ 
    initNav: function(){
        this.mainNav = [
            { 
                //menu label
                name: 'All Tests', 
                
                //id of an document section
                id: 'test-all', 
                
                //is position visible in menu?
                visible: true, 
                
                //callback action after user selects that menu item
                callback: $.proxy(this.showTests, this) 
            },
            { name: 'Add Test', id: 'test-edit', visible: true, callback: $.proxy(this.addTest, this) }
        ];
    },
    
    /**
     * Add subscribers to knockout.js observabled properties
     */ 
    initSubscribed: function(){
        
        //makes sure after AJAX request is finished to init datepickers
        this.isLoading.subscribe($.proxy(this.onLoadingFinished, this));
        
        this.selectedMainNav.subscribe($.proxy(this.onViewChange, this));
    },
    
    /**
     * When initial data has been loaded successfully
     */ 
    onLoadSuccess: function(response){
        //update 'alliances' array with mapped objects
        this.update('alliances', response.Alliances);
                
        //TO DO                
                
        this.isLoaded(true);
    },
    
    /**
     * Pulls initial data from server
     * @see test-all.json
     */ 
    load: function(){
        this.isLoading(true);
        
        $.getJSON(_3.utils.getJsonUrl('test-all.json'), $.proxy(function(response){
            
            if(response.Success){
                
                this.onLoadSuccess(response);
                
            }else{
                
                this.addAlert(response.Message);
            
            }
            
            this.isLoading(false);  
        }, this));  
    },
    
    /**
     * Sets single Test in 'edit' mode
     * @param test
     * @see tests
     */ 
    setEditedTest: function(test){
        if(test) this.editedTest(test); //set defined current Test
        else if(test === null || test === false) this.editedTest(null); //no Test is edited
        else {
            //create new Test and open to edit
            
            this.update('editedTest');
            this.editedTest().isNew(true);
        }
    },
    
    /**
     * Creates new Test object and opens edit section
     */ 
    addTest: function(){
        this.setEditedTest();
        this.selectedMainNav('test-edit');
    },
    
    /**
     * Opens Test grid section and drops edited Test
     */ 
    cancelTestEdit: function(){
        this.selectedMainNav('test-all');
        this.setEditedTest(null);
    },
    
    /**
     * Adds currently edited Test to tests array, finishes edit mode
     */ 
    saveTest: function(){
        if(this.tests.indexOf(this.editedTest()) == -1){
            this.tests.push(this.editedTest());    
        }
        this.cancelTestEdit();
    },
    
    /**
     * Opens Test edit section for existing Test
     */ 
    editTest: function(test){
        this.setEditedTest(test);
        this.selectedMainNav('test-edit');
    },
    
    /**
     * Opens section with Tests grid
     */ 
    showTests: function(){
        this.selectedMainNav('test-all');
    },
    
    onLoadingFinished: function(){
        if(this.isLoading() == false){
            this.initDatepickers()
        }
    },
    
    /**
     * Inits datepicker widget
     * @see foundation-datepicker.js
     */
    initDatepickers: function(){
        if(this.selectedMainNav() == 'test-edit'){
            
            //init only when Test edit section is opened
            
            $('.date-input').fdatepicker({
                format: 'mm/dd/yyyy',
                weekStart: 1
            });   
        }
    },
    
    onViewChange: function(){
        this.showValidation(false);  
        this.initDatepickers();
    },
    
    /**
     * Prompts user to confirm he wants to delete Test
     */ 
    removeTest: function(){
        $('#remove-test-modal').foundation('reveal', 'open');
    },
    
    /**
     * Closes remove Test modal window
     */ 
    cancelRemoveRule: function(){
        $('#remove-test-modal').foundation('reveal', 'close');
    },
    
    addAlert: function(message){
        this.alerts.push(new _3.Alert({ message: message }));
    },
    
    removeAlert: function(alert){
        this.alerts.remove(alert);
    },
    
    /**
     * Show validation alerts and focus on first invalid field
     */ 
    validate: function(){
        this.showValidation(true);
        
        var invalid = $('.validated-field.error:visible').first();
        if(invalid && invalid.size() > 0) invalid.find('input, textarea, select').first().focus();
    }
});

//set as globally accessable
_3.view = new _3.TestViewModel();