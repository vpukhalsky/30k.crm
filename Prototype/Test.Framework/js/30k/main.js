/**
 * Utils class
 * @author Artur Polkowski
 */ 

//30K class namespace
_3 = {};

_3.Utils = Class.extend({
    
    /**
     * For testing purpose
     */ 
    SOME_REQESTS_FAIL: false,
    
    /**
     * Ratio of failed requests
     */ 
    failRatio: 0.5,
    
    /**
     * Path to JSON folder
     * for prototype use only
     */ 
    jsonPath: '/json',
    
    /**
     * JSON subfolder with failed requests
     */ 
    failPath: '/fail',
    
    /**
     * Returns correct url of requested JSON file
     * @param string file
     * @return string URL
     */ 
    getJsonUrl: function(file){
        var parts = location.href.split('/');
        parts.splice(parts.length - 1, 1);
        
        var fail = Math.random() > this.failRatio ? false : this.SOME_REQESTS_FAIL;
        
        return parts.join('/') + this.jsonPath + (fail ? this.failPath : '') + '/'+ file;
    },
    
    toUpperCase: function(s){
        return s.toUpperCase();
    },
    
    /**
     * Simple filter to capitalize first letter of every word in string
     */ 
    toCapitalCase: function(s){
        var words = s.split(' ');
        
        for(var i = 0 ; i < words.length ; i++){
            words[i] = words[i].substr(0, 1).toUpperCase() + words[i].substr(1);
        }
        
        return words.join(' ');
    },
    
    getAbbr: function(phrase){
        var result = '';
        var words = phrase.split(' ');
        
        for(var i = 0; i < words.length ; i++){
            result += words[i].charAt(0).toUpperCase();
        }
        
        return result;
    }
});

$.extend(_3, {
    utils: new _3.Utils()
});

