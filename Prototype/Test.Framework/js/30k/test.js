/**
 * Test class
 * @author Artur Polkowski
 * 
 */ 
_3.Test = KClass.extend({
    
    /**
     * Test unique id
     * @type int
     */ 
    Id: null,
    
    observed: {
        
        /**
         * True if Test has never been submited to server
         */ 
        isNew: false,
        
        /**
         * Comments for Test, optional
         * @type string
         */ 
        Comments: ''
    },
    
    computed: {
        
        /**
         * Is Test valid to save?
         */ 
        isValid: function(){
            //TO DO
            return true;
        }
    },
    
    mapped: {
        
        //property -> class
        
    },
    
    /**
     * Properties not included in save Test JSON
     */ 
    filtered: [
        'observed',
        'computed',
        'mapped',
        'filtered',
        'IsValid'
    ],

    /**
     * Edit test
     */ 
    edit: function(){
        _3.view.editTest(this);
    },

    /**
     * Cancel program edition
     */ 
    cancelEdit: function(){
        
        //reload previous state
        if(!this.isNew()) this.load($.proxy(this.onCancelEditSuccess, this));
        else this.onCancelEditSuccess();

    },
    
    onCancelEditSuccess: function(){
        //switch to another view
        _3.view.cancelTestEdit();   
    },

    /**
     * Push Test data to server
     */ 
    save: function(callback){
        if(!this.isValid()){
            _3.view.validate(true);
            return;
        }
        
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl(
            this.isNew() ? 'test-add.json' : 'test-update.json'
        );
        
        var data = ko.toJSON(this, this.jsonFilter);
        
        $.post(url, data, $.proxy(function(response){
            if(response.Success){

                this.isNew(false);
                
                _3.view.saveTest();
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);
        }, this));  
    },
    
    /**
     * Pull Test data from server
     */ 
    load: function(callback){
        _3.view.isLoading(true);
        
        $.get(_3.utils.getJsonUrl('test-one.json'), {
           Id: this.Id
        }, $.proxy(function(response){
            
            if(response.Success){
                
                //overwrite properties with new data
                this.ext(response.Test);
                
                this.isLoaded(true);
                
                if($.isFunction(callback)) callback();
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);   
        }, this));  
    }
});
