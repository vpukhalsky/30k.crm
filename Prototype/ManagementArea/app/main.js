'use strict';

/* Controllers */

angular.module('30k.ma')

.controller('MainController', ['$scope', function($scope){
    
    $scope.getCurrentYear = function(){
        var now = new Date();
        return now.getFullYear();
    };
    
}]);