'use strict';

// Declare app level module which depends on filters, and services

angular.module('30k.ma', [
  
  //'ngSanitize',
  'mm.foundation',
  //'datePicker',
  //'checklist-model',
  
  '30k.routes',
  '30k.loader',
  '30k.alerts',
   
  '30k.ma.account',
  '30k.ma.user',
  '30k.ma.access'
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
    
    newUser: {
      path: '/users/new',
      controller: 'UserEditController',
      templateUrl: 'user/edit/edit.html'
    },
    
    editUser: {
      path: '/users/edit/:id',
      controller: 'UserEditController',
      templateUrl: 'user/edit/edit.html'
    },
    
    users: {
      path: '/users',
      controller: 'UserCollectionController',
      templateUrl: 'user/collection/collection.html'
    },
    
    account: {
      path: '/account',
      controller: 'AccountMainController',
      templateUrl: 'account/main/main.html'
    },
    
    newRole: {
      path: '/roles/new',
      controller: 'RoleEditController',
      templateUrl: 'access/edit/edit.html'
    },
    
    editRole: {
      path: '/roles/edit/:id',
      controller: 'RoleEditController',
      templateUrl: 'access/edit/edit.html'
    },
    
    roles: {
      path: '/roles',
      controller: 'RoleCollectionController',
      templateUrl: 'access/roles/roles.html'
    },
    
    main: {
      path: '/roles',
      controller: 'RoleCollectionController',
      templateUrl: 'access/roles/roles.html'
    }
    
  });
  
}]);
