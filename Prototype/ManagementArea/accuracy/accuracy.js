'use strict';

angular.module('30k.ma.accuracy', [
    
    'mm.foundation',
    'checklist-model',
    
    '30k.error',
    '30k.service',
    '30k.routes',
    
    '30k.ma'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
      
    newProcess: {
      path: '/processes/new',
      controller: 'ProcessEditController',
      templateUrl: 'accuracy/edit/edit.html'
    },
    
    editProcess: {
      path: '/processes/edit/:id',
      controller: 'ProcessEditController',
      templateUrl: 'accuracy/edit/edit.html'
    },
    
    accuracy: {
      path: '/processes',
      controller: 'ProcessCollectionController',
      templateUrl: 'accuracy/processes/processes.html'
    },
    
    validators: {
      path: '/validators',
      controller: 'ValidatorCollectionController',
      templateUrl: 'accuracy/validators/validators.html'
    },
    
    main: {
      path: '/processes',
      controller: 'ProcessCollectionController',
      templateUrl: 'accuracy/processes/processes.html'
    }
    
  });
  
}])

.service('accuracyService', ['kkService', '$filter', '$rootScope', function(kkService, $filter, $rootScope){
    
    var config = null;
    
    function getConfig(){
        
        if(config) return kkService.promise(config);
        else return kkService.sendGet('accuracy/config.json').then(function(response){
            config = response;
            return config;
        });
        
    }
    
    function run(items){
        
        var idleItems = $filter('filter')(items, { status: 'idle' });
        
        if(idleItems.length <= 0){
            
            return getConfig().then(function(response){
                $rootScope.$emit('kk-alert', { alert: { type: 'alert', msg: response.alreadyRunningError }});
                return kkService.promise([]);
            });
            
        }else{
            
            var ids = [];
            idleItems.forEach(function(item){
               ids.push(item.id);
            });
            
            return kkService.sendPost('accuracy/run.json', { items: ids }).then(function(result){
                return result.items;
            });
            
        }
    }
    
    return{
        getConfig: getConfig,
        run: run
    };
    
}]);