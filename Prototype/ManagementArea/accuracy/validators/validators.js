'use strict';

angular.module('30k.ma.accuracy')

.controller('ValidatorCollectionController', ['$scope', 'collectionService', 'kkRoutesService', '$timeout', function($scope, collectionService, kkRoutesService, $timeout){
    
    kkRoutesService.preventReload($scope, 'ValidatorCollectionController');
    
    var timeout = null;
    var delay = 600;
    
    $scope.items = null;
    $scope.config = null;
    
    $scope.filter = {
        type: 'all',
        ffp: null
    };
    
    $scope.delayedSubmit = function(form){
        
        if(timeout){
            $timeout.cancel(timeout);
            timeout = null;
        }
        
        timeout = $timeout(function(){
            $scope.submit(form);
        }, delay);
        
    }
    
    $scope.submit = function(form){
        
        if(!form.$valid) return false;
        
        collectionService.getPagedCollection('validators/get-validators.json', 'validator', $scope.filter).then(function(response){
            
            $scope.items.splice(0, $scope.items.length);
            
            if(angular.isArray(response.items)){
                for(var i = 0 ; i < response.items.length ; i++) $scope.items.push(response.items[i]);
            }
            
        });
        
    };
    
    $scope.reset = function(form){
        
        $scope.filter.type = 'all';
        $scope.filter.ffp = null;
        $scope.submit(form);
        
    };
    
    function load(){
        
        collectionService.getConfig('validators/config.json', 'validator').then(function(response){
            $scope.config = response;
            $scope.filterRuleTypeClass = 'even-' + (response.ruleTypes.length + 1);
        });
        
        collectionService.getCollection('validators/get-validators.json', 'validator').then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);