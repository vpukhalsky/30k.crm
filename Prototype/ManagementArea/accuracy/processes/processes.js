'use strict';

angular.module('30k.ma.accuracy')

.controller('ProcessCollectionController', ['$scope', 'collectionService', 'accuracyService', 'findFilter', 'kkRoutesService', 'filterFilter', function($scope, collectionService, accuracyService, findFilter, kkRoutesService, filterFilter){
    
    kkRoutesService.preventReload($scope, 'ProcessCollectionController');
    
    $scope.items = null;
    $scope.config = null;
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.run = function(){
        accuracyService.run($scope.selected.items).then(function(items){
            
            items.forEach(function(i){
                var item = findFilter($scope.items, i.id);
                if(item) angular.extend(item, i);
            });
            
        });
    };
    
    $scope.$watchCollection('selected.items', function(newValue){
        if(!angular.isArray($scope.items)) return;
        if(newValue.length >= $scope.items.length) $scope.selected.allNone = 'all';
        else $scope.selected.allNone = 'none';
    });
    
    $scope.isRunDisabled = function(){
        var item = findFilter($scope.selected.items, 'idle', 'status');
        return !angular.isObject(item);
    };
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.initItem = function(scope, item){
        scope.ffp = findFilter($scope.config.ffps, item.ffp.id);
    };
    
    $scope.getStatusName = function(item){
        var status = findFilter($scope.config.statuses, item.status, 'code');
        return status.name;
    };
    
    $scope.deleteProcess = function(process){
        collectionService.deleteItem('accuracy/delete-process.json', 'process', process);
    };
    
    function load(){
        
        accuracyService.getConfig().then(function(config){
            $scope.config = angular.copy(config);
        });
        
        collectionService.getCollection('accuracy/get-processes.json', 'process').then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);