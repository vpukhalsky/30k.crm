'use strict';

angular.module('30k.ma.accuracy')

.controller('ProcessEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'accuracyService', 'findFilter', function($scope, collectionService, kkRoutesService, $routeParams, accuracyService, findFilter){
    
    kkRoutesService.preventReload($scope, 'ProcessEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.config = null;
    $scope.tierModel = null;
    
    $scope.airportCodePattern = /^[a-zA-Z]{3}$/;
    $scope.airlineCodePattern = /^[a-zA-Z][a-zA-Z0-9]$/;
    
    $scope.$watchCollection('item.routes', function(newValue){
        $scope.itemForm.$setValidity('min-routes', angular.isArray(newValue) && newValue.length > 0);
    });
    
    $scope.$watchCollection('item.ffp.tierIds', function(newValue){
         var valid = angular.isArray(newValue) && newValue.length > 0;
         if($scope.itemForm.tiers) {
             $scope.itemForm.tiers.$setValidity('requiredx', valid);
         }
    });
    
    $scope.onProgramChange = function(){
        $scope.item.ffp.tierIds = [];
    };
    
    $scope.addRoute = function(){
        $scope.item.routes.push({});
    };
    
    $scope.removeRoute = function(index){
        $scope.item.routes.splice(index, 1);
    };
    
    $scope.selectAllTiers = function(){
        
        $scope.deselectTiers();
        
        var ffp = findFilter($scope.config.ffps, $scope.item.ffp.id);
        ffp.tiers.forEach(function(tier){
            $scope.item.ffp.tierIds.push(tier.id);
        });
        
    };
    
    $scope.deselectTiers = function(){
        while($scope.item.ffp.tierIds.length > 0) $scope.item.ffp.tierIds.pop();
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('accuracy');
        });
        
    };
    
    function reset(){
        
        $scope.item = {
            ffp: {
                tierIds: []
            },
            routes: [{}]
        };
        
        $scope.isNew = true;
    }
    
    function stringToFareCodes(){
        var item = angular.copy($scope.item);
        item.ffp.fareCodes = item.ffp.fareCodes.toUpperCase().split('');
        if(angular.isArray(item.routes)){
            item.routes.forEach(function(route){
                 if(angular.isString(route.fareCodes)) route.fareCodes = route.fareCodes.toUpperCase().split('');
            });
        }
        
        return item;
    }
    
    function submit(callback){
        var item = stringToFareCodes();
        collectionService.saveItem('accuracy/save-process.json', 'process', item).then(callback);
    }
    
    $scope.deleteProcess = function(item){
        collectionService.deleteItem('accuracy/delete-process.json', 'process', item).then(function(response){
            if(response) kkRoutesService.redirect('accuracy');
        });
    };
    
    function fareCodesToString(){
        
        $scope.item.ffp.fareCodes = $scope.item.ffp.fareCodes.join('').toUpperCase();
        
        if(angular.isArray($scope.item.routes)){
            $scope.item.routes.forEach(function(route){
                route.fareCodes = route.fareCodes.join('').toUpperCase();
            });
        }
        
    }

    function load(){
        
        accuracyService.getConfig().then(function(config){
            $scope.config = angular.copy(config);
        });
        
        if(angular.isDefined($routeParams.id)){
            
            var id = parseInt($routeParams.id, 10);
            
            collectionService.getItem('accuracy/get-process.json?v=1', 'process', id).then(function(item){
                $scope.item = angular.copy(item);
                $scope.isNew = false;
                fareCodesToString();
            });
            
        }else{
            reset();
        }
    }
    
    load();
    
}]);