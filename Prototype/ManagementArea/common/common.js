'use strict';

// Declare app level module which depends on filters, and services

angular.module('30k.ma', [
  
  'mm.foundation',
  
  '30k.user.filters',
  '30k.routes',
  '30k.loader',
  '30k.admin-review'
  
])

.controller('MainController', ['$scope', '$interval', '$timeout', 'kkService', '$filter', 'adminReviewConfig', function($scope, $interval, $timeout, kkService, $filter, adminReviewConfig){
    
    $scope.config = null;
    $scope.timeLeftToLogout = '0s';
    
    $scope.currentEnv = null;
    
    var idleInterval = null;
    var idleCounter = 0;
    
    /* Method used only for prototype development purpose. Remove in real env. */
    $scope.isAdminLoggedIn = function(){
        return adminReviewConfig.isEnabled();
    },
    
    $scope.getCurrentYear = function(){
        var now = new Date();
        return now.getFullYear();
    };
    
    function logout(){
        document.location = $scope.config.session.logoutUrl;
    }
    
    function updateIdleInterval(){
        
        idleCounter++;
        
        var remain = $scope.config.session.idleTimeout - idleCounter;
        if(remain < 0) remain = 0;
        
        var h = Math.floor(remain / 3600);
        var m = Math.floor((remain - h * 3600) / 60);
        var s = remain % 60;
        
        $scope.timeLeftToLogout = '';
        if(h > 0) $scope.timeLeftToLogout = h + 'h';
        if(m > 0) $scope.timeLeftToLogout += m + 'm';
        $scope.timeLeftToLogout += s + 's';
        
        if(remain <= 0) logout();
    }
    
    function updateIdleTimeout(){
        idleCounter = 0;
    }
    
    function initIdleTimeout(){
    
        idleInterval = $interval(updateIdleInterval, 1000);
        angular.element(document).on('click', updateIdleTimeout);
        
        $scope.$on('$destroy', function(){
          if(idleInterval) $timeout.cancel(idleInterval);
          angular.element(document).off('click', updateIdleTimeout);
        });
    
    }
    
    function selectCurrentEnv(){
        
        $scope.currentEnv = $filter('find')($scope.config.envs, $scope.config.currentEnv, 'code');

    }
    
    kkService.sendGet('common/config.json').then(function(c){
        
        $scope.config = angular.copy(c);
        initIdleTimeout();
        selectCurrentEnv();
    
    });
    
}]);