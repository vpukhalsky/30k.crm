'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'applyToAll', 'kkService', function($scope, collectionService, kkRoutesService, applyToAll, kkService){
    
    kkRoutesService.preventReload($scope, 'FfpCollectionController');
    
    $scope.items = null;
    $scope.config = null;
    
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.runRulesValidation = function(){
        
        var ids = $scope.selected.items.map(function(i){ return i.id; });
        
        kkService.sendPost('ffp/run.json', {
            ffpIds: ids
        }).then(function(){
            $scope.selectNone();
        });
        
    };
    
    $scope.deleteFfp = function(ffp){
        collectionService.deleteItem('ffp/delete-ffp.json', 'ffp', ffp);
    };
    
    function load(){
        
        applyToAll.reset();
        
        collectionService.getConfig('ffp/config.json', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
        collectionService.getCollection('ffp/get-ffps.json?v=1', 'ffp').then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);