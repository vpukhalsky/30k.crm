'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpUpgradeCollectionController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', 'applyToAll', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, applyToAll){
     
    kkRoutesService.preventReload($scope, 'FfpUpgradeCollectionController');
    
    $scope.items = null;
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.brandedFares = [];
    
    $scope.paged = {
        page: 1,
        totalItems: null,
        itemsPerPage: 50
    };
    
    $scope.$watchCollection('items', function(items){
        
        if(angular.isArray(items) && items.length > 0) loadBrandedFares(items);
        
    });
    
	$scope.areAllTiersIncluded = function (tiers) {
        return $scope.getAppliedTiers(tiers).length == $scope.ffp.tiers.length;
    };

    $scope.getAppliedTiers = function (tiers) {
        var appliedTiers = [];

        for (var i = 0; i < $scope.ffp.tiers.length; i++) {

            var ffpTier = $scope.ffp.tiers[i];

            for (var j = 0; j < tiers.length; j++) {

                if (ffpTier.id == tiers[j]) {
                    appliedTiers.push(ffpTier);
                    break;
                }
            }
        }

        return appliedTiers;
    };
	
    $scope.initItemRow = function(item, scope){
        
        scope.rules = $scope.getRules(item);
        
    };
    
    $scope.getRules = function(item){
        
        var rules = [];
        var find = $filter('find');
        
        item.rules.forEach(function(group){
            
            var season = null;
            var redemption = find($scope.ffp.redemptions, item.name);
            if(redemption) season = find(redemption.seasons, group.season);
            var first = true;
            
            group.items.forEach(function(rule){
                
                var temp = angular.copy(rule);
                if(first) temp.name = season.name;
                rules.push(temp);
                
            });
            
        });
        
        return rules;
          
    };
    
    $scope.getRuleIdClass = function(rule){
        
        var cls = [];
        
        var fromdates = [];
        var tilldates = [];
        
        var past = 0;
        var future = 0;

        var now = new Date();
        
        if(rule.purchase){
            if(rule.purchase.from) fromdates.push(new Date(rule.purchase.from));
            if(rule.purchase.until) tilldates.push(new Date(rule.purchase.until));
        }

        if(rule.flight){
            if(rule.flight.from) fromdates.push(new Date(rule.flight.from));
            if(rule.flight.until) tilldates.push(new Date(rule.flight.until));
        }

        fromdates.forEach(function(date){
            if (date.valueOf() > now.valueOf()) future++;
        });
            
        if (fromdates.length > 0 && fromdates.length == future) {
            cls.push('future');
            return cls;
        }
        
        tilldates.forEach(function (date) {
            if (date.valueOf() <= now.valueOf()) past++;
        });

        if (tilldates.length > 0 && tilldates.length == past) {
            cls.push('past');
            return cls;
        }
      
        if (tilldates.length > 0 || fromdates.length > 0) {
            cls.push('now');
        }
        
        return cls;
    };
    
    $scope.getRuleOneWay = function(rules, minMax){
        
        var factor = {
            count: 0,
            min: Number.MAX_VALUE,
            max: Number.MIN_VALUE
        };
        
        var fixed = {
            count: 0,
            min: Number.MAX_VALUE,
            max: Number.MIN_VALUE
        };
        
        rules.forEach(function(rule){
            
            if(angular.isNumber(rule.factor) && rule.factor > 0){
                factor.count++;
                if(factor.min > rule.factor) factor.min = rule.factor;
                if(factor.max < rule.factor) factor.max = rule.factor;
            }
            
            if(angular.isNumber(rule.oneWay) && rule.oneWay > 0){
                fixed.count++;
                if(fixed.min > rule.oneWay) fixed.min = rule.oneWay;
                if(fixed.max < rule.oneWay) fixed.max = rule.oneWay;
            }
            
        });
        
        if(fixed.count > 0){
            return minMax == 'min' ? fixed.min : fixed.max;
        }else{
            return minMax == 'min' ? factor.min : factor.max;
        }
        
    };
    
    $scope.areCodeshareRulesDirty = function(item){
        
        return !(item.marketingRules.length == 1 && item.operatingRules.length == 1 
        && item.marketingRules.indexOf('own') >= 0 && item.operatingRules.indexOf('own') >= 0);
          
    };
    
    $scope.initCodeshareTooltipData = function(scope){
        
        scope.codeshareTooltipData = {
            rule: scope.item,
            config: scope.config
        };
        
    };
    
    $scope.expand = function(item){
        item.expanded = true;
    };
    
    $scope.collapse = function(item){
        item.expanded = false;
    };
    
    $scope.onSelectPage = function(page){
        
        $scope.paged.page = page;
        loadItems($scope.ffp.id);
        
    };
    
    $scope.onItemsPerPageChange = function(){
        loadItems($scope.ffp.id);
    };
    
    $scope.$watchCollection('selected.items', function(newValue){
        
        if(!angular.isArray($scope.items)) return;
        if(newValue.length >= $scope.items.length) $scope.selected.allNone = 'all';
        else $scope.selected.allNone = 'none';
        
    });
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem('upgrade/delete-upgrade.json', 'upgrade', item);
    };
    
    $scope.deleteItems = function(){
        
        var items = $scope.selected.items;
        collectionService.deleteItems('upgrade/delete-upgrades.json', 'upgrade', items);
         
    };
    
    $scope.cloneItem = function(index){
        
        var id = $scope.items[index].id;
        var clone = angular.copy($scope.items[index]);
        delete clone.id;
        
        collectionService.saveItem('upgrade/clone-upgrade.json', 'upgrade', clone).then(function(response){
            
            replaceClone(id, response.item);
            
        });
        
    };
    
    function replaceClone(originalId, clone){
        
        var indexOf = $filter('indexOf');
        var cloneIndex = indexOf($scope.items, clone.id);
        $scope.items.splice(cloneIndex, 1);
        var originalIndex = indexOf($scope.items, originalId);
        $scope.items.splice(originalIndex + 1, 0, clone);
        
    }
    
    $scope.cloneItems = function(){
        
        var ids = [];
        
        $scope.selected.items.forEach(function(item){
            ids.push(item.id);
        });
        
        var clones = angular.copy($scope.selected.items);
        
        clones.forEach(function(clone){
            delete clone.id;
        });
        
        collectionService.saveItems('upgrade/clone-upgrades.json', 'upgrade', clones).then(function(response){
            
            for(var i = 0 ; i < response.items.length ; i++){
                if(ids[i]) replaceClone(ids[i], response.items[i]);
            }
            
        });
    };
    
    function loadItems(ffpId){
        
        var params = angular.copy($scope.paged);
        params.ffpId = ffpId;
        delete params.totalItems;
        
        collectionService.getPagedCollection('upgrade/get-upgrades.json', 'upgrade', params).then(function(response){
            
            $scope.items = response.items;
            $scope.paged.page = response.page;
            $scope.paged.totalItems = response.totalItems;
            
            ffpCmsHelper.setUpgradesCache({
                request: params,
                response: response
            });
            
        });
        
    }
    
    function loadParamsFromCache(){
        
        var cache = ffpCmsHelper.getUpgradesCache();
        
        if(angular.isObject(cache)){
            $scope.paged.page = cache.request.page;
            $scope.paged.itemsPerPage = cache.request.itemsPerPage;
        }
        
    }
    
    function loadBrandedFares(rules){
        
        ffpCmsHelper.loadBrandedFares(rules).then(function(response){
            
            $scope.brandedFares.splice(0, $scope.brandedFares.length);
            
            for(var i = 0 ; i < response.length ; i++){
                $scope.brandedFares.push(response[i]);
            }
            
        });
        
    }
    
    function load(){
        
        applyToAll.reset();
        
        var ffpId = null;
        
        if(angular.isDefined($routeParams.ffpid)){
            
            ffpId = parseInt($routeParams.ffpid, 10);
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                $scope.ffp = angular.copy(ffp);
            });
            
            ffpCmsHelper.getUpgradesConfig(ffpId).then(function(config){
                angular.extend($scope.config, config);
            });
            
        }else{
            kkRoutesService.redirect('ffps');
        }
        
        collectionService.getConfig('ffp/config.json?v=1', 'ffp').then(function(config){
            
            angular.extend($scope.config, config);
            $scope.paged.itemsPerPage = $scope.config.pagination.default;
            
            loadParamsFromCache();
            loadItems(ffpId);
            
        });
        
    }
    
    load();
    
}]);