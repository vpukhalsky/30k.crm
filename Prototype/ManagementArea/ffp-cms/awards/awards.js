'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpAwardCollectionController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', 'applyToAll', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, applyToAll){
     
    kkRoutesService.preventReload($scope, 'FfpAwardCollectionController');
    
    $scope.items = null;
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.paged = {
        page: 1,
        totalItems: null,
        itemsPerPage: 50
    };
    
    $scope.areCodeshareRulesDirty = function(item){
        
        return !(item.marketingRules.length == 1 && item.operatingRules.length == 1 
        && item.marketingRules.indexOf('own') >= 0 && item.operatingRules.indexOf('own') >= 0);
          
    };
    
    $scope.expand = function(item){
        item.expanded = true;
    };
    
    $scope.collapse = function(item){
        item.expanded = false;
    };
    
    $scope.onSelectPage = function(page){
        
        $scope.paged.page = page;
        loadItems($scope.ffp.id);
        
    };
    
    $scope.onItemsPerPageChange = function(){
        loadItems($scope.ffp.id);
    };
    
    $scope.$watchCollection('selected.items', function(newValue){
        
        if(!angular.isArray($scope.items)) return;
        if(newValue.length >= $scope.items.length) $scope.selected.allNone = 'all';
        else $scope.selected.allNone = 'none';
        
    });
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem('award/delete-award.json', 'award', item);
    };
    
    $scope.deleteItems = function(){
        
        var items = $scope.selected.items;
        collectionService.deleteItems('award/delete-awards.json', 'award', items);
         
    };
    
    $scope.cloneItem = function(index){
        
        var id = $scope.items[index].id;
        var clone = angular.copy($scope.items[index]);
        delete clone.id;
        
        collectionService.saveItem('award/clone-award.json', 'award', clone).then(function(response){
            
            replaceClone(id, response.item);
            
        });
        
    };
    
    function replaceClone(originalId, clone){
        
        var indexOf = $filter('indexOf');
        var cloneIndex = indexOf($scope.items, clone.id);
        $scope.items.splice(cloneIndex, 1);
        var originalIndex = indexOf($scope.items, originalId);
        $scope.items.splice(originalIndex + 1, 0, clone);
        
    }
    
    $scope.cloneItems = function(){
        
        var ids = [];
        
        $scope.selected.items.forEach(function(item){
            ids.push(item.id);
        });
        
        var clones = angular.copy($scope.selected.items);
        
        clones.forEach(function(clone){
            delete clone.id;
        });
        
        collectionService.saveItems('award/clone-awards.json', 'award', clones).then(function(response){
            
            for(var i = 0 ; i < response.items.length ; i++){
                if(ids[i]) replaceClone(ids[i], response.items[i]);
            }
            
        });
    };
    
    function loadItems(ffpId){
        
        var params = angular.copy($scope.paged);
        params.ffpId = ffpId;
        delete params.totalItems;
        
        collectionService.getPagedCollection('award/get-awards.json', 'award', params).then(function(response){
            
            $scope.items = response.items;
            $scope.paged.page = response.page;
            $scope.paged.totalItems = response.totalItems;
            
            ffpCmsHelper.setAwardsCache({
                request: params,
                response: response
            });
            
        });
        
    }
    
    function loadParamsFromCache(){
        
        var cache = ffpCmsHelper.getAwardsCache();
        
        if(angular.isObject(cache)){
            $scope.paged.page = cache.request.page;
            $scope.paged.itemsPerPage = cache.request.itemsPerPage;
        }
        
    }
    
    function load(){
        
        applyToAll.reset();
        
        var ffpId = null;
        
        if(angular.isDefined($routeParams.ffpid)){
            
            ffpId = parseInt($routeParams.ffpid, 10);
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                $scope.ffp = angular.copy(ffp);
            });
            
            ffpCmsHelper.getAwardsConfig(ffpId).then(function(config){
                angular.extend($scope.config, config);
            });
            
        }else{
            kkRoutesService.redirect('ffps');
        }
        
        collectionService.getConfig('ffp/config.json?v=1', 'ffp').then(function(config){
            
            angular.extend($scope.config, config);
            $scope.paged.itemsPerPage = $scope.config.pagination.default;
            
            loadParamsFromCache();
            loadItems(ffpId);
            
        });
        
    }
    
    load();
    
}]);