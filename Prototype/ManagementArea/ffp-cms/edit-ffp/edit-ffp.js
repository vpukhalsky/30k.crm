'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'ffpCmsHelper', '$modal', '$filter', 'applyToAll', '$timeout', 'kkService', function($scope, collectionService, kkRoutesService, $routeParams, ffpCmsHelper, $modal, $filter, applyToAll, $timeout, kkService){
    
    kkRoutesService.preventReload($scope, 'FfpEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.config = null;
    
    $scope.validTiers = null;
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    
    $scope.$watchCollection('item.tiers', function(newValue){
        if(!angular.isArray(newValue) || newValue.length <= 0) $scope.validTiers = null;
        else $scope.validTiers = newValue.length;
    });
    
    $scope.newAccrualsExist = function(){
        
        var exists = false;
        
        if($scope.item){
            $scope.item.accruals.forEach(function(a){
                if(!angular.isNumber(a.id)) exists = true;
                return false;
            });
        }
        
        return exists;
        
    };
    
    $scope.getButtonGroupClass = function(buttons, modificator){
        
        return ffpCmsHelper.getButtonGroupClass(buttons, modificator);
        
    };
    
    $scope.initAccrualTooltipData = function(scope){
        
        ffpCmsHelper.initAccrualTooltipData(scope, $scope.config);
        
    };
    
    $scope.getAccrualAbbr = function(accrual){

        return ffpCmsHelper.getAccrualAbbr(accrual, $scope.config);
        
    };
    
    function editTier(tier, isNew){
        
        $modal.open({
           controller: 'FfpTierEditController',
           templateUrl: 'ffp-cms/edit-ffp/edit-tier.html?v=1',
           resolve: {
               tier: function(){ return tier; },
               isNew: function(){ return isNew; },
               ffp: function(){ return $scope.item; }
           }
        }).result.then(function(t){
            
            if(angular.isObject(t)){
                
                angular.extend(tier, t);
                if(isNew) $scope.item.tiers.push(tier);  
                else{
                    var index = $filter('indexOf')($scope.item.tiers, tier.id);
                    if(index >= 0) $scope.item.tiers.splice(index, 1, tier);
                }
                
            }else if(!isNew){
                var index = $filter('indexOf')($scope.item.tiers, tier.id);
                if(index >= 0) $scope.item.tiers.splice(index, 1);
            }
            
        });
        
    }
    
    $scope.addTier = function(){
        var tier = ffpCmsHelper.setupTier(angular.copy($scope.config.defaultTier), $scope.item.accruals);
        editTier(tier, true);
    };
    
    $scope.editTier = function(tier){
        editTier(angular.copy(tier));
    };
    
    $scope.removeTier = function(index){
        $scope.item.tiers.splice(index, 1);
    };
    
    function editRedemption(redemption, isNew){
        
        $modal.open({
           controller: 'FfpRedemptionModalEditController',
           templateUrl: 'ffp-cms/edit-ffp/edit-redemption.html?v=1',
           windowClass: 'small edit-redemption-modal',
           resolve: {
               redemption: function(){ return angular.copy(redemption); },
               isNew: function(){ return isNew; },
               ffp: function(){ return $scope.item; }
           }
        }).result.then(function(t){
            
            if(angular.isObject(t)){
                
                angular.extend(redemption, t);
                if(isNew) $scope.item.redemptions.push(redemption);  
                
            }
            
        });
        
    }
    
    $scope.addRedemption = function(){
        
        var red = angular.copy($scope.config.defaultFfpRedemption);
		red.programId = $scope.item.id;
        editRedemption(red, true);
        
    };
    
    $scope.editRedemption = function(redemption){
        
        editRedemption(redemption);
        
    };
    
    $scope.removeRedemption = function(id){
        
        var indexOf = $filter('indexOf');
        var indexToRemove = indexOf($scope.item.redemptions, id);

        $scope.item.redemptions.splice(indexToRemove, 1);
        
    };
    
    $scope.addAccrual = function(){
        $scope.item.accruals.push(angular.copy($scope.config.defaultAccrual));
    };
    
    $scope.removeAccrual = function(index){
        $scope.item.accruals.splice(index, 1);
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(response){
            
            if(response.configuredAccrualsChanged){
                collectionService.flushCollection('rule');
                collectionService.flushCollection('redemption');
            }
            
            kkRoutesService.redirect('ffps');
        });
        
    };
    
    $scope.onAccrualDateChange = function(accrual, event){
        
        $timeout(function(){
            if($(event.target).val() == '') accrual.effectiveDate = null;
        });
        
    };
    
    function reset(){
        
        $scope.item = angular.copy($scope.config.defaultFfp);
        $scope.isNew = true;
    }
    
    function submit(callback){
        
        var item = angular.copy($scope.item);
        collectionService.saveItem('ffp/save-ffp.json', 'ffp', toSave(item)).then(callback);
        
    }
    
    $scope.deleteFfp = function(item){
        collectionService.deleteItem('ffp/delete-ffp.json', 'ffp', item).then(function(response){
            if(response) kkRoutesService.redirect('ffps');
        });
    };
    
    $scope.updateAuthFormInputs = function(){
        
        kkService.sendGet('ffp/get-auth-form-inputs.json?program=:ffp', {
            ffp: $scope.item.id
        }).then(function(response){
            
            if(response && response.item) $.extend($scope.item, response.item);
            
        });
          
    };
    
    function toEdit(item){
        
        if(!angular.isArray(item.tiers)) item.tiers = [];
        
        item.tiers.forEach(function(tier){
             tier = ffpCmsHelper.setupTier(tier, item.accruals);
             
             tier.qualification.forEach(function(q){
                 if(angular.isArray(q.legend)) q.legend = q.legend.join("\n");
             });
             
             tier.renewal.forEach(function(r){
                 if(angular.isArray(r.legend)) r.legend = r.legend.join("\n");
             });
        });
        
        item.accruals.forEach(function(accrual){
           if(angular.isString(accrual.effectiveDate)) accrual.effectiveDate = new Date(accrual.effectiveDate);
        });
        
        if(angular.isArray(item.authFormInputs) && item.authFormInputs.length > 0){
            
            for(var i = 0 ; i < item.authFormInputs.length ; i++){
                
                var input = item.authFormInputs[i];
                
                if(!angular.isString(input.type)){
                    if(input.name == 'password') input.type = 'password';
                    else input.type = 'text';
                }
                
                if(!angular.isNumber(input.order)){
                    input.order = (item.authFormInputs.length - i) * 4;
                }
                
                if(angular.isArray(input.options)){
                    if(input.type != 'select') input.type = 'select';
                }
            }
            
        }
        
        return item;
    }
    
    function toSave(item){
        
        item.tiers.forEach(function(tier){
             
             tier = ffpCmsHelper.setupTier(tier, item.accruals);
             
             tier.qualification.forEach(function(q){
                 if(q.legend) q.legend = q.legend.split("\n");
             });
             
             tier.renewal.forEach(function(r){
                 if(r.legend) r.legend = r.legend.split("\n");
             });
        });
        
        var localDate = $filter('localDate');
        
        item.accruals.forEach(function(accrual){
           if(accrual.effectiveDate) accrual.effectiveDate = localDate(accrual.effectiveDate);
        });
        
        return item;
    }
    
    function load(){
        
        applyToAll.reset();
        
        var configPromise = collectionService.getConfig('ffp/config.json?v=1', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
        if(angular.isDefined($routeParams.id)){
            
            var id = parseInt($routeParams.id, 10);
            
            collectionService.getFullItem('ffp/get-ffp.json?v=1', 'ffp', id).then(function(item){
                $scope.item = toEdit(angular.copy(item));
                $scope.isNew = false;
            });
            
        }else{
            configPromise.then(function(){
                reset();
            });
        }
    }
    
    load();
    
}])

.controller('FfpRedemptionModalEditController', ['$scope', '$modalInstance', 'redemption', 'isNew', 'ffp', 'collectionService', function($scope, $modalInstance, redemption, isNew, ffp, collectionService){
    
    $scope.item = redemption;
    $scope.ffp = ffp;
    $scope.isNew = isNew;
    
    $scope.submit = function(form){
        
        if(form.$valid){
            $modalInstance.close($scope.item);
        }
        
    };
    
    $scope.cancel = function(){
        
        $modalInstance.dismiss('cancel');
        
    };
    
    $scope.addSeason = function(){
        
        $scope.item.seasons.push(angular.copy($scope.config.defaultRedemptionSeason)); 
        
    };
    
    $scope.removeSeason = function(index){
        
        $scope.item.seasons.splice(index, 1);
        
    };
    
    function init(){
        
        collectionService.getConfig('ffp/config.json?v=1', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
    }
    
    init();
    
}]);