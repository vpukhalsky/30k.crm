'use strict';

angular.module('30k.ma.ffp-cms')

.service('editBenefitService', ['$compile', 'collectionService', function($compile, collectionService){
    
    var placeholder = '#benefit-type-directive-placeholder';
    var template = '<kk-benefit-type-{{code}} item="item" config="config"></kk-benefit-type-{{code}}>';
    
    var childDirectives = 0;
    var submitted = [];
    var scope = null;
    var deregisterEvent = null;
    var submitPromise = null;
    
    function inject(benefitCode){
        
        var container = $(placeholder);
        
        if(!angular.isString(benefitCode) || container.size() <= 0 || !scope) return false;
        
        var tpl = template.split('{{code}}').join(benefitCode.toLowerCase());
        
        var raw = angular.element(tpl);
        
        container.children().each(function(index, el){
            
            var c = $(el);
            var s = c.isolateScope();
            if(s) s.$destroy();
            
        }).remove();
        
        $compile(raw)(scope, function(el, $scope){
        
            container.append(el);
        
        });
        
    }
    
    function init(s){
        
        childDirectives = 0;
        scope = s;
        
        if(angular.isFunction(deregisterEvent)) deregisterEvent();
        
        deregisterEvent = scope.$on('benefit-submit-response', onSubmit);
        
        
    }
    
    function register(){
        
        childDirectives++;
        
    }
    
    function completeSubmit(){
        
        var item = submitted.shift();
        
        for(var i = 0 ; i < submitted.length ; i++){
            
            $.extend(item, submitted[i]);
            
        }
        
        submitted = [];
        
        collectionService.saveItem('benefit-rule/save-benefit-rule.json', 'benefit-rule', item).then(function(response){
            
            submitPromise.resolve(item, response);
            submitPromise = null;
            
        }, function(){
            
            submitPromise.reject();
            submitPromise = null;
            
        });
        
    }
    
    function onSubmit(event, item){
        
        submitted.push(item);
        
        if(submitted.length - 1 >= childDirectives){
            
            completeSubmit();
            
        }
        
    }
    
    function submit(item){
        
        submitPromise = $.Deferred();
        submitted = [item];
        
        if(childDirectives > 0){
            
            scope.$broadcast('benefit-submit-request', item);
            
        }else{
            
            completeSubmit();
            
        }
        
        return submitPromise.promise();
        
    }
    
    return {
        inject: inject,
        init: init,
        register: register,
        submit: submit
    };
    
}]);