'use strict';

angular.module('30k.ma.ffp-cms')

.directive('kkBenefitTypeXtrbgg', ['editBenefitService', 'findFilter', function(editBenefitService, findFilter){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-benefit/directives/benefit-type-baggage.html',
        scope: {
            item: '=',
            config: '='
        },
        controller: 'BenefitTypeBaggageController'
    };
    
}])

.directive('kkBenefitTypeXtrcbn', ['editBenefitService', 'findFilter', function(editBenefitService, findFilter){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-benefit/directives/benefit-type-cabin-baggage.html',
        scope: {
            item: '=',
            config: '='
        },
        controller: 'BenefitTypeBaggageController'
    };
    
}])

.directive('kkBenefitTypeWvdbgf', ['editBenefitService', 'findFilter', function(editBenefitService, findFilter){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-benefit/directives/benefit-type-baggage.html',
        scope: {
            item: '=',
            config: '='
        },
        controller: 'BenefitTypeBaggageController'
    };
    
}])

.directive('kkBenefitTypeSpcbgg', ['editBenefitService', 'findFilter', function(editBenefitService, findFilter){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-benefit/directives/benefit-type-special-baggage.html',
        scope: {
            item: '=',
            config: '='
        },
        controller: 'BenefitTypeBaggageController'
    };
    
}])

.controller('BenefitTypeBaggageController', ['$scope', '$element', 'editBenefitService', 'findFilter', function($scope, $element, editBenefitService, findFilter){
    
    var deregisterEvent = null;
    
    $scope.onHasQuantityChange = function(hasQuantity){
        
        if(hasQuantity) $scope.hasTotalWeight = false;
        if(!hasQuantity && !$scope.hasTotalWeight) $scope.hasWeight = false;
        
    };
    
    $scope.onHasWeight = function(hasWeight){
        
        if(hasWeight && !$scope.hasTotalWeight && !$scope.hasQuantity) $scope.hasQuantity = true;
        
    };
    
    $scope.onHasTotalWeight = function(hasTotalWeight){
        
        if(hasTotalWeight) $scope.hasQuantity = false;
        if(!hasTotalWeight && !$scope.hasQuantity) $scope.hasWeight = false;
        
    };
    
    $scope.onHasDimensions = function(hasDimensions){
        
        if(hasDimensions) $scope.hasTotalLength = false;
        
    };
    
    $scope.onHasTotalLengthChange = function(hasTotalLength){
        
        if(hasTotalLength) $scope.hasDimensions = false;
          
    };
    
    function onSubmitRequest(){
        
        var item = {
			conceptType: $scope.item.conceptType,
            quantity: ($scope.hasQuantity ? $scope.item.quantity : null),
            weight: ($scope.hasWeight ? $scope.item.weight : null),
            totalWeight: ($scope.hasTotalWeight ? $scope.item.totalWeight : null),
            weightMetricType: $scope.item.weightMetricType,
            sizeLength: ($scope.hasDimensions ? $scope.item.sizeLength : null),
            sizeHeight: ($scope.hasDimensions ? $scope.item.sizeHeight : null),
            sizeWidth: ($scope.hasDimensions ? $scope.item.sizeWidth : null),
            totalLength: ($scope.hasTotalLength ? $scope.item.totalLength : null),
            sizeMetricType: $scope.item.sizeMetricType,
            excludePersonalItem: $scope.item.excludePersonalItem,
            baggageType: ($scope.item.baggageType ? $scope.item.baggageType : null)
        };
        
        submit(item);
        
    }
    
    function submit(item){
        
        $scope.$emit('benefit-submit-response', item);
        
    }
    
    function initBenefit(){
        
        $scope.benefit = findFilter($scope.config.benefits, $scope.item.benefit);
        
        for(var i in $scope.benefit.defaultRule){
            
            if(!$scope.benefit.defaultRule.hasOwnProperty(i)) continue;
            
            var prop = $scope.benefit.defaultRule[i];
            
            if($scope.item[i] === undefined){
                
                $scope.item[i] = prop;
                
            }
            
        }
        
    }
    
    function initScope(){
        
        if(angular.isNumber($scope.item.quantity)){
            $scope.hasQuantity = true;
        }
        
        if(angular.isNumber($scope.item.weight)){
            $scope.hasWeight = true;
        }
        
        if(angular.isNumber($scope.item.totalWeight)){
            $scope.hasTotalWeight = true;
        }
        
        if(angular.isNumber($scope.item.sizeLength) || angular.isNumber($scope.item.sizeHeight) || angular.isNumber($scope.item.sizeWidth)){
            $scope.hasDimensions = true;
        }
        
        if(angular.isNumber($scope.item.totalLength)){
            $scope.hasTotalLength = true;
        }
        
    }
    
    function init(){
        
        editBenefitService.register();
        
        deregisterEvent = $scope.$on('benefit-submit-request', onSubmitRequest);
        
        $scope.$on('$destroy', function(){ 
            
            if(angular.isFunction(deregisterEvent)) deregisterEvent(); 
            
        });
        
        initBenefit();
        
        initScope();
        
    }
    
    init();
    
}])

.controller('BenefitTypeQuantityController', ['$scope', '$element', 'editBenefitService', 'findFilter', function($scope, $element, editBenefitService, findFilter){
    
    function onSubmitRequest(){
        
        var item = {
            quantity: $scope.item.quantity
        };
        
        submit(item);
        
    }
    
    function submit(item){
        
        $scope.$emit('benefit-submit-response', item);
        
    }
    
    function init(){
        
        editBenefitService.register();
        
        $scope.$on('benefit-submit-request', onSubmitRequest);
        
        $scope.benefit = findFilter($scope.config.benefits, $scope.item.benefit);
        if(!$scope.item.quantity) $scope.item.quantity = 1;
        
    }
    
    init();
    
}])

.directive('kkBenefitTypeLngacs', function(){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-benefit/directives/benefit-type-quantity.html',
        scope: { item: '=', config: '=' },
        controller: 'BenefitTypeQuantityController'
    };
    
})

.directive('kkBenefitTypeCmpstg', function(){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-benefit/directives/benefit-type-quantity.html',
        scope: { item: '=', config: '=' },
        controller: 'BenefitTypeQuantityController'
    };
    
})

.directive('kkBenefitTypeTktdst', function(){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-benefit/directives/benefit-type-quantity.html',
        scope: { item: '=', config: '=' },
        controller: 'BenefitTypeQuantityController'
    };
    
})

.directive('kkBenefitTypePfdstg', function(){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-benefit/directives/benefit-type-quantity.html',
        scope: { item: '=', config: '=' },
        controller: 'BenefitTypeQuantityController'
    };
    
});