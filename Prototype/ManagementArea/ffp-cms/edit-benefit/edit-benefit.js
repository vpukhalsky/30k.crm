'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpBenefitRuleEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', '$modal', '$timeout', 'editBenefitService', 'findFilter', 'applyToAll', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, $modal, $timeout, editBenefitService, findFilter, applyToAll){
    
    kkRoutesService.preventReload($scope, 'FfpBenefitRuleEditController');
    
    $scope.item = null;
    $scope.isNew = true; 
    
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.paged = null;
    
    $scope.applyToAll = false;
    $scope.applyTo = null;
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    $scope.ruleCodePattern = ffpCmsHelper.getRuleCodePattern();
    $scope.dateRangePatern = ffpCmsHelper.getDateRangePattern(); 
    
    $scope.brandedFares = [];
    
    $scope.section = {
        codeshare: true,
        geo: true,
        validity: true,
        comment: true
    };
    
    $scope.$watch('item.purchase.from', function(newValue, oldValue){
        if((angular.isObject(newValue) || (newValue === null && angular.isObject(oldValue))) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'purchase', 'from');
    });
    
    $scope.$watch('item.purchase.until', function(newValue, oldValue){
        if((angular.isObject(newValue) || (newValue === null && angular.isObject(oldValue))) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'purchase', 'until');
    });
    
    $scope.$watch('item.flight.from', function(newValue, oldValue){
        if((angular.isObject(newValue) || (newValue === null && angular.isObject(oldValue))) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'flight', 'from');
    });
    
    $scope.$watch('item.flight.until', function(newValue, oldValue){
        if((angular.isObject(newValue) || (newValue === null && angular.isObject(oldValue))) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'flight', 'until');
    });
    
    $scope.$watchCollection('item.marketingRules', function(value){
        updateBrandedFaresCodeshare('marketingRules', value);
    }); 
    
    $scope.$watchCollection('item.operatingRules', function(value){
        updateBrandedFaresCodeshare('operatingRules', value);
    });
    
    var brandedFaresWatcher = null;
    
    function addWatcherToBrandedFares(){
        
        brandedFaresWatcher = $scope.$watchCollection('item.brandedFares', function(newValue, oldValue){
            
            if(angular.isArray(newValue) && JSON.stringify(newValue) != JSON.stringify(oldValue) && applyToAll.isActive()){
                applyToAll.apply($scope.item, 'brandedFares');
            }
        });
        
    }
    
    $scope.preventDefault = function(event){
        event.stopPropagation();
    };
    
    $scope.$watch('item.brandedFaresActive', function(active){
        
        if(active && angular.isObject($scope.config)){
            
            $scope.item.marketingRules.splice(0, $scope.item.marketingRules.length);
            $scope.item.operatingRules.splice(0, $scope.item.operatingRules.length);
            $scope.item.marketingRules.push($scope.config.brandedFareAllowedCodeshare);
            $scope.item.operatingRules.push($scope.config.brandedFareAllowedCodeshare);
            
            updateBrandedFares($scope.item);
            
        }
        
    });
    
    $scope.$watch('item.airline.code', function(code){
        
        updateBrandedFares($scope.item);
        
    });
    
    function updateBrandedFaresCodeshare(name, codeshare){
        
        if(angular.isArray(codeshare) && angular.isObject($scope.item)){
            $scope.applyToAllSync(name);
        }
        
        if(angular.isObject($scope.config) && $scope.item.brandedFaresActive 
            && (codeshare.length > 1 || codeshare.indexOf($scope.config.brandedFareAllowedCodeshare) != 0)
        ){
            $scope.item.brandedFaresActive = false;
            $scope.applyToAllSync('brandedFaresActive');
        }
        
    }
    
    function updateBrandedFares(item){

        if(!angular.isObject(item) || !angular.isString(item.airline.code) || !item.brandedFaresActive || $scope.loadingBrandedFares){
            
            var d = $.Deferred();
            d.resolve(false);
            return d.promise();
            
        }
        
        var indexOf = $filter('indexOf');
        
        $scope.loadingBrandedFares = true;
        
        collectionService.flushCollection('branded-fare');
        
        return collectionService.getCollection('rule/branded-fares.json', 'branded-fare', {
            airline: item.airline.code
        }).then(function(response){
            
            $scope.brandedFares.splice(0, $scope.brandedFares.length);
            
            for(var i = 0 ; i < response.length ; i++){
                $scope.brandedFares.push(response[i]);
            }
            
            // check if saved branded fares are consistent with collection of available ones and remove which are not
            if(angular.isArray($scope.item.brandedFares) && $scope.item.brandedFares.length > 0){
                
                var allowed = [];
                
                for(var i = 0 ; i < $scope.item.brandedFares.length ; i++){
                    if(indexOf($scope.brandedFares, $scope.item.brandedFares[i], 'code') >= 0) allowed.push($scope.item.brandedFares[i]);
                }
                
                if(allowed.length != $scope.item.brandedFares.length){
                    
                    $scope.item.brandedFares.splice(0, $scope.item.brandedFares.length);
                    for(var i = 0 ; i < allowed.length ; i++) $scope.item.brandedFares.push(allowed[i]);
                    
                }
                
            }
            
            $scope.loadingBrandedFares = false;
            
            return null;
            
        });
        
    }
    
    $scope.applyToAll = function(){
        return applyToAll.isActive();
    };
    
    $scope.applyToAllSync = function(prop){ 
        
        if(applyToAll.isActive()) applyToAll.apply($scope.item, prop);
        
    };
    
    $scope.applyToAllSyncAirline = function(prop){
        if(applyToAll.isActive()) applyToAll.applyAirline($scope.item, prop);
    };
    
    $scope.onBenefitChange = function(benefitId){
        
        updateBenefit(benefitId);
        
    };
    
    $scope.onExcludeCodeshareChange = function(exclude){
        
        if(exclude){
            while($scope.item.operatingRules.length > 0) $scope.item.operatingRules.splice(0, 1);
        }
        
    };
    
    $scope.toggleSection = function(section){
        $scope.section[section] = ! $scope.section[section];
    };
    
    $scope.onDateChange = function(period, prop, event){
        
        $timeout(function(){
            if($(event.target).val() == '') period[prop] = null;
        });
        
    };
    
    function areFiltersEmpty(params){
        return params.orderBy == $scope.config.defaultBenefitsOrderBy 
            && !params.status 
            && !params.benefit 
            && params.itemsPerPage == $scope.config.pagination.default
            && !params.name;
    }
    
    function redirectToCollection(){
        
        var cache = ffpCmsHelper.getBenefitsCache();
        var name = 'benefitRules';
        
        var params = {
            ffpid: $scope.ffp.id
        };
        
        if(angular.isObject(cache) && angular.isObject(cache.request)){
            
            if(cache.request.page && cache.request.page != 1){
                name = 'pagedBenefitRules';
                params.page = cache.request.page;
            }
            
            if(!areFiltersEmpty(params)){
                
                if(params.page && params.page != 1){
                    name = 'searchPagedBenefitRules';
                }else{
                    name = 'searchBenefitRules';
                }
                
                params.filter = angular.copy(cache.request);
                if(!params.filter.name) params.filter.name = '';
                delete params.filter.page;
                delete params.filter.totalItems;
                  
            }
            
        }
        
        kkRoutesService.redirect(name, params);
        
    }
    
    $scope.submit = function(form){
        
        if(!form.$valid || ($scope.item.brandedFaresActive && $scope.item.brandedFares.length <= 0)) return;
        
        if(applyToAll.isActive()){
            
            redirectToCollection();
            
        }else{
            
            editBenefitService.submit(toSave($scope.item)).then(function(){
                
                redirectToCollection();
                
            });
            
        }
        
    };
    
    function reset(){
        
        var i = angular.copy($scope.config.defaultBenefit);
        i.currencyCode = $scope.config.defaultCurrencyCode;
        $scope.item = toEdit(i);
        $scope.isNew = true;
        $scope.paged = null;
        
    }
    
    $scope.deleteItem = function(item){
        
        collectionService.deleteItem('benefit-rule/delete-benefit-rule.json', 'benefit-rule', item).then(function(response){
            if(response){
                redirectToCollection();
            } 
            
        });
        
    };
    
    function isCodeshareDirty(item){
        
        var mr = $scope.config.defaultBenefit.marketingRules;
        var op = $scope.config.defaultBenefit.operatingRules;
        
        if(item.marketingRules.length != mr.length || item.operatingRules != op.length) return true;
        
        for(var i = 0 ; i < mr.length ; i++){
            if(item.marketingRules.indexOf(mr[i]) < 0) return true;
        }
        
        for(var i = 0 ; i < op.length ; i++){
            if(item.operatingRules.indexOf(op[i]) < 0) return true;
        }
        
        return false;
    }
    
    function toEdit(item){
        
        if(brandedFaresWatcher) {
            brandedFaresWatcher();
            brandedFaresWatcher = null;
        }
        
        $scope.section = {
            codeshare: isCodeshareDirty(item),
            geo: item.geographicalRules.length > 0,
            validity: (angular.isObject(item.flight) && (item.flight.from || item.flight.until)) || (angular.isObject(item.purchase) && (item.purchase.from || item.purchase.until)),
            comment: angular.isString(item.comment)
        };
        
        if(!item.purchase) item.purchase = {};
        if(angular.isString(item.purchase.from)) item.purchase.from = new Date(item.purchase.from);
        if(angular.isString(item.purchase.until)) item.purchase.until = new Date(item.purchase.until);
        
        if(!item.flight) item.flight = {};
        if(angular.isString(item.flight.from)) item.flight.from = new Date(item.flight.from);
        if(angular.isString(item.flight.until)) item.flight.until = new Date(item.flight.until);
        
		if(item.code) item.code = item.code.substring(5);
		else item.code = '';
		
		if(!angular.isNumber(item.status) && angular.isArray($scope.ffp.tiers) && $scope.ffp.tiers.length > 0){
		    item.status = $scope.ffp.tiers[0].id;
		}
		
		editBenefitService.init($scope);
		
		if(angular.isArray($scope.config.benefits) && $scope.config.benefits.length > 0){
		    
		    if(!angular.isNumber(item.benefit)){
		        item.benefit = $scope.config.benefits[0].id;
		    }
		    
		    updateBenefit(item.benefit);
		    
		}
		
		updateBrandedFares(item).then(function(){
            addWatcherToBrandedFares();
        });
		
        return item;
        
    }
    
    function updateBenefit(benefitId){
        
        var benefit = findFilter($scope.config.benefits, benefitId);
	    
	    if(benefit && benefit.code){
	        
	        $timeout(function(){
	            editBenefitService.inject(benefit.code);
	        });
	        
	    }
        
    }
    
    function toSave(item){
        
        var i = angular.copy(item); 
        
        var localDate = $filter('localDate');
        
        if(angular.isObject(i.flight)){
            i.flight.from = localDate(i.flight.from);
            i.flight.until = localDate(i.flight.until);
        }
        
        if(angular.isObject(i.purchase)){
            i.purchase.from = localDate(i.purchase.from);
            i.purchase.until = localDate(i.purchase.until);
        }
		
		i.code = "RB" + $scope.ffp.code + i.code;
		
		costToSave(i);
         
        if(!i.brandedFaresActive && i.brandedFares) delete i.brandedFares;
        
        return i; 
        
    }
    
    function costToSave(item){
        
        switch(item.memberCostType){
            case 'free':
                item.memberCost = 0;
                item.memberDiscount = 0;
                item.memberDiscountAvailable = false;
                break;
            case 'fee':
            case 'miles':
                if(!item.memberDiscountAvailable) item.memberDiscount = 0;
                break;
        }
        
        if(item.guestsMaxNumber > 0){
        
            switch(item.guestsCostType){
                case 'free':
                    item.guestsCost = 0;
                    item.guestsDiscount = 0;
                    item.guestsDiscountAvailable = false;
                    break;
                case 'fee':
                case 'miles':
                    if(!item.guestsDiscountAvailable) item.guestsDiscount = 0;
                    else if(item.memberCostType == item.guestsCostType) item.guestsCost = item.memberCost;
                    break;
            }
        
        }else{
            item.guestsCost = 0;
            item.guestsDiscount = 0;
            item.guestsDiscountAvailable = false;
        }
        
        return item;
        
    }
    
    $scope.onSelectPage = function(page){
        
        if(applyToAll.isActive()){
            
            var selected = applyToAll.getCollection();
            if(selected.length >= page){
                loadItem(selected[page - 1].id);
            }
            
        }else{
            
            var cache = ffpCmsHelper.getBenefitsCache();
            
            if(!angular.isObject(cache)) return;
            
            var items = cache.response.items;
            var params = cache.request;
            
            var collectionPage = Math.ceil(page / params.itemsPerPage);
            
            if(collectionPage - params.page == 0){
                
                var indexOf = $filter('indexOf');
                var index = indexOf(items, $scope.item.id);
                var diff = $scope.paged.page - page;
                index -= diff;
                loadItem(items[index].id);
                
            }else{
                
                var p = angular.copy(params);
                p.page = collectionPage;
                
                loadCollection(p).then(function(collection){
                    
    				var index = page - ((p.page - 1) * p.itemsPerPage) - 1;
    
                    if(index < collection.length && index >= 0) $scope.item = toEdit(angular.copy(collection[index]));
                    
                });
                
            }
        }
    };
    
    function loadCollection(params){
        
        return collectionService.getPagedCollection('benefit-rule/get-benefit-rules.json?v=1', 'benefit-rule', params).then(function(response){
            
            ffpCmsHelper.setBenefitsCache({
                request: params,
                response: response
            });
            
            return response.items;
            
        });
        
    }
    
    function loadItem(id){
        
        collectionService.getItem('benefit-rule/get-benefit-rule.json', 'benefit-rule', id).then(function(item){
            
			$scope.isNew = false;
			var indexOf = $filter('indexOf');
			
			if(applyToAll.isActive()){
                
                $scope.item = toEdit(item._copy);
                var selected = applyToAll.getCollection();
                
                $scope.paged = {
                    page: indexOf(selected, item.id) + 1,
                    itemsPerPage: 1,
                    totalItems: selected.length
                };
                
            }else{
    			
                $scope.item = toEdit(angular.copy(item));
                
                var cache = ffpCmsHelper.getBenefitsCache();
                
                if(angular.isObject(cache)){
                    
                    var items = cache.response.items;
                    var params = cache.request;
                    
                    var index = indexOf(items, item.id);
                    var page = params.itemsPerPage * (params.page - 1) + index + 1;
                    
                    if(index >= 0){
                        
                        $scope.paged = {
                            page: page,
                            itemsPerPage: 1,
                            totalItems: collectionService.getItemsCount('benefit-rule')
                        };
                        
                    }
                }
            }
        });
    }

    function load(){
        
        collectionService.getConfig('ffp/config.json?v=4', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
        if(angular.isDefined($routeParams.ffpid)){
            
            var ffpId = parseInt($routeParams.ffpid, 10);
            
            ffpCmsHelper.getBenefitsConfig(ffpId).then(function(config){
                angular.extend($scope.config, config);
            });
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                
                $scope.ffp = angular.copy(ffp);
                
                if(angular.isDefined($routeParams.id)){
                    
                    var id = $routeParams.id;
                    loadItem(id); 
                    
                }else{
                    
                    reset();
                    
                }
                
            }); 
            
        }else{
            kkRoutesService.redirect('ffps'); 
        }
        
    }
    
    load();
    
}]);