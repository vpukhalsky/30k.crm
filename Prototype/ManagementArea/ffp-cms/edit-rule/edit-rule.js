'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpRuleEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', '$modal', 'applyToAllEarningRules', 'adminReviewConfig', '$timeout', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, $modal, applyToAll, adminReviewConfig, $timeout){
    
    kkRoutesService.preventReload($scope, 'FfpRuleEditController');
    
    $scope.item = null;
    $scope.isNew = true; 
    
    $scope.config = null;
    $scope.ffp = null;
    
    $scope.paged = null;
    
    $scope.applyToAll = false;
    $scope.applyTo = null;
    
    $scope.brandedFares = [];
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    $scope.ruleCodePattern = ffpCmsHelper.getRuleCodePattern();
    
    $scope.section = {
        codeshare: true,
        geo: true,
        validity: true,
        tiers: true,
        comment: true
    };
    
    $scope.$watch('item.purchase.from', function(newValue){
        if(angular.isObject(newValue) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'purchase', 'from');
    });
    
    $scope.$watch('item.purchase.until', function(newValue){
        if(angular.isObject(newValue) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'purchase', 'until');
    });
    
    $scope.$watch('item.flight.from', function(newValue){
        if(angular.isObject(newValue) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'flight', 'from');
    });
    
    $scope.$watch('item.flight.until', function(newValue){
        if(angular.isObject(newValue) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'flight', 'until');
    });
    
    $scope.$watchCollection('item.marketingRules', updateBrandedFaresCodeshare);
    
    $scope.$watchCollection('item.operatingRules', updateBrandedFaresCodeshare);
    
    var brandedFaresWatcher = null;
    
    function addWatcherToBrandedFares(){
        
        brandedFaresWatcher = $scope.$watchCollection('item.brandedFares', function(newValue, oldValue){
            
            if(angular.isArray(newValue) && JSON.stringify(newValue) != JSON.stringify(oldValue) && applyToAll.isActive()){
                applyToAll.apply($scope.item, 'brandedFares');
            }
        });
        
    }
    
    $scope.preventDefault = function(event){
        event.stopPropagation();
    };
    
    $scope.$watch('item.brandedFaresActive', function(active){
        
        if(active && angular.isObject($scope.config)){
            
            $scope.item.marketingRules.splice(0, $scope.item.marketingRules.length);
            $scope.item.operatingRules.splice(0, $scope.item.operatingRules.length);
            $scope.item.marketingRules.push($scope.config.brandedFareAllowedCodeshare);
            $scope.item.operatingRules.push($scope.config.brandedFareAllowedCodeshare);
            
            updateBrandedFares($scope.item);
            
        }
        
    });
    
    $scope.$watch('item.airline.code', function(code){
        
        updateBrandedFares($scope.item);
        
    });
    
    function updateBrandedFaresCodeshare(codeshare){
        
        if(angular.isArray(codeshare) && angular.isObject($scope.item) && angular.isObject($scope.config) 
            && $scope.item.brandedFaresActive && (codeshare.length > 1 || codeshare.indexOf($scope.config.brandedFareAllowedCodeshare) != 0)
        ){
            $scope.item.brandedFaresActive = false;
        }
        
    }
    
    function updateBrandedFares(item){ 
        
        if(!angular.isObject(item) || !angular.isString(item.airline.code) || !item.brandedFaresActive || $scope.loadingBrandedFares){
            
            var d = $.Deferred();
            d.resolve(false);
            return d.promise();
            
        }
        
        var indexOf = $filter('indexOf');
        
        $scope.loadingBrandedFares = true;
        
        collectionService.flushCollection('branded-fare');
        
        return collectionService.getCollection('rule/branded-fares.json', 'branded-fare', {
            airline: item.airline.code
        }).then(function(response){
            
            $scope.brandedFares.splice(0, $scope.brandedFares.length);
            
            for(var i = 0 ; i < response.length ; i++){
                $scope.brandedFares.push(response[i]);
            }
            
            // check if saved branded fares are consistent with collection of available ones and remove which are not
            if(angular.isArray($scope.item.brandedFares) && $scope.item.brandedFares.length > 0){
                
                var allowed = [];
                
                for(var i = 0 ; i < $scope.item.brandedFares.length ; i++){
                    if(indexOf($scope.brandedFares, $scope.item.brandedFares[i], 'code') >= 0) allowed.push($scope.item.brandedFares[i]);
                }
                
                if(allowed.length != $scope.item.brandedFares.length){
                    
                    $scope.item.brandedFares.splice(0, $scope.item.brandedFares.length);
                    for(var i = 0 ; i < allowed.length ; i++) $scope.item.brandedFares.push(allowed[i]);
                    
                }
                
            }
            
            $scope.loadingBrandedFares = false;
            
            return null;
            
        });
        
    }
    
    $scope.editRuleRange = function(rule){
        
        $modal.open({
            templateUrl: 'ffp-cms/range-modal.html',
            controller: 'RuleRangeModalController',
            resolve: {
                rule: function(){ return angular.copy(rule); }
            },
            windowClass: 'medium',
        }).result.then(function(r){
            angular.extend(rule, r);
        });
        
    };
    
    $scope.applyToAll = function(){
        return applyToAll.isActive();
    };
    
    $scope.applyToAllSync = function(prop){
        if(applyToAll.isActive()) applyToAll.apply($scope.item, prop);
    };
    
    $scope.applyToAllSyncAirline = function(prop){
        if(applyToAll.isActive()) applyToAll.applyAirline($scope.item, prop);
    };
    
    function onTiersChange(){
        if(applyToAll.isActive()) applyToAll.apply($scope.item, 'tiers');
    }
    
    function updateRulesConfig(response){
        
        if(angular.isObject(response) && response.filters){
            ffpCmsHelper.getRulesConfig($scope.ffp.id).then(function(config){
                config.filters = response.filters;
            });
        }
        
    }
    
    $scope.toggleRulesSection = function(group){
        group.expandSection = !group.expandSection;
    };
    
    $scope.toggleSection = function(section){
        $scope.section[section] = ! $scope.section[section];
    };
    
    $scope.initAccrualTooltipData = function(scope){
        ffpCmsHelper.initAccrualTooltipData(scope, $scope.config);
    };
    
    $scope.getAccrualAbbr = function(accrual){
        return ffpCmsHelper.getAccrualAbbr(accrual, $scope.config);
    };
    
    function editTier(tier, isNew){
        
        $modal.open({
           controller: 'FfpTierEditController',
           templateUrl: 'ffp-cms/edit-ffp/edit-tier.html',
           windowClass: 'small',
           resolve: {
               tier: function(){ return tier; },
               isNew: function(){ return isNew; },
               ffp: function(){ return $scope.ffp; }
           }
        }).result.then(function(t){
            var indexOf = $filter('indexOf');
            if(angular.isObject(t)){
                angular.extend(tier, t);
                if(isNew) $scope.item.tiers.push(tier);  
                else{
                    var index = indexOf($scope.item.tiers, tier.id);
                    if(index >= 0) $scope.item.tiers.splice(index, 1, tier);
                }
                onTiersChange();
            }else if(!isNew){
                var index = indexOf($scope.item.tiers, tier.id);
                if(index >= 0) $scope.item.tiers.splice(index, 1);
                onTiersChange();
            }
            
        });
        
    }
    
    $scope.addTier = function(){
        var tier = ffpCmsHelper.setupTierAccruals(angular.copy($scope.config.defaultTier), $scope.ffp.accruals);
        editTier(tier, true);
    };
    
    $scope.editTier = function(tier){
        editTier(angular.copy(tier));
    };
    
    $scope.removeTier = function(index){
        $scope.item.tiers.splice(index, 1);
        onTiersChange();
    };
	
	$scope.resetCalculationTargetType = function ()
    {
        $scope.item.calculationTargetType = null;
        $scope.applyToAllSync('calculationTargetType');
    };
    
    $scope.onRelationshipChange = function(rel){
        
        if(rel == $scope.item.airline.relationship) return; 
        
        var ffp = $scope.ffp;
        var codeshare = ffp.codeshareBase;
        
        if(!$scope.isNew || ffpCmsHelper.areCodeshareRulesDirty($scope.item, ffp)) return;
        
        var mk = $scope.item.marketingRules;
        var op = $scope.item.operatingRules;
        
        //preset marketing rules if empty
        
        switch(codeshare){
            case 'op': //operating
            
                switch(rel){
                    case 'alliance': 
                        mk = ['own', 'alli']; break;
                    case 'private': 
                        mk = ['own']; break;
                }
                break;
                
            case 'mk': //marketing
                mk = []; break;
        }
        
        //preset operating rules if empty
        
        switch(codeshare){
            case 'op': //operating
                op = []; break;
                
            case 'mk': //marketing
                switch(rel){
                    case 'alliance':
                        op = ['own', 'alli']; break;
                        
                    case 'private':
                        op = ['own']; break;
                }
                break;
        }
        
        $scope.item.marketingRules.splice(0, $scope.item.marketingRules.length);
        $scope.item.operatingRules.splice(0, $scope.item.operatingRules.length);
        mk.forEach(function(item){ $scope.item.marketingRules.push(item); });
        op.forEach(function(item){ $scope.item.operatingRules.push(item); });
    
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid || ($scope.item.brandedFaresActive && $scope.item.brandedFares.length <= 0)) return;
        
        if(applyToAll.isActive()){
            
            kkRoutesService.redirect('rules', {
                ffpid: $scope.ffp.id
            });
            
        }else{
            
            submit(function(response){
            
                if($scope.paged) updateRulesConfig(response);
                
                kkRoutesService.redirect('rules', {
                    ffpid: $scope.ffp.id
                });
                
            });   
        }
        
    };
    
    function reset(){
        
        $scope.isNew = true;
        $scope.item = toEdit(angular.copy($scope.config.defaultRule));
        $scope.paged = null;
        
    }
    
    function submit(callback){
        
        var item = toSave($scope.item);
        
        collectionService.saveItem('rule/save-rule.json', 'rule', item).then(callback);
    }
    
    $scope.deleteRule = function(item){
        collectionService.deleteItem('rule/delete-rule.json', 'rule', item).then(function(response){
            if(response){
                if($scope.paged) updateRulesConfig(response);
                
                kkRoutesService.redirect('rules', {
                    ffpid: $scope.ffp.id
                });
            } 
            
        });
    };
    
    $scope.getButtonGroupClass = function(buttons, modificator){
        
        return ffpCmsHelper.getButtonGroupClass(buttons, modificator);
        
    };
    
    $scope.addAccrualRule = function(group, index){
        group.items.splice(index + 1, 0, {});
    };
    
    $scope.deleteAccrualRule = function(group, index){
        group.items.splice(index, 1);
    };
    
    function toEdit(item){
        
        if(brandedFaresWatcher) {
            brandedFaresWatcher();
            brandedFaresWatcher = null;
        }
        
        $scope.section = {
            codeshare: ffpCmsHelper.areCodeshareRulesDirty(item, $scope.ffp) || item.flightNoExcl.length > 0 || item.flightNoLimit.length > 0,
            geo: item.geographicalRules.length > 0 || item.codedConditions.length > 0,
            validity: (angular.isObject(item.flight) && (item.flight.from || item.flight.until)) || (angular.isObject(item.purchase) && (item.purchase.from || item.purchase.until)),
            tiers: item.applyToTiers.length > 0 || item.tiers.length > 0 || item.minMilesCalcMethod != $scope.config.defaultRule.minMilesCalcMethod || item.eliteBonusBase != $scope.config.defaultRule.eliteBonusBase,
            comment: angular.isString(item.comment)
        };
        
        var find = $filter('find');
        
        $scope.ffp.accruals.forEach(function(accrual){
            
            var group = find(item.rules, accrual.id, 'accrual');
            if(!group) item.rules.push({
                accrual: accrual.id,
                items: []
            });
            
        });
        
        item.rules.forEach(function(group){
            if(group.items.length <= 0) group.items.push({});
        });
        
        if(!item.editableRdbs) item.editableRdbs = item.rdbs.join('');
        
        if(!angular.isArray(item.tiers)) item.tiers = [];
        
        if($scope.isNew){
            
            if(!angular.isArray(item.applyToTiers)) item.applyToTiers = [];
            
            $scope.ffp.tiers.forEach(function(tier){
                item.applyToTiers.push(tier.id);
            });
            
        }
        
        item.tiers.forEach(function(tier){
             tier = ffpCmsHelper.setupTierAccruals(tier, $scope.ffp.accruals);
        });
        
        if(!item.purchase) item.purchase = {};
        if(angular.isString(item.purchase.from)) item.purchase.from = new Date(item.purchase.from);
        if(angular.isString(item.purchase.until)) item.purchase.until = new Date(item.purchase.until);
        
        if(!item.flight) item.flight = {};
        if(angular.isString(item.flight.from)) item.flight.from = new Date(item.flight.from);
        if(angular.isString(item.flight.until)) item.flight.until = new Date(item.flight.until);
        
        updateBrandedFares(item).then(function(){
            addWatcherToBrandedFares();
        });
        
        return item;
        
    }
    
    function toSave(item){
        
        var i = angular.copy(item);
        i.rdbs = i.editableRdbs.split('');
        delete i.editableRdbs;
        
        var localDate = $filter('localDate');
        
        if(angular.isObject(i.flight)){
            i.flight.from = localDate(i.flight.from);
            i.flight.until = localDate(i.flight.until);
        }
        
        if(angular.isObject(i.purchase)){
            i.purchase.from = localDate(i.purchase.from);
            i.purchase.until = localDate(i.purchase.until);
        }
        
        i.tiers.forEach(function(tier){
             tier = ffpCmsHelper.setupTierAccruals(tier, $scope.ffp.accruals);
        });
        
        i.rules.forEach(function(rule){
            if(angular.isDefined(rule.expandSection)) delete rule.expandSection;
        });
        
        if(!i.brandedFaresActive && i.brandedFares) delete i.brandedFares;
        
        return i;
        
    }
    
    $scope.onSelectPage = function(page){
        
        if(applyToAll.isActive()){
            
            var selected = applyToAll.getCollection();
            if(selected.length >= page){
                loadItem(selected[page - 1].id);
            }
            
        }else{
            
            var cache = ffpCmsHelper.getRulesCache();
            
            if(!angular.isObject(cache)) return;
            
            var items = cache.response.items;
            var params = cache.request;
            
            var collectionPage = Math.ceil(page / params.itemsPerPage);
            
            if(collectionPage - params.page == 0){
                
                var indexOf = $filter('indexOf');
                var index = indexOf(items, $scope.item.id);
                var diff = $scope.paged.page - page;
                index -= diff;
                loadItem(items[index].id);
                
            }else{
                
                var p = angular.copy(params);
                p.page = collectionPage;
                
                loadCollection(p).then(function(collection){
                    
                    var index = page - (p.page * p.itemsPerPage) - 1;
                    if(index < collection.length && index >= 0) $scope.item = toEdit(angular.copy(collection[index]));
                    
                });
                
            }
        }
    };
    
    function loadCollection(params){
        
        var url = 'rule/get-rules.json';
        if(adminReviewConfig.isEnabled()) url = 'rule/review/get-rules.json';
        
        return collectionService.getPagedCollection(url, 'rule', params).then(function(response){
            
            ffpCmsHelper.sortRules(response.items);
            
            ffpCmsHelper.setRulesCache({
                request: params,
                response: response
            });
            
            return response.items;
            
        });
        
    }
    
    function loadItem(id){
        
        var url = 'rule/get-rule.json';
        if(adminReviewConfig.isEnabled()) url = 'rule/review/get-rule.json?v=1';
        
        collectionService.getItem(url, 'rule', id).then(function(item){
            
            $scope.isNew = false;
            var indexOf = $filter('indexOf');
            
            if(applyToAll.isActive()){
                
                $scope.item = toEdit(item._copy);
                var selected = applyToAll.getCollection();
                
                $scope.paged = {
                    page: indexOf(selected, item.id) + 1,
                    itemsPerPage: 1,
                    totalItems: selected.length
                };
                
            }else{
                
                $scope.item = toEdit(angular.copy(item));
                
                var cache = ffpCmsHelper.getRulesCache();
                
                if(angular.isObject(cache)){
                    
                    var items = cache.response.items;
                    var params = cache.request;
                    
                    var index = indexOf(items, item.id);
                    var page = params.itemsPerPage * (params.page - 1) + index + 1;
                    
                    if(index >= 0){
                        
                        $scope.paged = {
                            page: page,
                            itemsPerPage: 1,
                            totalItems: cache.response.totalItems
                        };
                        
                    }
                }
            }
            
        });
    }

    $scope.onDateChange = function(period, prop, event){
        
        $timeout(function(){
            if($(event.target).val() == '') period[prop] = null;
        });
        
    };

    function load(){
        
        collectionService.getConfig('ffp/config.json?v=1', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
        if(angular.isDefined($routeParams.ffpid)){
            
            var ffpId = parseInt($routeParams.ffpid, 10);
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                $scope.ffp = angular.copy(ffp);
                
                if(angular.isDefined($routeParams.id)){
                    
                    var id = $routeParams.id;
                    loadItem(id);
                    
                }else{
                    
                    reset();
                    
                }
                
            });
            
        }else{
            kkRoutesService.redirect('ffps');
        }
        
    }
    
    load();
    
}]);
