'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpUpgradeEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', 'applyToAll', '$modal', '$timeout', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, applyToAll, $modal, $timeout){
    
    kkRoutesService.preventReload($scope, 'FfpUpgradeEditController');
    
    $scope.item = null;
    $scope.isNew = true; 
    
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.paged = null;
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    $scope.ruleCodePattern = ffpCmsHelper.getRuleCodePattern();
    $scope.dateRangePatern = ffpCmsHelper.getDateRangePattern(); 
    
    $scope.brandedFares = [];
    
    $scope.section = {
        codeshare: true,
        geo: true,
        validity: true,
        comment: true,
        legend: true
    };
    
    $scope.$watchCollection('item.marketingRules', updateBrandedFaresCodeshare);
    
    $scope.$watchCollection('item.operatingRules', updateBrandedFaresCodeshare);
    
    var brandedFaresWatcher = null;
    
    function addWatcherToBrandedFares(){
        
        brandedFaresWatcher = $scope.$watchCollection('item.brandedFares', function(newValue, oldValue){
            
            if(angular.isArray(newValue) && JSON.stringify(newValue) != JSON.stringify(oldValue) && applyToAll.isActive()){
                applyToAll.apply($scope.item, 'brandedFares');
            }
        });
        
    }
    
    $scope.preventDefault = function(event){
        event.stopPropagation();
    };
    
    $scope.$watch('item.brandedFaresActive', function(active){
        
        if(active && angular.isObject($scope.config)){
            
            $scope.item.marketingRules.splice(0, $scope.item.marketingRules.length);
            $scope.item.operatingRules.splice(0, $scope.item.operatingRules.length);
            $scope.item.marketingRules.push($scope.config.brandedFareAllowedCodeshare);
            $scope.item.operatingRules.push($scope.config.brandedFareAllowedCodeshare);
            
            updateBrandedFares($scope.item);
            
        }
        
    });
    
    $scope.$watch('item.airline.code', function(code){
        
        updateBrandedFares($scope.item);
        
    });
    
    function updateBrandedFaresCodeshare(codeshare){
        
        if(angular.isArray(codeshare) && angular.isObject($scope.item) && angular.isObject($scope.config) 
            && $scope.item.brandedFaresActive && (codeshare.length > 1 || codeshare.indexOf($scope.config.brandedFareAllowedCodeshare) != 0)
        ){
            $scope.item.brandedFaresActive = false;
        }
        
    }
    
    function updateBrandedFares(item){

        if(!angular.isObject(item) || !angular.isString(item.airline.code) || !item.brandedFaresActive || $scope.loadingBrandedFares) return;
        
        var indexOf = $filter('indexOf');
        
        $scope.loadingBrandedFares = true;
        
        collectionService.flushCollection('branded-fare');
        
        return collectionService.getCollection('rule/branded-fares.json', 'branded-fare', {
            airline: item.airline.code
        }).then(function(response){
            
            $scope.brandedFares.splice(0, $scope.brandedFares.length);
            
            for(var i = 0 ; i < response.length ; i++){
                $scope.brandedFares.push(response[i]);
            }
            
            // check if saved branded fares are consistent with collection of available ones and remove which are not
            if(angular.isArray($scope.item.brandedFares) && $scope.item.brandedFares.length > 0){
                
                var allowed = [];
                
                for(var i = 0 ; i < $scope.item.brandedFares.length ; i++){
                    if(indexOf($scope.brandedFares, $scope.item.brandedFares[i], 'code') >= 0) allowed.push($scope.item.brandedFares[i]);
                }
                
                if(allowed.length != $scope.item.brandedFares.length){
                    
                    $scope.item.brandedFares.splice(0, $scope.item.brandedFares.length);
                    for(var i = 0 ; i < allowed.length ; i++) $scope.item.brandedFares.push(allowed[i]);
                    
                }
                
            }
            
            $scope.loadingBrandedFares = false;
            
            return null;
            
        });
        
    }
    
    $scope.toggleSection = function(section){
        $scope.section[section] = ! $scope.section[section];
    };
    
    $scope.toggleRulesSection = function(group){
        group.expandSection = !group.expandSection;
    };
    
	$scope.areAllTiersIncluded = function(tiers) {
        return $scope.getAppliedTiers(tiers).length == $scope.ffp.tiers.length;
    };

    $scope.getAppliedTiers = function(tiers) {
        var appliedTiers = [];

        for (var i = 0; i < $scope.ffp.tiers.length; i++) {

            var ffpTier = $scope.ffp.tiers[i];

            for (var j = 0; j < tiers.length; j++) {

                if (ffpTier.id == tiers[j]) {
                    appliedTiers.push(ffpTier);
                    break;
                }
            }
        }

        return appliedTiers;
    };
    
    $scope.cloneRule = function(group, index, rule){
        group.items.splice(index, 0, angular.copy(rule));
    };
	
    function editRule(rule){
        
        $modal.open({
            templateUrl: 'ffp-cms/edit-award/edit-rule-modal.html?v=1',
            controller: 'AwardRuleEditModalController',
            resolve: {
                rule: function(){ return angular.copy(rule); },
                isNew: function(){ return false; },
                ffp: function(){ return $scope.ffp; }
            },
            windowClass: 'edit-redemption-rule medium',
        }).result.then(function(r){
            angular.extend(rule, r);
        });
        
    }
    
    $scope.editRule = function(rule){
        
        editRule(rule);
        
    };
    
    $scope.addRule = function(group, index){
        group.items.splice(index + 1, 0, angular.copy($scope.config.defaultRedemptionRule));
    };
    
    $scope.removeRule = function(group, index){
        group.items.splice(index, 1);
    };
    
    $scope.onDateChange = function(period, prop, event){
        
        $timeout(function(){
            if($(event.target).val() == '') period[prop] = null;
        });
        
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid || ($scope.item.brandedFaresActive && $scope.item.brandedFares.length <= 0)) return;
        
        submit(function(response){
            
            kkRoutesService.redirect('upgrades', {
                ffpid: $scope.ffp.id
            });
            
        });
        
    };
    
    function reset(){
        
        $scope.item = toEdit(angular.copy($scope.config.defaultUpgrade));
        $scope.isNew = true;
        $scope.paged = null;
        
    }
    
    function submit(callback){
        var item = toSave($scope.item);
        collectionService.saveItem('upgrade/save-upgrade.json', 'upgrade', item).then(callback);
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem('upgrade/delete-upgrade.json', 'upgrade', item).then(function(response){
            if(response){
                kkRoutesService.redirect('upgrades', {
                    ffpid: $scope.ffp.id
                });
            } 
            
        });
    };
    
    function isCodeshareDirty(item){
        
        if(!angular.isObject($scope.config)) return false;
        
        var mr = $scope.config.defaultUpgrade.marketingRules;
        var op = $scope.config.defaultUpgrade.operatingRules; 
        
        if(item.marketingRules.length != mr.length || item.operatingRules != op.length) return true;
        
        for(var i = 0 ; i < mr.length ; i++){
            if(item.marketingRules.indexOf(mr[i]) < 0) return true;
        }
        
        for(var i = 0 ; i < op.length ; i++){
            if(item.operatingRules.indexOf(op[i]) < 0) return true;
        }
        
        return false;
    }
    
    function toEdit(item){
        
        if(brandedFaresWatcher) {
            brandedFaresWatcher();
            brandedFaresWatcher = null;
        }
        
        $scope.section = {
            codeshare: isCodeshareDirty(item) || item.flightNoExcl.length > 0 || item.flightNoLimit.length > 0 || item.excludeCodeshare,
            geo: item.geographicalRules.length > 0 || item.codedConditions.length > 0,
            validity: (angular.isObject(item.flight) && (item.flight.from || item.flight.until)) || (angular.isObject(item.purchase) && (item.purchase.from || item.purchase.until)) 
                || (angular.isArray(item.excludedDepartures) && item.excludedDepartures.length > 0) || (angular.isArray(item.excludedArrivals) && item.excludedArrivals.length > 0),
            comment: angular.isString(item.comment),
            legend: angular.isString(item.legend) || item.hasLegend
        };
        
        var upgrades = $filter('filter')($scope.ffp.redemptions, { type : 'upgrade' });
        
        if(upgrades.length > 0){
            
            if(!item.name || $filter('indexOf')(upgrades, item.name) < 0){
                item.name = upgrades[0].id;
            }
            
            setupRules(item);
            
        }else{
            
            item.rules = [];
            
        }
        
        if(!item.purchase) item.purchase = {};
        if(angular.isString(item.purchase.from)) item.purchase.from = new Date(item.purchase.from);
        if(angular.isString(item.purchase.until)) item.purchase.until = new Date(item.purchase.until);
        
        if(!item.flight) item.flight = {};
        if(angular.isString(item.flight.from)) item.flight.from = new Date(item.flight.from);
        if(angular.isString(item.flight.until)) item.flight.until = new Date(item.flight.until);
        
        if(angular.isString(item.code)){
            item.code = item.code.substring(5);
        }
		
		updateBrandedFares(item).then(function(){
            addWatcherToBrandedFares();
        });
		
        return item;
        
    }
    
    $scope.onNameChange = function(item){
        console.log('name', item);
        setupRules(item);
        
    };
    
    function setupRules(item){
        
        var find = $filter('find');
        var redemption = find($scope.ffp.redemptions, item.name);
        var leaveGroups = [];
        
        item.rules.forEach(function(group){
            var season = find(redemption.seasons, group.season);
            if(season) leaveGroups.push(group);
        });
        
        redemption.seasons.forEach(function(season){
            
            var group = find(leaveGroups, season.id, 'season');
            
            if(!group) leaveGroups.push({
                season: season.id,
                items: []
            });
            
        });
        
        item.rules = leaveGroups;
        
        item.rules.forEach(function(group){
            
            if(group.items.length <= 0) group.items.push(angular.copy($scope.config.defaultRedemptionRule));
            
            group.items.forEach(function(rule){
                if(!angular.isArray(rule.gril)) rule.gril = [];
                if(!angular.isArray(rule.seasonalDates)) rule.seasonalDates = [];
                if(!angular.isString(rule.logicalOperand)) rule.logicalOperand = $scope.config.defaultRedemptionRule.logicalOperand;
				if(rule.applyToTiers.length == 0 && $scope.isNew == true) $scope.ffp.tiers.forEach(function(t) {
                    rule.applyToTiers.push(t.id);
                });
            });
            
        });
        
        return item;
    }
    
    function toSave(item){
        
        var i = angular.copy(item); 
        
        var localDate = $filter('localDate');
        
        if(angular.isObject(i.flight)){
            i.flight.from = localDate(i.flight.from);
            i.flight.until = localDate(i.flight.until);
        }
        
        if(angular.isObject(i.purchase)){
            i.purchase.from = localDate(i.purchase.from);
            i.purchase.until = localDate(i.purchase.until);
        }
		
		i.code = "RU" + $scope.ffp.code + i.code;
		
		i.rules.forEach(function(rule){
            if(angular.isDefined(rule.expandSection)) delete rule.expandSection;
        });
        
        if(!i.brandedFaresActive && i.brandedFares) delete i.brandedFares;

        return i; 
        
    }
    
    $scope.onExcludeCodeshareChange = function(exclude){
        
        if(exclude){
            while($scope.item.operatingRules.length > 0) $scope.item.operatingRules.splice(0, 1);
        }
        
    };
    
    $scope.onSelectPage = function(page){
        
        var cache = ffpCmsHelper.getUpgradesCache();
        
        if(!angular.isObject(cache)) return;
        
        var items = cache.response.items;
        var params = cache.request;
        
        var collectionPage = Math.ceil(page / params.itemsPerPage);
        
        if(collectionPage - params.page == 0){
            
            var indexOf = $filter('indexOf');
            var index = indexOf(items, $scope.item.id);
            var diff = $scope.paged.page - page;
            index -= diff;
            loadItem(items[index].id);
            
        }else{
            
            var p = angular.copy(params);
            p.page = collectionPage;
            
            loadCollection(p).then(function(collection){
                
                var index = page - (p.page * p.itemsPerPage) - 1;
                if(index < collection.length && index >= 0) $scope.item = toEdit(angular.copy(collection[index]));
                
            });
            
        }
    };
    
    function loadCollection(params){
        
        return collectionService.getPagedCollection('upgrade/get-upgrades.json', 'upgrade', params).then(function(response){
            
            ffpCmsHelper.setUpgradesCache({
                request: params,
                response: response
            });
            
            return response.items;
            
        });
        
    }
    
    function loadItem(id){
        
        collectionService.getItem('upgrade/get-upgrade.json', 'upgrade', id).then(function(item){
            
			$scope.isNew = false;
            $scope.item = toEdit(angular.copy(item));
            
            var cache = ffpCmsHelper.getUpgradesCache();
            
            if(angular.isObject(cache)){
                
                var items = cache.response.items;
                var params = cache.request;
                
                var indexOf = $filter('indexOf');
                var index = indexOf(items, item.id);
                var page = params.itemsPerPage* (params.page - 1) + index + 1;
                
                if(index >= 0){
                    
                    $scope.paged = {
                        page: page,
                        itemsPerPage: 1,
                        totalItems: cache.response.totalItems
                    };
                    
                }
            }
            
        });
    }

    function load(){
        
        applyToAll.reset();
        
        collectionService.getConfig('ffp/config.json?v=2', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
        if(angular.isDefined($routeParams.ffpid)){
            
            var ffpId = parseInt($routeParams.ffpid, 10);
            
            ffpCmsHelper.getUpgradesConfig(ffpId).then(function(config){
                angular.extend($scope.config, config);
            });
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                
                $scope.ffp = angular.copy(ffp);
                
                if(angular.isDefined($routeParams.id)){
                    
                    var id = $routeParams.id;
                    loadItem(id); 
                    
                    
                }else{
                    
                    reset();
                    
                }
                
            });
            
        }else{
            kkRoutesService.redirect('ffps');
        }
        
    }
    
    load();
    
}])

.controller('AwardRuleEditModalController', ['$scope', '$modalInstance', 'rule', 'isNew', 'ffp', 'ffpCmsHelper', function($scope, $modalInstance, rule, isNew, ffp, ffpCmsHelper){
    
    $scope.rule = rule;
    $scope.isNew = isNew;
    $scope.ffp = ffp;
    
    $scope.dateRangePatern = ffpCmsHelper.getDateRangePattern(); 
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.isValid = function(){
        var gril = angular.isArray($scope.rule.gril) && $scope.rule.gril.length > 0;
        var range = $scope.rule.rangeFrom || $scope.rule.rangeTo;
        return (gril && !range) || (!gril && range) || (!gril && !range);
    };
    
    $scope.submit = function(form){
        if(form.$valid && $scope.isValid()) $modalInstance.close(rule);
    };
    
}]);