'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpRuleCollectionController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', '$modal', 'applyToAllEarningRules', 'adminReviewConfig', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, $modal, applyToAll, adminReviewConfig){
     
    kkRoutesService.preventReload($scope, 'FfpRuleCollectionController');
    
    $scope.items = null;
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.filters = null;
    $scope.appliedFilters = null;
    
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.paged = {
        page: 1,
        totalItems: null,
        itemsPerPage: 50
    };
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    $scope.ruleCodePattern = ffpCmsHelper.getRuleCodePattern();
    
    $scope.filtersExpanded = false;
    
    $scope.brandedFares = [];
    
    var emptyFiltersHash = '';
    var appliedFiltersHash = '';
    
    $scope.$watch('appliedFilters', function(newValue){
        appliedFiltersHash = JSON.stringify(newValue); 
    });
    
    $scope.$watchCollection('items', function(items){
        
        if(angular.isArray(items) && items.length > 0) loadBrandedFares(items);
        
    });
    
    $scope.getRuleIdClass = function(rule){
        
        var cls = [];
        
        var fromdates = [];
        var tilldates = [];
        
        var past = 0;
        var future = 0;

        var now = new Date();
        
        if(rule.purchase){
            if(rule.purchase.from) fromdates.push(new Date(rule.purchase.from));
            if(rule.purchase.until) tilldates.push(new Date(rule.purchase.until));
        }

        if(rule.flight){
            if(rule.flight.from) fromdates.push(new Date(rule.flight.from));
            if(rule.flight.until) tilldates.push(new Date(rule.flight.until));
        }

        fromdates.forEach(function(date){
            if (date.valueOf() > now.valueOf()) future++;
        });
            
        if (fromdates.length > 0 && fromdates.length == future) {
            cls.push('future');
            return cls;
        }
        
        tilldates.forEach(function (date) {
            if (date.valueOf() <= now.valueOf()) past++;
        });

        if (tilldates.length > 0 && tilldates.length == past) {
            cls.push('past');
            return cls;
        }
      
        if (tilldates.length > 0 || fromdates.length > 0) {
            cls.push('now');
        }
        
        return cls;
    };
    
    $scope.openCodeshareFilterModal = function(){
        
        $modal.open({
            templateUrl: 'ffp-cms/rules/codeshare-filter-modal.html',
            controller: 'FfpRuleCodeshareFilterModalController',
            windowClass: 'small',
            resolve: {
                filters: function(){ return $scope.filters },
                config: function(){ return $scope.config }
            }
        }).result.then(function(result){
            angular.extend($scope.filters, result);
        });
        
    };
    
    $scope.preventDefault = function(event){
        event.stopPropagation();
    };
    
    $scope.getButtonGroupClass = function(buttons){
        return ffpCmsHelper.getButtonGroupClass(buttons);
    };
    
    $scope.onSelectPage = function(page){
        
        $scope.paged.page = page;
        loadItems($scope.ffp.id);
        
    };
    
    $scope.onItemsPerPageChange = function(){
        loadItems($scope.ffp.id);
    };
    
    $scope.$watchCollection('selected.items', function(newValue){
        
        if(!angular.isArray($scope.items)) return;
        if(newValue.length >= $scope.items.length) $scope.selected.allNone = 'all';
        else $scope.selected.allNone = 'none';
        
    });
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.initAccrualTooltipData = function(scope){
        
        ffpCmsHelper.initAccrualTooltipData(scope, $scope.config);
        
    };
    
    $scope.areCodeshareRulesDirty = function(item){
        
        return ffpCmsHelper.areCodeshareRulesDirty(item, $scope.ffp);
        
    };
    
    $scope.initCodeshareTooltipData = function(scope){
        
        scope.codeshareTooltipData = {
            rule: scope.item,
            config: scope.config
        };
        
    };
    
    $scope.initStatusTiersTooltipData = function(scope){
        
        var data = null;
        var areStatusTiersModified = $scope.areStatusTiersConfigurationModified(scope.item);
        var hasStatusTierDeviations = $scope.hasStatusTierDeviations(scope.item);
        var hasApplyToTierModification = $scope.hasApplyToTierModification(scope.item);
        
        if (areStatusTiersModified || hasStatusTierDeviations || hasApplyToTierModification) {
            data = {
                rule: scope.item,
                config: scope.config,
                areStatusTiersModified: areStatusTiersModified,
                hasStatusTierDeviations: hasStatusTierDeviations,
                hasApplyToTierModification: hasApplyToTierModification
            };

            data.config.tiers = $scope.ffp.tiers;
        }
        
        scope.statusTiersTooltipData = data;
        
    };
    
    $scope.getAccrualAbbr = function(accrual){
        
        return ffpCmsHelper.getAccrualAbbr(accrual, $scope.config);
        
    };
    
    $scope.getAirlineTooltipData = function(item){

        var rel = $filter('find')($scope.config.airlineRelationships, item.airline.relationship, 'code');
        return rel;
        
    };
    
    $scope.getRuleActionColspan = function(item){
        return $scope.applyToAll() && item.unlocked ? 2 : 1;
    };
    
    function deleteFromSelected(item){
        var index = $filter('indexOf')($scope.selected.items, item.id);
        if(index >= 0) $scope.selected.items.splice(index, 1);
    }
    
    $scope.deleteRule = function(rule){
        collectionService.deleteItem('rule/delete-rule.json', 'rule', rule).then(function(response){
            deleteFromSelected(response.item);
            updateRulesConfig(response);
        });
    };
    
    $scope.deleteRules = function(){
        
        var rules = $scope.selected.items;
        collectionService.deleteItems('rule/delete-rules.json', 'rule', rules).then(function(response){
            response.items.forEach(function(item){
                deleteFromSelected(item);    
            });
            updateRulesConfig(response);
        });
        
    };
    
    $scope.unlockRule = function(scope){
        
        applyToAll.unlock(scope.item);
        
    };
    
    $scope.lockRule = function(scope){
        
        applyToAll.lock(scope.item);
        
    };
    
    function toSave(item){
        
        return applyToAll.toSave(item);
        
    }
    
    $scope.saveRule = function(scope){
        
        if(!scope.itemForm.$valid) return;
        
        var item = toSave(scope.item);
         
        collectionService.saveItem('rule/save-rule.json', 'rule', item).then(updateRulesConfig);
        
    };
    
    $scope.cloneRule = function(index){
        
        collectionService.cloneItem('rule/clone-rule.json', 'rule', $scope.items[index]).then(function(response){
            
            ffpCmsHelper.setRulesCache(null);
            
        });
        
    };
    
    $scope.cloneRules = function(){
        
        $scope.selected.items.forEach(function(item){
            applyToAll.lock(item);
        });
        
        collectionService.cloneItems('rule/clone-rules.json', 'rule', $scope.selected.items).then(function(response){
            
            ffpCmsHelper.setRulesCache(null);
            
        });
    };
    
    $scope.applyToAll = function(){
        return applyToAll.isActive();
    };
    
    $scope.enableApplyToAll = function(){
        
        $scope.items.forEach(function(item){
            applyToAll.lock(item);
        });
        
        applyToAll.setCollection($scope.selected.items, $scope.ffp);
    };
    
    $scope.disableApplyToAll = function(){
        
        applyToAll.reset();
        
    };
    
    $scope.applyToAllSync = function(item, prop){
        
        applyToAll.apply(item, prop);
        
    };
    
    $scope.applyToAllSyncAccrual = function(item, accrual){
        
        applyToAll.applyAccrual(item, accrual);
        
    };
    
    $scope.applyToAllSyncAirline = function(item, prop){
        
        applyToAll.applyAirline(item, prop);
        
    };
    
    $scope.saveRules = function(form){
        
        if(!form.$valid) return;
        
        applyToAll.save().then(function(response){
            updateRulesConfig(response);  
        });
        
    };
    
    $scope.areStatusTiersModified = function(item)
    {
        return $scope.areStatusTiersConfigurationModified(item) ||
            $scope.hasStatusTierDeviations(item) ||
            $scope.hasApplyToTierModification(item);
    }

    $scope.areStatusTiersConfigurationModified = function(item){
        return item.eliteBonusBase != $scope.config.defaultRule.eliteBonusBase 
            || item.minMilesCalcMethod != $scope.config.defaultRule.minMilesCalcMethod;
    };
    
    $scope.hasStatusTierDeviations = function(item){
        return item.tiers && item.tiers.length > 0;
    }
    
    $scope.hasApplyToTierModification = function(item){
        return item.applyToTiers && item.applyToTiers.length != $scope.ffp.tiers.length;
    }
    
    $scope.areFiltersDirty = function(){
        return appliedFiltersHash != JSON.stringify($scope.filters);
    };
    
    $scope.areFiltersEmpty = function(){
        return emptyFiltersHash == appliedFiltersHash;
    };
    
    function submitFilters(){
        
        var filters = angular.copy($scope.filters);
        
        if(!filters.gril && filters.grilTags.length > 0) filters.grilTags.splice(0, filters.grilTags.length);
        if(!filters.codeshareDev && filters.marketingSpecificAirlines.length > 0) filters.marketingSpecificAirlines.splice(0, filters.marketingSpecificAirlines.length);
        if(!filters.codeshareDev && filters.operatingSpecificAirlines.length > 0) filters.operatingSpecificAirlines.splice(0, filters.operatingSpecificAirlines.length);
        
        $scope.appliedFilters = filters;
        $scope.paged.page = 1;
        
        loadItems($scope.ffp.id);
    }
    
    $scope.submitFilters = function(form){
        if(form.$valid) submitFilters();
    };
    
    $scope.resetFilters = function(){
        
        var def = $scope.config.defaultFilterValues;
        
        for(var i in def){
            
            if(!def.hasOwnProperty(i)) continue;
            
            if(angular.isArray(def[i])){
                
                if(!angular.isArray($scope.filters[i])) $scope.filters[i] = [];
                $scope.filters[i].splice(0, $scope.filters[i].length);
                
                def[i].forEach(function(j){
                    $scope.filters[i].push(j);
                });
                
            }else{
                $scope.filters[i] = def[i];
            }
            
        }
        
        if(!$scope.areFiltersEmpty()){
            submitFilters();
        }
    }; 
    
    $scope.toggleFilters = function(){
        $scope.filtersExpanded = ! $scope.filtersExpanded;
    };
    
    function updateRulesConfig(response){
        
        if(angular.isObject(response) && response.filters){
            ffpCmsHelper.getRulesConfig($scope.ffp.id).then(function(config){
                config.filters = response.filters;
                angular.extend($scope.config, config);
            });
        }
        
    }
    
    function loadItems(ffpId){
        
        var params = angular.copy($scope.paged);
        params.filters = angular.copy($scope.appliedFilters);
        params.ffpId = ffpId;
        delete params.totalItems;
        
        var url = 'rule/get-rules.json';
        if(adminReviewConfig.isEnabled()) url = 'rule/review/get-rules.json';
        
        collectionService.getPagedCollection(url, 'rule', params).then(function(response){
            
            ffpCmsHelper.sortRules(response.items);
            
            $scope.items = response.items;
            $scope.paged.page = response.page;
            $scope.paged.totalItems = response.totalItems;
            
            ffpCmsHelper.setRulesCache({
                request: params,
                response: response
            });
            
        });
        
    }
    
    function loadBrandedFares(rules){
        
        ffpCmsHelper.loadBrandedFares(rules).then(function(response){
            
            $scope.brandedFares.splice(0, $scope.brandedFares.length);
            
            for(var i = 0 ; i < response.length ; i++){
                $scope.brandedFares.push(response[i]);
            }
            
        });
        
    }
    
    function loadRulesConfig(ffpId){
        
        ffpCmsHelper.getRulesConfig(ffpId).then(function(config){
            
            angular.extend($scope.config, config);
            
            emptyFiltersHash = JSON.stringify($scope.config.defaultFilterValues);
            
            if(!$scope.filters){
                $scope.filters = angular.copy($scope.config.defaultFilterValues);
                $scope.appliedFilters = angular.copy($scope.filters); 
            }
            
        });
        
    }
    
    function loadParamsFromCache(){
        
        var cache = ffpCmsHelper.getRulesCache();
        
        if(angular.isObject(cache)){
            $scope.paged.page = cache.request.page;
            $scope.paged.itemsPerPage = cache.request.itemsPerPage;
            $scope.filters = angular.copy(cache.request.filters);
            $scope.appliedFilters = angular.copy($scope.filters);
        }
        
    }
    
    function load(){
        
        var ffpId = null;
        
        if(angular.isDefined($routeParams.ffpid)){
            
            ffpId = parseInt($routeParams.ffpid, 10);
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                $scope.ffp = angular.copy(ffp);
            });
            
            loadRulesConfig(ffpId);
            
        }else{
            kkRoutesService.redirect('ffps');
        }
        
        collectionService.getConfig('ffp/config.json?v=1', 'ffp').then(function(config){
            
            angular.extend($scope.config, config);
            $scope.paged.itemsPerPage = $scope.config.pagination.default;
            
            loadParamsFromCache();
            loadItems(ffpId);
            
        });
        
    }
    
    load();
    
}])

.controller('FfpRuleCodeshareFilterModalController', ['$scope', '$modalInstance', 'filters', 'config', function($scope, $modalInstance, filters, config){
    
    $scope.config = config;
    $scope.filters = filters;
    
    $scope.submit = function(){
        $modalInstance.close($scope.filters);
    };
    
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    
    $scope.toggleFilter = function(toggle){
        
        $scope.filters.codeshareDev = toggle;
        
        if(!toggle){
            $scope.submit();
        }
    };
    
}]);