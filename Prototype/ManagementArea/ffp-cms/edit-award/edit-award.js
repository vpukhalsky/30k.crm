'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpAwardEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', 'applyToAll', '$modal', '$timeout', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, applyToAll, $modal, $timeout){
    
    kkRoutesService.preventReload($scope, 'FfpAwardEditController');
    
    $scope.item = null;
    $scope.isNew = true; 
    
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.paged = null;
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    $scope.ruleCodePattern = ffpCmsHelper.getRuleCodePattern();
    $scope.dateRangePatern = ffpCmsHelper.getDateRangePattern(); 
    
    $scope.section = {
        codeshare: true,
        geo: true,
        validity: true,
        comment: true,
        legend: true
    };
    
    $scope.toggleSection = function(section){
        $scope.section[section] = ! $scope.section[section];
    };
    
    $scope.editRule = function(rule){
        
        editRule(rule);
        
    };
    
    $scope.addRule = function(){
        
        var rule = angular.copy($scope.config.defaultAwardRule);
        editRule(rule, true);
        
    };
    
    function editRule(rule, isNew){
        
        $modal.open({
            templateUrl: 'ffp-cms/edit-award/edit-rule-modal.html',
            controller: 'AwardRuleEditModalController',
            resolve: {
                rule: function(){ return angular.copy(rule); },
                isNew: function(){ return isNew; },
            },
            windowClass: 'edit-redemption-rule medium',
        }).result.then(function(r){
            if(!isNew) angular.extend(rule, r);
            else $scope.item.rules.push(r);
        });
        
    }
    
    $scope.removeRule = function(index){
        $scope.item.rules.splice(index);
    };
    
    $scope.onDateChange = function(period, prop, event){
        
        $timeout(function(){
            if($(event.target).val() == '') period[prop] = null;
        });
        
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(response){
            
            kkRoutesService.redirect('awards', {
                ffpid: $scope.ffp.id
            });
            
        });
        
    };
    
    function reset(){
        
        $scope.item = toEdit(angular.copy($scope.config.defaultAward));
        $scope.isNew = true;
        $scope.paged = null;
        
    }
    
    function submit(callback){
        var item = toSave($scope.item);
        collectionService.saveItem('award/save-award.json', 'award', item).then(callback);
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem('award/delete-award.json', 'award', item).then(function(response){
            if(response){
                kkRoutesService.redirect('awards', {
                    ffpid: $scope.ffp.id
                });
            } 
            
        });
    };
    
    function isCodeshareDirty(item){
        
        var mr = $scope.config.defaultAward.marketingRules;
        var op = $scope.config.defaultAward.operatingRules;
        
        if(item.marketingRules.length != mr.length || item.operatingRules != op.length) return true;
        
        for(var i = 0 ; i < mr.length ; i++){
            if(item.marketingRules.indexOf(mr[i]) < 0) return true;
        }
        
        for(var i = 0 ; i < op.length ; i++){
            if(item.operatingRules.indexOf(op[i]) < 0) return true;
        }
        
        return false;
    }
    
    function toEdit(item){
        
        $scope.section = {
            codeshare: isCodeshareDirty(item) || item.flightNoExcl.length > 0 || item.flightNoLimit.length > 0 || item.excludeCodeshare,
            geo: item.geographicalRules.length > 0 || item.codedConditions.length > 0,
            validity: (angular.isObject(item.flight) && (item.flight.from || item.flight.until)) || (angular.isObject(item.purchase) && (item.purchase.from || item.purchase.until)) 
                || (angular.isArray(item.excludedDepartures) && item.excludedDepartures.length > 0) || (angular.isArray(item.excludedArrivals) && item.excludedArrivals.length > 0),
            comment: angular.isString(item.comment),
            legend: angular.isString(item.legend) || item.hasLegend
        };
        
        item.rules.forEach(function(rule){
            if(!angular.isArray(rule.gril)) rule.gril = [];
            if(!angular.isArray(rule.seasonalDates)) rule.seasonalDates = [];
        });
        
        if(!item.purchase) item.purchase = {};
        if(angular.isString(item.purchase.from)) item.purchase.from = new Date(item.purchase.from);
        if(angular.isString(item.purchase.until)) item.purchase.until = new Date(item.purchase.until);
        
        if(!item.flight) item.flight = {};
        if(angular.isString(item.flight.from)) item.flight.from = new Date(item.flight.from);
        if(angular.isString(item.flight.until)) item.flight.until = new Date(item.flight.until);
        
        return item;
        
    }
    
    function toSave(item){
        
        var i = angular.copy(item); 
        
        var localDate = $filter('localDate');
        
        if(angular.isObject(i.flight)){
            i.flight.from = localDate(i.flight.from);
            i.flight.until = localDate(i.flight.until);
        }
        
        if(angular.isObject(i.purchase)){
            i.purchase.from = localDate(i.purchase.from);
            i.purchase.until = localDate(i.purchase.until);
        }

        return i; 
        
    }
    
    $scope.onSelectPage = function(page){
        
        var cache = ffpCmsHelper.getAwardsCache();
        
        if(!angular.isObject(cache)) return;
        
        var items = cache.response.items;
        var params = cache.request;
        
        var collectionPage = Math.ceil(page / params.itemsPerPage);
        
        if(collectionPage - params.page == 0){
            
            var indexOf = $filter('indexOf');
            var index = indexOf(items, $scope.item.id);
            var diff = $scope.paged.page - page;
            index -= diff;
            loadItem(items[index].id);
            
        }else{
            
            var p = angular.copy(params);
            p.page = collectionPage;
            
            loadCollection(p).then(function(collection){
                
                var index = page - (p.page * p.itemsPerPage) - 1;
                if(index < collection.length && index >= 0) $scope.item = toEdit(angular.copy(collection[index]));
                
            });
            
        }
    };
    
    function loadCollection(params){
        
        return collectionService.getPagedCollection('award/get-awards.json', 'award', params).then(function(response){
            
            ffpCmsHelper.setAwardsCache({
                request: params,
                response: response
            });
            
            return response.items;
            
        });
        
    }
    
    function loadItem(id){
        
        collectionService.getItem('award/get-award.json', 'award', id).then(function(item){
            
            $scope.item = toEdit(angular.copy(item));
            $scope.isNew = false;
            
            var cache = ffpCmsHelper.getAwardsCache();
            
            if(angular.isObject(cache)){
                
                var items = cache.response.items;
                var params = cache.request;
                
                var indexOf = $filter('indexOf');
                var index = indexOf(items, item.id);
                var page = params.itemsPerPage * (params.page - 1) + index + 1;
                
                if(index >= 0){
                    
                    $scope.paged = {
                        page: page,
                        itemsPerPage: 1,
                        totalItems: cache.response.totalItems
                    };
                    
                }
            }
            
        });
    }

    function load(){
        
        applyToAll.reset();
        
        collectionService.getConfig('ffp/config.json?v=1', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
        if(angular.isDefined($routeParams.ffpid)){
            
            var ffpId = parseInt($routeParams.ffpid, 10);
            
            ffpCmsHelper.getAwardsConfig(ffpId).then(function(config){
                angular.extend($scope.config, config);
            });
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                
                $scope.ffp = angular.copy(ffp);
                
                if(angular.isDefined($routeParams.id)){
                    
                    var id = $routeParams.id;
                    loadItem(id); 
                    
                    
                }else{
                    
                    reset();
                    
                }
                
            });
            
        }else{
            kkRoutesService.redirect('ffps');
        }
        
    }
    
    load();
    
}]);