'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpBenefitRuleCollectionController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', '$timeout', 'applyToAll', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, $timeout, applyToAll){
    
    $scope.items = null;
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.paged = {
        page: 1,
        totalItems: null, 
        itemsPerPage: 50,
        orderBy: 'id',
        benefit: null,
        status: null,
        name: null,
        marketingAirlines: [],
        operatingAirlines: [],
        operatingSpecificAirlines: [],
        marketingSpecificAirlines: []
    };
    
    $scope.brandedFares = [];
    
    $scope.$watchCollection('items', function(items){
        
        if(angular.isArray(items) && items.length > 0) loadBrandedFares(items);
        
    });
    
    $scope.onFilterChange = function(){
        
        if($scope.paged.marketingAirlines.indexOf('spe') < 0){
            $scope.paged.marketingSpecificAirlines.splice(0, $scope.paged.marketingSpecificAirlines.length);
        }
        
        if($scope.paged.operatingAirlines.indexOf('spe') < 0){
            $scope.paged.operatingSpecificAirlines.splice(0, $scope.paged.operatingSpecificAirlines.length);
        }
        
        update(1);
        
    };
    
    $scope.getRuleIdClass = function(rule){
        
        var cls = [];
        
        var fromdates = [];
        var tilldates = [];
        
        var past = 0;
        var future = 0;

        var now = new Date();
        
        if(rule.purchase){
            if(rule.purchase.from) fromdates.push(new Date(rule.purchase.from));
            if(rule.purchase.until) tilldates.push(new Date(rule.purchase.until));
        }

        if(rule.flight){
            if(rule.flight.from) fromdates.push(new Date(rule.flight.from));
            if(rule.flight.until) tilldates.push(new Date(rule.flight.until));
        }

        fromdates.forEach(function(date){
            if (date.valueOf() > now.valueOf()) future++;
        });
            
        if (fromdates.length > 0 && fromdates.length == future) {
            cls.push('future');
            return cls;
        }
        
        tilldates.forEach(function (date) {
            if (date.valueOf() <= now.valueOf()) past++;
        });

        if (tilldates.length > 0 && tilldates.length == past) {
            cls.push('past');
            return cls;
        }
      
        if (tilldates.length > 0 || fromdates.length > 0) {
            cls.push('now');
        }
        
        return cls;
    };
    
    $scope.areCodeshareRulesDirty = function(item){
        
        return !(item.marketingRules.length == 1 && item.operatingRules.length == 1 
        && item.marketingRules.indexOf('own') >= 0 && item.operatingRules.indexOf('own') >= 0);
          
    };
    
    function areFiltersEmpty(){
        return $scope.paged.orderBy == $scope.config.defaultBenefitsOrderBy 
            && !$scope.paged.status 
            && !$scope.paged.benefit 
            && $scope.paged.itemsPerPage == $scope.config.pagination.default
            && !$scope.paged.name
            && $scope.paged.marketingAirlines.length <= 0 
            && $scope.paged.operatingAirlines.length <= 0
            && $scope.paged.marketingSpecificAirlines.length <= 0
            && $scope.paged.operatingSpecificAirlines.length <= 0;
    }
    
    $scope.onSelectPage = function(page){
        update(page);
    };
    
    $scope.onItemsPerPageChange = function(){
        update(1);
    };
    
    function update(page){
        
        var params = {
            ffpid: $scope.paged.ffpid
        };
        
        var route = 'benefitRules';
        
        if(page && page != 1){
            params.page = page;
            route = 'pagedBenefitRules';
        }
        
        if(!areFiltersEmpty()) {
            
            if(params.page) route = 'searchPagedBenefitRules';
            else route = 'searchBenefitRules';
            
            params.filter = $scope.paged;
			
			if (!params.filter.name)
            {
                params.filter.name = ''; // to avoid "null" string in name
            }
            
            delete params.filter.page;
            delete params.filter.totalItems;
            
        }

        kkRoutesService.redirect(route, params);
        
    }
    
    $scope.$watchCollection('selected.items', function(newValue){
        
        if(!angular.isArray($scope.items)) return;
        if(newValue.length >= $scope.items.length) $scope.selected.allNone = 'all';
        else $scope.selected.allNone = 'none';
        
    });
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
        
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem('benefit-rule/delete-benefit-rule.json', 'benefit-rule', item);
    };
    
    $scope.deleteItems = function(){
        
        var items = $scope.selected.items;
        collectionService.deleteItems('benefit-rule/delete-benefit-rules.json', 'benefit-rule', items);
         
    };
    
    $scope.cloneItem = function(index){
        
        var id = $scope.items[index].id;
        var clone = angular.copy($scope.items[index]);
        delete clone.id;
        
        collectionService.saveItem('benefit-rule/clone-benefit-rule.json', 'benefit-rule', clone).then(function(response){
            
            replaceClone(id, response.item);
            
        });
        
    };
    
    function replaceClone(originalId, clone){
        
        var indexOf = $filter('indexOf');
        var cloneIndex = indexOf($scope.items, clone.id);
        $scope.items.splice(cloneIndex, 1);
        var originalIndex = indexOf($scope.items, originalId);
        $scope.items.splice(originalIndex + 1, 0, clone);
        
    }
    
    $scope.cloneItems = function(){
        
        var ids = [];
        
        $scope.selected.items.forEach(function(item){
            ids.push(item.id);
        });
        
        var clones = angular.copy($scope.selected.items);
        
        clones.forEach(function(clone){
            delete clone.id;
        });
        
        collectionService.saveItems('benefit-rule/clone-benefit-rules.json', 'benefit-rule', clones).then(function(response){
            
            for(var i = 0 ; i < response.items.length ; i++){
                if(ids[i]) replaceClone(ids[i], response.items[i]);
            }
            
        });
    };
    
    $scope.applyToAll = function(){
        
        return applyToAll.isActive();
        
    };
    
    $scope.enableApplyToAll = function(){
        
        $scope.items.forEach(function(item){
            applyToAll.lock(item);
        });
        
        applyToAll.setCollection($scope.selected.items, $scope.ffp);
        
    };
    
    $scope.disableApplyToAll = function(){
        
        applyToAll.reset();
        
    };
    
    $scope.applyToAllSync = function(item, prop){
        
        applyToAll.apply(item, prop);
        
    };
    
    $scope.saveItems = function(form){
        
        if(!form.$valid) return;
        
        applyToAll.save('benefit-rule/save-benefit-rules.json', 'benefit-rule').then(function(response){
             
        });
        
    };
    
    $scope.preventDefault = function(event){
        event.stopPropagation();
    };
    
    function loadItems(){
        
        var params = angular.copy($scope.paged);
        delete params.totalItems;
        
        collectionService.getPagedCollection('benefit-rule/get-benefit-rules.json?v=2', 'benefit-rule', params).then(function(response){
            
            $scope.items = response.items;
            $scope.paged.page = response.page;
            $scope.paged.totalItems = response.totalItems;
            
            ffpCmsHelper.setBenefitsCache({
                request: params,
                response: response
            });
            
        });
        
    }
    
    function loadBrandedFares(rules){
        
        ffpCmsHelper.loadBrandedFares(rules).then(function(response){
            
            $scope.brandedFares.splice(0, $scope.brandedFares.length);
            
            for(var i = 0 ; i < response.length ; i++){
                $scope.brandedFares.push(response[i]);
            }
            
        });
        
    }
    
    function load(){
        
        if(angular.isNumber($scope.paged.ffpid)){
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', $scope.paged.ffpid).then(function(ffp){
                $scope.ffp = angular.copy(ffp);
            }); 
            
            ffpCmsHelper.getBenefitsConfig($scope.paged.ffpid).then(function(config){
                angular.extend($scope.config, config);
            });
            
        }else{
            kkRoutesService.redirect('ffps');
        }
        
        collectionService.getConfig('ffp/config.json?v=2', 'ffp').then(function(config){
            
            angular.extend($scope.config, config);
            if(!$scope.paged.itemsPerPage) $scope.paged.itemsPerPage = $scope.config.pagination.default;
            if(!$scope.paged.orderBy) $scope.paged.orderBy = $scope.config.defaultBenefitsOrderBy;

            loadItems();
            
        });
        
    }
    
    function getParams(){
        
        var params = kkRoutesService.getParams({
            page: 'int',
            itemsPerPage: 'int',
            benefit: 'int',
            status: 'int',
            orderBy: 'string',
            ffpid: 'int',
            name: 'string',
            marketingAirlines: 'array',
            operatingAirlines: 'array',
            marketingSpecificAirlines: 'array',
            operatingSpecificAirlines: 'array'
        });
        
        delete params.filter;
        angular.extend($scope.paged, params);

    }
    
    getParams();
    load();
    
}]);