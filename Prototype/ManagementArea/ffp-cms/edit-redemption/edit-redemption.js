'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpRedemptionEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', 'applyToAll', '$modal', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, applyToAll, $modal){
    
    kkRoutesService.preventReload($scope, 'FfpRedemptionEditController');
    
    $scope.item = null;
    $scope.isNew = true; 
    
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.paged = null;
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    
    $scope.editRuleRange = function(rule){
        
        $modal.open({
            templateUrl: 'ffp-cms/range-modal.html',
            controller: 'RuleRangeModalController',
            resolve: {
                rule: function(){ return angular.copy(rule); }
            },
            windowClass: 'medium',
        }).result.then(function(r){
            angular.extend(rule, r);
        });
        
    };
    
    $scope.removeRule = function(index){
        $scope.item.rules.splice(index, 1);
    };
    
    $scope.addRule = function(){
        $scope.item.rules.push({
            ranges: []
        });
    };
    
    $scope.getButtonGroupClass = function(button, mod){
        return ffpCmsHelper.getButtonGroupClass(button, mod);
    };
    
    $scope.toggleCarrier = function(carrier){
        
        var index = $scope.item.carriers.indexOf(carrier.code);
        if(index >= 0) $scope.item.carriers.splice(index, 1);
        else $scope.item.carriers.push(carrier.code);
        
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(response){
            
            kkRoutesService.redirect('redemptions', {
                ffpid: $scope.ffp.id
            });
            
        });
        
    };
    
    function reset(){
        
        $scope.item = toEdit(angular.copy($scope.config.defaultRedemption));
        $scope.isNew = true;
        $scope.paged = null;
        
    }
    
    function submit(callback){
        var item = toSave($scope.item);
        collectionService.saveItem('redemption/save-redemption.json', 'redemption', item).then(callback);
    }
    
    $scope.deleteRedemption = function(item){
        collectionService.deleteItem('redemption/delete-redemption.json', 'redemption', item).then(function(response){
            if(response){
                kkRoutesService.redirect('redemptions', {
                    ffpid: $scope.ffp.id
                });
            } 
            
        });
    };
    
    function toEdit(item){
        
        item.editableFromRdbs = angular.isArray(item.fromRdbs) ? item.fromRdbs.join('') : '';
        item.editableToRdbs = angular.isArray(item.toRdbs) ? item.toRdbs.join('') : '';
        
        item.specificCarrierCodes = [];
        item.specificCarriers.forEach(function(c){
           item.specificCarrierCodes.push(c.code);
        });
        return item;
        
    }
    
    function toSave(item){
        
        var i = angular.copy(item);
        
        i.fromRdbs = i.editableFromRdbs.split('');
        i.toRdbs = i.editableToRdbs.split('');
        delete i.editableFromRdbs;
        delete i.editableToRdbs;
        
        if(i.carriers.indexOf('spec') <= 0) i.specificCarrierCodes = [];
        
        i.specificCarriers = [];
        i.specificCarrierCodes.forEach(function(c){
           i.specificCarriers.push({
               code: c,
               relationship: 'private'
           });
        });
        
        if(i.type == 'award'){
            if(i.upgradeType) delete i.upgradeType;
            if(i.fromCabin) delete i.fromCabin;
            if(i.toCabin) delete i.toCabin;
            if(i.toRdbs) delete i.toRdbs;
            if(i.fromRdbs) delete i.fromRdbs;
        }else if(i.type == 'upgrade'){
            if(i.bookingClass) delete i.bookingClass;
            if(i.bookingClassAlias) delete i.bookingClassAlias;
        }
        
        delete i.specificCarrierCodes;
        
        return i; 
        
    }
    
    $scope.onSelectPage = function(page){
        
        var cache = ffpCmsHelper.getRedemptionsCache();
        
        if(!angular.isObject(cache)) return;
        
        var items = cache.response.items;
        var params = cache.request;
        
        var collectionPage = Math.ceil(page / params.itemsPerPage);
        
        if(collectionPage - params.page == 0){
            
            var indexOf = $filter('indexOf');
            var index = indexOf(items, $scope.item.id);
            var diff = $scope.paged.page - page;
            index -= diff;
            loadItem(items[index].id);
            
        }else{
            
            var p = angular.copy(params);
            p.page = collectionPage;
            
            loadCollection(p).then(function(collection){
                
                var index = page - (p.page * p.itemsPerPage) - 1;
                if(index < collection.length && index >= 0) $scope.item = toEdit(angular.copy(collection[index]));
                
            });
            
        }
    };
    
    function loadCollection(params){
        
        return collectionService.getPagedCollection('redemption/get-redemptions.json', 'redemption', params).then(function(response){
            
            ffpCmsHelper.setRedemptionsCache({
                request: params,
                response: response
            });
            
            return response.items;
            
        });
        
    }
    
    function loadItem(id){
        
        collectionService.getItem('redemption/get-redemption.json', 'redemption', id).then(function(item){
            
            $scope.item = toEdit(angular.copy(item));
            $scope.isNew = false;
            
            var cache = ffpCmsHelper.getRedemptionsCache();
            
            if(angular.isObject(cache)){
                
                var items = cache.response.items;
                var params = cache.request;
                
                var indexOf = $filter('indexOf');
                var index = indexOf(items, item.id);
                var page = params.itemsPerPage * (params.page - 1) + index + 1;
                
                if(index >= 0){
                    
                    $scope.paged = {
                        page: page,
                        itemsPerPage: 1,
                        totalItems: cache.response.totalItems
                    };
                    
                }
            }
            
        });
    }

    function load(){
        
        applyToAll.reset();
        
        collectionService.getConfig('ffp/config.json?v=1', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
        if(angular.isDefined($routeParams.ffpid)){
            
            var ffpId = parseInt($routeParams.ffpid, 10);
            
            ffpCmsHelper.getRedemptionConfig(ffpId).then(function(config){
                angular.extend($scope.config, config);
            });
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                
                $scope.ffp = angular.copy(ffp);
                
                if(angular.isDefined($routeParams.id)){
                    
                    var id = $routeParams.id;
                    loadItem(id); 
                    
                    
                }else{
                    
                    reset();
                    
                }
                
            });
            
        }else{
            kkRoutesService.redirect('ffps');
        }
        
    }
    
    load();
    
}]);