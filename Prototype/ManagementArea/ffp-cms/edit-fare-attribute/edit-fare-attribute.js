'use strict';

angular.module('30k.ma.ffp-cms')

.controller('FfpFareAttributeRuleEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', 'ffpCmsHelper', '$modal', '$timeout', 'editFareAttributeService', 'findFilter', 'applyToAll', '$q', function($scope, collectionService, kkRoutesService, $routeParams, $filter, ffpCmsHelper, $modal, $timeout, editFareAttributeService, findFilter, applyToAll, $q){
    
    $scope.item = null;
    $scope.isNew = true; 
    
    $scope.config = {};
    $scope.ffp = null;
    
    $scope.paged = null;
    
    $scope.applyToAll = false; 
    $scope.applyTo = null;
    
    $scope.airlineCodePattern = ffpCmsHelper.getAirlineCodePattern();
    $scope.ruleCodePattern = ffpCmsHelper.getRuleCodePattern();
    $scope.dateRangePatern = ffpCmsHelper.getDateRangePattern(); 
    
    $scope.brandedFares = [];
    $scope.fareAttributes = [];
    
    $scope.section = {
        codeshare: true,
        geo: true,
        validity: true,
        comment: true
    };
    
    $scope.$watch('item.purchase.from', function(newValue, oldValue){
        if((angular.isObject(newValue) || (newValue === null && angular.isObject(oldValue))) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'purchase', 'from');
    });
    
    $scope.$watch('item.purchase.until', function(newValue, oldValue){
        if((angular.isObject(newValue) || (newValue === null && angular.isObject(oldValue))) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'purchase', 'until');
    });
    
    $scope.$watch('item.flight.from', function(newValue, oldValue){
        if((angular.isObject(newValue) || (newValue === null && angular.isObject(oldValue))) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'flight', 'from');
    });
    
    $scope.$watch('item.flight.until', function(newValue, oldValue){
        if((angular.isObject(newValue) || (newValue === null && angular.isObject(oldValue))) && applyToAll.isActive()) applyToAll.applyDate($scope.item, 'flight', 'until');
    });
    
    $scope.$watchCollection('item.marketingRules', function(value){
        updateBrandedFaresCodeshare('marketingRules', value);
    }); 
    
    $scope.$watchCollection('item.operatingRules', function(value){
        updateBrandedFaresCodeshare('operatingRules', value);
    });
    
    var brandedFaresWatcher = null;
    
    function addWatcherToBrandedFares(){
        
        brandedFaresWatcher = $scope.$watchCollection('item.brandedFares', function(newValue, oldValue){
            
            if(angular.isArray(newValue) && JSON.stringify(newValue) != JSON.stringify(oldValue) && applyToAll.isActive()){
                applyToAll.apply($scope.item, 'brandedFares');
            }
        });
        
    }
    
    $scope.preventDefault = function(event){
        event.stopPropagation();
    };
    
    $scope.$watch('item.brandedFaresActive', function(active){
        
        if(active && angular.isObject($scope.config)){
            
            $scope.item.marketingRules.splice(0, $scope.item.marketingRules.length);
            $scope.item.operatingRules.splice(0, $scope.item.operatingRules.length);
            $scope.item.marketingRules.push($scope.config.brandedFareAllowedCodeshare);
            $scope.item.operatingRules.push($scope.config.brandedFareAllowedCodeshare);
            
            updateBrandedFares($scope.item);
            
        }
        
    });
    
    $scope.$watch('item.airline.code', function(code){
        
        updateBrandedFares($scope.item);
        
    });
    
    $scope.getFareAttributes = function(){
        
        var result = [];
        
        $scope.fareAttributes.forEach(function(i){
            
            var j = $filter('find')($scope.config.fareAttributes, i.code, 'code');
            if(j) result.push(j);
            
        });
        
        return result;
        
    };
    
    function updateBrandedFaresCodeshare(name, codeshare){
        
        if(angular.isArray(codeshare) && angular.isObject($scope.item)){
            $scope.applyToAllSync(name);
        }
        
        if(angular.isObject($scope.config) && angular.isObject($scope.item) && $scope.item.brandedFaresActive 
            && (codeshare.length > 1 || codeshare.indexOf($scope.config.brandedFareAllowedCodeshare) != 0)
        ){
            $scope.item.brandedFaresActive = false;
            $scope.applyToAllSync('brandedFaresActive');
        }
        
    }
    
    function updateBrandedFares(item){

        if(!angular.isObject(item) || !angular.isString(item.airline.code) || $scope.loadingBrandedFares) return;
        
        $scope.loadingBrandedFares = true;
        
        collectionService.flushCollection('branded-fare');
        collectionService.flushCollection('fare-attribute-' + item.airline.code);
        
        var promise1 = collectionService.getCollection('fare-attribute-rule/fare-attributes.json', 'fare-attribute-' + item.airline.code, {
            airline: item.airline.code
        });
        
        var promise2 = collectionService.getCollection('rule/branded-fares.json', 'branded-fare', {
            airline: item.airline.code
        });
        
        return $q.all([promise1, promise2]).then(function(arr){
            
            var fa = arr[0];
            var bf = arr[1];
            
            $scope.brandedFares.splice(0, $scope.brandedFares.length);
            $scope.fareAttributes.splice(0, $scope.fareAttributes.length);
            
            for(var i = 0 ; i < bf.length ; i++){
                $scope.brandedFares.push(bf[i]);
            }
            
            for(var i = 0 ; i < fa.length ; i++){
                $scope.fareAttributes.push(fa[i]);
            }
            
            excludeItemsBasedOnAirline('brandedFares');
            excludeItemsBasedOnAirline('fareAttributes');
            
            $scope.loadingBrandedFares = false;
            
            return null;
            
        });
        
    }
    
    function excludeItemsBasedOnAirline(property){
        
        var indexOf = $filter('indexOf');
        
        // check if saved branded fares are consistent with collection of available ones and remove which are not
        if(angular.isArray($scope.item[property]) && $scope.item[property].length > 0){
            
            var allowed = [];
            
            for(var i = 0 ; i < $scope.item[property].length ; i++){
                if(indexOf($scope[property], $scope.item[property][i], 'code') >= 0) allowed.push($scope.item[property][i]);
            }
            
            if(allowed.length != $scope.item[property].length){
                
                $scope.item[property].splice(0, $scope.item[property].length);
                for(var i = 0 ; i < allowed.length ; i++) $scope.item[property].push(allowed[i]);
                
            }
            
        }
        
    }
    
    $scope.applyToAll = function(){
        return applyToAll.isActive();
    };
    
    $scope.applyToAllSync = function(prop){ 
        
        if(applyToAll.isActive()) applyToAll.apply($scope.item, prop);
        
    };
    
    $scope.applyToAllSyncAirline = function(prop){
        if(applyToAll.isActive()) applyToAll.applyAirline($scope.item, prop);
    };
    
    $scope.onFareAttributeChange = function(id){
        
        updateFareAttribute(id);
        
    };
    
    $scope.onExcludeCodeshareChange = function(exclude){
        
        if(exclude){
            while($scope.item.operatingRules.length > 0) $scope.item.operatingRules.splice(0, 1);
        }
        
    };
    
    $scope.toggleSection = function(section){
        $scope.section[section] = ! $scope.section[section];
    };
    
    $scope.onDateChange = function(period, prop, event){
        
        $timeout(function(){
            if($(event.target).val() == '') period[prop] = null;
        });
        
    };
    
    function areFiltersEmpty(params){
        
        return params.orderBy == $scope.config.defaultFareAttributesOrderBy
            && !params.code
            && params.marketingAirlines.length <= 0 
            && params.operatingAirlines.length <= 0
            && params.marketingSpecificAirlines.length <= 0
            && params.operatingSpecificAirlines.length <= 0
            && !params.name
            && params.airlines.length <= 0
            && !params.fareAttribute
            && !params.atpco
            && params.itemsPerPage == $scope.config.pagination.default;
        
    }
    
    function redirectToCollection(){
        
        var cache = ffpCmsHelper.getFareAttributesCache();
        var name = 'fareAttributeRules';
        
        var params = {
            ffpid: $scope.ffp.id
        };
        
        if(angular.isObject(cache) && angular.isObject(cache.request)){
            
            if(cache.request.page && cache.request.page != 1){
                name = 'pagedFareAttributeRules';
                params.page = cache.request.page;
            }
            
            if(!areFiltersEmpty(params)){
                
                if(params.page && params.page != 1){
                    name = 'searchPagedFareAttributeRules';
                }else{
                    name = 'searchFareAttributeRules';
                }
                
                params.filter = angular.copy(cache.request);
                if(!params.filter.name) params.filter.name = '';
                delete params.filter.page;
                delete params.filter.totalItems;
                
            }
            
        }
        
        kkRoutesService.redirect(name, params);
        
    }
    
    function submit(form, callback){
        
        if(!form.$valid || ($scope.item.brandedFaresActive && $scope.item.brandedFares.length <= 0)) return;
        
        if(applyToAll.isActive()){
            
            callback();
            
        }else{
            
            editFareAttributeService.submit(toSave($scope.item)).then(function(){
                
                callback();
                
            });
            
        }
        
    }
    
    $scope.submit = function(form){
        
        submit(form, redirectToCollection);
        
    };
    
    function reset(){
        
        var i = angular.copy($scope.config.defaultFareAttribute);
        i.currencyCode = $scope.config.defaultCurrencyCode;
        $scope.item = toEdit(i);
        $scope.isNew = true;
        $scope.paged = null;
        
    }
    
    $scope.deleteItem = function(item){
        
        collectionService.deleteItem('fare-attribute-rule/delete-fare-attribute-rule.json', 'fare-attribute-rule', item).then(function(response){
            if(response){
                redirectToCollection();
            } 
            
        });
        
    };
    
    function isCodeshareDirty(item){
        
        var mr = $scope.config.defaultFareAttribute.marketingRules;
        var op = $scope.config.defaultFareAttribute.operatingRules;
        
        if(item.marketingRules.length != mr.length || item.operatingRules != op.length) return true;
        
        for(var i = 0 ; i < mr.length ; i++){
            if(item.marketingRules.indexOf(mr[i]) < 0) return true;
        }
        
        for(var i = 0 ; i < op.length ; i++){
            if(item.operatingRules.indexOf(op[i]) < 0) return true;
        }
        
        return false;
    }
    
    function toEdit(item){
        
        if(brandedFaresWatcher) {
            brandedFaresWatcher();
            brandedFaresWatcher = null;
        }
        
        $scope.section = {
            codeshare: isCodeshareDirty(item),
            geo: item.geographicalRules.length > 0,
            validity: (angular.isObject(item.flight) && (item.flight.from || item.flight.until)) || (angular.isObject(item.purchase) && (item.purchase.from || item.purchase.until)),
            comment: angular.isString(item.comment),
            atpco: angular.isString(item.atpco)
        };
        
        if(!item.purchase) item.purchase = {};
        if(angular.isString(item.purchase.from)) item.purchase.from = new Date(item.purchase.from);
        if(angular.isString(item.purchase.until)) item.purchase.until = new Date(item.purchase.until);
        
        if(!item.flight) item.flight = {};
        if(angular.isString(item.flight.from)) item.flight.from = new Date(item.flight.from);
        if(angular.isString(item.flight.until)) item.flight.until = new Date(item.flight.until);
        
		if(item.code) item.code = item.code.substring(5);
		else item.code = '';
		
		editFareAttributeService.init($scope);
		
		if(angular.isArray($scope.config.fareAttributes) && $scope.config.fareAttributes.length > 0){
		    
		    if(!angular.isString(item.fareAttribute)){
		        item.fareAttribute = $scope.config.fareAttributes[0].code;
		    }
		    
		    updateFareAttribute(item.fareAttribute);
		    
		}
		
		updateBrandedFares(item).then(function(){
            addWatcherToBrandedFares();
        });
		
        return item;
        
    }
    
    function updateFareAttribute(code){
        
        var fareAttribute = findFilter($scope.config.fareAttributes, code, 'code');
	    
	    if(fareAttribute && fareAttribute.code){
	        
	        $timeout(function(){
	            editFareAttributeService.inject(fareAttribute.code);
	        });
	        
	    }
        
    }
    
    function toSave(item){
        
        var i = angular.copy(item); 
        
        var localDate = $filter('localDate');
        
        if(angular.isObject(i.flight)){
            i.flight.from = localDate(i.flight.from);
            i.flight.until = localDate(i.flight.until);
        }
        
        if(angular.isObject(i.purchase)){
            i.purchase.from = localDate(i.purchase.from);
            i.purchase.until = localDate(i.purchase.until);
        }
		
		i.code = "RB" + $scope.ffp.code + i.code;
		
		costToSave(i);
         
        if(!i.brandedFaresActive && i.brandedFares) delete i.brandedFares;
        
        return i; 
        
    }
    
    function costToSave(item){
        
        switch(item.memberCostType){
            case 'free':
                item.memberCost = 0;
                break;
        }

        return item;
        
    }
    
    $scope.onSelectPage = function(page, form){
        
        submit(form, function(page){
            
            if(applyToAll.isActive()){
                
                var selected = applyToAll.getCollection();
                
                if(selected.length >= page){
                    kkRoutesService.redirect('editFareAttributeRule', { id: selected[page - 1].id, ffpid: $scope.ffp.id });
                }
                
            }else{
                
                var cache = ffpCmsHelper.getFareAttributesCache();
                
                if(!angular.isObject(cache)) return;
                
                var items = cache.response.items;
                var params = cache.request;
                
                var index = page % params.itemsPerPage - 1;
                kkRoutesService.redirect('editFareAttributeRule', { id: items[index].id, ffpid: $scope.ffp.id });
                
            }
        
        }.bind(this, page));
        
    };
    
    $scope.backToCollection = function(){
        
        redirectToCollection();
        
    };
    
    function loadCollection(params){
        
        return collectionService.getPagedCollection('fare-attribute-rule/get-fare-attribute-rules.json?v=1', 'fare-attribute-rule', params).then(function(response){
            
            ffpCmsHelper.setFareAttributesCache({
                request: params,
                response: response
            });
            
            return response.items;
            
        });
        
    }
    
    function loadItem(id){
        
        collectionService.getItem('fare-attribute-rule/get-fare-attribute-rule.json', 'fare-attribute-rule', id).then(function(item){
            
			$scope.isNew = false;
			var indexOf = $filter('indexOf');
			
			if(applyToAll.isActive()){
                
                $scope.item = toEdit(item._copy);
                var selected = applyToAll.getCollection();
                
                $scope.paged = {
                    page: indexOf(selected, item.id) + 1,
                    itemsPerPage: 1,
                    totalItems: selected.length
                };
                
            }else{
    			
                $scope.item = toEdit(angular.copy(item));
                
                var cache = ffpCmsHelper.getFareAttributesCache();
                
                if(angular.isObject(cache)){
                    
                    var items = cache.response.items;
                    var params = cache.request;
                    
                    var index = indexOf(items, item.id);
                    var page = params.itemsPerPage * (params.page - 1) + index + 1;
                    
                    if(index >= 0){
                        
                        $scope.paged = {
                            page: page,
                            itemsPerPage: 1,
                            totalItems: cache.response.totalItems
                        };
                        
                    }
                }
            }
        });
    }

    function load(){
        
        collectionService.getConfig('ffp/config.json?v=4', 'ffp').then(function(config){
            $scope.config = angular.copy(config);
        });
        
        if(angular.isDefined($routeParams.ffpid)){
            
            var ffpId = parseInt($routeParams.ffpid, 10);
            
            ffpCmsHelper.getFareAttributesConfig(ffpId).then(function(config){
                angular.extend($scope.config, config);
            });
            
            collectionService.getFullItem('ffp/get-ffp.json', 'ffp', ffpId).then(function(ffp){
                
                $scope.ffp = angular.copy(ffp);
                
                if(angular.isDefined($routeParams.id)){
                    
                    var id = $routeParams.id;
                    loadItem(id); 
                    
                }else{
                    
                    reset();
                    
                }
                
            }); 
            
        }else{
            kkRoutesService.redirect('ffps'); 
        }
        
    }
    
    load();
    
}]); 