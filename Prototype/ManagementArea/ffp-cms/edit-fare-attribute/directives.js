'use strict';

angular.module('30k.ma.ffp-cms')

.directive('kkFareAttributeTypeFlgml', ['editFareAttributeService', 'findFilter', function(editFareAttributeService, findFilter){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-fare-attribute/directives/fare-attribute-type-meal.html',
        scope: {
            item: '=',
            config: '='
        },
        controller: 'FareAttributeTypeMealController'
    };
    
}])

.directive('kkFareAttributeTypeXtrbgg', ['editFareAttributeService', 'findFilter', function(editFareAttributeService, findFilter){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-fare-attribute/directives/fare-attribute-type-baggage.html',
        scope: {
            item: '=',
            config: '='
        },
        controller: 'FareAttributeTypeBaggageController'
    };
    
}])

.directive('kkFareAttributeTypeXtrcbn', ['editFareAttributeService', 'findFilter', function(editFareAttributeService, findFilter){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-fare-attribute/directives/fare-attribute-type-cabin-baggage.html',
        scope: {
            item: '=',
            config: '='
        },
        controller: 'FareAttributeTypeBaggageController'
    };
    
}])

.directive('kkFareAttributeTypeSpcbgg', ['editFareAttributeService', 'findFilter', function(editFareAttributeService, findFilter){
    
    return {
        restrict: 'E',
        templateUrl: 'ffp-cms/edit-fare-attribute/directives/fare-attribute-type-special-baggage.html',
        scope: {
            item: '=',
            config: '='
        },
        controller: 'FareAttributeTypeBaggageController'
    };
    
}])

.controller('FareAttributeTypeBaggageController', ['$scope', '$element', 'editFareAttributeService', 'findFilter', function($scope, $element, editFareAttributeService, findFilter){
    
    var deregisterEvent = null;
    
    $scope.onHasQuantityChange = function(hasQuantity){
        
        if(hasQuantity) $scope.hasTotalWeight = false;
        if(!hasQuantity && !$scope.hasTotalWeight) $scope.hasWeight = false;
        
    };
    
    $scope.onHasWeight = function(hasWeight){
        
        if(hasWeight && !$scope.hasTotalWeight && !$scope.hasQuantity) $scope.hasQuantity = true;
        
    };
    
    $scope.onHasTotalWeight = function(hasTotalWeight){
        
        if(hasTotalWeight) $scope.hasQuantity = false;
        if(!hasTotalWeight && !$scope.hasQuantity) $scope.hasWeight = false;
        
    };
    
    $scope.onHasDimensions = function(hasDimensions){
        
        if(hasDimensions) $scope.hasTotalLength = false;
        
    };
    
    $scope.onHasTotalLengthChange = function(hasTotalLength){
        
        if(hasTotalLength) $scope.hasDimensions = false;
          
    };
    
    function onSubmitRequest(){
        
        var item = {
			conceptType: $scope.item.conceptType,
            quantity: ($scope.hasQuantity ? $scope.item.quantity : null),
            weight: ($scope.hasWeight ? $scope.item.weight : null),
            totalWeight: ($scope.hasTotalWeight ? $scope.item.totalWeight : null),
            weightMetricType: $scope.item.weightMetricType,
            sizeLength: ($scope.hasDimensions ? $scope.item.sizeLength : null),
            sizeHeight: ($scope.hasDimensions ? $scope.item.sizeHeight : null),
            sizeWidth: ($scope.hasDimensions ? $scope.item.sizeWidth : null),
            totalLength: ($scope.hasTotalLength ? $scope.item.totalLength : null),
            sizeMetricType: $scope.item.sizeMetricType,
            excludePersonalItem: $scope.item.excludePersonalItem,
            baggageType: ($scope.item.baggageType ? $scope.item.baggageType : null)
        };
        
        submit(item);
        
    }
    
    function submit(item){
        
        $scope.$emit('fare-attribute-submit-response', item);
        
    }
    
    function initFareAttribute(){
        
        $scope.fareAttribute = findFilter($scope.config.fareAttributes, $scope.item.fareAttribute, 'code');
        
        if(!angular.isObject($scope.fareAttribute) || !angular.isObject($scope.fareAttribute.defaultRule)) return;
        
        for(var i in $scope.fareAttribute.defaultRule){
            
            if(!$scope.fareAttribute.defaultRule.hasOwnProperty(i)) continue;
            
            var prop = $scope.fareAttribute.defaultRule[i];
            
            if($scope.item[i] === undefined){
                
                $scope.item[i] = prop;
                
            }
            
        }
        
    }
    
    function initScope(){
        
        if(angular.isNumber($scope.item.quantity)){
            $scope.hasQuantity = true;
        }
        
        if(angular.isNumber($scope.item.weight)){
            $scope.hasWeight = true;
        }
        
        if(angular.isNumber($scope.item.totalWeight)){
            $scope.hasTotalWeight = true;
        }
        
        if(angular.isNumber($scope.item.sizeLength) || angular.isNumber($scope.item.sizeHeight) || angular.isNumber($scope.item.sizeWidth)){
            $scope.hasDimensions = true;
        }
        
        if(angular.isNumber($scope.item.totalLength)){
            $scope.hasTotalLength = true;
        }
        
    }
    
    function init(){
        
        editFareAttributeService.register();
        
        deregisterEvent = $scope.$on('fare-attribute-submit-request', onSubmitRequest);
        
        $scope.$on('$destroy', function(){ 
            
            if(angular.isFunction(deregisterEvent)) deregisterEvent(); 
            
        });
        
        initFareAttribute();
        
        initScope();
        
    }
    
    init();
    
}])

.controller('FareAttributeTypeMealController', ['$scope', '$element', 'editFareAttributeService', 'findFilter', function($scope, $element, editFareAttributeService, findFilter){
    
    var deregisterEvent = null;
    
    function onSubmitRequest(){
        
        var item = {
            mealTypes: angular.copy($scope.item.mealTypes)
        };
        
        submit(item);
        
    }
    
    function submit(item){
        
        $scope.$emit('fare-attribute-submit-response', item);
        
    }
    
    function initFareAttribute(){
        
        $scope.fareAttribute = findFilter($scope.config.fareAttributes, $scope.item.fareAttribute, 'code');
        
        if(!angular.isObject($scope.fareAttribute) || !angular.isObject($scope.fareAttribute.defaultRule)) return;
        
        for(var i in $scope.fareAttribute.defaultRule){
            
            if(!$scope.fareAttribute.defaultRule.hasOwnProperty(i)) continue;
            
            var prop = $scope.fareAttribute.defaultRule[i];
            
            if($scope.item[i] === undefined){
                
                $scope.item[i] = prop;
                
            }
            
        }
        
    }
    
    function init(){
        
        editFareAttributeService.register();
        
        deregisterEvent = $scope.$on('fare-attribute-submit-request', onSubmitRequest);
        
        $scope.$on('$destroy', function(){ 
            
            if(angular.isFunction(deregisterEvent)) deregisterEvent(); 
            
        });
        
        initFareAttribute();
        
    }
    
    init();
    
}]);