'use strict';

angular.module('30k.ma.ffp-cms')

.service('editFareAttributeService', ['$compile', 'collectionService', function($compile, collectionService){
    
    var placeholder = '#fare-attribute-type-directive-placeholder';
    var template = '<kk-fare-attribute-type-{{code}} item="item" config="config"></kk-fare-attribute-type-{{code}}>';
    
    var childDirectives = 0;
    var submitted = [];
    var scope = null;
    var deregisterEvent = null;
    var submitPromise = null;
    
    function inject(fareAttributeCode){
        
        var container = $(placeholder);
        
        if(!angular.isString(fareAttributeCode) || container.size() <= 0 || !scope) return false;
        
        var tpl = template.split('{{code}}').join(fareAttributeCode.toLowerCase());
        
        var raw = angular.element(tpl);
        
        container.children().each(function(index, el){
            
            var c = $(el);
            var s = c.isolateScope();
            if(s) s.$destroy();
            
        }).remove();
        
        $compile(raw)(scope, function(el, $scope){
        
            container.append(el);
        
        });
        
    }
    
    function init(s){
        
        childDirectives = 0;
        scope = s;
        
        if(angular.isFunction(deregisterEvent)) deregisterEvent();
        
        deregisterEvent = scope.$on('fare-attribute-submit-response', onSubmit);
        
        
    }
    
    function register(){
        
        childDirectives++;
        
    }
    
    function completeSubmit(){
        
        var item = submitted.shift();
        
        for(var i = 0 ; i < submitted.length ; i++){
            
            $.extend(item, submitted[i]);
            
        }
        
        submitted = [];
        
        collectionService.saveItem('fare-attribute-rule/save-fare-attribute-rule.json', 'fare-attribute-rule', item).then(function(response){
            
            submitPromise.resolve(item, response);
            submitPromise = null;
            
        }, function(){
            
            submitPromise.reject();
            submitPromise = null;
            
        });
        
    }
    
    function onSubmit(event, item){
        
        submitted.push(item);
        
        if(submitted.length - 1 >= childDirectives){
            
            completeSubmit();
            
        }
        
    }
    
    function submit(item){
        
        submitPromise = $.Deferred();
        submitted = [item];
        
        if(childDirectives > 0){
            
            scope.$broadcast('fare-attribute-submit-request', item);
            
        }else{
            
            completeSubmit();
            
        }
        
        return submitPromise.promise();
        
    }
    
    return {
        inject: inject,
        init: init,
        register: register,
        submit: submit
    };
    
}]);