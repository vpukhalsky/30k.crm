'use strict';

angular.module('30k.ma.ffp-cms', [
    
    'mm.foundation',
    'checklist-model',
    
    '30k.admin-review',
    '30k.error',
    '30k.routes',
    '30k.service',
    '30k.tooltip',
    '30k.tag-input',
    '30k.period-picker',
    '30k.multiselect',
    '30k.fixed-table-header',
    '30k.fixed-block',
    '30k.crm.benefit-text',
    
    '30k.ma'
    
])

.service('ffpCmsHelper', ['collectionService', '$filter', '$q', function(collectionService, $filter, $q){
  
  var rules = null;
  var awards = null;
  var upgrades = null;
  var benefits = null;
  var fareAttributes = null;
  
  var airlineCodePattern = /^[a-zA-Z0-9][a-zA-Z0-9]$/;
  var ruleCodePattern = /^[a-zA-Z0-9]+$/;
  var dateRangePatern = /^(\d?\d\/\d?\d(\/\d{4})?)(\-\d?\d\/\d?\d(\/\d{4})?)?$/;
  
  function loadBrandedFares(rules){
    
    var airlines = {};
    var count = 0;
    
    for(var i = 0 ; i < rules.length ; i++){
        
        var rule = rules[i];
        
        if(!angular.isObject(rule) || !angular.isObject(rule.airline) 
            || !angular.isString(rule.airline.code) || !rule.brandedFaresActive 
            || !angular.isArray(rule.brandedFares) || rule.brandedFares.length <= 0) continue;
        
        if(!angular.isArray(airlines[rule.airline.code])) airlines[rule.airline.code] = [];
        
        for(var j = 0 ; j < rule.brandedFares.length ; j++){
        
          var ruleBrandedFare = rule.brandedFares[j];
        
          if(airlines[rule.airline.code].indexOf(ruleBrandedFare) < 0){
              
              airlines[rule.airline.code].push(ruleBrandedFare);
              count++;
              
          }
        
        }
        
    }
    
    if(count <= 0){
      var d = $q.defer();
      d.resolve([]);
      return d.promise;
    }
    
    collectionService.flushCollection('airline-branded-fares');
    
    return collectionService.getCollection('ffp/branded-fares.json', 'airline-branded-fares', {
        brandedFares: airlines
    });
    
  }
  
  function setRulesCache(data){
    rules = data;
    return rules;
  }
  
  function getRulesCache(){
    return rules;
  }
  
  function setAwardsCache(data){
    awards = data;
    return awards;
  }
  
  function setUpgradesCache(data){
    upgrades = data;
    return upgrades;
  }
  
  function setBenefitsCache(data){
    benefits = data;
    return benefits;
  }
  
  function setFareAttributesCache(data){
    fareAttributes = data;
    return fareAttributes;
  }
  
  function getFareAttributesCache(){
    return fareAttributes;
  }
  
  function getAwardsCache(){
    return awards;
  }
  
  function getUpgradesCache(){
    return upgrades;
  }
  
  function getBenefitsCache(){
    return benefits;
  }
  
  function getRulesConfig(ffpId){
    return collectionService.getConfig('rule/config.json', 'ffpRule' + ffpId, { ffpId: ffpId });
  }
  
  function getAwardsConfig(ffpId){
    return collectionService.getConfig('award/config.json', 'ffpAward' + ffpId, { ffpId: ffpId });
  }
  
  function getUpgradesConfig(ffpId){
    return collectionService.getConfig('upgrade/config.json', 'ffpUpgrade' + ffpId, { ffpId: ffpId });
  }
  
  function getBenefitsConfig(ffpId){
    return collectionService.getConfig('benefit-rule/config.json', 'benefit-rule' + ffpId, { ffpId: ffpId });
  }
  
  function getFareAttributesConfig(ffpId){
    return collectionService.getConfig('fare-attribute-rule/config.json', 'fare-attribute-rule' + ffpId, { ffpId: ffpId });
  }
  
  function getButtonGroupClass(buttons, modificator){
      
      var cls = {};
      
      if(!angular.isDefined(modificator)) modificator = 0;
      
      if(angular.isArray(buttons) && buttons.length > 0){
          
          var length = 8;
          if(buttons.length + modificator < length) length = buttons.length + modificator;
          if(length < 1) length = 1;
          cls['even-' + length] = true;
          
      }
      
      return cls;
  }
  
  function getAirlineCodePattern(){
    return airlineCodePattern;
  }
  
  function getDateRangePattern(){
    return dateRangePatern;
  }
  
  function areCodeshareRulesDirty(item, ffp){

    var codeshare = ffp.codeshareBase;
    var rel = item.airline.relationship;

    var mr = item.marketingRules;
    var or = item.operatingRules;

    var preset =
        (
            codeshare == 'op'
            &&
            (
                (rel == 'alliance' && mr.length == 2 && mr.indexOf('own') != -1 && mr.indexOf('alli') != -1)
                ||
                (rel == 'private' && mr.length == 1 && mr.indexOf('own'))
            )
            &&
            (or.length == 1 && or.indexOf('subs') != -1)
        )
        ||
        (
            codeshare == 'mk'
            &&
            (
                (rel == 'alliance' && or.length == 3 && or.indexOf('subs') != -1 && or.indexOf('own') != -1 && or.indexOf('alli') != -1)
                ||
                (rel == 'private' && or.length == 2 && or.indexOf('subs') != -1 && or.indexOf('own') != -1)
            )
            &&
            (mr.length == 0)
        );

    return !preset;
  }
  
  function initAccrualTooltipData(scope, config){
      
      var accrual = scope.accrual;
      var accrualType = $filter('find')(config.accrualTypes, accrual.type);
      var eb = $filter('find')(config.earningBases, accrual.earningBase, 'code');
      
      var data = {
          name: accrualType.name,
          alias: accrual.name.toLowerCase() != accrualType.name.toLowerCase() ? accrual.name : null,
          earningBase: eb.name
      };
      
      scope.accrualTooltip = angular.copy(data);
  }
  
  function getAccrualAbbr(accrual, config){
    
    var name = accrual.name;
    var accrualType = $filter('find')(config.accrualTypes, accrual.type);

    if (angular.isObject(accrualType)) {
        name = accrualType.name;

        if (name.toUpperCase() == 'POINTS') {
            name = 'Pts';
        }

        return name;
    }

    var words = name.split(' ');
    var abbr = '';
    
    words.forEach(function(word){
        abbr += word[0];
    });
      
    return abbr.toUpperCase();
      
  }
  
  function setupTier(tier, accurals){
    
    if(!angular.isArray(tier.qualification)) tier.qualification = [];
    if(tier.qualification.length < 1) tier.qualification.push({ gril: [], accruals: [] });
    
    for(var i = 0 ; i < tier.qualification.length ; i++){
      tier.qualification[i] = setupTierAccruals(tier.qualification[i], accurals);
    }
      
    if(!angular.isArray(tier.renewal)) tier.renewal = [];
    if(tier.renewal.length < 1) tier.renewal.push({ gril: [], accruals: [] });
    
    for(var i = 0 ; i < tier.renewal.length ; i++){
      tier.renewal[i] = setupTierAccruals(tier.renewal[i], accurals);
    }
    
    return tier;
  }
  
  function setupTierAccruals(tier, accruals){
      
      var find = $filter('find');
      
      var toAdd = [];
      
      accruals.forEach(function(accrual){
          if(accrual.id){
              var a = find(tier.accruals, accrual.id);
              if(a) toAdd.push(a);
              else toAdd.push({
                  id: accrual.id,
                  value: null
              });
          }
      });
      
      tier.accruals = toAdd;
      
      return tier;
  }
  
  function getRuleCodePattern(){
    return ruleCodePattern;
  }
  
  function sortRules(rules){
    
    var temp = $filter('orderBy')(rules, 'airline.code');
    rules.splice(0, rules.length);
    temp.forEach(function(i){ rules.push(i); });
    
  }
  
  return {
    getButtonGroupClass: getButtonGroupClass,
    getAirlineCodePattern: getAirlineCodePattern,
    getRuleCodePattern: getRuleCodePattern,
    getDateRangePattern: getDateRangePattern,
    setRulesCache: setRulesCache,
    setBenefitsCache: setBenefitsCache,
    getRulesCache: getRulesCache,
    getBenefitsCache: getBenefitsCache,
    areCodeshareRulesDirty: areCodeshareRulesDirty,
    getRulesConfig: getRulesConfig,
    getBenefitsConfig: getBenefitsConfig,
    setAwardsCache: setAwardsCache,
    setUpgradesCache: setUpgradesCache,
    getAwardsCache: getAwardsCache,
    getUpgradesCache: getUpgradesCache,
    getAwardsConfig: getAwardsConfig,
    getUpgradesConfig: getUpgradesConfig,
    initAccrualTooltipData: initAccrualTooltipData,
    getAccrualAbbr: getAccrualAbbr,
    setupTier: setupTier,
    setupTierAccruals: setupTierAccruals,
    sortRules: sortRules,
    loadBrandedFares: loadBrandedFares,
    setFareAttributesCache: setFareAttributesCache,
    getFareAttributesConfig: getFareAttributesConfig,
    getFareAttributesCache: getFareAttributesCache
  };
}])

.service('applyToAll', ['collectionService', '$filter', function(collectionService, $filter){
  
  var collection = [];
  var ffp = null;
  
  function lock(item){
    
    if(item.unlocked) item.unlocked = false;
    if(item._copy) delete item._copy;
    
  }
  
  function unlock(item){
    
    item.unlocked = true;
    item._copy = angular.copy(item);
    item._copy.leaveGeoRules = true;
    
  }
  
  function reset(lockFunc){
    
    $('body').removeClass('vertical-alert');
    
    if(!angular.isFunction(lockFunc)) lockFunc = lock;
    
    collection.forEach(function(item){
        lockFunc(item);
    });
    
    ffp = null;
    collection = [];
    
  }
  
  function setCollection(items, program, unlockFunc){
    
    reset();
    
    ffp = program;
    
    collection = items;
    
    $('body').addClass('vertical-alert');
    
    if(!angular.isFunction(unlockFunc)) unlockFunc = unlock;
    
    collection.forEach(function(item){
        unlockFunc(item);
    });
    
  }
  
  function getCollection(){
    
    return collection;
    
  }
  
  function toSave(item){
    
    if(!item._copy.leaveGeoRules) item._copy.geographicalRules = [];
    
    var localDate = $filter('localDate');
        
    if(angular.isObject(item._copy.flight)){
        item._copy.flight.from = localDate(item._copy.flight.from);
        item._copy.flight.until = localDate(item._copy.flight.until);
    }
    
    if(angular.isObject(item._copy.purchase)){
        item._copy.purchase.from = localDate(item._copy.purchase.from);
        item._copy.purchase.until = localDate(item._copy.purchase.until);
    }
    
    angular.extend(item, item._copy);
    
    delete item._copy;
    delete item.unlocked;
    
    return item;
    
  }
  
  function getProgram(){
    
    return ffp;
    
  }
  
  function save(url, type, toSaveFunc, resetFunc){
    
    if(!angular.isFunction(toSaveFunc)) toSaveFunc = toSave;
    if(!angular.isFunction(resetFunc)) resetFunc = reset;
    
    var rules = [];
    
    collection.forEach(function(item){
        rules.push(toSaveFunc(item));
    });
    
    return collectionService.saveItems(url, type, rules).then(function(response){
      
        resetFunc();
        return response;
        
    });
    
  }
  
  function apply(item, prop){
    
    var value = angular.isObject(item._copy) ? item._copy[prop] : item[prop];
    
    collection.forEach(function(rule){
        
        if(rule.id != item.id){
            
            if(angular.isArray(value)){
              
              if(!angular.isArray(rule._copy[prop])) rule._copy[prop] = [];
              
              rule._copy[prop].splice(0, rule._copy[prop].length);
              
              value.forEach(function(i){
                rule._copy[prop].push(i);
              });
              
            }else{
              rule._copy[prop] = angular.copy(value);
            }
            
        }
        
    });
    
  }
  
  function applyDate(item, type, which){
    
    var value = angular.copy(angular.isObject(item._copy) ? item._copy[type][which] : item[type][which]);
    
    collection.forEach(function(rule){
        
        if(rule.id != item.id){
            if(!angular.isObject(rule._copy[type])) rule._copy[type] = {};
            rule._copy[type][which] = angular.copy(value);
        }
        
    });
    
  }
  
  function applyAirline(item, prop){
    
    var value = angular.isObject(item._copy) ? item._copy.airline[prop] : item.airline[prop];
    
    collection.forEach(function(rule){
        if(rule.id != item.id){
            rule._copy.airline[prop] = angular.copy(value);
        }
    });
    
  }
  
  function isActive(){
    
    return angular.isArray(collection) && collection.length > 0;
    
  }
  
  return {
    reset: reset,
    lock: lock,
    unlock: unlock,
    setCollection: setCollection,
    getCollection: getCollection,
    toSave: toSave,
    getProgram: getProgram,
    save: save,
    apply: apply,
    applyDate: applyDate,
    applyAirline: applyAirline,
    isActive: isActive
  };
  
}])

.service('applyToAllEarningRules', ['applyToAll', 'ffpCmsHelper', '$filter', function(applyToAll, ffpCmsHelper, $filter){
  
  function unlock(item){
    
    var ret = applyToAll.unlock(item);
    
    item._copy.editableRdbs = item._copy.rdbs.join('');
    item._copy.leaveSubsidiaries = true;
    
    return ret;
    
  }
  
  function setCollection(items, program){
    
    return applyToAll.setCollection(items, program, unlock);
    
  }
  
  function toSave(item){
    
    item._copy.rdbs = item._copy.editableRdbs.split('');
    
    if(!item._copy.leaveSubsidiaries) item._copy.subsidiaries = [];
    
    var ffp = applyToAll.getProgram();
    
    if(ffp){
      item._copy.tiers.forEach(function(tier){
           tier = ffpCmsHelper.setupTier(tier, ffp.accruals);
      });
    }
    
    return applyToAll.toSave(item);
    
  }
  
  function save(){
    
    return applyToAll.save('rule/save-rules.json', 'rule', toSave);
    
  }
  
  function applyAccrual(item, accrual){
    
    var value = accrual.summary;
    var find = $filter('find');
    
    applyToAll.getCollection().forEach(function(r){
        
        var rule = r._copy;
      
        if(rule.id != item.id){
            
            var ruleAccrual = find(rule.rules, accrual.accrual, 'accrual');
            
            if(ruleAccrual) ruleAccrual.summary = value;
            else{
                rule.rules.push({
                    accrual: accrual.accrual,
                    summary: value,
                    items: []
                });
            }
        } 
    });
    
  }
  
  return {
    reset: applyToAll.reset,
    lock: applyToAll.lock,
    unlock: unlock,
    setCollection: setCollection,
    getCollection: applyToAll.getCollection,
    toSave: toSave,
    save: save,
    apply: applyToAll.apply,
    applyDate: applyToAll.applyDate,
    applyAccrual: applyAccrual,
    applyAirline: applyToAll.applyAirline,
    isActive: applyToAll.isActive
  };
  
}])

.controller('FfpTierEditController', ['$scope', '$modalInstance', 'tier', 'isNew', 'ffp', 'collectionService', 'ffpCmsHelper', function($scope, $modalInstance, tier, isNew, ffp, collectionService, ffpCmsHelper){
    
    $scope.tier = tier;
    $scope.isNew = isNew;
    $scope.ffp = ffp;
    
    collectionService.getConfig('ffp/config.json', 'ffp').then(function(config){
        $scope.config = angular.copy(config);
    });
    
    $scope.remove = function(){
        $modalInstance.close(false);
    };
    
    $scope.submit = function(form){
        if(!form.$valid) return;
        $modalInstance.close($scope.tier);
    };
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.removeOption = function(collection, index){
      
      collection.splice(index, 1);
      
    };
    
    $scope.addOption = function(collection){
      
      var opt = { accruals: [], gril: [] };
      opt = ffpCmsHelper.setupTierAccruals(opt, ffp.accruals);
      collection.push(opt);
      
    };
    
}])

.controller('RuleRangeModalController', ['$scope', '$modalInstance', 'rule', function ($scope, $modalInstance, rule) {

    $scope.rule = rule;

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.isValid = function () {
        var gril = angular.isArray(rule.gril) && rule.gril.length > 0;
        var range = rule.rangeFrom || rule.rangeTo;
        return !(gril && range);
    };

    $scope.submit = function (form) {
        if (form.$valid && $scope.isValid()) $modalInstance.close(rule);
    };

}])

.config(['kkRoutesConfigProvider', 'adminReviewConfig', function(kkRoutesConfigProvider, adminReviewConfig){
  
  kkRoutesConfigProvider.init({
    
    newFareAttributeRule: {
        path: '/ffps/:ffpid/fare-attribute-rules/new',
        controller: 'FfpFareAttributeRuleEditController',
        templateUrl: 'ffp-cms/edit-fare-attribute/edit-fare-attribute.html'
    },
    
    editFareAttributeRule: {
        path: '/ffps/:ffpid/fare-attribute-rules/edit/:id',
        controller: 'FfpFareAttributeRuleEditController',
        templateUrl: 'ffp-cms/edit-fare-attribute/edit-fare-attribute.html'
    },
    
    searchPagedFareAttributeRules: {
      path: '/ffps/:ffpid/fare-attribute-rules/page/:page/filter/:filter*',
      controller: 'FfpFareAttributeRuleCollectionController',
      templateUrl: 'ffp-cms/fare-attributes/fare-attributes.html'
    },
    
    searchFareAttributeRules: {
      path: '/ffps/:ffpid/fare-attribute-rules/filter/:filter*',
      controller: 'FfpFareAttributeRuleCollectionController',
      templateUrl: 'ffp-cms/fare-attributes/fare-attributes.html'
    },
    
    pagedFareAttributeRules: {
      path: '/ffps/:ffpid/fare-attribute-rules/page/:page',
      controller: 'FfpFareAttributeRuleCollectionController',
      templateUrl: 'ffp-cms/fare-attributes/fare-attributes.html'
    },
    
    fareAttributeRules: {
        path: '/ffps/:ffpid/fare-attribute-rules',
        controller: 'FfpFareAttributeRuleCollectionController',
        templateUrl: 'ffp-cms/fare-attributes/fare-attributes.html' 
    },
    
    newBenefitRule: {
        path: '/ffps/:ffpid/benefit-rules/new',
        controller: 'FfpBenefitRuleEditController',
        templateUrl: 'ffp-cms/edit-benefit/edit-benefit.html'
    },
    
    editBenefitRule: {
        path: '/ffps/:ffpid/benefit-rules/edit/:id',
        controller: 'FfpBenefitRuleEditController',
        templateUrl: 'ffp-cms/edit-benefit/edit-benefit.html'
    },
    
    searchPagedBenefitRules: {
      path: '/ffps/:ffpid/benefit-rules/page/:page/filter/:filter*',
      controller: 'FfpBenefitRuleCollectionController',
      templateUrl: 'ffp-cms/benefits/benefits.html'
    },
    
    searchBenefitRules: {
      path: '/ffps/:ffpid/benefit-rules/filter/:filter*',
      controller: 'FfpBenefitRuleCollectionController',
      templateUrl: 'ffp-cms/benefits/benefits.html'
    },
    
    pagedBenefitRules: {
      path: '/ffps/:ffpid/benefit-rules/page/:page',
      controller: 'FfpBenefitRuleCollectionController',
      templateUrl: 'ffp-cms/benefits/benefits.html'
    },
    
    benefitRules: {
        path: '/ffps/:ffpid/benefit-rules',
        controller: 'FfpBenefitRuleCollectionController',
        templateUrl: 'ffp-cms/benefits/benefits.html' 
    },
    
    newAward: {
        path: '/ffps/:ffpid/award-rules/new',
        controller: 'FfpAwardEditController',
        templateUrl: 'ffp-cms/edit-award/edit-award.html'
    },
    
    editAward: {
        path: '/ffps/:ffpid/award-rules/edit/:id',
        controller: 'FfpAwardEditController',
        templateUrl: 'ffp-cms/edit-award/edit-award.html'
    },
    
    awards: {
        path: '/ffps/:ffpid/award-rules',
        controller: 'FfpAwardCollectionController',
        templateUrl: 'ffp-cms/awards/awards.html' 
    },
    
    newUpgrade: {
        path: '/ffps/:ffpid/upgrade-rules/new',
        controller: 'FfpUpgradeEditController',
        templateUrl: 'ffp-cms/edit-upgrade/edit-upgrade.html'
    },
    
    editUpgrade: {
        path: '/ffps/:ffpid/upgrade-rules/edit/:id',
        controller: 'FfpUpgradeEditController',
        templateUrl: 'ffp-cms/edit-upgrade/edit-upgrade.html'
    },
    
    upgrades: {
        path: '/ffps/:ffpid/upgrade-rules',
        controller: 'FfpUpgradeCollectionController',
        templateUrl: 'ffp-cms/upgrades/upgrades.html'
    },
    
    newRule: {
        path: '/ffps/:ffpid/earning-rules/new',
        controller: 'FfpRuleEditController',
        templateUrl: (adminReviewConfig.isEnabled() ? 'ffp-cms/edit-rule/edit-rule-review.html' : 'ffp-cms/edit-rule/edit-rule.html') 
    },
    
    editAndApplyRule: {
        path: '/ffps/:ffpid/earning-rules/edit/:id/apply-to/:ids',
        controller: 'FfpRuleEditController',
        templateUrl: (adminReviewConfig.isEnabled() ? 'ffp-cms/edit-rule/edit-rule-review.html' : 'ffp-cms/edit-rule/edit-rule.html') 
    },
    
    editRule: {
        path: '/ffps/:ffpid/earning-rules/edit/:id',
        controller: 'FfpRuleEditController',
        templateUrl: (adminReviewConfig.isEnabled() ? 'ffp-cms/edit-rule/edit-rule-review.html' : 'ffp-cms/edit-rule/edit-rule.html') 
    },
    
    rules: {
        path: '/ffps/:ffpid/earning-rules',
        controller: 'FfpRuleCollectionController',
        templateUrl: (adminReviewConfig.isEnabled() ? 'ffp-cms/rules/rules-review.html' : 'ffp-cms/rules/rules.html')
    },
    
    newFfp: {
      path: '/ffps/new',
      controller: 'FfpEditController',
      templateUrl: 'ffp-cms/edit-ffp/edit-ffp.html'
    },
    
    editFfp: {
      path: '/ffps/edit/:id',
      controller: 'FfpEditController',
      templateUrl: 'ffp-cms/edit-ffp/edit-ffp.html'
    },
    
    ffps: {
      path: '/ffps',
      controller: 'FfpCollectionController',
      templateUrl: 'ffp-cms/ffps/ffps.html'
    },
    
    main: {
      path: '/ffps',
      controller: 'FfpCollectionController',
      templateUrl: 'ffp-cms/ffps/ffps.html'
    }
    
  });
  
}]);