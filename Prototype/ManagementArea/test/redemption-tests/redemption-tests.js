'use strict';

angular.module('30k.ma.test')

.controller('RedemptionTestCollectionController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', function($scope, collectionService, kkRoutesService, $routeParams){
    
    $scope.items = null;
    $scope.config = null;
    
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.filters = null;
    $scope.filtersExpanded = false;
    $scope.filterFfps = null;
    
    $scope.paged = {
        page: 1,
        totalItems: null,
        itemsPerPage: 50
    };
    
    $scope.$watchCollection('selected.items', function(newValue){
        
        if(!angular.isArray($scope.items)) return;
        if(newValue.length >= $scope.items.length) $scope.selected.allNone = 'all';
        else $scope.selected.allNone = 'none';
        
    });
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.onSelectPage = function(page){
        
        $scope.paged.page = page;
        reload();
        
    };
    
    function reload(){
        
        kkRoutesService.redirect('pagedAndFilteredRedemptionTests', {
            page: $scope.paged.page,
            ffp: $scope.filters.ffp,
            result: $scope.filters.result,
            ipp: $scope.paged.itemsPerPage
        });
        
    }
    
    $scope.onItemsPerPageChange = function(){
        
        reload();
        
    };
    
    $scope.deleteTest = function(test){
        collectionService.deleteItem('redemption-test/delete-redemption-test.json', 'redemptionTest', test);
    };
    
    $scope.deleteTests = function(){
        
        var tests = $scope.selected.items;
        collectionService.deleteItems('redemption-test/delete-redemption-tests.json', 'redemptionTest', tests);
        
    };
    
    $scope.toggleObjective = function(scope){
        scope.fullObjective = !scope.fullObjective;
    };
    
    $scope.cloneTest = function(index){
        
        collectionService.cloneItem('redemption-test/clone-redemption-test.json', 'redemptionTest', $scope.items[index]);
        
    };
    
    $scope.cloneTests = function(){
        
        collectionService.cloneItems('redemption-test/clone-redemption-tests.json', 'redemptionTest', $scope.selected.items);
        
    };
    
    function submitFilters(){
        
        $scope.paged.page = 1;
        reload();
        
    }
    
    $scope.submitFilters = function(form){
        if(form.$valid) submitFilters();
    };
    
    $scope.toggleFilters = function(){
        $scope.filtersExpanded = ! $scope.filtersExpanded;
    };
    
    $scope.areFiltersEmpty = function(){
        
        return $scope.filters.ffp == $scope.config.defaultFilters.ffp.code;
        
    };
    
    function loadItems(){
        
        var params = angular.copy($scope.paged);
        params.ffp = $scope.filters.ffp;
        delete params.totalItems;
        
        collectionService.getPagedCollection('redemption-test/get-redemption-tests.json?v=1', 'redemptionTest', params).then(function(response){
            
            $scope.items = response.items;
            $scope.paged.page = response.page;
            $scope.paged.totalItems = response.totalItems;
            
        });
        
    }
    
    function load(){
        
        collectionService.getConfig('test/config.json?v=2', 'test').then(function(config){
            
            $scope.config = angular.copy(config);
            
            $scope.filterFfps = [angular.copy($scope.config.defaultFilters.ffp)];
            
            $scope.config.ffps.forEach(function(ffp){
                $scope.filterFfps.push({ code: ffp.code, name: ffp.name });
            });
            
            if(!angular.isObject($scope.filters)) $scope.filters = {};
            
            $scope.filters.ffp = angular.isDefined($routeParams.ffp) ? $routeParams.ffp : $scope.config.defaultFilters.ffp.code;
            $scope.paged.page = angular.isDefined($routeParams.page) ? parseInt($routeParams.page, 10) : 1;
            $scope.paged.itemsPerPage = angular.isDefined($routeParams.ipp) ? parseInt($routeParams.ipp, 10) : $scope.config.pagination['default'];
            
            $scope.filtersExpanded = !$scope.areFiltersEmpty();
            
            loadItems();
            
        });
        
    }
    
    load();
    
}]);