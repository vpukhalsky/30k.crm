'use strict';

angular.module('30k.ma.test', [
    
    'mm.foundation',
    'checklist-model',
    
    '30k.error',
    '30k.routes',
    '30k.service',
    '30k.tag-input',
    '30k.period-picker',
    '30k.tooltip',
    '30k.multiselect',
    '30k.crm.benefit-text',
    
    '30k.ma'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
    
    newFareAttributeTest: {
        path: '/fare-attribute-tests/new',
        controller: 'FareAttributeTestEditController',
        templateUrl: 'test/edit-fare-attribute-test/edit-fare-attribute-test.html'
    },
    
    editFareAttributeTest: {
        path: '/fare-attribute-tests/edit/:id',
        controller: 'FareAttributeTestEditController',
        templateUrl: 'test/edit-fare-attribute-test/edit-fare-attribute-test.html'
    },
    
    searchPagedFareAttributeTests: {
      path: '/fare-attribute-tests/page/:page/filter/:filter*',
      controller: 'FareAttributeTestCollectionController',
      templateUrl: 'test/fare-attribute-tests/fare-attribute-tests.html'
    },
    
    searchFareAttributeTests: {
      path: '/fare-attribute-tests/filter/:filter*',
      controller: 'FareAttributeTestCollectionController',
      templateUrl: 'test/fare-attribute-tests/fare-attribute-tests.html'
    },
    
    pagedFareAttributeTests: {
      path: '/fare-attribute-tests/page/:page',
      controller: 'FareAttributeTestCollectionController',
      templateUrl: 'test/fare-attribute-tests/fare-attribute-tests.html'
    },
    
    fareAttributeTests: {
        path: '/fare-attribute-tests',
        controller: 'FareAttributeTestCollectionController',
        templateUrl: 'test/fare-attribute-tests/fare-attribute-tests.html' 
    },
    
    newBenefitTest: {
      path: '/benefit-tests/new',
      controller: 'BenefitTestEditController',
      templateUrl: 'test/edit-benefit-test/edit-benefit-test.html'
    },
    
    editBenefitTest: {
      path: '/benefit-tests/edit/:id',
      controller: 'BenefitTestEditController',
      templateUrl: 'test/edit-benefit-test/edit-benefit-test.html'
    },
    
    pagedAndFilteredBenefitTests: {
      path: '/benefit-tests/ffp/:ffp/result/:result/ipp/:ipp/page/:page',
      controller: 'BenefitTestCollectionController',
      templateUrl: 'test/benefit-tests/benefit-tests.html'
    },
    
    filteredBenefitTests: {
      path: '/benefit-tests/ffp/:ffp/result/:result',
      controller: 'BenefitTestCollectionController',
      templateUrl: 'test/benefit-tests/benefit-tests.html'
    },
    
    pagedBenefitTests: {
      path: '/benefit-tests/ipp/:ipp/page/:page',
      controller: 'BenefitTestCollectionController',
      templateUrl: 'test/benefit-tests/benefit-tests.html'
    },
    
    benefitTests: {
      path: '/benefit-tests',
      controller: 'BenefitTestCollectionController',
      templateUrl: 'test/benefit-tests/benefit-tests.html'
    },
    
    newRedemptionTest: {
      path: '/redemption-tests/new',
      controller: 'RedemptionTestEditController',
      templateUrl: 'test/edit-redemption-test/edit-redemption-test.html'
    },
    
    editRedemptionTest: {
      path: '/redemption-tests/edit/:id',
      controller: 'RedemptionTestEditController',
      templateUrl: 'test/edit-redemption-test/edit-redemption-test.html'
    },
    
    pagedAndFilteredRedemptionTests: {
      path: '/redemption-tests/ffp/:ffp/result/:result/ipp/:ipp/page/:page',
      controller: 'RedemptionTestCollectionController',
      templateUrl: 'test/redemption-tests/redemption-tests.html'
    },
    
    filteredRedemptionTests: {
      path: '/redemption-tests/ffp/:ffp/result/:result',
      controller: 'RedemptionTestCollectionController',
      templateUrl: 'test/redemption-tests/redemption-tests.html'
    },
    
    pagedRedemptionTests: {
      path: '/redemption-tests/ipp/:ipp/page/:page',
      controller: 'RedemptionTestCollectionController',
      templateUrl: 'test/redemption-tests/redemption-tests.html'
    },
    
    redemptionTests: {
      path: '/redemption-tests',
      controller: 'RedemptionTestCollectionController',
      templateUrl: 'test/redemption-tests/redemption-tests.html'
    },
    
    newTest: {
      path: '/tests/new',
      controller: 'TestEditController',
      templateUrl: 'test/edit-test/edit-test.html'
    },
    
    editTest: {
      path: '/tests/edit/:id',
      controller: 'TestEditController',
      templateUrl: 'test/edit-test/edit-test.html'
    },
    
    pagedAndFilteredTests: {
      path: '/tests/ffp/:ffp/result/:result/ipp/:ipp/page/:page',
      controller: 'TestCollectionController',
      templateUrl: 'test/tests/tests.html'
    },
    
    filteredTests: {
      path: '/tests/ffp/:ffp/result/:result',
      controller: 'TestCollectionController',
      templateUrl: 'test/tests/tests.html'
    },
    
    pagedTests: {
      path: '/tests/ipp/:ipp/page/:page',
      controller: 'TestCollectionController',
      templateUrl: 'test/tests/tests.html'
    },
    
    tests: {
      path: '/tests',
      controller: 'TestCollectionController',
      templateUrl: 'test/tests/tests.html'
    },
    
    main: {
      path: '/tests',
      controller: 'TestCollectionController',
      templateUrl: 'test/tests/tests.html'
    }
    
  });
  
}]);