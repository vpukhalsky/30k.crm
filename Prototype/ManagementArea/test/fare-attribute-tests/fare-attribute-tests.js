'use strict';

angular.module('30k.ma.test')

.controller('FareAttributeTestCollectionController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', function($scope, collectionService, kkRoutesService, $routeParams){
    
    $scope.items = null;
    $scope.config = null;
    
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.filters = null;
    $scope.filtersExpanded = false;
    
    $scope.paged = {
        page: 1,
        totalItems: null,
        itemsPerPage: 50,
        marketingAirlines: [],
        operatingAirlines: []
    };
    
    $scope.$watchCollection('selected.items', function(newValue){
        
        if(!angular.isArray($scope.items)) return;
        if(newValue.length >= $scope.items.length) $scope.selected.allNone = 'all';
        else $scope.selected.allNone = 'none';
        
    });
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.onSelectPage = function(page){
        
        update(page);
        
    };
    
    $scope.onItemsPerPageChange = function(){
        
        update(1);
        
    };
    
    function areFiltersEmpty(){
        
        return $scope.paged.marketingAirlines.length <= 0 && $scope.paged.operatingAirlines.length <= 0;
        
    }
    
    function update(page){
        
        var params = {};
        var route = 'fareAttributeTests';
        
        if(page && page != 1){
            params.page = page;
            route = 'pagedFareAttributeTests';
        }
        
        if(!areFiltersEmpty()) {
            
            if(params.page) route = 'searchPagedFareAttributeTests';
            else route = 'searchFareAttributeTests';
            
            params.filter = $scope.paged;
            delete params.filter.page;
            delete params.filter.totalItems;
            
        }

        kkRoutesService.redirect(route, params);
        
    }

    $scope.deleteTest = function(test){
        collectionService.deleteItem('fare-attribute-test/delete-fare-attribute-test.json', 'fareAttributeTest', test);
    };
    
    $scope.deleteTests = function(){
        
        var tests = $scope.selected.items;
        collectionService.deleteItems('fare-attribute-test/delete-fare-attribute-tests.json', 'fareAttributeTest', tests);
        
    };
    
    $scope.toggleObjective = function(scope){
        scope.fullObjective = !scope.fullObjective;
    };
    
    $scope.cloneTest = function(index){
        
        collectionService.cloneItem('fare-attribute-test/clone-fare-attribute-test.json', 'fareAttributeTest', $scope.items[index]);
        
    };
    
    $scope.cloneTests = function(){
        
        collectionService.cloneItems('fare-attribute-test/clone-fare-attribute-tests.json', 'fareAttributeTest', $scope.selected.items);
        
    };
    
    $scope.onFilterChange = function(){
        
        update(1);
      
    };
    
    $scope.preventDefault = function(event){
        event.stopPropagation();
    };
    
    function loadItems(){
        
        var params = angular.copy($scope.paged);
        delete params.totalItems;
        
        collectionService.getPagedCollection('fare-attribute-test/get-fare-attribute-tests.json?v=1', 'fareAttributeTest', params).then(function(response){
            
            $scope.items = response.items;
            $scope.paged.page = response.page;
            $scope.paged.totalItems = response.totalItems;
            
        });
        
    }
    
    function load(){
        
        collectionService.getConfig('test/config.json?v=2', 'test').then(function(config){
            
            $scope.config = angular.copy(config);
            if(!$scope.paged.itemsPerPage) $scope.paged.itemsPerPage = $scope.config.pagination.default;
            loadItems();
            
        });
        
    }
    
    function getParams(){
        
        var params = kkRoutesService.getParams({
            page: 'int',
            itemsPerPage: 'int',
            marketingAirlines: 'array',
            operatingAirlines: 'array'
        });
        
        delete params.filter;
        angular.extend($scope.paged, params);

    }
    
    getParams();
    load();
    
}]);