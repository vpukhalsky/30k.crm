'use strict';

angular.module('30k.ma.test')

.controller('TestCollectionController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', function($scope, collectionService, kkRoutesService, $routeParams){
    
    $scope.items = null;
    $scope.config = null;
    
    $scope.selected = { items:[], allNone: 'none'};
    
    $scope.filters = null;
    $scope.filtersExpanded = false;
    $scope.filterFfps = null;
    
    $scope.paged = {
        page: 1,
        totalItems: null,
        itemsPerPage: 50
    };
    
    $scope.$watchCollection('selected.items', function(newValue){
        
        if(!angular.isArray($scope.items)) return;
        if(newValue.length >= $scope.items.length) $scope.selected.allNone = 'all';
        else $scope.selected.allNone = 'none';
        
    });
    
    $scope.toggleSelectAll = function(value){
        if(value == 'all') $scope.selectAll();
        else if(value == 'none') $scope.selectNone();
    };
    
    $scope.selectNone = function(){
        while($scope.selected.items.length > 0) $scope.selected.items.splice(0, 1);
    };
    
    $scope.selectAll = function(){
        
        $scope.selectNone();
        
        for(var i = 0 ; i < $scope.items.length ; i++){
            $scope.selected.items.push($scope.items[i]);
        }

    };
    
    $scope.onSelectPage = function(page){
        
        $scope.paged.page = page;
        reload();
        
    };
    
    function reload(){
        
        kkRoutesService.redirect('pagedAndFilteredTests', {
            page: $scope.paged.page,
            ffp: $scope.filters.ffp,
            result: $scope.filters.result,
            ipp: $scope.paged.itemsPerPage
        });
        
    }
    
    $scope.onItemsPerPageChange = function(){
        
        reload();
        
    };
    
    $scope.deleteTest = function(test){
        collectionService.deleteItem('test/delete-test.json', 'test', test);
    };
    
    $scope.deleteTests = function(){
        
        var tests = $scope.selected.items;
        collectionService.deleteItems('test/delete-tests.json', 'test', tests);
        
    };
    
    $scope.toggleObjective = function(scope){
        scope.fullObjective = !scope.fullObjective;
    };
    
    $scope.isTestSuccessful = function(test){
        
        var success = true;
        test.tests.normal.success.forEach(function(s){ success = success && s; });
        if(test.minimumTest) test.tests.minimum.success.forEach(function(s){ success = success && s; });
        return success;
        
    };
    
    $scope.runTest = function(test){
        collectionService.saveItem('test/run-test.json', 'test', test);
    };
    
    $scope.runTests = function(){
        collectionService.saveItems('test/run-tests.json', 'test', $scope.selected.items);
    };
    
    $scope.cloneTest = function(index){
        
        collectionService.cloneItem('test/clone-test.json', 'test', $scope.items[index]);
        
    };
    
    $scope.cloneTests = function(){
        
        collectionService.cloneItems('test/clone-tests.json', 'test', $scope.selected.items);
        
    };
    
    function submitFilters(){
        
        $scope.paged.page = 1;
        reload();
        
    }
    
    $scope.submitFilters = function(form){
        if(form.$valid) submitFilters();
    };
    
    $scope.toggleFilters = function(){
        $scope.filtersExpanded = ! $scope.filtersExpanded;
    };
    
    $scope.areFiltersEmpty = function(){
        
        return $scope.filters.ffp == $scope.config.defaultFilters.ffp.id && $scope.filters.result == $scope.config.defaultFilters.result;
        
    };
    
    function loadItems(){
        
        var params = angular.copy($scope.paged);
        params.filters = angular.copy($scope.filters);
        delete params.totalItems;
        
        collectionService.getPagedCollection('test/get-tests.json?v=1', 'test', params).then(function(response){
            
            $scope.items = response.items;
            $scope.paged.page = response.page;
            $scope.paged.totalItems = response.totalItems;
            
        });
        
    }
    
    function load(){
        
        collectionService.getConfig('test/config.json?v=2', 'test').then(function(config){
            
            $scope.config = angular.copy(config);
            
            $scope.filterFfps = [angular.copy($scope.config.defaultFilters.ffp)];
            
            $scope.config.ffps.forEach(function(ffp){
                $scope.filterFfps.push({ id: ffp.id, name: ffp.name });
            });
            
            if(!angular.isObject($scope.filters)) $scope.filters = {};
            $scope.filters.ffp = angular.isDefined($routeParams.ffp) ? parseInt($routeParams.ffp, 10) : $scope.config.defaultFilters.ffp.id;
            $scope.paged.page = angular.isDefined($routeParams.page) ? parseInt($routeParams.page, 10) : 1;
            $scope.filters.result = angular.isDefined($routeParams.result) ? $routeParams.result : $scope.config.defaultFilters.result;
            $scope.paged.itemsPerPage = angular.isDefined($routeParams.ipp) ? parseInt($routeParams.ipp, 10) : $scope.config.pagination['default'];
            
            $scope.filtersExpanded = !$scope.areFiltersEmpty();
            
            loadItems();
            
        });
        
    }
    
    load();
    
}]);