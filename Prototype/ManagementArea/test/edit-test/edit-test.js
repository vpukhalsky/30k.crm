'use strict';

angular.module('30k.ma.test')

.controller('TestEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', '$modal', 'alertService', 'kkService', '$timeout', function($scope, collectionService, kkRoutesService, $routeParams, $filter, $modal, alertService, kkService, $timeout){
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.config = null;
    $scope.ffp = null;
    
    $scope.airportCodePattern = /^[a-zA-Z]{3}$/;
    $scope.airlineCodePattern = /^([a-zA-Z][a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z])$/;
    
    $scope.showNormalTiers = true;
    $scope.showMinimumTiers = true;
    
    $scope.highlightResult = false;
    
    $scope.getLegClass = function(index, segments){
        
        var even = true;
        
        for(var i = 0 ; i <= index ; i++){
            if(segments[i].legStart) even = !even;
        }
        
        return even ? 'even' : 'odd';
        
    };
    
    $scope.onDateChange = function(obj, dateFieldName, event){
        
        $timeout(function(){
            var val = $(event.target).val();
            if(val == '') obj[dateFieldName] = null;
            else if(val.match(/^\d{4}\-\d{2}\-\d{2}$/)) obj[dateFieldName] = new Date(val);
        });
        
    };
    
    $scope.deleteSegment = function(index){
        $scope.item.segments.splice(index, 1);
    };
    
    $scope.addSegment = function(){
        
        var seg = {};
        
        if($scope.item.segments.length > 0){
            var last = $scope.item.segments[$scope.item.segments.length - 1];
            seg.departure = last.arrival;
        }
        
        $scope.item.segments.push(seg);
        
    };
    
    $scope.toggleNormalTiers = function(){
        $scope.showNormalTiers = !$scope.showNormalTiers;
    };
    
    $scope.toggleMinimumTiers = function(){
        $scope.showMinimumTiers = !$scope.showMinimumTiers;
    };
    
    $scope.initSegment = function(scope){
        scope.$watch('segment.mkCarrier', function(newValue){
            if(scope.segmentForm && scope.segmentForm.opCar.$pristine) scope.segment.opCarrier = newValue;
        });
        if(scope.$index == 0) scope.segment.legStart = true;
        if(!scope.segment.bookingChannel) scope.segment.bookingChannel = 'ota';
    };
    
    $scope.showCalculated = function(){
        return !$scope.isNew && $scope.item.ffp == $scope.item.tests.config.ffpId && $scope.item.tests.normal.calculated;
    };
    
    $scope.$watch('item.ffp', function(newValue){
        
        if(!angular.isNumber(newValue)) return;
        
        var find = $filter('find');
        
        $scope.ffp = find($scope.config.ffps, newValue);
        
        if(!angular.isObject($scope.item.tests.config)) $scope.item.tests.config = {};
        var config = $scope.item.tests.config;
        
        if(!angular.isArray(config.accrualIds)) config.accrualIds = [];
        config.accrualIds.splice(0, config.accrualIds.length);
        
        $scope.ffp.accruals.forEach(function(a){
            config.accrualIds.push(a.id);
        });
        
        if(!angular.isNumber(config.ffpId)) config.ffpId = $scope.ffp.id;
        
        var ne = $scope.item.tests.normal.expected;
        while(ne.length < config.accrualIds.length) ne.push({ v: undefined });
        while(ne.length > config.accrualIds.length) ne.splice(ne.length - 1, 1);
        
        var me = $scope.item.tests.minimum.expected;
        while(me.length < config.accrualIds.length) me.push({ v: undefined });
        while(me.length > config.accrualIds.length) me.splice(me.length - 1, 1);
        
        var ns = $scope.item.tests.normal.success;
        while(ns.length < config.accrualIds.length) ns.push(undefined);
        while(ns.length > config.accrualIds.length) ns.splice(ns.length - 1, 1);
        
        var ms = $scope.item.tests.minimum.success;
        while(ms.length < config.accrualIds.length) ms.push(undefined);
        while(ms.length > config.accrualIds.length) ms.splice(ms.length - 1, 1);

    });
    
    function clone(){
        collectionService.cloneItem('test/clone-test.json', 'test', toSave($scope.item)).then(function(response){
            kkRoutesService.redirect('editTest', { id: response.items[0].id });
        });
    }
    
    $scope.clone = function(form){
        if(form.$valid && !form.$dirty) clone();
        else{
            alertService.popup($scope.config.cloneAlertTitle, $scope.config.cloneAlertMessage);
        }
    };
    
    function run(form){
        collectionService.saveItem('test/run-test.json', 'test', toSave($scope.item)).then(function(response){
            
            $scope.item = toEdit(response.item);
            form.$setPristine(true);
            
            scrollToResult();
            
        });
    }
    
    function scrollToResult(){
        $('html,body').animate({ scrollTop: $('#test-result').offset().top + 'px' }, 600);
        $scope.highlightResult = true;
        $timeout(function(){ $scope.highlightResult = false; }, 600);
    }
    
    $scope.run = function(form){
        
        if(!form.$dirty && form.$valid){
            run(form);
        }else{
            $modal.open({
                templateUrl: 'test/edit-test/run-modal.html',
                controller: 'RunTestModalController',
                windowClass: 'tiny',
                resolve: {
                    form: function(){ return form; }
                }
            }).result.then(function(response){
                 if(response == 'save'){
                    submit(function(){
                        form.$setPristine(true);
                    });  
                 }
                 else if(response == 'save-and-run') submit(function(){
                    run(form);
                 });
            });
        }
        
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(response){
            //stay on the same page
            //kkRoutesService.redirect('tests');
        });
        
    };
    
    function reset(){
        
        $scope.item = toEdit(angular.copy($scope.config.defaultTest));
        
        $scope.isNew = true;
    }
    
    function toSave(item){
        
        var i = angular.copy(item);
        
        var ne = [];
        i.tests.normal.expected.forEach(function(e){
           ne.push(e.v);
        });
        
        var me = [];
        i.tests.minimum.expected.forEach(function(e){
           me.push(e.v);
        });
        
        i.tests.minimum = { expected: me };
        i.tests.normal = { expected: ne };
        i.tests.config = { accrualIds: i.tests.config.accrualIds };
        if(!i.minimumTest) delete i.tests.minimum;
        
        var localDate = $filter('localDate');
        
        if(i.purchaseDate) i.purchaseDate = localDate(i.purchaseDate);
        
        i.segments.forEach(function(s){
           if(s.departureDate) s.departureDate = localDate(s.departureDate);
        });
        
        return i;
    }
    
    function toEdit(item){
        
        var ne = [];
        item.tests.normal.expected.forEach(function(e){
            ne.push({ v : e });
        });
        item.tests.normal.expected = ne;
        
        var me = [];
        item.tests.minimum.expected.forEach(function(e){
            me.push({ v : e });
        });
        item.tests.minimum.expected = me;
        
        if(angular.isString(item.purchaseDate)) item.purchaseDate = new Date(item.purchaseDate);
        
        item.segments.forEach(function(s){
            if(angular.isString(s.departureDate)) s.departureDate = new Date(s.departureDate);
        });
        
        return item;
        
    }
    
    function submit(callback){
        collectionService.saveItem('test/save-test.json', 'test', toSave($scope.item)).then(function (response){
            callback(response);
            if ($scope.isNew) {
                kkRoutesService.redirect('editTest', { id: $scope.item.id });
                $scope.isNew = false;
            }
        });
    }
    
    $scope.deleteTest = function(item){
        collectionService.deleteItem('test/delete-test.json', 'test', item).then(function(response){
            if(response) kkRoutesService.redirect('tests');
        });
    };
    
    $scope.getAirports = function(input){
        
        return kkService.sendPost('test/airports.json', {
            input: input
        }, false).then(function(response){
            return response.airports;
        });
        
    };

    function load(){
        
        collectionService.getConfig('test/config.json?v=2', 'test').then(function(config){
            
            $scope.config = angular.copy(config);
            
            if(angular.isDefined($routeParams.id)){
                
                var id = parseInt($routeParams.id, 10);
                
                collectionService.getItem('test/get-test.json?v=3', 'test', id).then(function(item){
                    
                    $scope.isNew = false;
                    
                    $scope.item = toEdit(angular.copy(item));
                    $scope.ffp = $scope.item.ffp + 0;
                    
                });
                
            }else{
                
                reset();

            }
        });
    }
    
    load();
    
}])

.controller('RunTestModalController', ['$scope', '$modalInstance', 'form', function($scope, $modalInstance, form){
    
    $scope.form = form;
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.save = function(){
        $modalInstance.close('save');
    };
    
    $scope.saveAndRun = function(){
        $modalInstance.close('save-and-run');
    };
    
}]); 