'use strict';

angular.module('30k.ma.test')

.controller('FareAttributeTestEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', '$modal', 'alertService', 'kkService', '$timeout', function($scope, collectionService, kkRoutesService, $routeParams, $filter, $modal, alertService, kkService, $timeout){
    
    $scope.item = null;
    $scope.isNew = true;
	$scope.isRan = false;
    $scope.config = null;
    
    $scope.highlightResult = false;
    
    $scope.airportCodePattern = /^[a-zA-Z]{3}$/;
    $scope.airlineCodePattern = /^([a-zA-Z][a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z])$/;
    
    $scope.onDateChange = function(obj, dateFieldName, event){
        
        $timeout(function(){
            var val = $(event.target).val();
            if(val == '') obj[dateFieldName] = null;
            else if(val.match(/^\d{4}\-\d{2}\-\d{2}$/)) obj[dateFieldName] = new Date(val);
        });
        
    };
    
    $scope.deleteFare = function(index){
        $scope.item.fares.splice(index, 1);
    };
    
    $scope.addFare = function(index){
        
        $scope.item.fares.splice(index + 1, 0, {});
        
    };
    
    function clone(){
        collectionService.cloneItem('fare-attribute-test/clone-fare-attribute-test.json', 'fareAttributeTest', toSave($scope.item)).then(function(response){
             kkRoutesService.redirect('editFareAttributeTest', { id: response.item.id });
        });
    }
    
    $scope.clone = function(form){
        if(form.$valid && !form.$dirty) clone();
        else{
            alertService.popup($scope.config.cloneAlertTitle, $scope.config.cloneAlertMessage);
        }
    };
    
    function run(form){
        collectionService.saveItem('fare-attribute-test/run-fare-attribute-test.json', 'fareAttributeTest', toSave($scope.item)).then(function(response){
            
			$scope.isRan = true;
            $scope.item = toEdit(response.item);
            form.$setPristine(true);
            
            scrollToResult();
            
        });
    }
    
    function scrollToResult(){
        
        $('html,body').animate({ scrollTop: $('#test-result').offset().top + 'px' }, 600);
        $scope.highlightResult = true;
        $timeout(function(){ $scope.highlightResult = false; }, 600);
        
    }
    
    $scope.run = function(form){
        
        if(!form.$dirty && form.$valid){
            run(form);
        }else{
            $modal.open({
                templateUrl: 'test/edit-test/run-modal.html',
                controller: 'RunTestModalController',
                windowClass: 'tiny',
                resolve: {
                    form: function(){ return form; }
                }
            }).result.then(function(response){
                 if(response == 'save'){
                    submit(function(){
                        form.$setPristine(true);
                    });  
                 }
                 else if(response == 'save-and-run') submit(function(){
                    run(form);
                 });
            });
        }
        
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(response){form.$setPristine();});
        
    };
    
    function reset(){
        
        $scope.item = toEdit(angular.copy($scope.config.defaultFareAttributeTest));
        $scope.isNew = true;
		$scope.isRan = false;
        
    }
    
    function toSave(i){
        
        var item = angular.copy(i);
        if(!angular.isObject(item.test)) item.test = {};
        
        var localDate = $filter('localDate');
        
        if(i.purchaseDate) i.purchaseDate = localDate(i.purchaseDate);
        
        return item;
    }
    
    function toEdit(item){
        
        if(angular.isString(item.purchaseDate)) item.purchaseDate = new Date(item.purchaseDate);
        
        return item;
        
    }
    
    function submit(callback){
        
        collectionService.saveItem('fare-attribute-test/save-fare-attribute-test.json', 'fareAttributeTest', toSave($scope.item)).then(function (response){
            callback(response);
            if ($scope.isNew) {
                kkRoutesService.redirect('editFareAttributeTest', { id: $scope.item.id });
                $scope.isNew = false;
            }
        });
        
    }
    
    $scope.deleteTest = function(item){
        
        collectionService.deleteItem('fare-attribute-test/delete-fare-attribute-test.json', 'fareAttributeTest', item).then(function(response){
            if(response) kkRoutesService.redirect('fareAttributeTests');
        });
        
    };
    
    $scope.getAirports = function(input){
        
        return kkService.sendPost('test/airports.json', {
            input: input
        }, false).then(function(response){
            return response.airports;
        });
        
    };

    function load(){
        
        collectionService.getConfig('test/config.json?v=2', 'test').then(function(config){
            
            $scope.config = angular.copy(config);
            
            if(angular.isDefined($routeParams.id)){
                
                var id = parseInt($routeParams.id, 10);
                
                collectionService.getItem('fare-attribute-test/get-fare-attribute-test.json?v=2', 'fareAttributeTest', id).then(function(item){
                    
                    $scope.isNew = false;
                    $scope.item = toEdit(angular.copy(item));
                    
                });
                
            }else{
                
                reset();

            }
        });
    }
    
    load();
    
}]);