'use strict';

angular.module('30k.ma.test')

.controller('BenefitTestEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', '$filter', '$modal', 'alertService', 'kkService', '$timeout', function($scope, collectionService, kkRoutesService, $routeParams, $filter, $modal, alertService, kkService, $timeout){
    
    $scope.item = null;
    $scope.isNew = true;
	$scope.isRan = false;
    $scope.config = null;
    
    $scope.highlightResult = false;
    
    $scope.airportCodePattern = /^[a-zA-Z]{3}$/;
    $scope.airlineCodePattern = /^([a-zA-Z][a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z])$/;
    
    $scope.getBenefits = function(){
        
        var benefits = [];
        var ffp = $scope.getFfp();
        
        if(angular.isObject(ffp)){
            
            $scope.config.benefits.forEach(function(benefit){
                if(benefit.ffps.indexOf(ffp.code) >= 0) benefits.push(benefit);
            });
            
        }
        
        return benefits;
    };
    
    $scope.getLegClass = function(index, segments){
        
        var even = true;
        
        for(var i = 0 ; i <= index ; i++){
            if(segments[i].legStart) even = !even;
        }
        
        return even ? 'even' : 'odd';
        
    };
    
    $scope.getFfp = function(ffpId){
        
        var find = $filter('find');
        if(!ffpId) ffpId = $scope.item.ffp;
        var ffp = find($scope.config.ffps, ffpId);
        return ffp;
        
    };
    
    $scope.onFfpChange = function(item){

        item.status = null;
		$scope.isRan = false;
        
    };

    function selectDefaultFfp(item){
        
        if(angular.isArray($scope.config.ffps) && $scope.config.ffps.length > 0){
            item.ffp = $scope.config.ffps[0].id;
        }else{
            item.ffp = null;
        }
    
    }
    
    $scope.deleteSegment = function(index){
        $scope.item.segments.splice(index, 1);
    };
    
    $scope.addSegment = function(){
        
        var seg = {};
        
        if($scope.item.segments.length > 0){
            var last = $scope.item.segments[$scope.item.segments.length - 1];
            seg.departure = last.arrival;
        }
        
        $scope.item.segments.push(seg);
        
    };
    
    $scope.initSegment = function(scope){
        scope.$watch('segment.mkCarrier', function(newValue){
            if(scope.segmentForm && scope.segmentForm.opCar.$pristine) scope.segment.opCarrier = newValue;
        });
        if(scope.$index == 0) scope.segment.legStart = true;
    };
    
    function clone(){
        collectionService.cloneItem('benefit-test/clone-benefit-test.json', 'benefitTest', toSave($scope.item)).then(function(response){
             kkRoutesService.redirect('editBenefitTest', { id: response.item.id });
        });
    }
    
    $scope.clone = function(form){
        if(form.$valid && !form.$dirty) clone();
        else{
            alertService.popup($scope.config.cloneAlertTitle, $scope.config.cloneAlertMessage);
        }
    };
    
    function run(form){
        collectionService.saveItem('benefit-test/run-benefit-test.json', 'benefitTest', toSave($scope.item)).then(function(response){
            
			$scope.isRan = true;
            $scope.item = toEdit(response.item);
            form.$setPristine(true);
            
            scrollToResult();
            
        });
    }
    
    function scrollToResult(){
        $('html,body').animate({ scrollTop: $('#test-result').offset().top + 'px' }, 600);
        $scope.highlightResult = true;
        $timeout(function(){ $scope.highlightResult = false; }, 600);
    }
    
    $scope.run = function(form){
        
        if(!form.$dirty && form.$valid){
            run(form);
        }else{
            $modal.open({
                templateUrl: 'test/edit-test/run-modal.html',
                controller: 'RunTestModalController',
                windowClass: 'tiny',
                resolve: {
                    form: function(){ return form; }
                }
            }).result.then(function(response){
                 if(response == 'save'){
                    submit(function(){
                        form.$setPristine(true);
                    });  
                 }
                 else if(response == 'save-and-run') submit(function(){
                    run(form);
                 });
            });
        }
        
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(response){form.$setPristine();});
        
    };
    
    function reset(){
        
        $scope.item = toEdit(angular.copy($scope.config.defaultBenefitTest));
        $scope.isNew = true;
		$scope.isRan = false;
        
    }
    
    function toSave(i){
        
        var item = angular.copy(i);
        if(!angular.isObject(item.test)) item.test = {};
        
        return item;
    }
    
    function toEdit(item){
        
        if(!angular.isNumber(item.ffp)){
            selectDefaultFfp(item);
        }
        
        return item;
        
    }
    
    function submit(callback){
        collectionService.saveItem('benefit-test/save-benefit-test.json', 'benefitTest', toSave($scope.item)).then(function (response){
            callback(response);
            if ($scope.isNew) {
                kkRoutesService.redirect('editBenefitTest', { id: $scope.item.id });
                $scope.isNew = false;
            }
        });
    }
    
    $scope.deleteTest = function(item){
        collectionService.deleteItem('benefit-test/delete-benefit-test.json', 'benefitTest', item).then(function(response){
            if(response) kkRoutesService.redirect('benefitTests');
        });
    };
    
    $scope.getAirports = function(input){
        
        return kkService.sendPost('test/airports.json', {
            input: input
        }, false).then(function(response){
            return response.airports;
        });
        
    };

    function load(){
        
        collectionService.getConfig('test/config.json?v=2', 'test').then(function(config){
            
            $scope.config = angular.copy(config);
            
            if(angular.isDefined($routeParams.id)){
                
                var id = parseInt($routeParams.id, 10);
                
                collectionService.getItem('benefit-test/get-benefit-test.json?v=2', 'benefitTest', id).then(function(item){
                    
                    $scope.isNew = false;
                    $scope.item = toEdit(angular.copy(item));
                    
                });
                
            }else{
                
                reset();

            }
        });
    }
    
    load();
    
}]);