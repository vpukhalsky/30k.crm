'use strict';

angular.module('30k.ma.static-data')

.controller('AirportCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', '$timeout', '$routeParams', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig, $timeout, $routeParams){
    
    $scope.items = null;
    $scope.totalItems = 0;
    
    $scope.continents = null;
    $scope.countries = null;
    
    $scope.params = {
        page: 1,
        itemsPerPage: staticDataConfig.itemsPerPage,
        search: '',
        city: '',
        countries: [],
        continents: []
    };
    
    $scope.filters = {
        search: '',
        city: '',
        countries: [],
        continents: []
    };
    
    $scope.filtersExpanded = false;
    
    $scope.toggleFilters = function(){
        $scope.filtersExpanded = ! $scope.filtersExpanded;
    };
    
    $scope.areFiltersEmpty = function(){
        return $scope.params.city == '' && $scope.params.search == '' && $scope.params.countries.length <= 0 && $scope.params.continents.length <= 0;
    };
    
    $scope.onSelectPage = function(page){
        
        $scope.params.page = page;
        var route = 'pagedAirports';
        var params = angular.copy($scope.params);
        if(angular.isArray(params.countries)) params.countries = params.countries.join(',');
        if(angular.isArray(params.continents)) params.continents = params.continents.join(',');
        
        if(!$scope.areFiltersEmpty()) route = getSearchRouteName('searchPagedAirports', params);

        kkRoutesService.redirect(route, params);
        
    };
    
    function getSearchRouteName(path, params){
        
        if(params.search) path += 'Search';
        if(params.city) path += 'City';
        if(params.countries) path += 'Countries';
        if(params.continents) path += 'Continents';
        
        return path;
        
    }
    
    $scope.deleteAirport = function(airport){
        collectionService.deleteItem(staticDataUrls.airport.remove, staticDataConfig.types.airport, airport).then(function(response){
            
            if(response){
                if($scope.items.length == 0 && $scope.params.page > 1) $scope.params.page--;
                load();
            }
            
        });
    };
    
    function filter(){
        
        $scope.params.page = 1;
        $scope.params.search = angular.copy($scope.filters.search);
        $scope.params.city = angular.copy($scope.filters.city);
        $scope.params.countries = angular.copy($scope.filters.countries);
        $scope.params.continents = angular.copy($scope.filters.continents);
        
        var params = angular.copy($scope.params);
        params.countries = params.countries.join(',');
        params.continents = params.continents.join(',');
        
        if(!$scope.areFiltersEmpty()){
            var path = getSearchRouteName('searchAirports', params);
            console.log(params, path);
            kkRoutesService.redirect(path, params);
            
        }
        else kkRoutesService.redirect('airports');
        
    }
    
    $scope.submitFilters = function(form){
        
        if(!form.$valid) return;
        filter();
        
    };
    
    $scope.resetFilter = function(){
        
        kkRoutesService.redirect('airports');
        
    };
    
    $scope.preventDefault = function(event){
        event.stopPropagation();
    };
    
    function load(){
        
        var params = angular.copy($scope.params);
        
        params.countries = params.countries.join(',');
        params.continents = params.continents.join(',');
        
        collectionService.getPagedCollection(staticDataUrls.airport.collection, staticDataConfig.types.airport, $scope.params).then(function(response){
            $scope.items = response.items;
            $scope.totalItems = response.totalItems;
        });
        
        collectionService.getCollection(staticDataUrls.continent.collection, staticDataConfig.types.continent).then(function(response){
            $scope.continents = response;
        });
        
        collectionService.getCollection(staticDataUrls.country.collection, staticDataConfig.types.country).then(function(response){
            $scope.countries = response;
        });
        
    }
    
    
    if(angular.isDefined($routeParams.page)){
        $scope.params.page = parseInt($routeParams.page, 10);
    }
    
    if(angular.isDefined($routeParams.search)){
        $scope.params.search = decodeURIComponent($routeParams.search);
    }
    
    if(angular.isDefined($routeParams.city)){
        $scope.params.city = decodeURIComponent($routeParams.city);
    }
    
    if(angular.isDefined($routeParams.countries)){
        $scope.params.countries = decodeURIComponent($routeParams.countries).split(',').map(function(i){ return parseInt(i, 10); });
    }
    
    if(angular.isDefined($routeParams.continents)){
        $scope.params.continents = decodeURIComponent($routeParams.continents).split(',').map(function(i){ return parseInt(i, 10); });
    }
    
    $scope.filters.search = angular.copy($scope.params.search);
    $scope.filters.city = angular.copy($scope.params.city);
    $scope.filters.countries = angular.copy($scope.params.countries);
    $scope.filters.continents = angular.copy($scope.params.continents);
    
    load();
    
}]);