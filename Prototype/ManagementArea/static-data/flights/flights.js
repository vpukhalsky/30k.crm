'use strict';

angular.module('30k.ma.static-data')

.controller('FlightCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', '$timeout', '$routeParams', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig, $timeout, $routeParams){
    
    $scope.items = null;
    $scope.totalItems = 0;
    
    $scope.flightRegExp = /^\d+\s*(((,\s*\d+\s*)*)|(\-\s*\d+))$/;
    
    $scope.params = {
        page: 1,
        itemsPerPage: staticDataConfig.itemsPerPage,
        airlines: [],
        flights: [],
        departure: [],
        arrival: [],
        stops: [],
        operating: [],
        cabin: '',
        classes: '',
        mktFlights: [],
        operatingByPartner: false,
        wetlease: false,
        /*startDateFrom: null,
        startDateUntil: null,
        endDateFrom: null,
        endDateUntil: null,
        numOfstops: null,
        distanceMin: null,
        distanceMax: null,
        durationMax: null,
        durationMin: null*/
    };
    
    $scope.filters = {
        airlines: [],
        flights: [],
        departure: [],
        arrival: [],
        stops: [],
        operating: [],
        cabin: '',
        classes: '',
        mktFlights: [],
        operatingByPartner: false,
        wetlease: false,
        /*startDateFrom: null,
        startDateUntil: null,
        endDateFrom: null,
        endDateUntil: null,
        numOfstops: null,
        distanceMax: null,
        distanceMin: null,
        durationMin: null,
        durationMax: null*/
    };
    
    $scope.filtersExpanded = false;
    
    $scope.getAirlines = function(match){
        return collectionService.getCollection('airline/get-airlines.json', 'airline', { match: match }, false);
    };
    
    $scope.getAirports = function(match){
        return collectionService.getCollection('airport/get-airports.json', 'airport', { match: match }, false);
    };
    
    $scope.toggleFilters = function(){
        $scope.filtersExpanded = ! $scope.filtersExpanded;
    };
    
    $scope.areFiltersEmpty = function(){
        
        for(var i in $scope.params){
            if($scope.params.hasOwnProperty(i)){
                var val = $scope.params[i];
                if((angular.isArray(val) || angular.isString(val)) && val.length > 0) return false;
                else if(typeof val == 'boolean' && val === true)  return false;
                else if(val !== null && val !== undefined) return false;
            }
        }
        
        return true;
    };
    
    $scope.onSelectPage = function(page){
        
        var params = { page: page };
        var route = 'pagedFlights';
        
        if(!$scope.areFiltersEmpty()) {
            route = 'searchPagedFlights';
            params.filter = $scope.params;
            delete params.filter.page;
        }

        kkRoutesService.redirect(route, params);
        
    };
    
    function filter(){
        
        var filter = angular.copy($scope.filters);
        filter.airlines = filter.airlines.map(function(i){ return i.code; });
        filter.operating = filter.operating.map(function(i){ return i.code; });
        filter.stops = filter.stops.map(function(i){ return i.code; });
        
        var params = {
            filter: filter
        };
        
        $scope.params = filter;

        if(!$scope.areFiltersEmpty()){
            var path = 'searchFlights';
            kkRoutesService.redirect(path, params);
        }
        else kkRoutesService.redirect('flights');
        
    }
    
    $scope.submitFilters = function(form){
        
        if(!form.$valid) return;
        filter();
        
    };
    
    $scope.resetFilter = function(){
        
        kkRoutesService.redirect('flights');
        
    };
    
    function load(){
        
        var params = angular.copy($scope.params);
        
        collectionService.getPagedCollection(staticDataUrls.flight.collection, staticDataConfig.types.flight, $scope.params).then(function(response){
            $scope.items = response.items;
            $scope.totalItems = response.totalItems;
        }); 
        
    }
    
    function getParams(){
        
        var params = kkRoutesService.getParams({
            page: 'int',
            airlines: 'array',
            flights: 'array',
            operating: 'array',
            departure: 'array',
            arrival: 'array',
            stops: 'array',
            cabin: 'string',
            classes: 'string',
            operatingByPartner: 'bool',
            wetlease: 'bool',
            mktFlights: 'array',
            startDateFrom: 'date',
            startDateUntil: 'date',
            endDateFrom: 'date',
            endDateUntil: 'date',
            numOfStops: 'int',
            distanceMin: 'int',
            distanceMax: 'int',
            durationMax: 'int',
            durationMin: 'int'
        });
        
        delete params.filter;
        angular.extend($scope.params, params);
        angular.extend($scope.filters, params);
        
        $scope.filters.airlines = $scope.filters.airlines.map(function(i){ return { name: i, code: i }; });
        $scope.filters.operating = $scope.filters.operating.map(function(i){ return { name: i, code: i }; });
        $scope.filters.stops = $scope.filters.stops.map(function(i){ return { name: i, code: i }; });
        
        delete $scope.filters.page;
        
    }

    getParams();
    load();
    
}]);