'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', '$filter', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig, $filter){
    
    //kkRoutesService.preventReload($scope, 'AtpcoCollectionController');
    
    $scope.items = null;
    $scope.config = null;
    $scope.filters = {};
    $scope.totalItems = 0;
    
    $scope.subgroups = [];
    $scope.desc1 = [];
    $scope.desc2 = [];
    
    $scope.params = {
        page: 1,
        itemsPerPage: staticDataConfig.itemsPerPage,
        desc2: '',
        desc1: '',
        subgroup: '',
        group: '',
        subcode: '',
        name: ''
    };
    
    var filter = $filter('filter');
    
    $scope.areFiltersEmpty = function(){
        
        return !$scope.filters.desc2 && !$scope.filters.desc1 && !$scope.filters.subgroup 
            && !$scope.filters.group && !$scope.filters.subcode && !$scope.filters.name;
          
    };
    
    $scope.onSelectPage = function(page){
        
        var params = { page: page };
        var route = 'pagedAtpco';
        
        if(!$scope.areFiltersEmpty()) {
            route = 'searchPagedAtpco';
            params.filter = $scope.params;
            delete params.filter.page;
            delete params.filter.itemsPerPage;
        }

        kkRoutesService.redirect(route, params);
        
    };
    
    $scope.submitFilters = function(form){
        
        if(!form.$valid) return;
        
        var filters = angular.copy($scope.filters);
        
        var params = {
            filter: filters
        };
        
        var empty = true;
        
        for(var i in params.filter){
            
            if(params.filter[i]){
                empty = false;
                break;
            }
            
        }
        
        if(empty) kkRoutesService.redirect('atpco');
        else kkRoutesService.redirect('searchAtpco', params);
        
    };
    
    $scope.resetFilters = function(form){
        
        $scope.filters.group = null;
        $scope.filters.subcode = null;
        $scope.filters.subgroup = null;
        $scope.filters.desc1 = null;
        $scope.filters.desc2 = null;
        $scope.filters.name = null;
        
        update();
        
        $scope.submitFilters(form);
          
    };
    
    $scope.onChange = function(field){
        
        update(field);
          
    };
    
    function update(field){
        
        if(!angular.isObject($scope.config)) return false;
        
        $scope.subgroups.splice(0, $scope.subgroups.length);
        $scope.desc1.splice(0, $scope.desc1.length);
        $scope.desc2.splice(0, $scope.desc2.length);
        
        var p = {};
        
        if(angular.isString($scope.filters.group)){
            p.group = $scope.filters.group;
        }
        
        filter($scope.config.subgroups, p).forEach(function(el){ $scope.subgroups.push(el); });
        
        if(angular.isString($scope.filters.subgroup)){
            p.subgroup = $scope.filters.subgroup;
        }
        
        filter($scope.config.desc1, p).forEach(function(el){ $scope.desc1.push(el); });
        
        if(angular.isString($scope.filters.desc1)){
            p.desc1 = $scope.filters.desc1;
        }
        
        filter($scope.config.desc2, p).forEach(function(el){ $scope.desc2.push(el); });
        
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpco.remove, staticDataConfig.types.atpco, item);
    };
    
    function load(){
        
        var params = getParams();
        
        collectionService.getConfig(staticDataUrls.atpco.config, staticDataConfig.types.atpco).then(function(response){
           $scope.config = response;
           update();
        });
        
        collectionService.getPagedCollection(staticDataUrls.atpco.collection, staticDataConfig.types.atpco, params).then(function(response){
            $scope.items = response.items;
            $scope.totalItems = response.totalItems;
            update();
        });
        
    }
    
    function getParams(){
        
        var params = kkRoutesService.getParams({
            subcode: 'string',
            group: 'string',
            subgroup: 'string',
            desc1: 'string',
            desc2: 'string',
            name: 'string',
            page: 'int'
        });
        
        delete params.filter;
        angular.extend($scope.filters, params);
        angular.extend($scope.params, params);
        delete $scope.filters.page;
        delete $scope.filters.itemsPerPage;
        
        return params;
        
    }

    load();
    
}]);