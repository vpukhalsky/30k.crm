'use strict';

angular.module('30k.ma.static-data')

.controller('CountryCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', '$timeout', '$routeParams', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig, $timeout, $routeParams){
    
    var filterPromise = null;
    
    $scope.items = null;
    $scope.totalItems = 0;
    
    $scope.continents = null;
    
    $scope.params = {
        page: 1,
        itemsPerPage: staticDataConfig.itemsPerPage,
        search: ''
    };
    
    $scope.onSelectPage = function(page){
        
        $scope.params.page = page;
        var route = 'pagedCountries';
        if($scope.params.search) route = 'searchPagedCountries';
        kkRoutesService.redirect(route, $scope.params);
        
    };
    
    $scope.deleteCountry = function(country){
        collectionService.deleteItem(staticDataUrls.country.remove, staticDataConfig.types.country, country).then(function(response){
            
            if(response){
                if($scope.items.length == 0 && $scope.params.page > 1) $scope.params.page--;
                load();
            }
            
        });
    };
    
    
    function cancelFilterPromise(){
        
        if(filterPromise) {
            $timeout.cancel(filterPromise);
            filterPromise = null;
        }
        
    }
    
    function filter(){
        
        cancelFilterPromise();
        $scope.params.page = 1;
        if($scope.params.search) kkRoutesService.redirect('searchCountries', $scope.params);
        else kkRoutesService.redirect('countries', $scope.params);
        
    }
    
    $scope.filter = function(form){
        
        if(!form.$valid) return;
        filter();
        
    };
    
    $scope.requestFilter = function(form){
        
        if(!form.$valid) return;
        
        cancelFilterPromise();
        
        filterPromise = $timeout(function(){
            filter();
        }, staticDataConfig.filterDelay);
        
    };
    
    $scope.resetFilter = function(){
        
        $scope.params.search = '';
        filter();
        
    };
    
    function load(){
        
        collectionService.getPagedCollection(staticDataUrls.country.collection, staticDataConfig.types.country, $scope.params).then(function(response){
            $scope.items = response.items;
            $scope.totalItems = response.totalItems;
        });
        
        collectionService.getCollection(staticDataUrls.continent.collection, staticDataConfig.types.continent).then(function(response){
            $scope.continents = response;
        });
        
    }
    
    if(angular.isDefined($routeParams.page)){
        $scope.params.page = parseInt($routeParams.page, 10);
    }
    
    if(angular.isDefined($routeParams.search)){
        $scope.params.search = decodeURIComponent($routeParams.search);
    }
    
    load();
    
}]);