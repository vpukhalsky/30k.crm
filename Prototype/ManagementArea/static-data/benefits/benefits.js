'use strict';

angular.module('30k.ma.static-data')

.controller('BenefitCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'BenefitCollectionController');
    
    $scope.items = null;
    $scope.config = null;
    $scope.descLimit = 20;
    $scope.ffpLimit = 4;
    
    $scope.deleteBenefit = function(item){
        collectionService.deleteItem(staticDataUrls.benefit.remove, staticDataConfig.types.benefit, item);
    };
    
    function load(){
        
        collectionService.getConfig(staticDataUrls.benefit.config, staticDataConfig.types.benefit).then(function(response){
           $scope.config = response; 
        });
        
        collectionService.getCollection(staticDataUrls.benefit.collection, staticDataConfig.types.benefit).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);