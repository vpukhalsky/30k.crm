'use strict';

angular.module('30k.ma.static-data')

.controller('AirlineCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', '$timeout', '$routeParams', '$route', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig, $timeout, $routeParams, $route){
    
    var filterPromise = null;
    
    $scope.items = null;
    $scope.totalItems = 0;
    
    $scope.params = {
        page: 1,
        itemsPerPage: staticDataConfig.itemsPerPage,
        search: '',
        withBrandedFares: false
    };
    
    $scope.onSelectPage = function(page){
        
        $scope.params.page = page;
        
        var route = 'pagedAirlines';
        
        if($scope.params.search) route = 'searchPagedAirlines';
        
        if($scope.params.withBrandedFares) route += 'WithBrandedFares';
        
        kkRoutesService.redirect(route, $scope.params);
    };
    
    $scope.deleteAirline = function(airline){
        collectionService.deleteItem(staticDataUrls.airline.remove, staticDataConfig.types.airline, airline).then(function(response){
            
            if(response){
                if($scope.items.length == 0 && $scope.params.page > 1) $scope.params.page--;
                load();
            }
            
        });
    };
    
    function cancelFilterPromise(){
        
        if(filterPromise) {
            $timeout.cancel(filterPromise);
            filterPromise = null;
        }
        
    }
    
    function filter(){
        
        cancelFilterPromise();
        $scope.params.page = 1;
        
        var route = 'airlines';
        
        if($scope.params.search) route = 'searchAirlines'; 
        if($scope.params.withBrandedFares)  route += 'WithBrandedFares';
        
        kkRoutesService.redirect(route, $scope.params);
        
    }
    
    $scope.filter = function(form){
        
        if(!form.$valid) return;
        filter();
        
    };
    
    $scope.requestFilter = function(form){
        
        if(!form.$valid) return;
        
        cancelFilterPromise();
        
        filterPromise = $timeout(function(){
            filter();
        }, staticDataConfig.filterDelay);
        
    };
    
    $scope.resetFilter = function(){
        
        $scope.params.search = '';
        filter();
        
    };
    
    function load(){
        
        collectionService.getPagedCollection(staticDataUrls.airline.collection, staticDataConfig.types.airline, $scope.params).then(function(response){
            $scope.items = response.items;
            $scope.totalItems = response.totalItems;
        });
        
    }
    
    if(angular.isDefined($routeParams.page)){
        $scope.params.page = parseInt($routeParams.page, 10);
    }
    
    if(angular.isDefined($routeParams.search)){
        $scope.params.search = decodeURIComponent($routeParams.search);
    }
    
    if(angular.isDefined($routeParams.withBrandedFares)){
        $scope.params.withBrandedFares = decodeURIComponent($routeParams.withBrandedFares) == 'true';
    }
    
    load();
    
}]);