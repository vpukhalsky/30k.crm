'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoGroupCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AtpcoGroupCollectionController');
    
    $scope.items = null;
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpcoGroup.remove, staticDataConfig.types.atpcoGroup, item);
    };
    
    function load(){
        
        collectionService.getCollection(staticDataUrls.atpcoGroup.collection, staticDataConfig.types.atpcoGroup).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);