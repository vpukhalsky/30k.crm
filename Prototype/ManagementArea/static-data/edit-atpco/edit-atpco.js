'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', '$filter', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig, $filter){
    
    kkRoutesService.preventReload($scope, 'AtpcoEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.config = null;
    
    $scope.subgroups = [];
    $scope.desc1 = [];
    $scope.desc2 = [];
    
    var filter = $filter('filter');
    
    $scope.onChange = function(field){
        
        update(field);
        
    };
    
    function update(field){
        
        if(!angular.isObject($scope.item) || !angular.isObject($scope.config)) return false;
        
        switch (field) {
            case 'group':
                $scope.item.subgroup = null;
                $scope.item.desc1 = null;
                $scope.item.desc2 = null;
                break;
            case 'subgroup':
                $scope.item.desc1 = null;
                $scope.item.desc2 = null;
                break;
            case 'desc1':
                $scope.item.desc2 = null;
                break;
        }
        
        $scope.subgroups.splice(0, $scope.subgroups.length);
        $scope.desc1.splice(0, $scope.desc1.length);
        $scope.desc2.splice(0, $scope.desc2.length);
        
        var p = {};
        
        if(angular.isString($scope.item.group)){
            p.group = $scope.item.group;
        }
        
        filter($scope.config.subgroups, p).forEach(function(el){ $scope.subgroups.push(el); });
        
        if(angular.isString($scope.item.subgroup)){
            p.subgroup = $scope.item.subgroup;
        }
        
        filter($scope.config.desc1, p).forEach(function(el){ $scope.desc1.push(el); });
        
        if(angular.isString($scope.item.desc1)){
            p.desc1 = $scope.item.desc1;
        }
        
        filter($scope.config.desc2, p).forEach(function(el){ $scope.desc2.push(el); });
        
    }
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('atpco');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        if($scope.item){
            
            $scope.item.group = null;
            $scope.item.subgroup = null;
            $scope.item.desc1 = null;
            $scope.item.desc2 = null;
            
        }else{
            
            $scope.item = {};
        }
        
        $scope.isNew = true;
        update();
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.atpco.save, staticDataConfig.types.atpco, $scope.item).then(callback);
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpco.remove, staticDataConfig.types.atpco, item).then(function(response){
            if(response) kkRoutesService.redirect('atpco');
        });
    };
    
    collectionService.getConfig(staticDataUrls.atpco.config, staticDataConfig.types.atpco).then(function(response){
       $scope.config = response; 
       update();
    });

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.atpco.single, staticDataConfig.types.atpco, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
            update();
        });
        
    }else{
        reset();
    }
    
}]);