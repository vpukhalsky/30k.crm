'use strict';

angular.module('30k.ma.static-data')

.controller('FareAttributeEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'FareAttributeEditController');
    
    $scope.item = null;
    $scope.config = null;
    $scope.isNew = true;
    
    $scope.airlineCodePattern = /^[A-Za-z0-9]{2}$/;
    
    $scope.submit = function(form){
        
        if(!form.$valid || !$scope.isValid()) return;
        
        submit(function(item){
            kkRoutesService.redirect('fareAttributes');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid || !$scope.isValid()) return;
        
        submit(function(item){
            reset();
        });
    };
    
    $scope.isValid = function(){
        return angular.isObject($scope.item) && ($scope.item.alliances.length > 0 || $scope.item.airlines.length > 0);
    };
    
    $scope.selectAllAlliances = function(){
        
        $scope.deselectAlliances();
        $scope.item.alliances.push.apply($scope.item.alliances, $scope.config.alliances.map(function(i){ return i.id; }));
        
    };
    
    $scope.deselectAlliances = function(){
        $scope.item.alliances.splice(0, $scope.item.alliances.length);
    };
    
    function reset(){
        
        $scope.item = {
            alliances: [],
            airlines: []
        };
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.fareAttribute.save, staticDataConfig.types.fareAttribute, $scope.item).then(callback);
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.fareAttribute.remove, staticDataConfig.types.fareAttribute, item).then(function(response){
            if(response) kkRoutesService.redirect('fareAttributes');
        });
    };
    
    function initConfig(){
        
        collectionService.getConfig(staticDataUrls.fareAttribute.config, staticDataConfig.types.fareAttribute).then(function(response){
           $scope.config = response; 
        });
        
    }
    
    function initItem(){
        
        if(angular.isDefined($routeParams.id)){
            
            var id = parseInt($routeParams.id, 10);
            
            collectionService.getItem(staticDataUrls.fareAttribute.single, staticDataConfig.types.fareAttribute, id).then(function(item){
				if (item.items) item = item.items[0];
				
                $scope.item = angular.copy(item);
                $scope.isNew = false;
            });
            
        }else{
            reset();
        }
          
    }
    
    function init(){
        
        initItem();
        initConfig();
        
    }
    
    init();
    
}]);