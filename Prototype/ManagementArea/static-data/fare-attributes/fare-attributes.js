'use strict';

angular.module('30k.ma.static-data')

.controller('FareAttributeCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'FareAttributeCollectionController');
    
    $scope.items = null;
    $scope.config = null;
    $scope.descLimit = 20;
    $scope.airlineLimit = 5;
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.fareAttribute.remove, staticDataConfig.types.fareAttribute, item);
    };
    
    function load(){
        
        collectionService.getConfig(staticDataUrls.fareAttribute.config, staticDataConfig.types.fareAttribute).then(function(response){
           $scope.config = response; 
        });
        
        collectionService.getCollection(staticDataUrls.fareAttribute.collection, staticDataConfig.types.fareAttribute).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);