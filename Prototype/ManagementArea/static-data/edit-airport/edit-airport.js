'use strict';

angular.module('30k.ma.static-data')

.controller('AirportEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', '$filter', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig, $filter){
    
    kkRoutesService.preventReload($scope, 'AirportEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.countries = null;
    $scope.continents = null;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('airports');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        $scope.item = {
            country: {}
        };
        $scope.isNew = true;
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.airport.save, staticDataConfig.types.airport, $scope.item).then(callback);
    }
    
    $scope.deleteAirport = function(airport){
        collectionService.deleteItem(staticDataUrls.airport.remove, staticDataConfig.types.airport, airport).then(function(response){
            if(response) kkRoutesService.redirect('airports');
        });
    };
    
    function load(param, paramName){
        
        collectionService.getItem(staticDataUrls.airport.single, staticDataConfig.types.airport, param, paramName).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }

    if(angular.isDefined($routeParams.id)){
        
        var param = parseInt($routeParams.id, 10);
        var paramName = 'id';
        load(param, paramName);
        
    }else if(angular.isDefined($routeParams.code)){
        
        var param = $routeParams.code;
        var paramName = 'code';
        load(param, paramName);
        
    }else{
        reset();
    }
    
    $scope.getContinent = function(){
        
        if(!angular.isArray($scope.continents) || !angular.isArray($scope.countries) || !$scope.item.country.code) return '';
        
        var find = $filter('find');
        var country = find($scope.countries, $scope.item.country.code, 'code');
        
        if(!country) return '';
        
        var continent = find($scope.continents, country.continent);
        
        return continent.name;
        
    };
    
    collectionService.getCollection(staticDataUrls.country.collection, staticDataConfig.types.country).then(function(response){
        $scope.countries = response;
    });
    
    collectionService.getCollection(staticDataUrls.continent.collection, staticDataConfig.types.continent).then(function(response){
        $scope.continents = response;
    });
    
}]);