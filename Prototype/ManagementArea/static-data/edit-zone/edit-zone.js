'use strict';

angular.module('30k.ma.static-data')

.controller('ZoneEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'ZoneEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    
    $scope.countryCodePattern = /^[a-zA-Z]{2,3}$/;
    $scope.airportCodePattern = /^[a-zA-Z]{3}$/;
    
    $scope.$watchCollection('item.countries', function(newValue){
        
        var valid = (angular.isArray(newValue) && newValue.length > 0) 
            || (angular.isArray($scope.item.airports) && $scope.item.airports.length > 0);
        
        $scope.itemForm.$setValidity('codes', valid);
        
    });
    
    $scope.$watchCollection('item.airports', function(newValue){
        
        var valid = (angular.isArray(newValue) && newValue.length > 0) 
            || (angular.isArray($scope.item.countries) && $scope.item.countries.length > 0);
        
        $scope.itemForm.$setValidity('codes', valid);
        
    });
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('zones');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        $scope.item = {
            countries: [],
            airports: []
        };
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.zone.save, staticDataConfig.types.zone, $scope.item).then(callback);
    }
    
    $scope.deleteZone = function(zone){
        collectionService.deleteItem(staticDataUrls.zone.remove, staticDataConfig.types.zone, zone).then(function(response){
            if(response) kkRoutesService.redirect('zones');
        });
    };

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.zone.single, staticDataConfig.types.zone, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }else{
        reset();
    }
    
}]);