'use strict';

angular.module('30k.ma.static-data')

.controller('ZoneCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'ZoneCollectionController');
    
    $scope.items = null;
    
    $scope.deleteZone = function(zone){
        collectionService.deleteItem(staticDataUrls.zone.remove, staticDataConfig.types.zone, zone);
    };
    
    function load(){
        
        collectionService.getCollection(staticDataUrls.zone.collection, staticDataConfig.types.zone).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);