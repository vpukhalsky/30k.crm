'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoDesc1EditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AtpcoDesc1EditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.config = null;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('atpcoDesc1');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        if($scope.item){
            $scope.item.group = null;
            $scope.item.subgroup = null;
        }
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.atpcoDesc1.save, staticDataConfig.types.atpcoDesc1, $scope.item).then(callback);
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpcoDesc1.remove, staticDataConfig.types.atpcoDesc1, item).then(function(response){
            if(response) kkRoutesService.redirect('atpcoDesc1');
        });
    };
    
    collectionService.getConfig(staticDataUrls.atpcoDesc1.config, staticDataConfig.types.atpcoDesc1).then(function(response){
       $scope.config = response; 
    });

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.atpcoDesc1.single, staticDataConfig.types.atpcoDesc1, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }else{
        reset();
    }
    
}]);