'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoSubgroupEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AtpcoSubgroupEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.config = null;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('atpcoSubgroups');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        if($scope.item){
            $scope.item.group = null;
        }
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.atpcoSubgroup.save, staticDataConfig.types.atpcoSubgroup, $scope.item).then(callback);
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpcoSubgroup.remove, staticDataConfig.types.atpcoSubgroup, item).then(function(response){
            if(response) kkRoutesService.redirect('atpcoSubgroups');
        });
    };
    
    collectionService.getConfig(staticDataUrls.atpcoSubgroup.config, staticDataConfig.types.atpcoSubgroup).then(function(response){
       $scope.config = response; 
    });

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.atpcoSubgroup.single, staticDataConfig.types.atpcoSubgroup, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }else{
        reset();
    }
    
}]);