'use strict';

angular.module('30k.ma.static-data')

.controller('ContinentCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'ContinentCollectionController');
    
    $scope.items = null;
    
    $scope.deleteContinent = function(continent){
        collectionService.deleteItem(staticDataUrls.continent.remove, staticDataConfig.types.continent, continent);
    };
    
    function load(){
        
        collectionService.getCollection(staticDataUrls.continent.collection, staticDataConfig.types.continent).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);