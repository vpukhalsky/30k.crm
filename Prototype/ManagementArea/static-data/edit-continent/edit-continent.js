'use strict';

angular.module('30k.ma.static-data')

.controller('ContinentEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'ContinentEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    
    $scope.countryCodePattern = /^[a-zA-Z]{2,3}$/;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('continents');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        $scope.item = {
            countries: []
        };
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.continent.save, staticDataConfig.types.continent, $scope.item).then(callback);
    }
    
    $scope.deleteContinent = function(continent){
        collectionService.deleteItem(staticDataUrls.continent.remove, staticDataConfig.types.continent, continent).then(function(response){
            if(response) kkRoutesService.redirect('continents');
        });
    };

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.continent.single, staticDataConfig.types.continent, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }else{
        reset();
    }
    
}]);