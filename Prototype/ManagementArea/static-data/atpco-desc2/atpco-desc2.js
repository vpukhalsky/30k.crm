'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoDesc2CollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AtpcoDesc2CollectionController');
    
    $scope.items = null;
    $scope.config = null;
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpcoDesc2.remove, staticDataConfig.types.atpcoDesc2, item);
    };
    
    function load(){
        
        collectionService.getConfig(staticDataUrls.atpcoDesc2.config, staticDataConfig.types.atpcoDesc2).then(function(response){
           $scope.config = response; 
        });
        
        collectionService.getCollection(staticDataUrls.atpcoDesc2.collection, staticDataConfig.types.atpcoDesc2).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);