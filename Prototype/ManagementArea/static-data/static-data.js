'use strict';

angular.module('30k.ma.static-data', [
    
    'mm.foundation',

    '30k.routes',
    '30k.error',
    '30k.service',
    '30k.tag-input',
    '30k.multiselect',
    '30k.period-picker',
    '30k.range-selector',
    
    '30k.ma'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
    
    atpcoGroups: {
      path: '/atpco-groups',
      controller: 'AtpcoGroupCollectionController',
      templateUrl: 'static-data/atpco-groups/atpco-groups.html'
    },
    
    editAtpcoGroup: {
      path: '/atpco-groups/edit/:id',
      controller: 'AtpcoGroupEditController',
      templateUrl: 'static-data/edit-atpco-group/edit-atpco-group.html'
    },
    
    newAtpcoGroup: {
      path: '/atpco-groups/new',
      controller: 'AtpcoGroupEditController',
      templateUrl: 'static-data/edit-atpco-group/edit-atpco-group.html'
    },
    
    atpcoSubgroups: {
      path: '/atpco-subgroups',
      controller: 'AtpcoSubgroupCollectionController',
      templateUrl: 'static-data/atpco-subgroups/atpco-subgroups.html'
    },
    
    editAtpcoSubgroup: {
      path: '/atpco-subgroups/edit/:id',
      controller: 'AtpcoSubgroupEditController',
      templateUrl: 'static-data/edit-atpco-subgroup/edit-atpco-subgroup.html'
    },
    
    newAtpcoSubgroup: {
      path: '/atpco-subgroups/new',
      controller: 'AtpcoSubgroupEditController',
      templateUrl: 'static-data/edit-atpco-subgroup/edit-atpco-subgroup.html'
    },
    
    atpcoDesc1: {
      path: '/atpco-desc1',
      controller: 'AtpcoDesc1CollectionController',
      templateUrl: 'static-data/atpco-desc1/atpco-desc1.html'
    },
    
    editAtpcoDesc1: {
      path: '/atpco-desc1/edit/:id',
      controller: 'AtpcoDesc1EditController',
      templateUrl: 'static-data/edit-atpco-desc1/edit-atpco-desc1.html'
    },
    
    newAtpcoDesc1: {
      path: '/atpco-desc1/new',
      controller: 'AtpcoDesc1EditController',
      templateUrl: 'static-data/edit-atpco-desc1/edit-atpco-desc1.html'
    },
    
    atpcoDesc2: {
      path: '/atpco-desc2',
      controller: 'AtpcoDesc2CollectionController',
      templateUrl: 'static-data/atpco-desc2/atpco-desc2.html'
    },
    
    editAtpcoDesc2: {
      path: '/atpco-desc2/edit/:id',
      controller: 'AtpcoDesc2EditController',
      templateUrl: 'static-data/edit-atpco-desc2/edit-atpco-desc2.html'
    },
    
    newAtpcoDesc2: {
      path: '/atpco-desc2/new',
      controller: 'AtpcoDesc2EditController',
      templateUrl: 'static-data/edit-atpco-desc2/edit-atpco-desc2.html'
    },
    
    searchPagedAtpco: {
      path: '/atpco/page/:page/filter/:filter*',
      controller: 'AtpcoCollectionController',
      templateUrl: 'static-data/atpco/atpco.html'
    },
    
    searchAtpco: {
      path: '/atpco/filter/:filter*',
      controller: 'AtpcoCollectionController',
      templateUrl: 'static-data/atpco/atpco.html'
    },
    
    pagedAtpco: {
      path: '/atpco/page/:page',
      controller: 'AtpcoCollectionController',
      templateUrl: 'static-data/atpco/atpco.html'
    },
    
    atpco: {
      path: '/atpco',
      controller: 'AtpcoCollectionController',
      templateUrl: 'static-data/atpco/atpco.html'
    },
    
    editAtpco: {
      path: '/atpco/edit/:id',
      controller: 'AtpcoEditController',
      templateUrl: 'static-data/edit-atpco/edit-atpco.html'
    },
    
    newAtpco: {
      path: '/atpco/new',
      controller: 'AtpcoEditController',
      templateUrl: 'static-data/edit-atpco/edit-atpco.html'
    },
    
    benefits: {
      path: '/benefits',
      controller: 'BenefitCollectionController',
      templateUrl: 'static-data/benefits/benefits.html'
    },
    
    editBenefit: {
      path: '/benefits/edit/:id',
      controller: 'BenefitEditController',
      templateUrl: 'static-data/edit-benefit/edit-benefit.html'
    },
    
    newBenefit: {
      path: '/benefits/new',
      controller: 'BenefitEditController',
      templateUrl: 'static-data/edit-benefit/edit-benefit.html'
    },
    
    fareAttributes: {
      path: '/fare-attributes',
      controller: 'FareAttributeCollectionController',
      templateUrl: 'static-data/fare-attributes/fare-attributes.html'
    },
    
    editFareAttribute: {
      path: '/fare-attributes/edit/:id',
      controller: 'FareAttributeEditController',
      templateUrl: 'static-data/edit-fare-attribute/edit-fare-attribute.html'
    },
    
    newFareAttribute: {
      path: '/fare-attributes/new',
      controller: 'FareAttributeEditController',
      templateUrl: 'static-data/edit-fare-attribute/edit-fare-attribute.html'
    },
    
    airlines: {
      path: '/airlines',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    },
    
    airlinesWithBrandedFares: {
      path: '/airlines/branded-fares/:withBrandedFares',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    },
    
    searchPagedAirlines: {
      path: '/airlines/search/:search/page/:page',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    },
    
    searchPagedAirlinesWithBrandedFares: {
      path: '/airlines/branded-fares/:withBrandedFares/search/:search/page/:page',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    },
    
    searchAirlines: {
      path: '/airlines/search/:search',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    },
    
    searchAirlinesWithBrandedFares: {
      path: '/airlines/branded-fares/:withBrandedFares/search/:search',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    },
    
    pagedAirlines: {
      path: '/airlines/page/:page',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    },
    
    pagedAirlinesWithBrandedFares: {
      path: '/airlines/branded-fares/:withBrandedFares/page/:page',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    },
    
    editAirline: {
      path: '/airlines/edit/:id',
      controller: 'AirlineEditController',
      templateUrl: 'static-data/edit-airline/edit-airline.html'
    },
    
    newAirline: {
      path: '/airlines/new',
      controller: 'AirlineEditController',
      templateUrl: 'static-data/edit-airline/edit-airline.html'
    },
    
    alliances: {
      path: '/alliances',
      controller: 'AllianceCollectionController',
      templateUrl: 'static-data/alliances/alliances.html'
    },
    
    editAlliance: {
      path: '/alliances/edit/:id',
      controller: 'AllianceEditController',
      templateUrl: 'static-data/edit-alliance/edit-alliance.html'
    },
    
    newAlliance: {
      path: '/alliances/new',
      controller: 'AllianceEditController',
      templateUrl: 'static-data/edit-alliance/edit-alliance.html'
    },
    
    searchPagedAirportsSearchCityCountriesContinents: {
      path: '/airports/search/:search/city/:city/countries/:countries/continents/:continents/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsSearch: {
      path: '/airports/search/:search/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsCity: {
      path: '/airports/city/:city/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsCountries: {
      path: '/airports/countries/:countries/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsContinents: {
      path: '/airports/continents/:continents/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsSearchCity: {
      path: '/airports/search/:search/city/:city/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsSearchCountries: {
      path: '/airports/search/:search/countries/:countries/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsSearchContinents: {
      path: '/airports/search/:search/continents/:continents/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsSearchCityCountries: {
      path: '/airports/search/:search/city/:city/countries/:countries/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsSearchCityContinents: {
      path: '/airports/search/:search/city/:city/continents/:continents/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsSearchCountriesContinents: {
      path: '/airports/search/:search/countries/:countries/continents/:continents/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsCityCountries: {
      path: '/airports/city/:city/countries/:countries/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsCityCountriesContinents: {
      path: '/airports/city/:city/countries/:countries/continents/:continents/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsCityContinents: {
      path: '/airports/city/:city/continents/:continents/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchPagedAirportsCountriesContinents: {
      path: '/airports/countries/:countries/continents/:continents/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsSearchCityCountriesContinents: {
      path: '/airports/search/:search/city/:city/countries/:countries/continents/:continents',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsSearch: {
      path: '/airports/search/:search',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsCity: {
      path: '/airports/city/:city',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsCountries: {
      path: '/airports/countries/:countries',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsContinents: {
      path: '/airports/continents/:continents',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsSearchCity: {
      path: '/airports/search/:search/city/:city',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsSearchCountries: {
      path: '/airports/search/:search/countries/:countries',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsSearchContinents: {
      path: '/airports/search/:search/continents/:continents',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsSearchCityCountries: {
      path: '/airports/search/:search/city/:city/countries/:countries',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsSearchCityContinents: {
      path: '/airports/search/:search/city/:city/continents/:continents',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsSearchCountriesContinents: {
      path: '/airports/search/:search/countries/:countries/continents/:continents',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsCityCountries: {
      path: '/airports/city/:city/countries/:countries',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsCityCountriesContinents: {
      path: '/airports/city/:city/countries/:countries/continents/:continents',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsCityContinents: {
      path: '/airports/city/:city/continents/:continents',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    searchAirportsCountriesContinents: {
      path: '/airports/countries/:countries/continents/:continents',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    pagedAirports: {
      path: '/airports/page/:page',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    airports: {
      path: '/airports',
      controller: 'AirportCollectionController',
      templateUrl: 'static-data/airports/airports.html'
    },
    
    editAirportByCode: {
      path: '/airports/edit/code/:code',
      controller: 'AirportEditController',
      templateUrl: 'static-data/edit-airport/edit-airport.html'
    },
    
    editAirport: {
      path: '/airports/edit/:id',
      controller: 'AirportEditController',
      templateUrl: 'static-data/edit-airport/edit-airport.html'
    },
    
    newAirport: {
      path: '/airports/new',
      controller: 'AirportEditController',
      templateUrl: 'static-data/edit-airport/edit-airport.html'
    },
    
    zones: {
      path: '/zones',
      controller: 'ZoneCollectionController',
      templateUrl: 'static-data/zones/zones.html'
    },
    
    editZone: {
      path: '/zones/edit/:id',
      controller: 'ZoneEditController',
      templateUrl: 'static-data/edit-zone/edit-zone.html'
    },
    
    newZone: {
      path: '/zones/new',
      controller: 'ZoneEditController',
      templateUrl: 'static-data/edit-zone/edit-zone.html'
    },
    
    continents: {
      path: '/continents',
      controller: 'ContinentCollectionController',
      templateUrl: 'static-data/continents/continents.html'
    },
    
    editContinent: {
      path: '/continents/edit/:id',
      controller: 'ContinentEditController',
      templateUrl: 'static-data/edit-continent/edit-continent.html'
    },
    
    newContinent: {
      path: '/continents/new',
      controller: 'ContinentEditController',
      templateUrl: 'static-data/edit-continent/edit-continent.html'
    },
    
    countries: {
      path: '/countries',
      controller: 'CountryCollectionController',
      templateUrl: 'static-data/countries/countries.html'
    },
    
    searchPagedCountries: {
      path: '/countries/search/:search/page/:page',
      controller: 'CountryCollectionController',
      templateUrl: 'static-data/countries/countries.html'
    },
    
    searchCountries: {
      path: '/countries/search/:search',
      controller: 'CountryCollectionController',
      templateUrl: 'static-data/countries/countries.html'
    },
    
    pagedCountries: {
      path: '/countries/page/:page',
      controller: 'CountryCollectionController',
      templateUrl: 'static-data/countries/countries.html'
    },
    
    editCountry: {
      path: '/countries/edit/:id',
      controller: 'CountryEditController',
      templateUrl: 'static-data/edit-country/edit-country.html'
    },
    
    newCountry: {
      path: '/countries/new',
      controller: 'CountryEditController',
      templateUrl: 'static-data/edit-country/edit-country.html'
    },
    
    searchPagedFlights: {
      path: '/flight-numbers/page/:page/filter/:filter*',
      controller: 'FlightCollectionController',
      templateUrl: 'static-data/flights/flights.html'
    },
    
    searchFlights: {
      path: '/flight-numbers/filter/:filter*',
      controller: 'FlightCollectionController',
      templateUrl: 'static-data/flights/flights.html'
    },
    
    pagedFlights: {
      path: '/flight-numbers/page/:page',
      controller: 'FlightCollectionController',
      templateUrl: 'static-data/flights/flights.html'
    },
    
    flights: {
      path: '/flight-numbers',
      controller: 'FlightCollectionController',
      templateUrl: 'static-data/flights/flights.html'
    },
    
    main: {
      path: '/airlines',
      controller: 'AirlineCollectionController',
      templateUrl: 'static-data/airlines/airlines.html'
    }
    
  });
  
}])

.constant('staticDataUrls',{
    
    atpco: {
        remove: 'atpco/delete-atpco.json',
        collection: 'atpco/get-atpcos.json',
        single: 'atpco/get-atpco.json',
        save: 'atpco/save-atpco.json',
        config: 'atpco/config.json'
    },
    
    atpcoGroup: {
        remove: 'atpco-group/delete-atpco-group.json',
        collection: 'atpco-group/get-atpco-groups.json',
        single: 'atpco-group/get-atpco-group.json',
        save: 'atpco-group/save-atpco-group.json'
    },
    
    atpcoSubgroup: {
        remove: 'atpco-subgroup/delete-atpco-subgroup.json',
        collection: 'atpco-subgroup/get-atpco-subgroups.json',
        single: 'atpco-subgroup/get-atpco-subgroup.json',
        save: 'atpco-subgroup/save-atpco-subgroup.json',
        config: 'atpco-subgroup/config.json'
    },
    
    atpcoDesc1: {
        remove: 'atpco-desc1/delete-atpco-desc1.json',
        collection: 'atpco-desc1/get-atpco-desc1s.json',
        single: 'atpco-desc1/get-atpco-desc1.json',
        save: 'atpco-desc1/save-atpco-desc1.json',
        config: 'atpco-desc1/config.json'
    },
    
    atpcoDesc2: {
        remove: 'atpco-desc2/delete-atpco-desc2.json',
        collection: 'atpco-desc2/get-atpco-desc2s.json',
        single: 'atpco-desc2/get-atpco-desc2.json',
        save: 'atpco-desc2/save-atpco-desc2.json',
        config: 'atpco-desc2/config.json'
    },
    
    fareAttribute: {
        remove: 'fare-attribute/delete-fare-attribute.json',
        collection: 'fare-attribute/get-fare-attributes.json',
        single: 'fare-attribute/get-fare-attribute.json',
        save: 'fare-attribute/save-fare-attribute.json',
        config: 'fare-attribute/config.json'
    },
    
    benefit: {
        remove: 'benefit/delete-benefit.json',
        collection: 'benefit/get-benefits.json',
        single: 'benefit/get-benefit.json',
        save: 'benefit/save-benefit.json',
        config: 'benefit/config.json'
    },
    
    airline: {
        remove: 'airline/delete-airline.json',
        collection: 'airline/get-airlines.json',
        single: 'airline/get-airline.json',
        save: 'airline/save-airline.json'
    },
    
    alliance: {
      remove: 'alliance/delete-alliance.json',
      collection: 'alliance/get-alliances.json',
      single: 'alliance/get-alliance.json',
      save: 'alliance/save-alliance.json'
    },
    
    airport: {
      remove: 'airport/delete-airport.json',
      collection: 'airport/get-airports.json',
      single: 'airport/get-airport.json',
      save: 'airport/save-airport.json'
    },
    
    country: {
      remove: 'country/delete-country.json',
      collection: 'country/get-countries.json',
      single: 'country/get-country.json',
      save: 'country/save-country.json'
    },
    
    zone: {
      remove: 'zone/delete-zone.json',
      collection: 'zone/get-zones.json',
      single: 'zone/get-zone.json',
      save: 'zone/save-zone.json'
    },
    
    continent: {
      remove: 'continent/delete-continent.json',
      collection: 'continent/get-continents.json',
      single: 'continent/get-continent.json',
      save: 'continent/save-continent.json'
    },
    
    flight: {
      collection: 'flight/get-flights.json'
    }
    
})

.constant('staticDataConfig', {
    itemsPerPage: 6,
    filterDelay: 500,
    types: {
        atpcoGroup: 'atpcoGroup',
        atpcoSubgroup: 'atpcoSubgroup',
        atpcoDesc1: 'atpcoDesc1',
        atpcoDesc2: 'atpcoDesc2',
        atpco: 'atpco',
        fareAttribute: 'fareAttribute',
        airline: 'airline',
        airport: 'airport',
        alliance: 'alliance',
        country: 'country',
        zone: 'zone',
        continent: 'continent',
        flight: 'flight',
        benefit: 'benefit'
    },
    fareBasisCodeMaxLength: 15,
    brandedFareInputTypes: {
      position: { id: 'position', name: 'Position' },
      regExp: { id: 'regexp', name: 'RegExp' }
    }
});