'use strict';

angular.module('30k.ma.static-data')

.controller('BenefitEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'BenefitEditController');
    
    $scope.item = null;
    $scope.config = null;
    $scope.isNew = true;
    
    $scope.ffpCodePattern = /^[A-Za-z0-9]{3}$/;
    
    $scope.submit = function(form){
        
        if(!form.$valid || !$scope.isValid()) return;
        
        submit(function(item){
            kkRoutesService.redirect('benefits');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid || !$scope.isValid()) return;
        
        submit(function(item){
            reset();
        });
    };
    
    $scope.isValid = function(){
        return angular.isObject($scope.item) && ($scope.item.alliances.length > 0 || $scope.item.ffps.length > 0);
    };
    
    $scope.selectAllAlliances = function(){
        
        $scope.deselectAlliances();
        $scope.item.alliances.push.apply($scope.item.alliances, $scope.config.alliances.map(function(i){ return i.id; }));
        
    };
    
    $scope.deselectAlliances = function(){
        $scope.item.alliances.splice(0, $scope.item.alliances.length);
    };
    
    function reset(){
        
        $scope.item = {
            alliances: [],
            ffps: []
        };
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.benefit.save, staticDataConfig.types.benefit, $scope.item).then(callback);
    }
    
    $scope.deleteBenefit = function(benefit){
        collectionService.deleteItem(staticDataUrls.benefit.remove, staticDataConfig.types.benefit, benefit).then(function(response){
            if(response) kkRoutesService.redirect('benefits');
        });
    };
    
    function initConfig(){
        
        collectionService.getConfig(staticDataUrls.benefit.config, staticDataConfig.types.benefit).then(function(response){
           $scope.config = response; 
        });
        
    }
    
    function initItem(){
        
        if(angular.isDefined($routeParams.id)){
            
            var id = parseInt($routeParams.id, 10);
            
            collectionService.getItem(staticDataUrls.benefit.single, staticDataConfig.types.benefit, id).then(function(item){
				if (item.items) item = item.items[0];
				
                $scope.item = angular.copy(item);
                $scope.isNew = false;
            });
            
        }else{
            reset();
        }
          
    }
    
    function init(){
        
        initItem();
        initConfig();
        
    }
    
    init();
    
}]);