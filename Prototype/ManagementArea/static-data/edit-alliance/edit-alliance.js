'use strict';

angular.module('30k.ma.static-data')

.controller('AllianceEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AllianceEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    
    $scope.codePattern = /^[a-zA-Z][a-zA-Z0-9]$/;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('alliances');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        $scope.item = {
            members: []
        };
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.alliance.save, staticDataConfig.types.alliance, $scope.item).then(callback);
    }
    
    $scope.deleteAlliance = function(alliance){
        collectionService.deleteItem(staticDataUrls.alliance.remove, staticDataConfig.types.alliance, alliance).then(function(response){
            if(response) kkRoutesService.redirect('alliances');
        });
    };

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.alliance.single, staticDataConfig.types.alliance, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }else{
        reset();
    }
    
}]);