'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoDesc2EditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AtpcoDesc2EditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.config = null;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('atpcoDesc2');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        if($scope.item){
            $scope.item.group = null;
            $scope.item.subgroup = null;
        }
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.atpcoDesc2.save, staticDataConfig.types.atpcoDesc2, $scope.item).then(callback);
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpcoDesc2.remove, staticDataConfig.types.atpcoDesc2, item).then(function(response){
            if(response) kkRoutesService.redirect('atpcoDesc2');
        });
    };
    
    collectionService.getConfig(staticDataUrls.atpcoDesc2.config, staticDataConfig.types.atpcoDesc2).then(function(response){
       $scope.config = response; 
    });

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.atpcoDesc2.single, staticDataConfig.types.atpcoDesc2, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }else{
        reset();
    }
    
}]);