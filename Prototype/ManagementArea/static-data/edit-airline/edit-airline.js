'use strict';

angular.module('30k.ma.static-data')

.controller('AirlineEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', '$timeout', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig, $timeout){
    
    kkRoutesService.preventReload($scope, 'AirlineEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.config = staticDataConfig;
    
    $scope.addBrandedFare = function(){
        
        $scope.item.brandedFares.push({
            code: '',
            name: '',
            inputType: staticDataConfig.brandedFareInputTypes.position.id,
            nameOptions: []
        });
        
    };
    
    $scope.removeBrandedFare = function(index){
        
        $scope.item.brandedFares.splice(index, 1);
          
    };
    
    $scope.getPlaceholder = function(bf){
        
        var result = '';
        
        if(angular.isNumber(bf.substringLength) && bf.substringLength > 0){
            
            for(var i = 0 ; i < bf.substringLength ; i++){
                result += 'X';
            }
            
        }
        
        return result;
          
    };
    
    $scope.submit = function(form){
        
        if(!form.$valid) return false;
        
        if(angular.isArray($scope.item.brandedFares)){
            
            for(var i = 0 ; i < $scope.item.brandedFares.length ; i++){
                
                var bf = $scope.item.brandedFares[i];
                
                if(bf.inputType == staticDataConfig.brandedFareInputTypes.position.id 
                    && (!angular.isNumber(bf.substringStart) || !angular.isNumber(bf.substringLength) || bf.substringLength <= 0 )
                ){
                    return false;
                } 
                
            }
            
        }
        
        submit(function(response){
            kkRoutesService.redirect('airlines');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        $scope.item = {
            active: true,
            brandedFares: [],
            brandedFaresActive: false
        };
        
        $scope.isNew = true;
        
    }
    
    function toSave(item){
        
        if(angular.isArray(item.brandedFares)){
            
            for(var j = 0 ; j < item.brandedFares.length ; j++){
                
                var bf = item.brandedFares[j];
                if(bf.code) bf.code = bf.code.toUpperCase();
                
                if(bf.inputType == staticDataConfig.brandedFareInputTypes.position.id){
                    if(bf.regExp) delete bf.regExp;
                }else if(bf.inputType == staticDataConfig.brandedFareInputTypes.regExp.id){
                    if(bf.code) delete bf.code;
                    if(bf.substringLength) delete bf.substringLength;
                    if(bf.substringStart) delete bf.substringStart;
                }
                
                //if(angular.isString(bf.regExp)) bf.regExp = '^' + bf.regExp + '$';
                
            }
            
        }
        
        return item;
        
    }
    
    function submit(callback){
        
        var item = toSave($scope.item);
        
        collectionService.saveItem(staticDataUrls.airline.save, staticDataConfig.types.airline, item).then(callback);
    }
    
    $scope.deleteAirline = function(airline){
        collectionService.deleteItem(staticDataUrls.airline.remove, staticDataConfig.types.airline, airline).then(function(response){
            if(response) kkRoutesService.redirect('airlines');
        });
    };

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.airline.single, staticDataConfig.types.airline, id).then(function(item){
            
            $scope.item = angular.copy(item);
            $scope.isNew = false;
            
        });
        
    }else{
        reset();
    }
    
}]);