'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoDesc1CollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AtpcoDesc1CollectionController');
    
    $scope.items = null;
    $scope.config = null;
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpcoDesc1.remove, staticDataConfig.types.atpcoDesc1, item);
    };
    
    function load(){
        
        collectionService.getConfig(staticDataUrls.atpcoDesc1.config, staticDataConfig.types.atpcoDesc1).then(function(response){
           $scope.config = response; 
        });
        
        collectionService.getCollection(staticDataUrls.atpcoDesc1.collection, staticDataConfig.types.atpcoDesc1).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);