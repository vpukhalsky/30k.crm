'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoGroupEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AtpcoGroupEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('atpcoGroups');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        $scope.isNew = true;
        
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.atpcoGroup.save, staticDataConfig.types.atpcoGroup, $scope.item).then(callback);
    }
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpcoGroup.remove, staticDataConfig.types.atpcoGroup, item).then(function(response){
            if(response) kkRoutesService.redirect('atpcoGroups');
        });
    };

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.atpcoGroup.single, staticDataConfig.types.atpcoGroup, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }else{
        reset();
    }
    
}]);