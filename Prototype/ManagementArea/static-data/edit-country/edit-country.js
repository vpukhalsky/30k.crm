'use strict';

angular.module('30k.ma.static-data')

.controller('CountryEditController', ['$scope', 'collectionService', 'kkRoutesService', '$routeParams', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, $routeParams, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'CountryEditController');
    
    $scope.item = null;
    $scope.isNew = true;
    $scope.continents = null;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        submit(function(item){
            kkRoutesService.redirect('countries');
        });
        
    };
    
    $scope.submitAndStay = function(form){
        
        $scope.$broadcast('kk-error', form);
        if(!form.$valid) return;
        
        submit(function(item){
            reset();
        });
    };
    
    function reset(){
        
        $scope.item = {
            active: true
        };
        
        $scope.isNew = true;
    }
    
    function submit(callback){
        collectionService.saveItem(staticDataUrls.country.save, staticDataConfig.types.country, $scope.item).then(callback);
    }
    
    $scope.deleteCountry = function(country){
        collectionService.deleteItem(staticDataUrls.country.remove, staticDataConfig.types.country, country).then(function(response){
            if(response) kkRoutesService.redirect('countries');
        });
    };

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        collectionService.getItem(staticDataUrls.country.single, staticDataConfig.types.country, id).then(function(item){
            $scope.item = angular.copy(item);
            $scope.isNew = false;
        });
        
    }else{
        reset();
    }
    
    collectionService.getCollection(staticDataUrls.continent.collection, staticDataConfig.types.continent).then(function(response){
        $scope.continents = response;
    });
    
}]);