'use strict';

angular.module('30k.ma.static-data')

.controller('AtpcoSubgroupCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AtpcoSubgroupCollectionController');
    
    $scope.items = null;
    $scope.config = null;
    
    $scope.deleteItem = function(item){
        collectionService.deleteItem(staticDataUrls.atpcoSubgroup.remove, staticDataConfig.types.atpcoSubgroup, item);
    };
    
    function load(){
        
        collectionService.getConfig(staticDataUrls.atpcoSubgroup.config, staticDataConfig.types.atpcoSubgroup).then(function(response){
           $scope.config = response; 
        });
        
        collectionService.getCollection(staticDataUrls.atpcoSubgroup.collection, staticDataConfig.types.atpcoSubgroup).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);