'use strict';

angular.module('30k.ma.static-data')

.controller('AllianceCollectionController', ['$scope', 'collectionService', 'kkRoutesService', 'staticDataUrls', 'staticDataConfig', function($scope, collectionService, kkRoutesService, staticDataUrls, staticDataConfig){
    
    kkRoutesService.preventReload($scope, 'AllianceCollectionController');
    
    $scope.items = null;
    
    $scope.deleteAlliance = function(alliance){
        collectionService.deleteItem(staticDataUrls.alliance.remove, staticDataConfig.types.alliance, alliance);
    };
    
    function load(){
        
        collectionService.getCollection(staticDataUrls.alliance.collection, staticDataConfig.types.alliance).then(function(response){
            $scope.items = response;
        });
        
    }
    
    load();
    
}]);