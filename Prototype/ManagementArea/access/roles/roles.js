'use strict';

angular.module('30k.ma.access')

.controller('RoleCollectionController', ['$scope', 'roleService', 'toolService', 'kkRoutes', 'findFilter', '$modal', 'kkRoutesService', function($scope, roleService, toolService, kkRoutes, findFilter, $modal, kkRoutesService){
    
    kkRoutesService.preventReload($scope, 'RoleCollectionController');
    
    $scope.roles = null;
    $scope.tools = null;
    
    var totalPermissions = 0;
    
    function getToolPermissionTypes(tool){
        
        var types = [];
        var t = findFilter($scope.tools, tool.id);
        
        if(t && angular.isArray(tool.permissions)){
            for(var i = 0 ; i < tool.permissions.length ; i++)
            var p = findFilter(t.permissions, tool.permissions[i]);
            if(p) types.push(p.type);
        }
        
        return types;
        
    }
    
    $scope.canView = function(tool){
        var types = getToolPermissionTypes(tool);
        return types.indexOf('view');
    };
    
    $scope.canEdit = function(tool){
        var types = getToolPermissionTypes(tool);
        return types.indexOf('edit');
    };
    
    $scope.canDelete = function(tool){
        var types = getToolPermissionTypes(tool);
        return types.indexOf('delete');
    };
    
    $scope.canOther = function(tool){
        var types = getToolPermissionTypes(tool);
        return types.indexOf('other');
    };
    
    $scope.getRoleEditUrl = function(id){
        return kkRoutes.editRole.path.replace(':id', id);
    };
    
    $scope.getSingleToolsDescription = function(role){
        
        var result = null;  
        var permissions = 0;
        
        for(var i = 0 ; i < role.tools.length ; i++){
            if(angular.isArray(role.tools[i].permissions)) permissions += role.tools[i].permissions;
        }
        
        if(permissions <= 0) result = 'None';
        else if(permissions >= totalPermissions) result = 'Full Access';
        
        return result;
        
    };
    
    $scope.getToolName = function(tool){

        var t = findFilter($scope.tools, tool.id);
        if(t) return t.name;
        else return '';
        
    };
    
    $scope.promptDeleteRole = function(role){
        
        var modalInstance = $modal.open({
            templateUrl: 'access/delete-role-modal.html',
            controller: 'AccessDeleteRoleModalController',
            windowClass: 'tiny',
            resolve: {
                role: function(){
                    return role;
                }
            }
        });
        
        modalInstance.result.then(function(result){
            if(result.remove == true){
                if($scope.roles.length > 1) promptReplaceDeletedRole(result.role);
                else deleteRole(role.id);
            }
        });
        
    };
    
    function promptReplaceDeletedRole(role){
        
        var modalInstance = $modal.open({
            templateUrl: 'access/replace-role-modal.html',
            controller: 'AccessReplaceRoleModalController',
            windowClass: 'tiny',
            resolve: {
                role: function(){
                    return role;
                },
                roles: function(){
                    return $scope.roles;
                }
            }
        });
        
        modalInstance.result.then(function(result){
            deleteRole(result.role, result.replaceWith);
        });
        
    }
    
    function deleteRole(role, replaceWith){
        roleService.deleteRole(role, replaceWith);
    }
    
    $scope.$watchCollection('tools', function(newValue){
        
        totalPermissions = 0;
        if(!angular.isArray(newValue)) return; 
        
        for(var i = 0 ; i < newValue.length ; i++){
            if(angular.isArray(newValue[i].permissions)) totalPermissions += newValue[i].permissions.length;
        }
        
    });
    
    roleService.getCollection().then(function(roles){
        $scope.roles = roles;
    });
    
    toolService.getCollection().then(function(tools){
        $scope.tools = tools;
    });
    
}]);