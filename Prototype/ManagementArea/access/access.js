'use strict';

angular.module('30k.ma.access', [
    
    'mm.foundation',
    'checklist-model',
    
    '30k.error',
    '30k.role.service',
    '30k.tool.service',
    '30k.routes',
    '30k.tooltip',
    
    '30k.ma'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
      
    newRole: {
      path: '/roles/new',
      controller: 'RoleEditController',
      templateUrl: 'access/edit/edit.html'
    },
    
    editRole: {
      path: '/roles/edit/:id',
      controller: 'RoleEditController',
      templateUrl: 'access/edit/edit.html'
    },
    
    roles: {
      path: '/roles',
      controller: 'RoleCollectionController',
      templateUrl: 'access/roles/roles.html'
    },
    
    main: {
      path: '/roles',
      controller: 'RoleCollectionController',
      templateUrl: 'access/roles/roles.html'
    }
    
  });
  
}])


.controller('AccessDeleteRoleModalController', ['$scope', '$modalInstance', 'role', function($scope, $modalInstance, role){
    
    $scope.role = role;
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.confirm = function(){
        $modalInstance.close({
            remove: true,
            role: role
        });
    };
    
}])

.controller('AccessReplaceRoleModalController', ['$scope', '$modalInstance', 'role', 'roles', 'roleService', function($scope, $modalInstance, role, roles, roleService){
    
    $scope.role = role;
    $scope.roles = [];
    
    $scope.newRole = 0;
    
    roleService.getDefaultRoleId().then(function(roleId){
        $scope.newRole = roleId;
    });
    
    for(var i = 0 ; i < roles.length ; i++){
        if(roles[i].id != role.id) $scope.roles.push(roles[i]);
    }
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.confirm = function(form){
        
        if(!form.$valid) return;
        
        $modalInstance.close({
            remove: true,
            role: role.id,
            replaceWith: $scope.newRole
        });
        
    };
    
}]);