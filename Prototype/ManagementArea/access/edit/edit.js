'use strict';

angular.module('30k.ma.access')

.controller('RoleEditController', ['$scope', 'roleService', 'toolService', '$modal', 'kkRoutesService', '$routeParams', 'findFilter', function($scope, roleService, toolService, $modal, kkRoutesService, $routeParams, findFilter){
    
    kkRoutesService.preventReload($scope, 'RoleEditController');
    
    $scope.role = null;
    $scope.isNew = true;
    $scope.tools = null;
    $scope.permissions = [];
    
    var roles = null;
    var totalPermissions = 0;
    
    $scope.arePermissionsSelected = function(type){
        
        var result = false;
        
        if(type == 'all'){
            
            result = $scope.permissions.length >= totalPermissions;
            
        }else{
            
            var permIds = getPermIdsByType(type);
            
            if($scope.permissions.length >= permIds.length){
                
                result = true;
                
                for(var i = 0 ; i < permIds.length ; i++){
                    if($scope.permissions.indexOf(permIds[i]) < 0){
                        result = false;
                        break;
                    }
                }

            }
        }
        
        return result;
    };
    
    function getPermIdsByType(type){
        
        var result = [];
        
        for(var i = 0 ; i < $scope.tools.length ; i++){
            var tool = $scope.tools[i];
            
            if(angular.isArray(tool.permissions)){
                for(var j = 0 ; j < tool.permissions.length ; j++){
                    var perm = tool.permissions[j];
                    if(perm.type == type) result.push(perm.id);
                }
            }
        }
        
        return result;
    }
    
    $scope.select = function(type){
        
        if(type == 'all'){
            
            $scope.select('none');
            
            for(var i = 0 ; i < $scope.tools.length ; i++){
                var tool = $scope.tools[i];
                if(angular.isArray(tool.permissions)){
                    for(var j = 0 ; j < tool.permissions.length ; j++){
                        var perm = tool.permissions[j];
                        $scope.permissions.push(perm.id);
                    }
                }
            }
            
        }else if(type == 'none'){
            
            while($scope.permissions.length > 0) $scope.permissions.splice(0, 1);
            
        }else{
            
            $scope.select('none');
            var permIds = getPermIdsByType(type);
            
            for(var i = 0 ; i < permIds.length ; i++){
                $scope.permissions.push(permIds[i]);
            }
            
        }
        
    };
    
    $scope.$watchCollection('tools', function(newValue){
        
        totalPermissions = 0;
        
        if(angular.isArray(newValue)){
            
            for(var i = 0 ; i < newValue.length ; i++){
                if(angular.isArray(newValue[i].permissions)) totalPermissions += newValue[i].permissions.length;
            }
            
        }
        
    });
    
    $scope.$watchCollection('permissions', function(newValue, oldValue){
        
        if(!$scope.tools) return;
        if(!angular.isArray(newValue)) newValue = [];
        if(!angular.isArray(oldValue)) oldValue = [];
        
        if(newValue.length == oldValue.length) return;
        
        var added = true;
        var diff = [];
        
        if(newValue.length < oldValue.length){
            added = false;
            var temp = oldValue;
            oldValue = newValue;
            newValue = temp;
        }
        
        for(var i = 0 ; i < newValue.length ; i++){
            if(oldValue.indexOf(newValue[i]) < 0) diff.push(newValue[i]);
        }
        
        for(var i = 0 ; i < $scope.tools.length ; i++){
            var tool = $scope.tools[i];
            if(angular.isArray(tool.permissions)){
                
                for(var j = 0 ; j < diff.length ; j++){
                    var perm = findFilter(tool.permissions, diff);
                    if(perm) setupRelatedPermissions(tool, perm, added);
                }
                
            }
        }
        
    });
    
    function setupRelatedPermissions(tool, perm, added){
        
        for(var i = 0 ; i < tool.permissions.length ; i++){
            
            var p = tool.permissions[i];
            
            if(
                added 
                && ((perm.type == 'delete' && (p.type == 'edit' || p.type == 'view')) || (perm.type == 'edit' && p.type == 'view')) 
                && $scope.permissions.indexOf(p.id) < 0
            ){
                $scope.permissions.push(p.id);
            }else if(
                !added 
                && ((perm.type == 'edit' && p.type == 'delete') || (perm.type == 'view' && (p.type == 'edit' || p.type == 'delete')))
                && $scope.permissions.indexOf(p.id) >= 0
            ){
                var index = $scope.permissions.indexOf(p.id);
                $scope.permissions.splice(index, 1);
            }
        }
        
    }
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        savePermits();
        
        roleService.saveRole($scope.role).then(function(role){
            if(angular.isObject(role)) angular.extend($scope.role, role);
            kkRoutesService.redirect('roles');
        });
        
    };
    
    $scope.promptDeleteRole = function(role){
        
        var modalInstance = $modal.open({
            templateUrl: 'access/delete-role-modal.html',
            controller: 'AccessDeleteRoleModalController',
            windowClass: 'tiny',
            resolve: {
                role: function(){
                    return role;
                }
            }
        });
        
        modalInstance.result.then(function(result){
            
            if(result.remove == true){
                
                roleService.getList().then(function(list){
                    
                    roles = list;
                    if(list.length > 1) promptReplaceDeletedRole(result.role);
                    else deleteRole(role.id);
                    
                });
                
            }
        });
        
    };
    
    function promptReplaceDeletedRole(role){
        
        var modalInstance = $modal.open({
            templateUrl: 'access/replace-role-modal.html',
            controller: 'AccessReplaceRoleModalController',
            windowClass: 'tiny',
            resolve: {
                role: function(){
                    return role;
                },
                roles: function(){
                    return roles;
                }
            }
        });
        
        modalInstance.result.then(function(result){
            deleteRole(result.role, result.replaceWith);
        });
        
    }
    
    function deleteRole(role, replaceWith){
        roleService.deleteRole(role, replaceWith).then(function(){
           kkRoutesService.redirect('roles');
        });
    }
    
    function savePermits(){
        
        $scope.role.tools = [];
        
        for(var i = 0 ; i < $scope.tools.length ; i++){
            
            var tool = $scope.tools[i];
            
            var toolData = {
                id: tool.id,
                permissions: []
            };
            
            if(angular.isArray(tool.permissions)){
                for(var j = 0 ; j < tool.permissions.length ; j++){
                    
                    var perm = tool.permissions[j];
                    
                    if($scope.permissions.indexOf(perm.id) >= 0){
                        toolData.permissions.push(perm.id);
                    }
                }
            }
            
            $scope.role.tools.push(toolData);
        }

    }
    
    function loadPermits(){
        
        var tools = $scope.role.tools;
        
        $scope.select('none');
        
        for(var i = 0 ; i < tools.length ; i++){
            if(angular.isArray(tools[i].permissions)){
                var perms = tools[i].permissions;
                for(var j = 0 ; j < perms.length ; j++){
                    $scope.permissions.push(perms[j]);
                }
            }
        }
        
    }
    
    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        
        roleService.getRole(id).then(function(role){
            $scope.role = angular.copy(role);
            loadPermits();
            $scope.isNew = false;
        });
        
    }else{
        $scope.role = {
            active: true
        };
    }
    
    toolService.getCollection().then(function(tools){
        $scope.tools = tools;
    });
    
}]);