'use strict';

angular.module('30k.ma.account')

.controller('AccountMainController', ['$scope', 'userService', 'kkRoutesService', function($scope, userService, kkRoutesService){
    
    kkRoutesService.preventReload($scope, 'AccountMainController');
    
    $scope.user = null;
    $scope.passwordInputType = 'password';
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        userService.saveUser($scope.user).then(function(user){
            if(angular.isObject(user)) angular.extend($scope.user, user);
        });
        
    };
    
    $scope.togglePasswordInputType = function(){
        
        if($scope.passwordInputType == 'password'){
            $scope.passwordInputType = 'text';
        }else if($scope.passwordInputType == 'text'){
            $scope.passwordInputType = 'password';
        }
    };
    
    userService.getUser().then(function(user){
        $scope.user = angular.copy(user);
    });
    
}]);