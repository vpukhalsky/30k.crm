'use strict';

angular.module('30k.ma.account', [
    
    'mm.foundation',
    
    '30k.ma',
    
    '30k.error',
    '30k.user.service',
    '30k.routes'
    
])


.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
    
    account: {
      path: '/account',
      controller: 'AccountMainController',
      templateUrl: 'account/main/main.html'
    },
    
    main: {
      path: '/account',
      controller: 'AccountMainController',
      templateUrl: 'account/main/main.html'
    }
    
  });
  
}]);
