'use strict';

angular.module('30k.ma.online-tool')

.controller('OnlineToolCollectionController', ['$scope', 'toolService', 'kkRoutesService', function($scope, toolService, kkRoutesService){
    
    kkRoutesService.preventReload($scope, 'OnlineToolCollectionController');
    
    $scope.tools = null;
    
    toolService.getOnlineCollection().then(function(tools){
        $scope.tools = tools;
    });
    
}]);