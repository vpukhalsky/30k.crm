'use strict';

angular.module('30k.ma.online-tool', [
    
    'mm.foundation',

    '30k.tool.service',
    '30k.routes',
    
    '30k.ma'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
      
    onlineTools: {
      path: '/online-tools',
      controller: 'OnlineToolCollectionController',
      templateUrl: 'online-tool/collection/collection.html'
    },
    
    main: {
      path: '/online-tools',
      controller: 'OnlineToolCollectionController',
      templateUrl: 'online-tool/collection/collection.html'
    }
    
  });
  
}]);