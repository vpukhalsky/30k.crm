'use strict';

angular.module('30k.ma.crm', [
    
    'mm.foundation',
    'googlechart',
    
    '30k.error',
    '30k.routes',
    '30k.service',
    '30k.period-picker',
    '30k.uploader',
    
    '30k.crm.customer-service',
    
    '30k.ma'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
    
    newCustomer: {
        path: '/customers/new',
        controller: 'CustomerEditController',
        templateUrl: 'crm/edit-customer/edit-customer.html'
    },
    
    editCustomer: {
        path: '/customers/edit/:id',
        controller: 'CustomerEditController',
        templateUrl: 'crm/edit-customer/edit-customer.html'
    },
    
    searchPagedCustomers: {
        path: '/customers/page/:page/filter/:filter',
        controller: 'CustomerCollectionController',
        templateUrl: 'crm/customers/customers.html'
    },
    
    searchCustomers: {
        path: '/customers/filter/:filter',
        controller: 'CustomerCollectionController',
        templateUrl: 'crm/customers/customers.html'
    },
    
    pagedCustomers: {
        path: '/customers/page/:page',
        controller: 'CustomerCollectionController',
        templateUrl: 'crm/customers/customers.html'
    },
    
    customers: {
        path: '/customers',
        controller: 'CustomerCollectionController',
        templateUrl: 'crm/customers/customers.html'
    },
    
    main: {
        path: '/customers',
        controller: 'CustomerCollectionController',
        templateUrl: 'crm/customers/customers.html'
    }
    
  });
  
}]);