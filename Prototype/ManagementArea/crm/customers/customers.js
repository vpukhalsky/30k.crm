'use strict';

angular.module('30k.ma.crm')

.controller('CustomerCollectionController', ['$scope', 'collectionService', 'kkRoutesService', function($scope, collectionService, kkRoutesService){
    
    $scope.protocol = 'http://';
    
    $scope.items = null;
    $scope.totalItems = 0;
    $scope.period = new Date();
    
    $scope.params = {
      itemsPerPage: 10,
      page: 1,
      filter: {}
    };
    
    var queryParams = {};
    
    $scope.deleteCustomer = function(item){
        
        collectionService.deleteItem('customer/delete-customer.json', 'customer', item);
        
    };
    
    $scope.areFiltersEmpty = function(){
      
      return !angular.isDefined(queryParams.filter.month) 
        && !angular.isDefined(queryParams.filter.year) && angular.isDefined(queryParams.filter.query);
      
    };
    
    $scope.onSelectPage = function(page){
        
        $scope.params.page = page;
        queryParams.page = $scope.params.page;
        
        reload();
        
    };
    
    $scope.onDatepickerInit = function(scope){
        
        scope.$on('setDate', function(e, date, view){
          
          var today = new Date(); 
          if(date.valueOf() > today.valueOf()) return;
          
          $scope.params.filter.month = date.getMonth() + 1;
          $scope.params.filter.year = date.getFullYear();
          
          queryParams.filter.month = $scope.params.filter.month;
          queryParams.filter.year = $scope.params.filter.year;
          queryParams.page = 1;
          
          reload(); 
          
        });
        
    };
    
    $scope.filter = function(form){
      
      if(!form.$valid) return;
      queryParams.filter.query = $scope.params.filter.query;
      queryParams.page = 1;
      reload();
      
    };
    
    $scope.getTotal = function(field){
        
        var total = 0;
        
        for(var i = 0 ; i < $scope.items.length ; i++){
          total += $scope.items[i][field];
        }
        
        return total;
        
    };
    
    function reload(){
      
      var path = 'customers';
      
      if(queryParams.page > 1){
        path = 'pagedCustomers';
        if(!$scope.areFiltersEmpty()) path = 'searchPagedCustomers';
      }else if(!$scope.areFiltersEmpty()){
        path = 'searchCustomers';
      }
      
      kkRoutesService.redirect(path, queryParams);
      
    }
    
    function load(){
    
      collectionService.getPagedCollection('customer/get-customers.json', 'customer', queryParams).then(function(response){
        
        $scope.items = response.items;
        $scope.totalItems = response.totalItems;
        
      });
    
    }
    
    function getDefaultParams(){
      
      var p = {
        page: 1
      };
      
      var today = new Date();
      p.month = today.getMonth() + 1;
      p.year = today.getFullYear();
      
      return p;
     
    }
    
    function readParams(){
      
      var params = kkRoutesService.getParams({
          page: 'int',
          month: 'int',
          year: 'int',
          query: 'string'
      });
      
      params = angular.extend(getDefaultParams(), params);
      if(angular.isDefined(params.filter)) delete params.filter;
      
      $scope.params.page = params.page;
      angular.extend($scope.params.filter, params);
      delete $scope.params.filter.page;
      
      queryParams = angular.copy($scope.params);
      $scope.params.filter.period = new Date(queryParams.filter.year, queryParams.filter.month, 0, 0, 0, 0, 0);
    
    }
    
    function init(){
      
      readParams();
      load();
      
    }
    
    init();
    
}]);