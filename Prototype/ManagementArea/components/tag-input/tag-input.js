'use strict';

/* Form inputs error messages module */

angular.module('30k.tag-input', [
    '30k.error',
    '30k.service',
    '30k.user.filters',
    '30k.alerts'
])

.directive('kkTagInput', ['$timeout', 'kkService', '$filter', 'alertService', function($timeout, kkService, $filter, alertService){
    
    return {
        
        scope: {
            maxlength: '@?',
            type: '@?',
            pattern: '=?',
            required: '@?',
            collection: '=',
            uppercase: '@?',
            onChange: '&?',
            wrap: '@?',
            validationUrl: '@?',
            typeaheadKey: '@?',
            typeaheadValue: '@?',
            typeaheadTag: '@?',
            typeaheadCallback: '=?',
            unique: '=?'
        },
        
        templateUrl: 'components/tag-input/tag-input.html?v=1',
        restrict: 'AE', 
        
        controller: function($scope, $element){
            
            var ENTER_KEY = 13;
            
            if(!angular.isDefined($scope.maxlength)) $scope.maxlength = -1;
            else $scope.maxlength = parseInt($scope.maxlength, 10);
            if(!angular.isDefined($scope.type)) $scope.type = 'text';
            if(!angular.isDefined($scope.pattern)) $scope.pattern = /^.+$/;
            if(!angular.isDefined($scope.required)) $scope.required = false;
            else $scope.required = 'true';
            if(!angular.isDefined($scope.uppercase)) $scope.uppercase = false;
            else $scope.uppercase = $scope.uppercase == 'true';
            if(!angular.isDefined($scope.wrap)) $scope.wrap = false;
            else $scope.wrap = $scope.wrap == 'true';
            if(!angular.isDefined($scope.unique)) $scope.unique = true;
            else $scope.unique = false;
            
            if(angular.isString($scope.typeaheadValue)){

                $scope.typeahead = { 
                    value: $scope.typeaheadValue,
                    key: (angular.isString($scope.typeaheadKey) ? $scope.typeaheadKey : 'id'),
                    tag: (angular.isString($scope.typeaheadTag) ? $scope.typeaheadTag : $scope.typeaheadValue),
                    config: 'taObj as taObj.' + $scope.typeaheadValue + ' for taObj in typeaheadCallback($viewValue) | filter: { ' + $scope.typeaheadValue + ': $viewValue }'
                };
                
            }else $scope.typeahead = null;
            
            $scope.inputType = $scope.type;
            $scope.placeholder = '';
            $scope.model = { value: '' };
            $scope.event = null;
            $scope.loading = false;
            
            if($scope.wrap){
                $element.addClass('wrap-tags');
            }
            
            $scope.$watchCollection('collection', function(newValue){
                
                if(angular.isArray(newValue) && $scope.tagInput.tag){
                    $scope.tagInput.tag.$setValidity(
                        'required', 
                        !$scope.required || newValue.length > 0
                    );
                }
                
            });
            
            if($scope.maxlength){
                for(var i = 0 ; i < $scope.maxlength ; i++) $scope.placeholder += 'X';
            }
            
            function onChange(){
                if(angular.isFunction($scope.onChange)) $scope.onChange();
            }
            
            function updateUnique(value){
                
                var unique = true;
                
                if($scope.unique){
                        
                    unique = false;
                    if($scope.collection.indexOf(value) < 0) unique = true;

                }
                
                $scope.tagInput.tag.$setValidity('unique', unique);
                
                return unique;
            }
            
            function addToCollection(value){
                
                $scope.collection.push(value);
                $scope.model.value = '';
                onChange();
                
            }
            
            $scope.formatTypeahead = function(){
                return '';
            };
            
            $scope.onTypeaheadSelect = function(obj){
                
                var indexOf = $filter('indexOf');
                
                if(indexOf($scope.collection, obj[$scope.typeahead.key], $scope.typeahead.key) < 0){
                    addToCollection(obj);
                }else{
                    alertService.popup('Error', 'This value already exists');
                }
                
            };
            
            $scope.onKeyPress = function($event){
                
                if($event.keyCode == ENTER_KEY) $event.preventDefault();
                
                $timeout(function(){
                
                    var value = $scope.model.value;
                    if(!value) value = '';
                    if($scope.uppercase) value = value.toUpperCase();
                    if($scope.tagInput.$error.server) $scope.tagInput.tag.$setValidity('server', true);
                
                    updateUnique(value);
                    
                    if(value != '' && $event.keyCode == ENTER_KEY && !$scope.tagInput.$error.unique && !$scope.tagInput.$error.pattern){
                    
                        if(!$scope.validationUrl) addToCollection(value);
                        else{
                            
                            $scope.loading = true;
                            
                            kkService.sendPost($scope.validationUrl, {
                                value: value
                            }).then(function(response){
                                
                                $scope.loading = false;
                                if(response.valid) addToCollection(response.value);
                                else showServerError(response.message);
                                
                            }, function(){
                                
                                $scope.loading = false;
                                
                            });
                        }
                    }
                });
            };
            
            $scope.isRequired = function(){
                return $scope.required && angular.isArray($scope.collection) && $scope.collection.length <= 0;
            };
            
            function showServerError(error){
                $scope.tagInput.tag.$setValidity('server', false);
                $scope.serverError = error;
            }
            
            $scope.remove = function(item){
                var index = $scope.collection.indexOf(item);
                $scope.collection.splice(index, 1);
                onChange();
            };
            
        }
    };
}]);