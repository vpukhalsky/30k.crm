'use strict';

/* Ajax loading overlay module */

angular.module('30k.loader', [
  'ngRoute'
])

.directive('kkLoader', ['$rootScope', function($rootScope){
    
    return{
        
        restrict: 'A',
        
        controller: function($scope, $element){
            
            $rootScope.loading = 0;
            
            $rootScope.$watch('loading', function(newValue, oldValue){
              resolve();
            });
            
            function resolve(){
              if($rootScope.loading > 0) $element.removeClass('ng-hide');
              else $element.addClass('ng-hide');
            }
            
            $rootScope.$on('$routeChangeStart', function(){
              
              $rootScope.loading++;
            });
            
            $rootScope.$on('$routeChangeSuccess', function(){
              
              $rootScope.loading--;
            });
            
            $rootScope.$on('$routeChangeError', function(){
              
              $rootScope.loading--;
            });
            
            resolve();
        }
    };
    
}]);