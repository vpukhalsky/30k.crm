'use strict';

/* Admin review module */

angular.module('30k.admin-review', [
    'mm.foundation',
    'perfect_scrollbar',
    '30k.tooltip',
    '30k.user.filters',
    '30k.error'
])

.directive('kkAdminReviewNotif', ['adminReviewService', '$filter', '$interval', function(adminReviewService, $filter, $interval){
    
    return {
        templateUrl: 'components/admin-review/notif.html',
        scope: {
            itemsPerPage: '=?',
            caption: '@'
        },
        replace: true,
        controller: function($scope, $element){
            
            $scope.config = null;
            $scope.opened = false;
            $scope.loading = false;
            $scope.items = null;
            
            var updateElapsedEverySec = 10;
            var page = 0;
            
            if(!angular.isNumber($scope.itemsPerPage)) $scope.itemsPerPage = 10;
            
            function updateElapsed(){
                
                $scope.items.forEach(function(i){
                    i.elapsed = $filter('timeForHumans')(i.timestamp);
                });
                
            }
            
            $scope.onScroll = function(current, max){
                
                if(current >= max && $scope.config.allNotifs > $scope.items.length){
                    load(page + 1);
                }
                
            };
            
            $scope.toggle = function(){
                
                $scope.opened = !$scope.opened;
                
                if($scope.opened == true && $scope.items === null && page == 0) load(1);
                
            };
            
            $scope.read = function(item){
                
                if(!item.read){
                    adminReviewService.setNotificationRead(item).then(function(){
                        $scope.opened = false;
                        if($scope.config.unreadNotifs > 0) $scope.config.unreadNotifs--;
                        window.location = item.url;
                    });
                }else{
                    $scope.opened = false;
                    window.location = item.url;
                }
                
            };
            
            function load(p){
                
                if(!$scope.loading){
                    
                    $scope.loading = true;
                    
                    adminReviewService.getNotifications(p, $scope.itemsPerPage).then(function(items){
                        
                        if(!angular.isArray($scope.items)) $scope.items = [];
                        $scope.items.splice(0, $scope.items.length);
                        items.forEach(function(i){ $scope.items.push(i); });
                        
                        updateElapsed();
                        page = p;
                        
                        $scope.loading = false;
                        
                    }, function(){
                        
                        $scope.loading = false;
                        
                    });
                }
                
            }
            
            adminReviewService.getConfig().then(function(config){
                $scope.config = config;
            });
            
            $interval(function(){
                if(angular.isArray($scope.items)) updateElapsed();
            }, updateElapsedEverySec * 1000);
            
        }
    };
    
}])

.constant('adminReviewConfig', {
    
    enabled: null,
    
    setIsEnabled: function(en){
        this.enabled = en;
        return this.enabled;
    },
    
    isEnabled: function(){
        
        if(this.enabled === null){
        
            if(location.search.length < 2 || location.search.charAt(0) != '?') return this.setIsEnabled(false);
            
            var search = location.search.substring(1);
            var pairs = search.split('&');
            
            if(pairs.length < 1) return this.setIsEnabled(false);
            
            pairs.forEach(function(pair){
                
                var keyValue = pair.split('=');
                
                if(keyValue[0] == 'role' && keyValue[1] == 'admin'){
                    this.setIsEnabled(true);
                    return false;
                }
                
            }, this);
            
            return this.setIsEnabled(this.enabled === true);
            
        }
        
        return this.enabled;
        
    }
    
})

.service('adminReviewService', ['collectionService', '$filter', function(collectionService, $filter){
    
    var configPromise = null;
    var notifs = [];
    
    function getConfig(){
        if(!configPromise) configPromise = collectionService.getConfig('review/config.json?v=1', 'review');
        return configPromise;
    }
    
    function getNotifications(page, itemsPerPage){
        
        return collectionService.getPagedCollection('review/get-notifications-' + page + '.json', 'notif', {
            page: page,
            itemsPerPage: itemsPerPage
        }, false).then(function(response){
            
            var indexOf = $filter('indexOf');
            
            response.items.forEach(function(i){
                if(indexOf(notifs, i.id) < 0){
                    notifs.push(i);
                }
            });
            
            return notifs;
            
        });
        
    }
    
    function setNotificationRead(notif){
        
        return collectionService.saveItem('review/set-read-notification.json', 'notif', notif);
        
    }
    
    return {
        getConfig: getConfig,
        getNotifications: getNotifications,
        setNotificationRead: setNotificationRead
    };
    
}])

.directive('kkAdminReviewHeader', ['adminReviewService', function(adminReviewService){
    
    return {
        template: '<th><span ng-transclude></span></th>',
        replace: true,
        transclude: true
    };
    
}])

.directive('kkAdminReviewColumn', ['adminReviewService', function(adminReviewService){
    
    return {
        templateUrl: 'components/admin-review/column.html',
        replace: true,
        scope: {
            kkAdminReviewColumn: '='
        },
        controller: function($scope, $element){
            
            $scope.item = $scope.kkAdminReviewColumn;
            $scope.review = $scope.kkAdminReviewColumn.review;
            
            adminReviewService.getConfig().then(function(config){
                $scope.config = config;
            });
            
        }
    };
    
}])

.directive('kkAdminReviewBox', ['adminReviewService', '$modal', function(adminReviewService, $modal){
    
    return {
        templateUrl: 'components/admin-review/box.html',
        scope: {
            item: '=kkAdminReviewBox',
            save: '&'
        },
        controller: function($scope, $element){
            
            $scope.config = null;
            
            $scope.approve = function(){
                
                $scope.item.status = 'approved';
                $scope.save();
                
            };
            
            function editDescription(review, isNew){
                
                return $modal.open({
                    windowClass: 'small',
                    templateUrl: 'components/admin-review/box-modal.html',
                    resolve: {
                        review: function(){ return review },
                        isNew: function(){ return isNew }
                    },
                    controller: function($scope, $modalInstance, review, isNew){
                        
                        $scope.review = review;
                        $scope.isNew = isNew;
                        
                        $scope.submit = function(form){
                            
                            if(form.$valid) $modalInstance.close($scope.review);
                            
                        };
                        
                        $scope.cancel = function(){
                            
                            $modalInstance.dismiss('cancel');
                            
                        };
                        
                    }
                    
                }).result.then(function(review){
                    
                    $scope.item.review = review;
                    
                });
                
            }
            
            $scope.editDescription = function(){
                
                editDescription(angular.copy($scope.item.review), false);
                
            };
            
            $scope.decline = function(){
                
                var review = angular.copy($scope.item.review);
                review.status = 'declined';
                
                editDescription(review, true).then(function(){
                    $scope.save();
                });
                
            };
            
            adminReviewService.getConfig().then(function(config){
                $scope.config = config;
            });
            
        }
    };
    
}])

.directive('kkAdminReviewHint', ['adminReviewService', '$modal', '$filter', function(adminReviewService, $modal, $filter){
    
    return {
        templateUrl: 'components/admin-review/hint.html?v',
        transclude: true,
        scope: {
            item: '=kkAdminReviewHint',
            property: '@',
            map: '=?',
            mapKey: '@?',
            mapValue: '@?',
            hPos: '@?',
            vPos: '@?',
            inline: '@?',
            filter: '=?',
            asNew: '=?',
            asRemoved: '=?',
            removedTemplateUrl: '@?',
            removedKey: '@?',
            removedTemplateData: '=?'
        },
        controller: function($scope, $element){
            
            $scope.prev = undefined;
            $scope.cur = undefined;
            $scope.type = null;
            $scope.stl = {};
            $scope.cls = [];
            $scope.removed = [];
            
            var mapKey = angular.isDefined($scope.mapKey) ? $scope.mapKey : 'id';
            var mapValue = angular.isDefined($scope.mapValue) ? $scope.mapValue : 'name';
            
            if(angular.isDefined($scope.hPos)){
                if($scope.hPos == 'left') $scope.cls.push('hleft');
                else if($scope.hPos == 'right') $scope.cls.push('hright');
                else if($scope.hPos != 'center') $scope.stl.left = $scope.hPos;
            }
            
            if(angular.isDefined($scope.vPos)){
                if($scope.vPos == 'top') $scope.cls.push('vtop');
                else if($scope.vPos == 'bottom') $scope.cls.push('vbottom');
                else if($scope.vPos == 'center') $scope.cls.push('vcenter');
                else $scope.stl.top = $scope.vPos;
            }
            
            $scope.inline = $scope.inline == 'true';
            
            function getType(val){
                
                var type = undefined;
                
                if(angular.isArray(val)) type = 'array';
                else if(angular.isDate(val)) type = 'date';
                else if(angular.isNumber(val)) type = 'number';
                else if(angular.isObject(val)) type = 'object';
                else if(angular.isString(val)) type = 'string';
                else if(angular.isDefined(val) && (val === false || val === true)) type = 'boolean';
                else type = null;
                
                return type;
            }
            
            function getMapped(map, key, keyName, valueName){
                
                var mapped = $filter('find')(map, key, keyName);
                    
                if(angular.isObject(mapped) && angular.isDefined(mapped[valueName])){
                    return mapped[valueName];
                }
                
                return null;
                
            }
            
            function map(val){
                
                var type = getType(val);
                
                if((type == 'number' || type == 'string' || type == 'array') && angular.isArray($scope.map)){
                    
                    if(type == 'number' || type == 'string'){
                        val = getMapped($scope.map, val, mapKey, mapValue);
                    }else{
                        
                        var mapped = [];
                        
                        val.forEach(function(i){
                            mapped.push(getMapped($scope.map, i, mapKey, mapValue));
                        });
                        
                        val = mapped;
                        
                    }
                    
                }
                
                return val;
                
            }
            
            function filter(val){
                
                var filters = [];
                var f = $filter('filter');
                var p = $filter('prop');
                
                if(!angular.isDefined($scope.filter)) return val;
                if(!angular.isArray($scope.filter) && (angular.isObject($scope.filter) || angular.isString($scope.filter))){
                    filters.push(angular.copy($scope.filter));
                }else if(angular.isArray($scope.filter)) filters = angular.copy($scope.filter);
                
                var result = angular.copy(val);
                
                filters.forEach(function(filter){
                    
                    if(result === undefined || result === null) return false;
                    if(angular.isString(filter)) result = p(result, filter);
                    else if(angular.isObject(filter)){
                        var r = f(result, filter, true);
                        if(angular.isArray(r) && r.length > 0) result = r[0];
                        else result  = null;
                    }
                    
                });
                
                return result;
                
            }
            
            function updatePreviewClass(){
                
                if($scope.showAsRemoved() && angular.isString($scope.removedTemplateUrl)){
                    if($scope.cls.indexOf('preview') < 0) $scope.cls.push('preview');
                }else{
                    var idx = $scope.cls.indexOf('preview');
                    if(idx >= 0) $scope.cls.splice(idx);
                }
                
            }
            
            function getRemoved(){
                
                var key = 'id';
                
                if(angular.isString($scope.removedKey)) key = $scope.removedKey;
                
                var result = [];
                var indexOf = $filter('indexOf');
                
                if(!angular.isArray($scope.prev)) return result;
                
                $scope.prev.forEach(function(i){
                    if(!angular.isArray($scope.cur) || indexOf($scope.cur, i[key], key) < 0) result.push(i);
                });
                
                return result;
                
            }
            
            if(angular.isDefined($scope.property)){
                
                $scope.$watch('item.__prev.' + $scope.property, function(newValue){
                
                    $scope.prev = angular.copy(newValue);
                    $scope.prev = filter($scope.prev);
                    $scope.prev = map($scope.prev);
                    $scope.type = getType($scope.prev);
                    
                });
                
                if($scope.asRemoved === true){
                    
                    $scope.$watch('item.' + $scope.property, function(newValue){
                        
                        $scope.cur = angular.copy(newValue);
                        $scope.cur = filter($scope.cur);
                        $scope.cur = map($scope.cur);
                        
                        $scope.removed = getRemoved();
                        
                        updatePreviewClass();
                        
                    });
                    
                }
                
            }
            
            $scope.show = function(){
                return $scope.prev !== undefined && angular.isString($scope.type) && $scope.type != 'object' && !$scope.asRemoved;
            };
            
            $scope.showAsNew = function(){
                return $scope.prev !== undefined && !angular.isString($scope.type) && angular.isDefined($scope.filter) && $scope.asNew === true && !$scope.asRemoved;
            };
            
            $scope.showAsRemoved = function(){
                return $scope.type == 'array' 
                    && $scope.asRemoved === true 
                    && !$scope.asNew
                    && $scope.removed.length > 0;
            };
            
            $scope.showRemovedPreview = function(){
                
                if(!angular.isString($scope.removedTemplateUrl)) return;
                
                $modal.open({
                    windowClass: 'large',
                    controller: 'AdminReviewRemovedModalController',
                    templateUrl: 'components/admin-review/removed-preview.html',
                    resolve: {
                        config: function(){ return $scope.removedTemplateData; },
                        removed: function(){ return $scope.removed; },
                        template: function(){ return $scope.removedTemplateUrl;}
                    }
                });
                
            };
            
        }
    };
        
}])

.controller('AdminReviewRemovedModalController', ['$scope', '$modalInstance', 'config', 'removed', 'template', function($scope, $modalInstance, config, removed, template){
    
    $scope.data = config;
    $scope.items = removed;
    $scope.template = template;
    $scope.loading = true;
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.onLoad = function(){
        $scope.loading = false;
    };
    
}]);