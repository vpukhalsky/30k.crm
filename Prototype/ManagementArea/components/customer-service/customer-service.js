'use strict';

/* Services */

angular.module('30k.crm.customer-service', [
    '30k.service'
])

.service('customerService', ['kkService', '$q', '$filter', function(kkService, $q, $filter){
    
    var collection = {};
    var customers = {};
    var stats = {};
    
    var config = null;
    
    function getConfig(){
        
        if(angular.isObject(config)){
            var deferred = $q.defer();
            deferred.resolve(config);
            return deferred.promise;
        }else{
            return kkService.sendGet('customer/config.json').then(function(response){
                config = response;
                return config;
            });
        }
        
    }
    
    function _generateKey(month, year){
        if(month === undefined || year === undefined){
            var today = new Date();
            month = today.getMonth();
            year = today.getFullYear();
        }
        
        return month + '_' + year;
    }
    
    function getCollection(month, year){
        
        var key = _generateKey(month, year);
        
        if(!angular.isObject(collection[key])){
            
            return kkService.sendPost('customer/get-customers.json', {
                month: month,
                year: year
            }).then(function(response){
                
              var key = _generateKey(response.month, response.year);
              collection[key] = response.customers;
              return response.customers;
              
            }, function(response){
              //TODO
            });   
            
        }else{
            
            var deferred = $q.defer();
            deferred.resolve(collection[key]);
            return deferred.promise;
            
        }
    }
    
    function _generateCustomerKey(id){
        return 'cust_' + id;
    }
    
    function getOne(id){
        var key = _generateCustomerKey(id);
        
        if(angular.isObject(customers[key])){
            
            var deferred = $q.defer();
            deferred.resolve(customers[key]);
            return deferred.promise;
            
        }else{
            
            return kkService.sendPost('customer/get-customer.json', {
                id: id
            }).then(function(response){
                
              var key = _generateCustomerKey(response.customer.id);
              customers[key] = response.customer;
              return response.customer;
              
            }, function(response){
              //TODO
            }); 
            
        }
    }
    
    var toSave = null;
    
    function saveCallback(response){ 
        var key = _generateCustomerKey(response.customer.id);
        if(customers[key] === undefined){
            //new customer - invalidate cache
            collection = {};
        }
        customers[key] = angular.extend(toSave, response.customer);
        return customers[key];
    }
    
    function save(customer){
        toSave = customer;
        return kkService.sendPost('customer/save-customer.json', customer).then(saveCallback, saveCallback);
    }
    
    function generateApiPass(customer){
        return kkService.sendPost('customer/get-api-pass.json', {
            id: customer.id
        }).then(function(response){
            if(customer.id == response.customer.id) customer = angular.extend(customer, response.customer);
            return customer;
        }, function(response){
            if(customer.id == response.customer.id) customer = angular.extend(customer, response.customer);
            return customer;
        });
    }
    
    function _generateStatsKey(customer, period){
        return customer.id + '_' 
            + period.start.getFullYear() + '_' 
            + period.start.getMonth() + '_' 
            + period.start.getDate() + '_' 
            + period.end.getFullYear() + '_' 
            + period.end.getMonth() + '_' 
            + period.end.getDate();
    }
    
    function getStats(customer, period){
        
        var key = _generateStatsKey(customer, period);
        
        if(angular.isObject(stats[key])){
            
            var deferred = $q.defer();
            deferred.resolve(stats[key]);
            return deferred.promise;
            
        }else{
            
            return kkService.sendPost('customer/get-stats.json',{
                id: customer.id,
                start: period.start,
                end: period.end
            }).then(function(response){
                
                return _addStats(response);
                
            }, function(){
                //TODO
            }); 
            
        }
    }
    
    function _prepareGraphsData(data){
        
        for(var i = 0 ; i < data.length ; i++){
            var date = new Date(data[i].c[0].v);
            var parts = date.toDateString().split(' ');
            data[i].c[0].v = parts[1] + ' ' + parts[3];
        }
        return data;
        
    }
    
    function _addStats(response){
        
        var period = {
            start: new Date(response.start),
            end: new Date(response.end)
        };
                
        var key = _generateStatsKey(response.customer, period);
        
        for(var i in response.data.graphs){
            response.data.graphs[i] = _prepareGraphsData(response.data.graphs[i]);
        }
        
        var total = 0;
        var items = response.data.billings.items;
        
        for(var i = 0 ; i < items.length ; i++){
            total += items[i].subtotal;
        }
        
        response.data.billings.total = total;
        
        stats[key] = response.data;
        
        return stats[key];
    }
    
    function deleteInvoice(customer, invoice){
        
        return kkService.sendPost('customer/delete-invoice.json', {
            customerId: customer.id,
            invoice: invoice.id
        }).then(function(response){
            
            var indexOf = $filter('indexOf');
            var idx = indexOf(customer.invoices, response.invoice.id);
            customer.invoices.splice(idx, 1);
            
        });
        
    }
    
    return {
        getCollection: getCollection,
        getOne: getOne,
        save: save,
        generateApiPass: generateApiPass,
        getStats: getStats,
        getConfig: getConfig,
        deleteInvoice: deleteInvoice
    };
}]);