'use strict';

/* Button-based period picker */

angular.module('30k.period-picker', [
    'mm.foundation',
    'datePicker'
])

.value('kkPeriodPickerCounter', 0)

.filter('localDate', function(){
    
    return function(date){
        
        if(angular.isString(date)){
            date = new Date(date);
        }
        
        if(angular.isObject(date)){
            
            var d = new Date();
            d.setUTCFullYear(date.getFullYear());
            d.setUTCMonth(date.getMonth());
            d.setUTCDate(date.getDate());
            d.setUTCHours(0);
            d.setUTCMinutes(0);
            d.setUTCSeconds(0);
            d.setUTCMilliseconds(0);
            return d;
            
        }else{
            
            return null;
            
        }
    };
    
})

.directive('kkPeriodPicker', ['$timeout', '$compile', 'kkPeriodPickerCounter', '$window', function($timeout, $compile, counter, $window){

    return {
        restrict: 'E',
        scope: {
            from: '=',
            until: '=',
            minView: '@?',
            fromText: '@?',
            untilText: '@?'
        },
        templateUrl: 'components/period-picker/button.html',
        controller: function($scope, $element, $attrs){

            var dropdownTemplate = '<div ng-init="init(this)" ng-attr-id="period-picker-{{counter}}" class="f-dropdown period-picker" ng-click="preventDefault($event)"><div date-range start="start" end="end" ng-attr-min-view="{{mv}}"></div></div>';
            
            counter++;
            
            $scope.counter = counter;
            
            $scope.start = $scope.from ? new Date($scope.from) : new Date();
            $scope.end = $scope.until ? new Date($scope.until) : new Date();
            
            if(!$scope.minView) $scope.minView = 'date';
            $scope.mv = $scope.minView;
            
            if(!$scope.fromText) $scope.fText = 'From';
            else $scope.fText = $scope.fromText;
            if(!$scope.untilText) $scope.uText = 'Until';
            else $scope.uText = $scope.untilText;
            
            $scope.clearFrom = function(){
                $scope.from = null;
            };
            
            $scope.clearUntil = function(){
                $scope.until = null;
            };
            
            function onDateSet(event, date, view){
                
                var start = dropdown.find('[date-picker="start"]');
                var end = dropdown.find('[date-picker="end"]');
                var startScope = start.isolateScope();
                var endScope = end.isolateScope();
                
                var finalView = false;
                if(view == $scope.mv) finalView = true;
                
                var d = new Date(date);
                
                if(event.targetScope === startScope){
                    
                    if(angular.isObject(d) && finalView) $scope.from = d;
                    
                }else if(event.targetScope === endScope){
                    
                    if(angular.isObject(d) && finalView){
                        $scope.until = d;
                        var range = d.valueOf() - $scope.from.valueOf();
                        if(range > 0) closePicker();
                    }
                    
                }
                
            }
            
            $scope.init = function(scope){
                scope.$on('setDate', onDateSet);
            };
    
            $scope.preventDefault = function($event){
                $event.preventDefault(); 
                $event.stopPropagation();
            };
            
            $scope.onClick = function($event){
    
                $timeout(function(){
                    
                    var picker = document.getElementById('period-picker-' + $scope.counter);
                    var matchLeft = picker.style.left.match(/\d+\.?\d*/);
                    if(!angular.isArray(matchLeft) || matchLeft.length < 1) return;
                    var left = parseFloat(matchLeft[0]);
                    var width = picker.offsetWidth;
                    var maxWidth = $window.innerWidth;
                    var offset = 30;
                    var diff = left + width - maxWidth + offset;
                    
                    if(diff > 0){
                        left -= diff;
                        angular.element(picker).css('left', left + 'px');
                    }
                
                }, 200);
    
            };
    
            function closePicker(){
            
                $timeout(function(){
                    if($('#period-picker-' + $scope.counter + ':visible').size() > 0) $('body').click();
                });
                
            }
            
            var dropdown = angular.element(dropdownTemplate);
            dropdown = $compile(dropdown)($scope, function(el, scope){
                angular.element('body').append(el);
            });
            
            $scope.$on('$destroy', function(){
                dropdown.detach();
            });
            
        }
    };
    
}]);
