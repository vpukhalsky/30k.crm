'use strict';

/* Form inputs error messages module */

angular.module('30k.error', [])

.directive('kkError', [function(){
    
    return {
        
        scope: {
            field: '=',
            type: '@?'
        },
        
        template: '<small class="error kk-error" ng-show="show()" ng-transclude></small>',
        replace: true,
        restrict: 'AE',
        transclude: true,
        
        controller: function($scope, $element){
            
            var submited = false;
            
            $scope.show = function(){
                
                if(!$scope.field) return false;
                
                if($scope.field.$invalid && ($scope.field.$dirty || submited)){
                    
                    if(angular.isString($scope.type) && !$scope.field.$error[$scope.type]){
                        return false;
                    }
                    
                    return true;
                }
                
                return false;
            };
            
            function onFormSubmit(){
                submited = true;
            }
            
            var form = $element.closest('form');
            form.on('submit', onFormSubmit);
            
            $scope.$on('$destroy', function(){
                form.off('submit', onFormSubmit);
            });
            
            $scope.$on('kk-error', function(event){
                onFormSubmit();
            });
        }
    };
}]);