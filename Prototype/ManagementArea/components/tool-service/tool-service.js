'use strict';

angular.module('30k.tool.service', [
    
    '30k.service'
    
])

.service('toolService', ['kkService', '$filter', function(kkService, $filter){
    
    var tools = null;
    var online = null;
    var config = null;
    
    function deleteTool(tool){
        
        return kkService.sendPost('tool/delete-tool.json', { id: tool.id }).then(function(response){
            
            var index = $filter('indexOf')(tools, response.tool.id);
            if(index >= 0) tools.splice(index, 1);

            return response;
        });
        
    }
    
    function getCollection(){
        
        if(angular.isArray(tools)) return kkService.promise(tools);
        else return kkService.sendGet('tool/get-tools.json?v1').then(function(response){
            
            tools = response.tools;
            return tools;
            
        });
        
    }
    
    function getTool(id){
        
        if(angular.isArray(tools)){
            var tool = $filter('find')(tools, id);
            if(tool) return kkService.promise(tool);
        }
        
        return kkService.sendPost('tool/get-tool.json', { id: id }).then(function(response){
            return response.tool;
        });
    }
    
    function _updateTool(tool){
        
        var r = $filter('find')(tools, tool.id);
        
        if(r) angular.extend(r, tool);
        else{
            r = tool; 
            if(angular.isArray(tools)) tools.push(r);
        }
        
        return r;
    }
    
    function saveTool(tool){
        
        return kkService.sendPost('tool/save-tool.json', tool).then(function(response){
            
            var tool = _updateTool(response.tool);
            return tool;
            
        });
        
    }
    
    function getOnlineCollection(){
        
        if(angular.isArray(online)){
            
            return kkService.promise(online);
            
        }else if(angular.isArray(tools)){
            
            online = [];
            
            for(var i = 0 ; i < tools.length ; i++){
                if(angular.isString(tools[i].externalUrl)) online.push(tools[i]);
            }
            
            return kkService.promise(online);
        }
        else return kkService.sendGet('online-tool/get-tools.json').then(function(response){
            
            online = response.tools;
            return online;
            
        });
        
    }
    
    function getConfig(){
        
        if(angular.isObject(config)) return kkService.promise(config);
        else return kkService.sendGet('tool/get-config.json').then(function(response){
            config = response;
            return config;
        });
        
    }
    
    return {
        getCollection: getCollection,
        deleteTool: deleteTool,
        getTool: getTool,
        saveTool: saveTool,
        getOnlineCollection: getOnlineCollection,
        getConfig: getConfig
    };
    
}]);