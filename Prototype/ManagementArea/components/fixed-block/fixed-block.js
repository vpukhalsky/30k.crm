'use strict';

/* Fixes block on top of screen */

angular.module('30k.fixed-block', [
  
])

.directive('kkFixedBlock', ['$http', '$templateCache', '$compile', '$timeout', function($http, $templateCache, $compile, $timeout){
    
    return{
        
        scope: {
            attachTo: '@?'
        },
        transclude: true,
        template: '<div ng-transclude></div>',
        
        controller: function($scope, $element){
            
            var height = 0;
            var width = 0;
            
            function init(){
                
                angular.element(window).on('scroll', onScroll);
                angular.element(window).on('resize', onResize);
                
                height = $element.children().eq(0).height();
                width = $element.width();
                
            }
            
            function onScroll(e){
                update();
            }
            
            function onResize(e){
                
                height = $element.children().eq(0).height();
                width = $element.width();
                
                update();
            }
            
            function update(){
                
                var bottom = Number.MAX_VALUE;
                var scrollTop = angular.element(window).scrollTop();
                
                if($scope.attachTo){
                    var wrapper = angular.element($scope.attachTo);
                    bottom = wrapper.offset().top + wrapper.height();
                }
                
                var top = $element.offset().top;
                
                $element.children().eq(0).children().eq(0).width(width);
                
                if(scrollTop > top && scrollTop < bottom - height){
                    fix();
                }else{
                    unfix();
                }
                
            }
            
            function fix(){
                if(!$element.hasClass('fixed-block')){
                    $element.addClass('fixed-block');
                    height = $element.children().eq(0).height();
                    $element.height(height);
                }
            }
            
            function unfix(){
                $element.removeClass('fixed-block');
            }
            
            $scope.$on('$destroy', function(){
                angular.element(window).off('scroll', onScroll);
                angular.element(window).on('resize', onResize);
            });
            
            init();
            
        }
    };
    
}]);