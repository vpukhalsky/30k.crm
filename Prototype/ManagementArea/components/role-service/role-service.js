'use strict';

angular.module('30k.role.service', [
    
    '30k.service'
    
])

.service('roleService', ['kkService', '$filter', function(kkService, $filter){
    
    var list = null;
    var roles = null;
    var config = null;
    
    function getList(){
        
        if(list) return kkService.promise(list);
        else return kkService.sendGet('user/get-roles.json').then(function(response){
             list = response.roles;
             return list;
        });
        
    }
    
    function getDefaultRoleId(){
        
        return _getConfig().then(function(response){
           return config.defaultRoleId; 
        });
        
    }
    
    function _getConfig(){
        
        if(!angular.isObject(config)) return kkService.sendGet('access/get-config.json').then(function(response){
            config = response;
            return config;
        });
        else return kkService.promise(config);
        
    }
    
    function deleteRole(role, replaceWith){
        
        var params = { id: role };
        if(angular.isNumber(replaceWith)) params.replaceWithId = replaceWith;
        
        return kkService.sendPost('access/delete-role.json', params).then(function(response){
            
            var index = $filter('indexOf')(roles, response.role.id);
            if(index >= 0) roles.splice(index, 1);
            
            index = $filter('indexOf')(list, response.role.id);
            if(index >= 0) list.splice(index, 1);

            return response;
        });
        
    }
    
    function getCollection(){
        
        if(angular.isArray(roles)) return kkService.promise(roles);
        else return kkService.sendGet('access/get-roles.json').then(function(response){
            
            roles = response.roles;
            if(!list) list = roles;
            
            return roles;
            
        });
        
    }
    
    function getRole(id){
        
        if(angular.isArray(roles)){
            var role = $filter('find')(roles, id);
            if(role) return kkService.promise(role);
        }
        
        return kkService.sendPost('access/get-role.json', { id: id }).then(function(response){
            return response.role;
        });
    }
    
    function _updateRole(role){
        
        var r = $filter('find')(roles, role.id);
        
        if(r) angular.extend(r, role);
        else{
            r = role; 
            if(angular.isArray(roles)) roles.push(r);
        }
        
        return r;
    }
    
    function saveRole(role){
        
        return kkService.sendPost('access/save-role.json', role).then(function(response){
            
            var role = _updateRole(response.role);
            return role;
            
        });
        
    }
    
    return {
        getList: getList,
        getDefaultRoleId: getDefaultRoleId,
        getCollection: getCollection,
        deleteRole: deleteRole,
        getRole: getRole,
        saveRole: saveRole
    };
    
}]);