'use strict';

/* Routing module */

angular.module('30k.routes', [
    'ngRoute'
])

.constant('kkRoutes', {})

.service('kkRoutesService', ['$route', '$rootScope', '$location', 'kkRoutes', '$routeParams', function($route, $rootScope, $location, kkRoutes, $routeParams){
  
  var lastRoute = $route.current;
  var arraySeparator = ';';
  
  //prevents route reload on location hash change (eg. #result)
  function preventReload($scope, controller){
    
    $scope.$on('$locationChangeSuccess', function(event) {
        
        if($route.current.$$route.controller === controller){ 

            $route.current = lastRoute;
            if(angular.isDefined($rootScope.loading)) $rootScope.loading--;
            
        }
        
    });
    
  }
  
  function redirect(pathKey, params){
    
    var path = kkRoutes[pathKey].path;
    
    if(angular.isObject(params)){
      for(var i in params){
        if(params.hasOwnProperty(i)){
          
          var param = params[i];
          var re = new RegExp('\\:' + i + '\\*?');
          
          if(angular.isObject(param)){
            path = path.replace(re, getSearchParam(i, param));
          }else{
            path = path.replace(re, encodeParam(param));
          }
          
        }
      }
    }
    
    $location.url(path);
  }
  
  function encodeParam(param){
    
    var p = angular.copy(param);
    if(angular.isArray(param)) p = param.join(arraySeparator);
    if(param instanceof Date) p = param.toJSON();
    return encodeURIComponent(p);
    
  }
  
  function getSearchParam(name, params){
    
    var result = encodeURIComponent(name) + '?';
    var j = 0;
    
    for(var i in params){
        if(params.hasOwnProperty(i)){
          
          var val = encodeParam(params[i]);
          if(val.length > 0){
            if(j > 0) result += '&';
            result += encodeURIComponent(i) + '=' + val;
            j++;
          } 
          
        }
    }
    
    return result;
  }
  
  function getParamType(config, param){
    
    if(angular.isObject(config) && config[param]) return config[param];
    else return 'string';
    
  }
  
  function getParams(config){
    
    var result = {};
    
    for(var i in $routeParams){
      
      if(!$routeParams.hasOwnProperty(i)) continue;
      var type = getParamType(config, i);
      var val = $routeParams[i];  
       
      switch(type){
        case 'string':
          result[i] = decodeURIComponent(val);
          break;
        case 'bool':
          result[i] = decodeURIComponent(val) == 'true';
          break;
        case 'int': 
          result[i] = parseInt(decodeURIComponent(val), 10);
          break;
        case 'float': 
          result[i] = parseFloat(decodeURIComponent(val), 10);
          break;
        case 'array': 
          result[i] = decodeURIComponent(val).split(arraySeparator);
          break;
        case 'date':
          var v = decodeURIComponent(val);
          if(v && v != 'null') result[i] = new Date(decodeURIComponent(val));
          break;
      }
      
    }
    
    return result;
    
  }
  
  return {
    preventReload: preventReload,
    redirect: redirect,
    getParams: getParams
  };
  
}])

.provider('kkRoutesConfig', ['kkRoutes', '$routeProvider', function kkRoutesConfigProvider (kkRoutes, $routeProvider){
    
    function _resetRoutes(){
        while(kkRoutes.length > 0) kkRoutes.splice(0, 1);
    }
    
    function _addRoutes(routes){
        angular.extend(kkRoutes, routes);
    }
    
    this.init = function(routes){
        
        _resetRoutes();
        _addRoutes(routes);
        
        for(var r in kkRoutes){
            $routeProvider.when(kkRoutes[r].path, { templateUrl: kkRoutes[r].templateUrl, controller: kkRoutes[r].controller });
        }
        
        $routeProvider.otherwise({redirectTo: kkRoutes.main.path});
        
    };
    
    this.$get = [function(){
      return kkRoutes;
    }];
    
}])

.directive('kkNav', ['kkRoutes', '$location', '$rootScope', function(kkRoutes, $location, $rootScope) {
  
  return {
    
    restrict: 'A',
    
    link: function($scope, $element, config){
      
      var link = $element;
      var path = '';
      var enabled = true;
      
      var route = config.route || config.kkNav;
      if(!route) route = $scope.route;
      if(!route) return; 
      
      if(config.navDisabled){
        $scope.$watch(config.navDisabled, function(newValue){
          enabled = !newValue;
          if(enabled) $element.removeClass('disabled');
          else $element.addClass('disabled');
        });
      }
      
      var tagName = link[0].tagName.toLowerCase();
      
      if(tagName != 'a' && tagName != 'button'){
        var el = link.find('a');
        if(el.length <= 0) el = link.find('button');
        link = el.eq(0);
      }
      
      link.on('click', onLinkClick);
      
      function onLinkClick(event){
        
        event.preventDefault();
        event.stopPropagation();
        
        if(enabled){
          var p = getPath();
          
          $scope.$apply(function(){
            $location.path(p);  
          });
        }
        
      }
      
      function getPath(){
        
        path = kkRoutes[route].path;
        var params = path.match(/\/\:([^\/]+)/gi);
          
        if(params){
            
          for(var i = 0 ; i < params.length; i++){
              
            var paramName = params[i].substring(2);
            var paramValue = $element.attr('param-' + paramName);
              
            if(paramValue){
              path = path.replace(params[i], '/' + paramValue);
            }else{
              path = path.replace(params[i], '');
            }
          }
            
        }
        
        return path;
      }
      
      function updateIsActive(){
        var current = $location.path();
        if(current.indexOf(getPath()) === 0) $element.addClass('active');
        else $element.removeClass('active');
      }
      
      $rootScope.$on('$routeChangeSuccess', function(){
        updateIsActive();
      });
            
      $rootScope.$on('$routeChangeError', function(){
        updateIsActive();
      });
      
      $scope.$on('$destroy', function(){
        link.off('click', onLinkClick);
      });
      
      updateIsActive();
      
    }
  };
  
}]);