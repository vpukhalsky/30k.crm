'use strict';

/* Fixes table header on top of screen */

angular.module('30k.fixed-table-header', [
  
])

.directive('kkFixedTableHeader', ['$http', '$templateCache', '$compile', '$timeout', function($http, $templateCache, $compile, $timeout){
    
    return{
        
        scope: {
            template: '@',
            tableData: '=',
            attachTo: '@?',
            after: '@?'
        },
        
        controller: function($scope, $element){
            
            var template = '';
            var fixedWrapperTemplate = '<div class="fixed-table-header"><table><thead>{{template}}</thead></table></div>';
            
            var normal = null;
            var fixed = null;
            
            $scope.toHash = function(data){
                return angular.toJson(data);
            };
            
            var dataHash = $scope.toHash($scope.tableData);
            
            template = $templateCache.get($scope.template);
            
            if(!template){
                loadTemplate().then(init);
            }else{
                init();
            }
            
            function loadTemplate(){
                
                return $http.get($scope.template).then(function(response){
                    
                    template = response.data;
                    $templateCache.put($scope.template, template);
                    return template;
                    
                });
                
            }
            
            function init(){
                
                var temp = angular.element(template);
                normal = $compile(temp)($scope.$parent, function(result, scope){
                    $element.append(result);
                });
                
                angular.element(window).on('scroll', onScroll);
                angular.element(window).on('resize', onResize);
                
                $scope.$watch('toHash(tableData)', function(newHash){
                    if(newHash != dataHash && fixed) $timeout(updateCols, 100);
                    dataHash = $scope.toHash(newHash);
                });
                
            }
            
            function onScroll(e){
                
                var top = $element.offset().top;
                var scrollTop = angular.element(window).scrollTop();
                var bottom = top + $element.parent().height();
                
                if(scrollTop > top && scrollTop < bottom - $element.height()){
                    if(!fixed || fixed.is(':hidden')) showFixed();
                    updateTopPos();
                }else{
                    if(fixed && fixed.is(':visible')) hideFixed();
                }
                
            }
            
            function onResize(e){
                
                if(fixed){
                    updatePos();
                    updateCols();
                }
                
            }
            
            function initFixed(){
                
                var tempTemplate = fixedWrapperTemplate.replace('{{template}}', template);
                var temp = angular.element(tempTemplate);
                
                if(!$scope.attachTo) $scope.attachTo = 'body';
                
                fixed = $compile(temp)($scope.$parent, function(result, scope){
                    angular.element($scope.attachTo).append(result);
                });
                
                updatePos();
                updateCols();
                
                hideFixed();
                
            }
            
            function updatePos(){
                
                var table = $element.parent();
                var offset = table.offset();
                var width = table.outerWidth();
                
                fixed.css({
                    left: offset.left + 'px',
                    width: width + 'px'
                });
                
                updateTopPos();
                
            }
            
            function updateTopPos(){
                
                var top = 0;
                
                if($scope.after){
                    
                    var after = angular.element($scope.after);
                    top = after.offset().top - angular.element(window).scrollTop() + after.outerHeight();
                    
                }
                
                fixed.css({ top: top + 'px' });
            }
            
            function updateCols(){
                
                var frows = fixed.find('tr');
                
                for(var i = 0 ; i < normal.size() ; i++){
                    
                    var cols = normal.eq(i).children();
                    var fcols = frows.eq(i).children();
                    
                    for(var j = 0 ; j < cols.size() ; j++){
                        fcols.eq(j).css('width', cols.eq(j).outerWidth() + 'px');
                    }
                    
                }
                
            }
            
            function showFixed(){
                
                if(!fixed) initFixed();
                fixed.show();
                updateCols();
                
            }
            
            function hideFixed(){
                if(fixed) fixed.hide();
            }
            
            $scope.$on('$destroy', function(){
                angular.element(window).off('scroll', onScroll);
                angular.element(window).on('resize', onResize);
                if(fixed) fixed.detach();
            });
            
        }
    };
    
}]);