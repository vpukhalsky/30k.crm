'use strict';

/* Input component returning position and length of a generic string */

angular.module('30k.range-selector', [
    
])

.directive('kkRangeSelector', [function(){
    
    return {
        
        scope: {
            start: '=',
            size: '=',
            range: '=?'
        },
        
        templateUrl: 'components/range-selector/range-selector.html?v=1',
        restrict: 'E', 
        
        controller: ['$scope', '$element', '$timeout', function($scope, $element, $timeout){
            
            var max = 10;
            
            $scope.chars = [];
            $scope.selectionInProgress = false;
            $scope.update = true;
            
            $scope.$watch('substringLength', function(newValue){
                
                $scope.update = false;
                
                $timeout(function(){
                    $scope.update = true;
                });
            
            });
            
            $scope.onRangeClick = function(index){
                
                if(!$scope.selectionInProgress){
                    
                    $scope.selectionInProgress = true;
                    $scope.start = index;
                    
                }else{
                    
                    if(index == $scope.start){
                        
                        $scope.start = index;
                        $scope.size = 1;
                        
                    }else if(index < $scope.start){
                        
                        var temp = $scope.start;
                        $scope.start = index;
                        $scope.size = temp - index + 1;
                        
                    }else{
                        
                        $scope.size = index - $scope.start + 1;
                        
                    }
                    
                    $scope.selectionInProgress = false;
                    
                }
                
                updateRange();
                  
            };
            
            $scope.onRangeEnter = function(index, event){
                
                updateRange(index);
                
            };
            
            $scope.onRangeLeave = function(index, event){
                
                updateRange();
                
            };
            
            function updateRange(index){
                
                for(var i = 0 ; i < $scope.chars.length ; i++){
                    
                    var char = $scope.chars[i];
                    
                    char.inRange = false;
                    char.selected = false;
                    
                    if($scope.start == i && ($scope.selectionInProgress || $scope.size > 0)){
                        
                        // start
                        char.selected = true;
                        
                    }else if((angular.isNumber(index) && $scope.selectionInProgress && index == i) || (!$scope.selectionInProgress 
                        && $scope.start + $scope.size - 1 == i && $scope.size > 0)
                    ){
                        
                        // end
                        char.selected = true;
                        
                    }else if((!$scope.selectionInProgress && $scope.size > 0 
                        && i > $scope.start && i < $scope.start + $scope.size - 1)
                        || ($scope.selectionInProgress && angular.isNumber(index) 
                        && ( (i > index && index < $scope.start && i < $scope.start) || (i < index && index > $scope.start && i > $scope.start) ) )
                    ){
                        
                        // range
                        char.inRange = true;
                        
                    }
                    
                }
                
            }
            
            (function init(){
                
                $scope.selectionInProgress = false;
                
                if(!angular.isNumber($scope.start) || $scope.start < 0){
                    $scope.start = 0;
                }
                
                if(!angular.isNumber($scope.size) || $scope.size < 0){
                    $scope.size = 0;
                }
                
                if(max < $scope.start + $scope.size) max = $scope.start + $scope.size;
                
                $scope.chars.splice(0, $scope.chars.length);
                
                for(var i = 0 ; i < max ; i++){
                    
                    $scope.chars.push({
                        id: i,
                        selected: false,
                        inRange: false
                    });
                    
                }
                
                updateRange();
                
            })();
            
        }]
    };
}]);