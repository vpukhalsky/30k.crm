'use strict';

angular.module('30k.service', [
  '30k.alerts'
])

.service('kkService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){
    
    var jsonPath = 'json/';
    
    function _handleError(response){
        
        if(response.config.loader && $rootScope.loading > 0) $rootScope.loading--;
        
        var message = 'An unknown server error occurred.';
 
        if (!response.data.message){
            
            _showAlert('An unknown server error occured. Please try again or refresh page.');
            
        }else{
            
            _showAlert(response.data.message);
            message = response.data.message;
        }
        
        return $q.reject(message);

    }
         
    function _handleSuccess(response){
        
        if(response.config.loader && $rootScope.loading > 0) $rootScope.loading--;
        
        if(!angular.isObject(response.data) || !response.data.success){
            
            if(response.data.message){
                _showAlert(response.data.message);
            }
            
        }else{
            if(response.data.message){
                _showAlert(response.data.message, 'success');
            }
        }
        
        return response.data.result;
        
    }
    
    function _showAlert(msg, type){
        
        if(type === undefined) type = 'alert';

        $rootScope.$emit('kk-alert', { alert: { type: type, msg: msg }});
    }
    
    function sendPost(urlSlug, data, loader){
        
        if(loader === undefined) loader = true;
        
        if(loader) $rootScope.loading++;
        
        var request = $http({
            method: 'post',
            url: jsonPath + urlSlug,
            data: data,
            loader: loader
        });
    
        return request.then(_handleSuccess, _handleError);
        
    }
    
    function _prepareGetUrl(url, params){
        
        if(params && angular.isObject(params)){
            for(var i in params){
                url = url.replace(':' + i, params[i]);
            }
        }
        
        return url;
    }
    
    function sendGet(urlSlug, params, loader){
        
        if(loader === undefined) loader = true;
        
        if(loader) $rootScope.loading++;
        
        var url = _prepareGetUrl(urlSlug, params);
        
        var request = $http({
            method: 'get',
            url: jsonPath + url,
            loader: loader
        });
        
        return request.then(_handleSuccess, _handleError);
    }
    
    function promise(data){
        
        var d = $q.defer();
        d.resolve(data);
        return d.promise;
        
    }
    
    return {
        sendPost: sendPost,
        sendGet: sendGet,
        promise: promise
    };
    
}])

.service('collectionService', ['kkService', '$filter', '$modal', function(kkService, $filter, $modal){
    
    var collections = {};
    var meta = {};
    
    function getConfig(url, type, params){
        
        if(!angular.isObject(meta[type])) meta[type] = {};
        
        if(angular.isDefined(meta[type].config)){
            return kkService.promise(meta[type].config);
        }
        
        var promise = null;
        
        if(params){
            promise = kkService.sendPost(url, params);
        }else{
            promise = kkService.sendGet(url);
        }
        
        return promise.then(function(response){
            meta[type].config = response;
            return meta[type].config;
        });
        
    }
    
    function _openDeleteItemsModal(url, type, items){
        
        return $modal.open({
            templateUrl: 'components/service/delete-items-modal.html',
            controller: 'DeleteItemsModalController',
            windowClass: 'tiny',
            resolve: {
                items: function(){
                    return items;
                },
                type: function(){
                    return type;
                },
                url: function(){
                    return url;
                }
            }
        });
        
    }
    
    function deleteItem(url, type, item){
        
        var modalInstance = _openDeleteItemsModal(url, type, [item]);
        
        return modalInstance.result.then(function(result){
            if(result.remove == true) return _deleteItem(result.url, result.type, result.items[0].id);
            return false;
        });
        
    }
    
    function deleteItems(url, type, items){
        
        var modalInstance = _openDeleteItemsModal(url, type, items);
        
        return modalInstance.result.then(function(result){
            var itemIds = result.items.map(function(item){ return item.id });
            if(result.remove == true) return _deleteItems(result.url, result.type, itemIds);
            return false;
        });
        
    }
    
    function _deleteItem(url, type, id){
        
        return kkService.sendPost(url, { id: id }).then(function(response){
            
            _deleteItemFromCollection(type, response.item);

            return response;
        });
        
    }
    
    function _deleteItems(url, type, ids){
        
        return kkService.sendPost(url, { ids: ids }).then(function(response){
            
            response.items.forEach(function(item){
                _deleteItemFromCollection(type, item);
            });
            
            return response;
        });
        
    }
    
    function _deleteItemFromCollection(type, item){
        
        var collection = collections[type];
        
        var index = $filter('indexOf')(collection, item.id);
        if(index >= 0) collection.splice(index, 1);
        
        if(angular.isObject(meta[type]) && angular.isArray(meta[type].fullItems)){
            var i = meta[type].fullItems.indexOf(item.id);
            if(i >= 0) meta[type].fullItems.splice(i, 1);
        }
        
    }
    
    function getCollection(url, type, params, loader){
        
        if(!angular.isDefined(params)) params = {};
        if(loader === undefined) loader = true;
        
        if(angular.isArray(collections[type]) && meta[type].paged === false) return kkService.promise(collections[type]);
        else{
            return kkService.sendPost(url, params, loader).then(function(response){
                
                collections[type] = response.items;
                
                if(!angular.isObject(meta[type])) meta[type] = {};
                meta[type].paged = false;
                meta[type].fullItems = [];
                
                return collections[type];
                
            });
        }
        
    }
    
    function getPagedCollection(url, type, params, loader){
        
        var key = JSON.stringify(params);
        if(loader === undefined) loader = true;
        
        if(angular.isArray(collections[type]) && angular.isObject(meta[type]) && meta[type].paged && meta[type].lastPagedKey == key){
            return kkService.promise(meta[type].lastPagedResponse);
        }
        
        return kkService.sendPost(url, params, loader).then(function(response){
            collections[type] = response.items;
            
            if(!angular.isObject(meta[type])) meta[type] = {};
            
            meta[type].paged = true;
            meta[type].lastPagedKey = key;
            meta[type].lastPagedResponse = response;
            
            return response;
        });
        
    }
    
    function getFullItem(url, type, value, paramName){
        
        if(!angular.isDefined(paramName)) paramName = 'id';
        
        if(angular.isObject(meta[type]) 
            && angular.isArray(meta[type].fullItems) 
            && meta[type].fullItems.indexOf(value) >= 0
        ){
            var item = $filter('find')(collections[type], value, paramName);
            if(item) return kkService.promise(item);
        }
        
        var params = {};
        params[paramName] = value;
        
        return kkService.sendPost(url, params).then(function(response){
            
            if(!angular.isArray(collections[type])) collections[type] = [];
            var item = _updateItem(type, response.item);
            
            if(!angular.isObject(meta[type])) meta[type] = {};
            if(!angular.isArray(meta[type].fullItems)) meta[type].fullItems = [];
            meta[type].fullItems.push(value);
            
            return item;
        });
    
    }
    
    function getItem(url, type, value, paramName){
        
        if(!angular.isDefined(paramName)) paramName = 'id';
        
        if(angular.isArray(collections[type])){
            var item = $filter('find')(collections[type], value, paramName);
            if(item) return kkService.promise(item);
        }
        
        var params = {};
        params[paramName] = value;
        
        return kkService.sendPost(url, params).then(function(response){
            return response.item;
        });
    }
    
    function _updateItem(type, item){
        
        var collection = collections[type];
        
        var r = $filter('find')(collection, item.id);
        
        if(r) angular.extend(r, item);
        else{
            r = item; 
            if(angular.isArray(collection)) collection.push(r);
        }
        
        return r;
    }
    
    function saveItem(url, type, item){
        
        return kkService.sendPost(url, item).then(function(response){
            angular.extend(item, response.item);
            response.item = _updateItem(type, item);
            return response;
            
        });
        
    }
    
    function saveItems(url, type, items){
        
        return kkService.sendPost(url, items).then(function(response){
            
            var find = $filter('find');
            
            response.items.forEach(function(item){
                
                var i = find(items, item.id);
                
                if(i) {
                    angular.extend(i, item);
                }else{
                    i = item;
                }
                
                item = _updateItem(type, i);
            });
            
            return response;
            
        });
        
    }
    
    function cloneItem(url, type, item){
        
        var clone = angular.copy(item);
        delete clone.id;
        
        return saveItem(url, type, clone).then(function(response){
            
            if(collections[type]) _replaceClone(collections[type], item.id, response.item);
            return response;
            
        });
        
    }
    
    function _replaceClone(collection, originalId, clone){
        
        var indexOf = $filter('indexOf');
        var cloneIndex = indexOf(collection, clone.id);
        collection.splice(cloneIndex, 1);
        var originalIndex = indexOf(collection, originalId);
        collection.splice(originalIndex + 1, 0, clone);
        
    }
    
    function cloneItems(url, type, items){
        
        var ids = [];
        
        var clones = angular.copy(items);
        
        clones.forEach(function(clone){
            ids.push(clone.id);
            delete clone.id;
        });
        
        return saveItems(url, type, clones).then(function(response){
            if(collections[type]){
                for(var i = 0 ; i < response.items.length ; i++){
                    if(ids[i]) _replaceClone(collections[type], ids[i], response.items[i]);
                }
            }
            
            return response;
            
        });
        
    }
    
    function flushCollection(type){
        
        if(collections[type]) delete collections[type];
        if(meta[type]) delete meta[type];
        
    }
	
	function getItemsCount(type) {
        if (collections[type] && angular.isArray(collections[type])) {
            return collections[type].length;
        }

        return 0;
    }

    return {
        getCollection: getCollection,
        getPagedCollection: getPagedCollection,
        deleteItem: deleteItem,
        deleteItems: deleteItems,
        getItem: getItem,
        saveItem: saveItem,
        saveItems: saveItems,
        getFullItem: getFullItem,
        getConfig: getConfig,
        cloneItem: cloneItem,
        cloneItems: cloneItems,
        flushCollection: flushCollection,
        getItemsCount: getItemsCount
    };
    
}])

.controller('DeleteItemsModalController', ['$scope', '$modalInstance', 'items', 'type', 'url', function($scope, $modalInstance, items, type, url){
    
    $scope.items = items;
    $scope.type = type;
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.confirm = function(){
        $modalInstance.close({
            remove: true,
            items: items,
            type: type,
            url: url
        });
    };
    
}]);