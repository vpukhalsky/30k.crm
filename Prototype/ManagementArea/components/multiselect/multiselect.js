'use strict';

/* Checkbox-based multiselect */

angular.module('30k.multiselect', [
    'mm.foundation',
    'checklist-model'
])

.directive('kkMultiselect', [function(){
  
  return {
    
    templateUrl: 'components/multiselect/multiselect.html',
    
    scope: {
      
      list: '=',
      selected: '=',
      cols: '@',
      id: '@',
      label: '@?',
      property: '@?'
      
    },
    
    controller: function($scope, $element){
      
      var parent = $element.parent();
      if(parent.hasClass('f-dropdown')){
        parent.addClass('multiselect');
      }
      
      $scope.getValue = function(item){
        if($scope.property) return item[$scope.property];
        else return item;
      };
      
      $scope.selectAll = function(){
        
        $scope.deselect();
        
        for(var i = 0 ; i < $scope.list.length ; i++){
          var val = $scope.list[i];
          if($scope.property) val = val[$scope.property];
          $scope.selected.push(val);
        }
        
      };
      
      $scope.deselect = function(){
        $scope.selected.splice(0, $scope.selected.length);
      };
      
    }
    
  };
  
}]);