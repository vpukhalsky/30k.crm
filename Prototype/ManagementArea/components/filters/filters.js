'use strict';

angular.module('30k.user.filters', [])

.filter('find', [function(){
    return function(array, value, key){
        
        if(!angular.isArray(array)) return null;
        
        if(!angular.isString(key)) key = 'id';
        
        var result = null;
        
        for(var i = 0 ; i < array.length ; i++){
            if(array[i][key] == value){
                result = array[i];
                break;
            }
        }
    
        return result;
        
    };
}])

.filter('prop', [function(){
    return function(obj, prop){
        if(!angular.isObject(obj) || !angular.isDefined(obj[prop]) || !angular.isString(prop)) return null;
        return obj[prop];
    };
}])

.filter('indexOf', [function(){
    return function(array, value, key){
        
        if(!angular.isArray(array)) return -1;
        
        if(!angular.isString(key)) key = 'id';
        
        for(var i = 0 ; i < array.length ; i++){
            if(array[i][key] == value){
                return i;
            }
        }
        
        return -1;
    };
}])

.filter('domain', function() {
  
  return function(text) {
      return text.replace(/^https?\:\/\/w{0,3}\.?/, '');
  };
  
})

.filter('join', function(){
    
    return function(arr, sep){
        if(!angular.isDefined(sep)) sep = ', ';
        return arr.join(sep);
    };
    
})

.filter('trim', function(){
    
    return function(text, max, symbol){
        if(!max) max = 3;
        if(!symbol) symbol = '.';
        if(text.length < max) return text;
        else return text.substring(0, max) + symbol;
    };
    
})

.filter('hexToRgb', function(){
    
    return function(hex){
        
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });
    
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
        
    };
    
})

.filter('rgbToHex', function(){
    return function(rgb){
      
      var hex = '#';
      
      var c = rgb.r.toString(16);
      hex += c.length == 1 ? "0" + c : c;
      
      c = rgb.g.toString(16);
      hex += c.length == 1 ? "0" + c : c;
      
      c = rgb.b.toString(16);
      hex += c.length == 1 ? "0" + c : c;
      
      return hex;
      
    };
})

.filter('lighten', ['hexToRgbFilter', 'rgbToHexFilter', function(hexToRgbFilter, rgbToHexFilter){
    
    return function(color, pct){
        
        if(!angular.isNumber(pct)) pct = 0.1;
        
        if(!angular.isObject(color)){
            color = hexToRgbFilter(color);
        }
        
        var max = 255;
        var val = Math.floor(pct * max);
        color.r + val <= max ? color.r = color.r + val : color.r = max;
        color.g + val <= max ? color.g = color.g + val : color.g = max;
        color.b + val <= max ? color.b = color.b + val : color.b = max;
        
        return rgbToHexFilter(color);
    };
    
}])

.filter('timeForHumans', ['dateFilter', function(dateFilter){
    
    return function(time){
        
        var now = new Date();
        var ts = new Date(time);
        
        var diff = now.valueOf() - ts.valueOf();
        
        var sec = 1000;
        var min = 60 * sec;
        var hour = 60 * min;
        var day = 24 * hour;
        var week = 7 * day;
        var month = 30 * day;
        var year = 365 * day;
        
        var y = Math.floor(diff / year);
        
        if(y > 1) return y + ' years ago';
        else if(y == 1) return 'One year ago';
        
        var m = Math.floor(diff /month);
        
        if(m > 1) return m + ' months ago';
        else if(m == 1) return 'One month ago';
        
        var w = Math.floor(diff / week);
        if(w > 1) return w + ' weeks ago';
        else if(w == 1) return 'One week ago';
        
        var d = Math.floor(diff / day);
        if(d > 1) return d + ' days ago';
        else if(d == 1) return 'One day ago';
        
        var h = Math.floor(diff / hour);
        if(h > 1) return h + ' hours ago';
        else if(h == 1) return 'One hour ago';
        
        m = Math.floor(diff / min);
        if(m > 1) return m + ' minutes ago';
        else if(m == 1) return 'One minute ago';
        
        var s = Math.floor(diff / sec);
        if(s > 1) return s + ' seconds ago';
        else return 'Just now';
        
        return dateFilter(time, 'medium');
        
    };
    
}])

.filter('duration', function(){
    return function(min){
        var h = Math.floor(min / 60);
        var m = min % 60;
        return h + 'h ' + (m < 10 ? '0' : '') + m + 'm';
    };
})

.filter('sumArraysLength', function () {
    return function (data, arrayKey) {
        if (typeof (data) === 'undefined' || !angular.isArray(data)) {
            return 0;
        }

        var sum = 0;
        for (var i = data.length - 1; i >= 0; i--) {
            sum += data[i][arrayKey].length;
        }

        return sum;
    };
});