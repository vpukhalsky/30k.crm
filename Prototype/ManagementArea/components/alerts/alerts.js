'use strict';

/* Success and error alerts module */

angular.module('30k.alerts', [
    'mm.foundation'
])

.directive('kkAlerts', ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll){
    
    return{
        restrict: 'A',
        
        template: '<alert ng-repeat="alert in alerts" type="alert.type" close="closeAlert($index)">{{alert.msg}}</alert>',
        
        controller: function($scope, $element){
            
            $scope.alerts = [];
            
            $scope.$watchCollection('alerts', function(newValue, oldValue){
              
              if(newValue.length > oldValue.length){
                $location.hash('window-top');
                $anchorScroll();
              }
              
            });
            
            $scope.addAlert = function(alert){
                addAlert(alert);
            };
            
            function addAlert(alert){
                $scope.alerts.push(alert);
            }
            
            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };
            
            function onAlert(event, args){
                if(angular.isObject(args.alert)){
                    addAlert(args.alert);
                }
            }
            
            $rootScope.$on('kk-alert', onAlert);
        }
    };
    
}])

.service('alertService', ['$rootScope', '$modal', function($rootScope, $modal){
    
    function show(message, type){
        if(!type) type = 'success';
        $rootScope.$emit('kk-alert', { alert: { type: type, msg: message }});
    }
    
    function popup(title, message, callback){
        
        $modal.open({
            template: '<h3>{{title}}</h3><p>{{message}}</p><button ng-click="cancel()" class="secondary small">OK</button><a class="close-reveal-modal" ng-click="cancel()">&#215;</a>',
            windowClass: 'tiny',
            resolve: {
                title: function(){
                    return title;
                },
                message: function(){
                    return message;
                }
            },
            controller: function($scope, $modalInstance, title, message){
                
                $scope.title = title;
                $scope.message = message;
                
                $scope.cancel = function(){
                    $modalInstance.dismiss('cancel');
                };
                
            }
        }).result.then(function(){
            if(callback) callback();
        });
    }
    
    return {
        show: show,
        popup: popup
    };
    
}]);
