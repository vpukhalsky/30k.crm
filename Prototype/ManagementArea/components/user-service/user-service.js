'use strict';

angular.module('30k.user.service', [
    
    '30k.service',
    '30k.user.filters'
    
])

.service('userService', ['kkService', '$filter', function(kkService, $filter){
    
    var loggedUser = null;
    var users = null;
    
    function _updateUser(response){
        
        var user = $filter('find')(users, response.user.id);
        
        if(user) angular.extend(user, response.user);
        else{
            user = response.user; 
            if(angular.isArray(users)) users.push(response.user);
        }
        
        return user;
    }
    
    function _getLoggedInUser(){
        if(angular.isObject(loggedUser)) return kkService.promise(loggedUser);
        else{
            return kkService.sendGet('account/get-logged-user.json').then(function(response){
                return loggedUser = response.user;
            });
        }
    }
    
    function _getUser(id){
        
        var user = $filter('find')(users, id);
        
        if(user) return kkService.promise(user);
        else{
            return kkService.sendPost('user/get-user.json', id).then(_updateUser);
        }
    }
    
    function getUser(id){
        
        if(!angular.isNumber(id)){
            return _getLoggedInUser();
        }else{
            return _getUser(id);
        }
        
    }
    
    function saveUser(userData){
        
        return kkService.sendPost('account/save-user.json', userData).then(function(response){
            
            var user = response.user;
            
            if(_isLoggedUser(response.user.id)) user = loggedUser = response.user;
            else user = _updateUser(response);
            
            return user;
        });
        
    }
    
    function _isLoggedUser(id){
        return angular.isObject(loggedUser) && id == loggedUser.id;
    }
    
    function deleteUser(id){
        
        var url = 'user/delete-user.json';
        
        return kkService.sendPost(url, id).then(function(response){
            
            var index = $filter('indexOf')(users, response.user.id);
            if(index >= 0) {
                users.splice(index, 1);
            }

            return response;
        });
        
    }
    
    function getCollection(){
        
        if(angular.isArray(users)) return kkService.promise(users);
        else return kkService.sendGet('user/get-users.json').then(function(response){
            
            users = response.users;
            return users;
            
        });
        
    }
    
    return {
        getUser: getUser,
        saveUser: saveUser,
        deleteUser: deleteUser,
        getCollection: getCollection
    };
    
}]);