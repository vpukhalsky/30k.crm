'use strict';

/* Single button file uploader */

angular.module('30k.uploader', [
    'ngUpload',
    '30k.alerts'
])

.directive('kkUploader', ['alertService', function(alertService){
    
    return {
        
        restrict: 'E',
        
        scope: {
            action: '@',
            onComplete: '=',
            btnClass: '=',
            params: '='
        },
        
        templateUrl: 'components/uploader/uploader.html',
        transclude: true,
        
        controller: function($scope, $element){
            
            if(!$scope.btnClass) $scope.btnClass = {};
            if(!$scope.params) $scope.params = {};
            
            var initialized = false;
            
            $scope.loading = false;
            
            $scope.onUploaded = function(response){
                
                $scope.loading = false;
                
                if(angular.isArray($scope.btnClass)){
                    var index = $scope.btnClass.indexOf('loading');
                    if(index) $scope.btnClass.splice(index, 1);
                }else if(angular.isObject($scope.btnClass)){
                    if(angular.isDefined($scope.btnClass.loading)) delete $scope.btnClass.loading;
                }
                
                if(response.success && angular.isString(response.message)){
                    
                    alertService.show(response.message, 'success');
                    $scope.onComplete(response.result);
                    
                }else if(!response.success && angular.isString(response.message)){
                    alertService.show(response.message, 'alert');
                }
                
            };
            
            function onFileSelected(e){
                
                var input = $(e.currentTarget);
                var form = input.closest('form');
                if(input.val()) form.submit();
                
                $scope.loading = true;
                
                if(angular.isArray($scope.btnClass)){
                    $scope.btnClass.push('loading');
                }else if(angular.isObject($scope.btnClass)){
                    $scope.btnClass.loading = true;
                }
                
            }
            
            $scope.upload = function(){
                
                var input = $element.find('input[type="file"]');
                
                if(!initialized){
                    input.on('change', onFileSelected);
                    initialized = true;
                }
                
                input.click();
            };
            
            $scope.$on('$destroy', function(){
               
               if(initialized){
                   var input = $element.find('input[type="file"]');
                   input.off('change', onFileSelected);
               }
               
            });
            
        }
    };
}]);