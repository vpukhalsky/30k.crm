'use strict';

/* Services */

angular.module('30k.crm.benefit-text', [])

.directive('kkBenefitText', [function(){
    
    return {
        scope: {
            item: '=',
            benefits: '='
        },
        templateUrl: 'components/benefit-text/benefit-text.html',
        restrict: 'E',
        controller: function($scope, $element){
            
        }
    };
    
}]);