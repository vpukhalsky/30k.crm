'use strict';

angular.module('30k.ma.tool', [
    
    'mm.foundation',
    
    '30k.error',
    '30k.tool.service',
    '30k.routes',
    
    '30k.ma'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
      
    newTool: {
      path: '/tools/new',
      controller: 'ToolEditController',
      templateUrl: 'tool/edit/edit.html'
    },
    
    editTool: {
      path: '/tools/edit/:id',
      controller: 'ToolEditController',
      templateUrl: 'tool/edit/edit.html'
    },
    
    tools: {
      path: '/tools',
      controller: 'ToolCollectionController',
      templateUrl: 'tool/collection/collection.html'
    },
    
    main: {
      path: '/tools',
      controller: 'ToolCollectionController',
      templateUrl: 'tool/collection/collection.html'
    }
    
  });
  
}])

.controller('ToolDeleteToolModalController', ['$scope', '$modalInstance', 'tool', function($scope, $modalInstance, tool){
    
    $scope.tool = tool;
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.confirm = function(){
        $modalInstance.close({
            remove: true,
            tool: tool
        });
    };
    
}]);