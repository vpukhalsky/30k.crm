'use strict';

angular.module('30k.ma.tool')

.controller('ToolEditController', ['$scope', 'toolService', '$modal', 'kkRoutesService', '$routeParams', 'findFilter', function($scope, toolService, $modal, kkRoutesService, $routeParams, findFilter){
    
    kkRoutesService.preventReload($scope, 'ToolEditController');
    
    $scope.config = null;
    $scope.tool = null;
    $scope.isNew = true;
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        toolService.saveTool($scope.tool).then(function(tool){
            if(angular.isObject(tool)) angular.extend($scope.tool, tool);
            kkRoutesService.redirect('tools');
        });
        
    };
    
    $scope.promptDeleteTool = function(tool){
        
        var modalInstance = $modal.open({
            templateUrl: 'tool/delete-tool-modal.html',
            controller: 'ToolDeleteToolModalController',
            windowClass: 'tiny',
            resolve: {
                tool: function(){
                    return tool;
                }
            }
        });
        
        modalInstance.result.then(function(result){
            if(result.remove == true) deleteTool(tool.id);
        });
        
    };
    
    function deleteTool(tool){
        toolService.deleteTool(tool).then(function(){
            kkRoutesService.redirect('tools');
        });
    }
    
    $scope.onPermissionTypeChange = function(permission, select){
        
        var valid = true;
        
        if(permission.type != 'other'){
            
            for(var i = 0 ; i < $scope.tool.permissions.length ; i++){
                
                var perm = $scope.tool.permissions[i];
                if(perm === permission) continue;
                else if(perm.type == permission.type){
                    valid = false;
                    break;
                }
                
            }
            
        }
        
        select.$setValidity('duplicate', valid);
        
    };
    
    $scope.addPermission = function(){
        $scope.tool.permissions.push(angular.copy($scope.config.defaultPermission));
    };
    
    $scope.deletePermission = function(index){
        $scope.tool.permissions.splice(index, 1);
    };

    if(angular.isDefined($routeParams.id)){
        
        var id = parseInt($routeParams.id, 10);
        $scope.isNew = false;
        
        toolService.getTool(id).then(function(tool){
            $scope.tool = angular.copy(tool);
        });
        
    }
    
    toolService.getConfig().then(function(config){
        $scope.config = config;
        if($scope.isNew) $scope.tool = angular.copy($scope.config.defaultTool);
    });
    
}]);