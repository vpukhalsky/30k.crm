'use strict';

angular.module('30k.ma.tool')

.controller('ToolCollectionController', ['$scope', 'toolService', 'kkRoutes', 'findFilter', '$modal', 'kkRoutesService', function($scope, toolService, kkRoutes, findFilter, $modal, kkRoutesService){
    
    kkRoutesService.preventReload($scope, 'ToolCollectionController');
    
    $scope.tools = null;
    
    $scope.promptDeleteTool = function(tool){
        
        var modalInstance = $modal.open({
            templateUrl: 'tool/delete-tool-modal.html',
            controller: 'ToolDeleteToolModalController',
            windowClass: 'tiny',
            resolve: {
                tool: function(){
                    return tool;
                }
            }
        });
        
        modalInstance.result.then(function(result){
            if(result.remove == true) deleteTool(tool.id);
        });
        
    };
    
    function deleteTool(tool){
        toolService.deleteTool(tool);
    }
    
    toolService.getCollection().then(function(tools){
        $scope.tools = tools;
    });
    
}]);