'use strict';

angular.module('30k.ma.wallet', [
    
    'mm.foundation',
    
    '30k.error',
    '30k.routes',
    '30k.service',
    
    '30k.ma'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
    
    wallet: {
      path: '/wallet',
      controller: 'WalletController',
      templateUrl: 'wallet/wallet/wallet.html'
    },
    
    main: {
      path: '/wallet',
      controller: 'WalletController',
      templateUrl: 'wallet/wallet/wallet.html'
    }
    
  });
  
}]);