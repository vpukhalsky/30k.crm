'use strict';

angular.module('30k.ma.wallet')

.controller('WalletController', ['$scope', 'collectionService', 'kkRoutesService', '$filter', function($scope, collectionService, kkRoutesService, $filter){
    
    kkRoutesService.preventReload($scope, 'WalletController');
    
    $scope.selectable = [];
    $scope.config = null;
    $scope.items = null;
    $scope.item = {};
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        collectionService.saveItem('wallet/save-ffp.json', 'program', $scope.item).then(function(response){
            updateSelectable();
            form.$setPristine(true);
            $scope.item = {};
        });
        
    };
    
    $scope.toggle = function(item){
        item.expanded = !item.expanded;
    };
    
    $scope.refresh = function(item){
        
        collectionService.saveItem('wallet/get-ffp.json', 'program', item).then(function(response){
            updateSelectable();
        });
        
    };
    
    $scope.remove = function(item){
        
        collectionService.deleteItem('wallet/delete-ffp.json', 'program', item).then(function(response){
            updateSelectable();
        });
        
    };
    
    function updateSelectable(){
        
        if(!$scope.config || !$scope.items) return;
        
        if($scope.selectable.length > 0) $scope.selectable.splice(0, $scope.selectable.length);
        
        var indexOf = $filter('indexOf');
        
        $scope.config.ffps.forEach(function(ffp){
            if(indexOf($scope.items, ffp.id) < 0) $scope.selectable.push(ffp);
        });
        
    }
    
    collectionService.getConfig('wallet/config.json', 'program').then(function(config){
        $scope.config = config;
        updateSelectable();
    });
    
    collectionService.getCollection('wallet/get-ffps.json', 'program').then(function(response){
        $scope.items = response;
        updateSelectable();
    });
    
}]);