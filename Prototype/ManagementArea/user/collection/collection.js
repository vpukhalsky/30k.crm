'use strict';

angular.module('30k.ma.user')

.controller('UserCollectionController', ['$scope', 'userService', 'roleService', '$filter', 'kkRoutes', '$modal', 'kkRoutesService', function($scope, userService, roleService, $filter, kkRoutes, $modal, kkRoutesService){
    
    kkRoutesService.preventReload($scope, 'UserCollectionController');
    
    $scope.roles = null;
    $scope.users = null;
    
    $scope.getRoleName = function(roleId){
        
        var name = '';
        
        if($scope.roles){
        
            var role = $filter('filter')($scope.roles, { id : roleId });
            if(role.length > 0) name = role[0].name;
            
        }
        
        return name;
    };
    
    $scope.promptDeleteUser = function(user){
        
        var modalInstance = $modal.open({
            templateUrl: 'user/delete-user-modal.html',
            controller: 'UserDeleteUserModalController',
            windowClass: 'tiny',
            resolve: {
                user: function(){
                    return user;
                }
            }
        });
        
        modalInstance.result.then(function(result){
            if(result.remove == true) deleteUser(result.user);
        });
        
    };
    
    $scope.getUserEditUrl = function(userId){
        return '#' + kkRoutes.editUser.path.replace(':id', userId);
    };
    
    function deleteUser(user){
        
        userService.deleteUser(user.id);
        
    }
    
    roleService.getList().then(function(roles){
        $scope.roles = roles;
    });
    
    userService.getCollection().then(function(users){
        $scope.users = users;
    });
    
}]);