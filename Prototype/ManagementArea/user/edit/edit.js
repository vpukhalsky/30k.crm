'use strict';

angular.module('30k.ma.user')

.controller('UserEditController', ['$scope', 'userService', 'roleService', '$modal', '$routeParams', 'kkRoutes', '$location', 'kkRoutesService', 'collectionService', function($scope, userService, roleService, $modal, $routeParams, kkRoutes, $location, kkRoutesService, collectionService){
    
    kkRoutesService.preventReload($scope, 'UserEditController');
    
    $scope.isNew = true;
    $scope.user = {};
    $scope.config = null;
    
    $scope.roles = null;
    $scope.defaultRoleId = 0;
    
    $scope.passwordInputType = 'password';
    
    $scope.submit = function(form){
        
        if(!form.$valid) return;
        
        userService.saveUser($scope.user).then(function(user){
            if(angular.isObject(user)) angular.extend($scope.user, user);
            redirectToUsers();
        });
        
    };
    
    $scope.togglePasswordInputType = function(){
        
        if($scope.passwordInputType == 'password'){
            $scope.passwordInputType = 'text';
        }else if($scope.passwordInputType == 'text'){
            $scope.passwordInputType = 'password';
        }
    };
    
    $scope.promptDeleteUser = function(user){
        
        var modalInstance = $modal.open({
            templateUrl: 'user/delete-user-modal.html',
            controller: 'UserDeleteUserModalController',
            windowClass: 'tiny',
            resolve: {
                user: function(){
                    return $scope.user;
                }
            }
        });
        
        modalInstance.result.then(function(result){
            if(result.remove == true) deleteUser(result.user);
        });
        
    };
    
    function deleteUser(user){
        
        userService.deleteUser(user.id).then(function(response){
            redirectToUsers();
        });
        
    }
    
    function redirectToUsers(){
        $location.path(kkRoutes.users.path);  
    } 
    
    if($routeParams.id){
        
        $scope.isNew = false;
        
        var userId = parseInt($routeParams.id, 10);
        
        userService.getUser(userId).then(function(user){
            $scope.user = angular.copy(user);
        });
        
    }
    
    roleService.getList().then(function(roles){
        
        $scope.roles = roles;
        
        if($scope.isNew){
            
            roleService.getDefaultRoleId().then(function(roleId){
                $scope.user.roleId = roleId;
            });
            
            $scope.user.active = true;
        }
        
    });
    
    collectionService.getConfig('user/config.json', 'user').then(function(response){
       $scope.config = response; 
    });

}]);