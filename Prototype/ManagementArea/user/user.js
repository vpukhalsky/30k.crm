'use strict';

angular.module('30k.ma.user', [
    
    'mm.foundation',
    'checklist-model',
    
    '30k.ma',
    
    '30k.error',
    '30k.user.service',
    '30k.role.service',
    '30k.routes'
    
])

.config(['kkRoutesConfigProvider', function(kkRoutesConfigProvider){
  
  kkRoutesConfigProvider.init({
    
    newUser: {
      path: '/users/new',
      controller: 'UserEditController',
      templateUrl: 'user/edit/edit.html'
    },
    
    editUser: {
      path: '/users/edit/:id',
      controller: 'UserEditController',
      templateUrl: 'user/edit/edit.html'
    },
    
    users: {
      path: '/users',
      controller: 'UserCollectionController',
      templateUrl: 'user/collection/collection.html'
    },
    
    main: {
      path: '/users',
      controller: 'UserCollectionController',
      templateUrl: 'user/collection/collection.html'
    }
    
  });
  
}])

.controller('UserDeleteUserModalController', ['$scope', '$modalInstance', 'user', function($scope, $modalInstance, user){
    
    $scope.user = user;
    
    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };
    
    $scope.confirm = function(){
        $modalInstance.close({
            remove: true,
            user: user
        });
    };
    
}]);