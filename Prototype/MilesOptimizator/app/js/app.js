'use strict';


// Declare app level module which depends on filters, and services
angular.module('30k_mopt', [
  'ngRoute',
  'mm.foundation',
  'datePicker',
  '30k_mopt.filters',
  '30k_mopt.services',
  '30k_mopt.directives',
  '30k_mopt.controllers'
])

.config(['$routeProvider', 'routes', function($routeProvider, routes) {
  
  for(var r in routes){
    $routeProvider.when(routes[r].path, { templateUrl: routes[r].templateUrl, controller: routes[r].controller });
  }
  
  $routeProvider.otherwise({redirectTo: routes.its.path});
  
}]);
