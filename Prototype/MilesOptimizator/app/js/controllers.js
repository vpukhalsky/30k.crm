'use strict';

/* Controllers */

angular.module('30k_mopt.controllers', [])

.controller('ItEditController', ['$scope', 'itService', '$routeParams', 'routes', '$location', '$modal', 'session', '$filter', function($scope, itService, $routeParams, routes, $location, $modal, session, $filter) {
    
    $scope.it = null;
    
    $scope.config = null;
    $scope.create = true;
    
    $scope.selectedTrip = null;
    $scope.showMileType = 'rdm';
    
    $scope.sortBy = false;
    $scope.sortDir = true;
    
    $scope.getApplicablePrograms = function(value, index){
        
        var earnings = [];
        
        for (var i = 0; i < $scope.it.trips.length; i++) {

            var tripEarnings = $scope.it.trips[i].earnings;
            
            if (tripEarnings) {
                for (var y = 0; y < tripEarnings.length; y++) {
                    earnings.push(tripEarnings[y]);
                }
            }
        }
        
        var programs = $filter('filter')(earnings, { 'programId' : value.id });
        
        return programs.length >= 1;
        
    };
    
    $scope.getTripTooltip = function(trip){
        
        var text = '';
        
        for(var i = 0 ; i < trip.route.length ; i++){
            
            if(i > 0) text += ' › ';
            text += trip.route[i].code;
        }
        
        return text;
    };
    
    $scope.editTrip = function(trip){
        
        session.editedTrip = trip;
        
        var url = routes.tripEdit.path
            .replace(':it', angular.isNumber($scope.it.id) ? $scope.it.id : 'new')
            .replace(':trip', angular.isNumber(trip.id) ? trip.id : 'new');
        
        $location.path(url);
        
    };
    
    $scope.removeTrip = function(trip){
        
        itService.removeTrip($scope.it, trip).then(function(){
            $scope.selectedTrip = null;
        });

    };
    
    $scope.addTrip = function(){
        $location.path(routes.tripNew.path.replace(':it', angular.isNumber($scope.it.id) ? $scope.it.id : 'new'));
    };
    
    $scope.selectTrip = function(trip){
        if(angular.isObject($scope.selectedTrip) && $scope.selectedTrip.id == trip.id) $scope.selectedTrip = null;
        else $scope.selectedTrip = trip;
    };
    
    function updateOrder(){
        
        if(!$scope.config || !$scope.it) return;
        
        var rows = $scope.config.programs;
        
        for(var i = 0 ; i < rows.length ; i++){
            
            var row = rows[i];
            var order = 0;
            
            if(!angular.isObject($scope.sortBy)){
                order = $scope.getTotalProgramEarnings(row, $scope.showMileType);    
            }else{
                order = $scope.getEarnings(row, $scope.sortBy, $scope.showMileType);
            }
            
            row.order = order;
        }
    }
    
    $scope.setMileType = function(type){
        
        $scope.showMileType = type;
        updateOrder();
        
    };
    
    $scope.setOrder = function(column, dir){
        
        $scope.sortBy = column;
        $scope.sortDir = dir;
        
        updateOrder();
        
    };
    
    $scope.getTotalTripEarnings = function(trip, mileType){
        
        var e = trip.earnings;
        var total = 0;
        
        for(var i = 0 ; i < e.length ; i++){
            total += e[i][mileType];
        }
        
        return total;
    };
    
    $scope.getTotalProgramEarnings = function(program, mileType){
        
        var trips = $scope.it.trips;
        var total = 0;
        
        for(var i = 0 ; i < trips.length ; i++){
            
            total += $scope.getEarnings(program, trips[i], mileType);

        }
        
        return total;
    };
    
    $scope.getEarnings = function (program, trip, mileType) {
        
        var e = trip.earnings;
        
        for(var i = 0 ; i < e.length ; i++){
            if (e[i].programId == program.id) {
                return e[i][mileType];
            }
        }
        
        return 0;
    };
    
    $scope.isLoaded = function(){
        return angular.isObject($scope.config) && angular.isObject($scope.it);
    };
    
    $scope.save = function(){
        itService.save($scope.it).then(function(it){
            $scope.it = it;
            //$location.path(routes.its.path);
	    $location.path(routes.itEdit.path.replace(':id', it.id));
        });
    };
    
    $scope.remove = function(){
        
        var modalInstance = $modal.open({
            templateUrl: 'partials/it-delete.html',
            controller: 'ItDeleteController',
            windowClass: 'small',
            resolve: {
                it: function () {
                    return $scope.it;
                }
            }
        });
        
        modalInstance.result.then(function(it){
            
            itService.remove($scope.it).then(function(it){
                $location.path(routes.its.path);
            });
            
        });
        
    };
    
    function init(){
        
        var id = $routeParams.id;
        
        if(id !== undefined && id.match(/^\d+$/)){
          
            $scope.create = false;
            
            if(!angular.isObject(session.editedIt) || session.editedIt.id != id){
                
                itService.getOne(parseInt(id, 10)).then(function(it){
                    session.editedIt = angular.copy(it);
                    $scope.it = session.editedIt;
                    updateOrder();
                });   
                
            }else{
                $scope.it = session.editedIt;
                updateOrder();
            }
            
        }
        
        itService.getConfig().then(function(config){
            $scope.config = angular.copy(config);
            if($scope.create) $scope.it = angular.copy($scope.config.defaultItinerary);
            updateOrder();
        });
        
    }
    
    init();
    
    
}])

.controller('ItDeleteController', ['$scope', '$modalInstance', 'it', function($scope, $modalInstance, it){
   
   $scope.it = it;
   
   $scope.confirm = function(){
       $modalInstance.close($scope.it);
   };
   
   $scope.cancel = function(){
       $modalInstance.dismiss('cancel');
   };
   
}])

.controller('ItsController', ['$scope', 'itService', 'session', '$location', 'routes', function($scope, itService, session, $location, routes) {
    
    $scope.its = [];
    $scope.currentIt = null;
    $scope.selectedIt = null;
    
    $scope.onItMouseEnter = function(it){
        $scope.currentIt = it;
    };
    
    $scope.onItMouseLeave = function(it){
        $scope.currentIt = $scope.selectedIt;
    };
    
    $scope.editIt = function(it){
        $location.path(routes.itEdit.path.replace(':id', it.id));
    };
    
    $scope.onItClick = function(it){
        if(angular.isObject($scope.selectedIt) && it.id == $scope.selectedIt.id){
            if(it.id == $scope.currentIt.id) $scope.currentIt = null;
            $scope.selectedIt = null;
        }else{
            $scope.selectedIt = it;
            $scope.currentIt = $scope.selectedIt;
        }
    };
    
    function load(){
        
        session.editedIt = null;
        session.editedTrip = null;
    
        itService.getCollection().then(function(its){
          
          $scope.its = its;
          
        }, function(){
          //TODO
        });
    }
    
    load();
}])

.controller('TripEditController', ['$scope', '$routeParams', 'session', '$location', 'routes', 'itService', '$filter', function($scope, $routeParams, session, $location, routes, itService, $filter){
    
    $scope.config = null;
    $scope.it = null;
    $scope.trip = {};
    $scope.create = true;
    
    $scope.purchaseDate = null;
    $scope.departureDate = null;
    
    $scope.sortBy = 'eqm';
    $scope.sortDir = true;
    
    function updateOrder(){
        
        if(!$scope.config || !$scope.it || !$scope.trip) return;
        
        var rows = $scope.config.programs;
        
        for(var i = 0 ; i < rows.length ; i++){
            
            var row = rows[i];
            var order = 0;
            
            order = $scope.getEarnings(row, $scope.sortBy);
            
            row.order = order;
        }
    }
    
    $scope.setOrder = function(column, dir){
        
        $scope.sortBy = column;
        $scope.sortDir = dir;
        
        updateOrder();
    };
    
    $scope.onMkCarrierChange = function(segment){
        if(!segment.opCarrier || segment.opCarrier.length < 2) segment.opCarrier = segment.mkCarrier;
    };
    
    $scope.getAirports = function(match){
        
        return itService.getAirports(match);
        
    };
    
    $scope.getApplicablePrograms = function(value, index){
        
        var earnings = $scope.trip.earnings;
        var programs = $filter('filter')(earnings, { 'programId' : value.id });
        
        return programs.length >= 1;
        
    };
    
    $scope.getAirportTabindex = function($index){ 
        return 10 + ($index == 0 ? 0 : ( ($index - 1) * 5 + 1) );
    },

    $scope.getEarnings = function (program, mileType) {
        
        for(var i = 0 ; i < $scope.trip.earnings.length ; i++){
            if ($scope.trip.earnings[i].programId == program.id) return $scope.trip.earnings[i][mileType];
        }
        
        return '';
    };
    
    $scope.cancel = function(){
        
        redirectToItEdit();
    };
    
    $scope.save = function(){
        
        itService.saveTrip($scope.it, $scope.trip).then(function(response){
            redirectToItEdit();
        });
        
    };
    
    $scope.remove = function(){
        
        itService.removeTrip($scope.it, $scope.trip).then(function(){
            redirectToItEdit();
        });
        
    };
    
    function redirectToItEdit(){
        
        var path = routes.itNew.path;
        if(angular.isNumber($scope.it.id)){
            path = routes.itEdit.path;
        }
        
        $location.path(path.replace(':id', $scope.it.id));
        
    }
    
    $scope.initPurchaseDatepicker = function(scope){
        scope.$on('setDate', function(e, date, view){
           setDate('purchaseDate', date);
        });
    };
    
    $scope.initDepartureDatepicker = function(scope){
        scope.$on('setDate', function(e, date, view){
           setDate('departureDate', date);
        });
    };
    
    function setDate(property, date){
        
        if(angular.isObject(date)){
            
            var text = (date.getDate() < 10 ? '0' : '') + date.getDate() + '-' 
                + (date.getMonth() < 9 ? '0' : '') +  (date.getMonth() + 1) + '-' + date.getFullYear();
            
            $scope.trip[property] = text;
            
        }
        
    }
    
    $scope.removeSegment = function($index){
        $scope.trip.segments.splice($index, 1);
        $scope.trip.route.splice($index + 1, 1);
    };
    
    $scope.addSegment = function(){
        
        var segment = angular.copy($scope.config.defaultTrip.segments[0]);
        var airport = { code: '' };
        
        $scope.trip.segments.push(segment);
        $scope.trip.route.push(airport);
    };
    
    $scope.getAirlinePlaceholder = function($index){
        return $index <= 0 ? 'From' : 'To';
    };
    
    function addTripToScope(tripId){
        
        for(var i = 0 ; i < $scope.it.trips.length ; i++){
            if($scope.it.trips[i].id == tripId){
                $scope.trip = angular.copy($scope.it.trips[i]);
                $scope.purchaseDate = strToDate($scope.trip.purchaseDate);
                $scope.departureDate = strToDate($scope.trip.departureDate);
                break;
            }
        }
        
    }
    
    function strToDate(str){
        
        if(!str) return null;
        var parts = str.split('-');
        if(parts.length != 3) return null;
        var date = new Date(str);
        
        return date;
    }
    
    function init(){
        
        var itId = $routeParams.it;
        var tripId = $routeParams.trip;
        
        if((!itId || !itId.match(/^\d+$/)) && itId != 'new'){
            $location.path(routes.its.path);
            return;
        }else if(itId == 'new'){
            itId.id = session.editedIt.id;
        }
        
        itId = parseInt(itId, 10);
        
        if(tripId !== undefined && tripId.match(/^\d+$/)){
            tripId = parseInt(tripId, 10);
            $scope.create = false;
        }else if(tripId == 'new'){
            tripId = session.editedTrip.id;
            $scope.create = false;
        }
        
        if(angular.isObject(session.editedIt) && session.editedIt.id == itId){

            $scope.it = session.editedIt;
            addTripToScope(tripId);
            
        }else{
            
            itService.getOne(itId).then(function(it){
                $scope.it = angular.copy(it);
                session.editedIt = $scope.it;
                addTripToScope(tripId);
            });
            
        }
        
        itService.getConfig().then(function(config){
            $scope.config = angular.copy(config);
            if($scope.create) $scope.trip = angular.copy($scope.config.defaultTrip);
        });
        
    }
    
    init();
   
}]);