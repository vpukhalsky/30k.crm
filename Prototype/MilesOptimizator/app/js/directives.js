'use strict';

/* Directives */


angular.module('30k_mopt.directives', [])

.directive('kkVersion', ['version', function(version) {
  
  return function(scope, elm, attrs) {
    elm.text(version);
  };
  
}])

.directive('kkNav', ['routes', '$location', '$rootScope', function(routes, $location, $rootScope) {
  
  return {
    
    restrict: 'A',
    
    scope: {
      route: '@'
    },
    
    link: function($scope, $element){
      
      var link = $element;
      var path = '';
      
      var tagName = link[0].tagName.toLowerCase();
      
      if(tagName != 'a' && tagName != 'button'){
        var el = link.find('a');
        if(el.length <= 0) el = link.find('button');
        link = el.eq(0);
      }
      
      link.on('click', onLinkClick);
      
      function onLinkClick(event){
        
        event.preventDefault();
        event.stopPropagation();
        
        var p = getPath();
        
        $scope.$apply(function(){
          $location.path(p);  
        });
        
      }
      
      function getPath(){
        
        path = routes[$scope.route].path;
        var params = path.match(/\/\:([^\/]+)/gi);
          
        if(params){
            
          for(var i = 0 ; i < params.length; i++){
              
            var paramName = params[i].substring(2);
            var paramValue = $element.attr('param-' + paramName);
              
            if(paramValue){
              path = path.replace(params[i], '/' + paramValue);
            }else{
              path = path.replace(params[i], '');
            }
          }
            
        }
        
        return path;
      }
      
      function updateIsActive(){
        var current = $location.path();
        if(current.indexOf(getPath()) === 0) $element.addClass('active');
        else $element.removeClass('active');
      }
      
      $rootScope.$on('$routeChangeSuccess', function(){
        updateIsActive();
      });
            
      $rootScope.$on('$routeChangeError', function(){
        updateIsActive();
      });
      
      $scope.$on('$destroy', function(){
        link.off('click', onLinkClick);
      });
      
      updateIsActive();
      
    }
  }
  
}])

.directive('kkLoader', ['$rootScope', function($rootScope){
    
    return{
        
        restrict: 'A',
        
        controller: function($scope, $element){
            
            $rootScope.loading = 0;
            
            $rootScope.$watch('loading', function(newValue, oldValue){
              resolve();
            });
            
            function resolve(){
              if($rootScope.loading > 0) $element.removeClass('ng-hide');
              else $element.addClass('ng-hide');
            }
            
            $rootScope.$on('$routeChangeStart', function(){
              $rootScope.loading++;
            });
            
            $rootScope.$on('$routeChangeSuccess', function(){
              $rootScope.loading--;
            });
            
            $rootScope.$on('$routeChangeError', function(){
              $rootScope.loading--;
            });
            
            resolve();
        }
    };
    
}])

.directive('kkAlerts', ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll){
    
    return{
        restrict: 'A',
        
        template: '<alert ng-repeat="alert in alerts" type="alert.type" close="closeAlert($index)">{{alert.msg}}</alert>',
        
        controller: function($scope, $element){
            
            $scope.alerts = [];
            
            $scope.$watchCollection('alerts', function(newValue, oldValue){
              
              if(newValue.length > oldValue.length){
                $location.hash('window-top');
                $anchorScroll();
              }
              
            });
            
            function addAlert(alert){
                $scope.alerts.push(alert);
            }
            
            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };
            
            function onAlert(event, args){
                if(angular.isObject(args.alert)){
                    addAlert(args.alert);
                }
            }
            
            $rootScope.$on('kk-alert', onAlert);
        }
    };
    
}])
