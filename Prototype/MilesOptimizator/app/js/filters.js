'use strict';

/* Filters */

angular.module('30k_mopt.filters', [])

.filter('domain', function() {
  
  return function(text) {
      return text.replace(/^https?\:\/\/w{0,3}\.?/, '');
  };
  
});
