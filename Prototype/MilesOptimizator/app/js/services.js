'use strict';

/* Services */

angular.module('30k_mopt.services', [])

.value('version', '1.0')

.value('config', { its: null })

.value('session', {})

.constant('routes', {
    its: {
        path: '/itineraries',
        templateUrl: 'partials/its.html', 
        controller: 'ItsController'
    },
    itNew: {
        path: '/itinerary/add',
        templateUrl: 'partials/it-edit.html',
        controller: 'ItEditController'
    },
    itEdit: {
        path: '/itinerary/:id/edit',
        templateUrl: 'partials/it-edit.html',
        controller: 'ItEditController'
    },
    tripNew: {
        path: '/itinerary/:it/trip/new',
        templateUrl: 'partials/trip-edit.html',
        controller: 'TripEditController'
    },
    tripEdit: {
        path: '/itinerary/:it/trip/:trip/edit',
        templateUrl: 'partials/trip-edit.html',
        controller: 'TripEditController'
    }
})

.service('itService', ['kkService', '$q', 'config', 'session', function(kkService, $q, config, session){
    
    var its = [];
    var loaded = false;
    
    function getAirports(input){
        
        return kkService.sendPost('trips/airports.json', {
            input: input
        }, false).then(function(response){
            return response.airports;
        });
        
    }
    
    function getCollection(){
        
        if(loaded){
            return _returnPromise(its);
        }else{
            
            return kkService.sendGet('its/collection.json').then(function(response){
                
                loaded = true;
                its = response.itineraries;
                return its;
                
            }, function(response){
                //TODO
            });      
            
        }
        
    }
    
    function merge(item){
        var it = getById(item.id);
        if(it){
            angular.extend(it, item);
            return it;
        }else{
            its.push(item);
            return item;
        }
    }
    
    function getById(id){
        
        for(var i = 0 ; i < its.length ; i++){
            if(its[i].id == id){
                return its[i];
            }
        }
        
        return null;
    }
    
    function getOne(id){
        
        var it = getById(id);
        
        if(angular.isObject(it) && it.loaded){
            return _returnPromise(it);
        }else{
            return kkService.sendGet('its/single.json').then(function(response){
                
                var it = response.itinerary;
                it.loaded = true;
                merge(it);
                return it;
                
            }, function(response){
                //TODO
            });  
        }
    }
    
    function _returnPromise(data){
        var deferred = $q.defer();
        deferred.resolve(data);
        return deferred.promise;
    }
    
    function getConfig(){
        
        var result = null;
        
        if(angular.isObject(config.its)){
            
            result = _returnPromise(config.its);
            
        }else{
            
            result = kkService.sendGet('its/config.json?l').then(function(response){
                config.its = response;
                return config.its;
            }, function(response){
              //TODO
            });
        }
        
        return result;
    }
    
    function save(it){
        
        return kkService.sendPost('its/save.json', {
            itinerary: {
                id: it.id,
                name: it.name
            }
        }).then(function(response){
            
            it = angular.extend(it, response.itinerary);
            merge(it);
            return it;
            
        }, function(response){
            //TODO
        });
        
    }
    
    function remove(it){
        
        return kkService.sendPost('its/delete.json', {
            itineraryId: it.id
        }).then(function(response){
            
            var it = getById(response.itinerary.id);
            if(it){
                var index = its.indexOf(it);
                its.splice(index, 1);
            }
            
        }, function(response){
            //TODO
        });
        
    }
    
    function removeTrip(it, trip){
        
        return kkService.sendPost('trips/delete.json', {
            
            tripId: trip.id,
            itineraryId: it.id
            
        }).then(function(response){
            
            var it = getById(response.itinerary.id);
            var tripId = response.trip.id;
            _removeTrip(it, tripId);
            
            if(session.editedIt){
                _removeTrip(session.editedIt, tripId);
            }
            
            return it;
        });
        
    }
    
    function _removeTrip(it, tripId){
        for(var i = 0 ; i < it.trips.length ; i++){
            if(it.trips[i].id == tripId){
                it.trips.splice(i, 1);
                break;
            }
        }
    }
    
    function _saveTrip(it, trip){
        var create = true;
            
        for(var i = 0 ; i < it.trips.length ; i++){
            
            var t = it.trips[i];
            if(t.id == trip.id){
                angular.extend(t, trip);
                create = false;
                break;
            }
        }
            
        if(create) it.trips.push(trip);
    }
    
    function saveTrip(it, trip){
        return kkService.sendPost('trips/save.json', {
            
            trip: trip,
            itineraryId: it.id
            
        }).then(function(response){
            
            var it = getById(response.itinerary.id);
            var trip = response.trip;
            
            _saveTrip(it, trip);
            
            if(session.editedIt){
                _saveTrip(session.editedIt, trip);
            }
            
            return it;
        });
    }
    
    return {
        getCollection: getCollection,
        getOne: getOne,
        getConfig: getConfig,
        save: save,
        remove: remove,
        removeTrip: removeTrip,
        saveTrip: saveTrip,
        getAirports: getAirports
    };
    
}])

.service('kkService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){
    
    var jsonPath = 'json/';
    
    function _handleError(response){
        
        $rootScope.loading--;
        var message = 'An unknown server error occurred.';
 
        if (!response.data.message){
            
            _showAlert('An unknown server error occured. Please try again or refresh page.');
            
        }else{
            
            _showAlert(response.data.message);
            message = response.data.message;
        }
        
        return $q.reject(message);

    }
         
    function _handleSuccess(response){
        
        $rootScope.loading--;
        
        if(!angular.isObject(response.data) || !response.data.success){
            
            if(response.data.message){
                _showAlert(response.data.message);
            }
            
        }else{
            if(response.data.message){
                _showAlert(response.data.message, 'success');
            }
        }
        
        return response.data.result;
        
    }
    
    function _showAlert(msg, type){
        
        if(type === undefined) type = 'alert';

        $rootScope.$emit('kk-alert', { alert: { type: type, msg: msg }});
    }
    
    function sendPost(urlSlug, data){
        
        $rootScope.loading++;
        
        var request = $http({
            method: 'post',
            url: jsonPath + urlSlug,
            data: data
        });
    
        return request.then(_handleSuccess, _handleError);
        
    }
    
    function _prepareGetUrl(url, params){
        
        if(params && angular.isObject(params)){
            for(var i in params){
                url = url.replace(':' + i, params[i]);
            }
        }
        
        return url;
    }
    
    function sendGet(urlSlug, params){
        
        $rootScope.loading++;
        
        var url = _prepareGetUrl(urlSlug, params);
        
        var request = $http({
            method: 'get',
            url: jsonPath + url
        });
        
        return request.then(_handleSuccess, _handleError);
    }
    
    return {
        sendPost: sendPost,
        sendGet: sendGet
    };
    
}]);
