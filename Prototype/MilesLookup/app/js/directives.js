'use strict';

/* Directives */


angular.module('30k_milo.directives', [])

.directive('kkTooltip', ['$compile', '$timeout', function($compile, $timeout){
  
  return {
    restrict: 'A',
    scope: {
      kkTooltip: '@'
    },
    controller: function($scope, $element){
      
      var idPrefix = 'kk-tooltip-';
      var counter = getCounter();
      var tip = null;
      var template = '<div ng-attr-id="{{id}}" class="kk-tooltip" ng-class="{ down: down }" ng-style="style"><span class="tip" ng-style="tipStyle"></span><span ng-bind-html="kkTooltip"></span></div>';
      var tipOffset = {
        top: -14,
        left: 5
      };
      
      $scope.id = null;
      
      $scope.style = {
        top: 0,
        left: 0,
        visibility: 'hidden'
      };
      
      $scope.tipStyle = {
        top: tipOffset.top + 'px',
        left: tipOffset.left + 'px'
      };
      
      $scope.down = false;
      
      function getCounter(){
        var body = $('body');
        return body.data('kk-tooltip-counter');
      }
      
      function setCounter(val){
        var body = $('body');
        return body.data('kk-tooltip-counter', val);
      }
      
      function createTooltip(){
        var el = angular.element(template);
        tip = $compile(el)($scope, function(tip, $scope){
          $('body').append(tip);
        });
      }
      
      function show(){
        
        $timeout(function(){
          
          var wnd = $(window);
          var maxHeight = wnd.height() + wnd.scrollTop();
          var maxWidth = wnd.width();
          
          var width = tip.outerWidth();
          var height = tip.outerHeight();
          var offset = $element.offset();
          var tipHeight = Math.abs(tipOffset.top / 2);
          var elHeight = $element.outerHeight();

          var left = 0;
          var top = 0;
          var tipLeft = 0;
          var tipTop = 0;
          
          if(offset.left + width > maxWidth){
            
            left = maxWidth - width;
            tipLeft = offset.left - left + tipOffset.left;
            
          }else{
            
            left = offset.left;
            tipLeft = tipOffset.left;
            
          }
          
          if(offset.top + elHeight + height +  tipHeight > maxHeight){
            
            top = offset.top - height - tipHeight;
            tipTop = height;
            $scope.down = true;
            
          }else{
            
            top = offset.top + elHeight + tipHeight;
            tipTop = tipOffset.top;
            $scope.down = false;
            
          }
          
          $scope.style.left = left + 'px';
          $scope.style.top = top + 'px';
          $scope.style.visibility = 'visible';
          $scope.tipStyle.left = tipLeft + 'px';
          $scope.tipStyle.top = tipTop + 'px';
          
        });
        
        $('body').on('mousemove', onMouseMove);
        
      }
      
      function hide(){
        
        $timeout(function(){
          $scope.style.visibility = 'hidden';
        });
        
        $('body').off('mousemove', onMouseMove);
      }
      
      function onMouseEnter(e){
        
        if($scope.kkTooltip){
          if(!tip) createTooltip();
          show();
        }
      }
      
      function onMouseLeave(e){
        
        hide();
      }
      
      function onMouseMove(e){
        
        var x = e.pageX;
        var y = e.pageY;
        var offset = $element.offset();
        var width = $element.outerWidth();
        var height = $element.outerHeight();
        
        if((x < offset.left || x > offset.left + width) && (y < offset.top || y > offset.top + height)){
        
          hide();
        }
      }
      
      if(counter === null || counter === undefined){
        counter = 0;
      }else{
        counter++;
      }
      
      setCounter(counter);
      
      $scope.id = idPrefix + counter;
      
      if(angular.isArray($scope.kkTooltip)){
        $scope.kkTooltip = $scope.kkTooltip.join('<br />');
      }
      
      $element.on('mouseenter', onMouseEnter);
      $element.on('mouseleave', onMouseLeave);
      
      $scope.$on('$destroy', function(){
        $element.off('mouseenter', onMouseEnter);
        $element.off('mouseleave', onMouseLeave);
      });
      
    }
    
  };
  
}])

.directive('kkVersion', ['version', function(version) {
  
  return function(scope, elm, attrs) {
    elm.text(version);
  };
  
}])

.directive('kkMultiselect', [function(){
  
  return {
    
    restrict: 'A',
    
    templateUrl: 'partials/multiselect.html',
    
    scope: {
      
      list: '=',
      selected: '=',
      cols: '@',
      id: '@',
      label: '@?'
      
    },
    
    controller: function($scope, $element){
      
      $scope.selectAll = function(){
        
        $scope.deselect();
        
        for(var i = 0 ; i < $scope.list.length ; i++){
          $scope.selected.push($scope.list[i]);
        }
        
      };
      
      $scope.deselect = function(){
        while($scope.selected.length > 0) $scope.selected.shift();
      };
      
      $scope.$watch('list', function(){
        $scope.selectAll();  
      });
      
      
    }
    
  };
  
}])

.directive('kkNav', ['routes', '$location', '$rootScope', function(routes, $location, $rootScope) {
  
  return {
    
    restrict: 'A',
    
    scope: {
      route: '@'
    },
    
    link: function($scope, $element){
      
      var link = $element;
      var path = '';
      
      var tagName = link[0].tagName.toLowerCase();
      
      if(tagName != 'a' && tagName != 'button'){
        var el = link.find('a');
        if(el.length <= 0) el = link.find('button');
        link = el.eq(0);
      }
      
      link.on('click', onLinkClick);
      
      function onLinkClick(event){
        
        event.preventDefault();
        event.stopPropagation();
        
        var p = getPath();
        
        $scope.$apply(function(){
          $location.path(p);  
        });
        
      }
      
      function getPath(){
        
        path = routes[$scope.route].path;
        var params = path.match(/\/\:([^\/]+)/gi);
          
        if(params){
            
          for(var i = 0 ; i < params.length; i++){
              
            var paramName = params[i].substring(2);
            var paramValue = $element.attr('param-' + paramName);
              
            if(paramValue){
              path = path.replace(params[i], '/' + paramValue);
            }else{
              path = path.replace(params[i], '');
            }
          }
            
        }
        
        return path;
      }
      
      function updateIsActive(){
        var current = $location.path();
        if(current.indexOf(getPath()) === 0) $element.addClass('active');
        else $element.removeClass('active');
      }
      
      $rootScope.$on('$routeChangeSuccess', function(){
        updateIsActive();
      });
            
      $rootScope.$on('$routeChangeError', function(){
        updateIsActive();
      });
      
      $scope.$on('$destroy', function(){
        link.off('click', onLinkClick);
      });
      
      updateIsActive();
      
    }
  };
  
}])

.directive('kkLoader', ['$rootScope', function($rootScope){
    
    return{
        
        restrict: 'A',
        
        controller: function($scope, $element){
            
            $rootScope.loading = 0;
            
            $rootScope.$watch('loading', function(newValue, oldValue){
              resolve();
            });
            
            function resolve(){
              if($rootScope.loading > 0) $element.removeClass('ng-hide');
              else $element.addClass('ng-hide');
            }
            
            $rootScope.$on('$routeChangeStart', function(){
              $rootScope.loading++;
            });
            
            $rootScope.$on('$routeChangeSuccess', function(){
              $rootScope.loading--;
            });
            
            $rootScope.$on('$routeChangeError', function(){
              $rootScope.loading--;
            });
            
            resolve();
        }
    };
    
}])

.directive('kkAlerts', ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll){
    
    return{
        restrict: 'A',
        
        template: '<alert ng-repeat="alert in alerts" type="alert.type" close="closeAlert($index)">{{alert.msg}}</alert>',
        
        controller: function($scope, $element){
            
            $scope.alerts = [];
            
            $scope.$watchCollection('alerts', function(newValue, oldValue){
              
              if(newValue.length > oldValue.length){
                $location.hash('window-top');
                $anchorScroll();
              }
              
            });
            
            function addAlert(alert){
                $scope.alerts.push(alert);
            }
            
            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };
            
            function onAlert(event, args){
                if(angular.isObject(args.alert)){
                    addAlert(args.alert);
                }
            }
            
            $rootScope.$on('kk-alert', onAlert);
        }
    };
    
}]);
