'use strict';

/* Controllers */

angular.module('30k_milo.controllers', [])

.controller('MainController', ['$scope', 'miloService', '$filter', '$anchorScroll', '$location', '$route', '$rootScope', function($scope, miloService, $filter, $anchorScroll, $location, $route, $rootScope) {
    
    $scope.config = null;
    $scope.airlineConfig = null;
    
    $scope.expanded = false;
    $scope.searchType = 'airline';
    $scope.layout = 'fareCode';
    $scope.ffps = [];
    $scope.fareCodes = [];
    $scope.opAirlines = [];
    
    $scope.pdate = '';
    $scope.ddate = '';
    $scope.airline = '';
    
    $scope.opAirline = '';
    $scope.flight = '';
    $scope.arrAirport = '';
    $scope.depAirport = '';
    $scope.purDate = '';
    $scope.depDate = '';
    $scope.selectedFfps = [];
    $scope.selectedFareCodes = [];
    
    $scope.mileType = 'rdm';
    
    $scope.result = null;
    $scope.resultLayout = 'fareCode';
    
    $scope.hoverRow = null;
    $scope.hoverColumn = null;
    
    var lastRoute = $route.current;
    
    //prevents route reload on location hash change (eg. #result)
    $scope.$on('$locationChangeSuccess', function(event) {
        
        if($route.current.$$route.controller === 'MainController'){ 

            $route.current = lastRoute;
            $rootScope.loading--;
            
        }
        
    });
    
    $scope.onRowMouseEnter = function($event, $index){
        $scope.hoverRow = $index;
    };
    
    $scope.onRowMouseLeave = function($event, $index){
        $scope.hoverRow = null;
    };
    
    
    $scope.onColMouseEnter = function($event, $index){
        $scope.hoverColumn = $index;
    };
    
    $scope.onColMouseLeave = function($event, $index){
        $scope.hoverColumn = null;
    };
    
    $scope.setMileType = function(type){
        
        $scope.mileType = type;  
        
    };
    
    $scope.onFlightBlur = function($event){
        
        if(angular.isString($scope.flight) && $scope.flight.length > 1){
           
           var code = $scope.flight.substring(0, 2).toUpperCase();
           var airlines = $filter('filter')($scope.config.airlines, { code: code });
           
           if(airlines.length > 0){
               
               $scope.airline = airlines[0];
               
           }
       }
       
    };
    
    $scope.getAirports = function(match){
        
        return miloService.getAirports(match);
        
    };
    
    $scope.initPurchaseDate = function(scope){
        scope.$on('setDate', function(e, date, view){
            $scope.purDate = date;
        });
    };
    
    $scope.initDepartureDate = function(scope){
        scope.$on('setDate', function(e, date, view){
            $scope.depDate = date;
        });
    };
    
    $scope.getMultiselectLabel = function(selected, list, property){
        
        if(selected.length < 1){
            return 'None';
        }else if(selected.length == 1){
            
            var item = selected[0];
            var name = item;
            if(angular.isString(property)) name = item[property];
            return name;
            
        }else if(selected.length == list.length){
            return 'All applicable';
        }else{
            return selected.length + ' selected';
        }
    };
    
    $scope.getMultiselectClass = function(selected, list){
        var cls = { alert: false, secondary: false };
        if(selected.length <= 0) cls.alert = true;
        else if(selected.length == list.length) cls.secondary = true;
        return cls;
    };
    
    $scope.setLayout = function(type){
        $scope.layout = type;
    };
    
    function dateToStr(date){
        return (date.getDate() < 10 ? '0' : '') + date.getDate() + '-' 
                + (date.getMonth() < 9 ? '0' : '') +  (date.getMonth() + 1) + '-' + date.getFullYear();
    }
    
    $scope.search = function(){
        
        var airline = $scope.searchType == 'airline' ? (angular.isObject($scope.airline) ? $scope.airline.code : $scope.airline) : null;
        var flight = $scope.searchType == 'flight' ? $scope.flight : null;
        
        var purDate = angular.isObject($scope.purDate) ? dateToStr($scope.purDate) : null;
        var depDate = angular.isObject($scope.depDate) ? dateToStr($scope.depDate) : null;
        
        var depAirport = angular.isObject($scope.depAirport) ? $scope.depAirport.code : $scope.depAirport;
        var arrAirport = angular.isObject($scope.arrAirport) ? $scope.arrAirport.code : $scope.arrAirport;
        
        var opAirline = angular.isObject($scope.opAirline) ? $scope.opAirline.code : $scope.opAirline;
        
        miloService.search({
            
            airline: airline,
            flightNumber: flight,
            ffps: $scope.selectedFfps,
            display: $scope.layout,
            purchaseDate: purDate,
            departureDate: depDate,
            fareCodes: $scope.selectedFareCodes,
            departureAirport: depAirport,
            arrivalAirport: arrAirport,
            operatingAirline: opAirline
            
        }).then(function(result){

            $scope.resultLayout = result.display;
            $scope.result = result;
            
            $location.hash('result');
            $anchorScroll();  

        });
        
    };
    
    $scope.toggleExpanded = function(){
        $scope.expanded = !$scope.expanded;
    };
    
    function updateAirline(){
        
        $scope.ffps = angular.copy($scope.config.ffps);
        $scope.fareCodes = angular.copy($scope.config.fareCodes);
        $scope.opAirlines = angular.copy($scope.config.airlines);
        
        removeNotApplicable($scope.selectedFfps, $scope.ffps);
        removeNotApplicable($scope.selectedFareCodes, $scope.fareCodes);
    }
    
    $scope.getById = function(id, list, index){
        
        if(index === undefined) index = false;
        
        for(var i = 0 ; i < list.length ; i++){
            if(list[i].id == id){
                if(index) return i;
                else return list[i];
            }
        }
        
        return null;
    };
    
    function removeNotApplicable(selected, options){
        
        var left = [];
        
        for(var i = 0 ; i < selected.length ; i++){
            var item = $scope.getById(selected[i], options);
            if(item != null) left.push(selected[i]);
        }
        
        selected = left;
    }
    
    $scope.preventDefault = function($event){
        
        $event.stopPropagation();
        
    };
    
    function init(){
        
        miloService.getConfig().then(function(config){
            
            $scope.config = angular.copy(config);
            
            updateAirline();
            
        });
        
    }
    
    init();
    
}]);