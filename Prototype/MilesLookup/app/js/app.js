'use strict';


// Declare app level module which depends on filters, and services
angular.module('30k_milo', [
  'ngRoute',
  'ngSanitize',
  'mm.foundation',
  'datePicker',
  'checklist-model',
  '30k_milo.filters',
  '30k_milo.services',
  '30k_milo.directives',
  '30k_milo.controllers'
])

.config(['$routeProvider', 'routes', function($routeProvider, routes) {
  
  for(var r in routes){
    $routeProvider.when(routes[r].path, { templateUrl: routes[r].templateUrl, controller: routes[r].controller });
  }
  
  $routeProvider.otherwise({redirectTo: routes.main.path});
  
}]);
