'use strict';

/* Filters */

angular.module('30k_milo.filters', [])

.filter('domain', function() {
  
  return function(text) {
      return text.replace(/^https?\:\/\/w{0,3}\.?/, '');
  };
  
})

.filter('percent', ['$filter', function($filter){
  
  return function(text){
      var percent = $filter('number')(text, 0);
      if(percent) percent += '%';
      return percent;
  };
  
}])

.filter('join', function(){
  
  return function(text, symbol){
    if(symbol === undefined) symbol = ', ';
    
    if(angular.isArray(text)){
      text = text.join(symbol);
    }
    
    return text;
  };
  
});
