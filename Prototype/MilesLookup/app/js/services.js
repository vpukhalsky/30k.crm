'use strict';

/* Services */

angular.module('30k_milo.services', [])

.value('version', '1.0')

.value('config', { })

.constant('routes', {
    main: {
        path: '/lookup',
        templateUrl: 'partials/main.html', 
        controller: 'MainController'
    }
})

.service('miloService', ['kkService', 'config', function(kkService, config){
    
    function search(params){
        
        var url = 'search-' + params.display + '.json';
        
        return kkService.sendPost(url, params).then(function(response){
            
            return _transformSearchResponse(response);
        });
    }
    
    function _transformSearchResponse(response){
        
        if(response.display == 'percentage') return response;
        
        var ffpPrefix = 'pid-';
        var ffps = [];
        var fareCodes = {};
        
        for(var i = 0 ; i < response.ffps.length ; i++){
            
            var ffp = response.ffps[i];
            
            ffps.push({
                name: ffp.name,
                color: ffp.color,
                id: ffp.id
            });
            
            for(var j = 0 ; j < ffp.fareCodes.length ; j++){

                var fareCode = ffp.fareCodes[j];
                var key = ffpPrefix + ffp.id;

                if(!angular.isObject(fareCodes[fareCode.code])){
                    fareCodes[fareCode.code] = { };
                }
                
                fareCodes[fareCode.code][key] = fareCode;
            }
        }
        
        return {
            ffpPrefix: ffpPrefix,
            display: response.display,
            ffps: ffps,
            fareCodes: fareCodes
        };
    }
    
    function getAirports(input){
        
        return kkService.sendPost('airports.json', {
            input: input
        }, false).then(function(response){
            return response.airports;
        });
        
    }
    
    function getConfig(){
        
        if(angular.isObject(config.milo)){
            
            return kkService.promise(config.milo);
            
        }else{
            
            return kkService.sendGet('config.json').then(function(response){
                 config.milo = response;
                 return config.milo;
            });
            
        }
    }
    
    return{
        search: search,
        getConfig: getConfig,
        getAirports: getAirports
    };
}])

.service('kkService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){
    
    var jsonPath = 'json/';
    
    function _handleError(response){
        
        if(response.config.loader && $rootScope.loading > 0) $rootScope.loading--;
        
        var message = 'An unknown server error occurred.';
 
        if (!response.data.message){
            
            _showAlert('An unknown server error occured. Please try again or refresh page.');
            
        }else{
            
            _showAlert(response.data.message);
            message = response.data.message;
        }
        
        return $q.reject(message);

    }
         
    function _handleSuccess(response){
        
        if(response.config.loader && $rootScope.loading > 0) $rootScope.loading--;
        
        if(!angular.isObject(response.data) || !response.data.success){
            
            if(response.data.message){
                _showAlert(response.data.message);
            }
            
        }else{
            if(response.data.message){
                _showAlert(response.data.message, 'success');
            }
        }
        
        return response.data.result;
        
    }
    
    function _showAlert(msg, type){
        
        if(type === undefined) type = 'alert';

        $rootScope.$emit('kk-alert', { alert: { type: type, msg: msg }});
    }
    
    function sendPost(urlSlug, data, loader){
        
        if(loader === undefined) loader = true;
        
        if(loader) $rootScope.loading++;
        
        var request = $http({
            method: 'post',
            url: jsonPath + urlSlug,
            data: data,
            loader: loader
        });
    
        return request.then(_handleSuccess, _handleError);
        
    }
    
    function _prepareGetUrl(url, params){
        
        if(params && angular.isObject(params)){
            for(var i in params){
                url = url.replace(':' + i, params[i]);
            }
        }
        
        return url;
    }
    
    function sendGet(urlSlug, params, loader){
        
        if(loader === undefined) loader = true;
        
        if(loader) $rootScope.loading++;
        
        var url = _prepareGetUrl(urlSlug, params);
        
        var request = $http({
            method: 'get',
            url: jsonPath + url,
            loader: loader
        });
        
        return request.then(_handleSuccess, _handleError);
    }
    
    function promise(data){
        
        var d = $q.defer();
        d.resolve(data);
        return d.promise;
        
    }
    
    return {
        sendPost: sendPost,
        sendGet: sendGet,
        promise: promise
    };
    
}]);
