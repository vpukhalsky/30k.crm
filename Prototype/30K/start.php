<?php
include './phpsass-master/SassParser.php';

function warn($text, $context) {
	print "/** WARN: $text, on line {$context->node->token->line} of {$context->node->token->filename} **/\n";
}
function debug($text, $context) {
	print "/** DEBUG: $text, on line {$context->node->token->line} of {$context->node->token->filename} **/\n";
}

$input = './css/style.sass';
$output = './css/style.css';

$options = array(
	'style' => 'nested',
    'cache' => false,
    'syntax' => 'sass',
    'debug' => false,
    'callbacks' => array(
    	'warn' => 'cb_warn',
    	'debug' => 'cb_debug',
	)
);

$parser = new SassParser($options);

try {
	$content = $parser->toCss($input);		
	file_put_contents($output, $content);
	header('Content-Type: text/html; charset=UTF-8');
	echo file_get_contents($_GET['goto']);
} catch (Exception $e) {
	print $e->getMessage();	
}