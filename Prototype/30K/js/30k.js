/*
 
Vitaly:

----------------- List of changes ------------------------
27.08.2013
- added twitter sidebar and modal

19.06.2013:
- added addAlertToggleListener() method to collapse/expand alert1 and alert2, added listener at end of addAlert2Listeners() and addAlert1Listeners()
- added method addFlightOptionsSelectListener() to make clickable entire row in tables within flight details panel (listener added to method addFlightListeners())

13.06.2013
- Changed initChangeProgramMiles() method to support optgroup element inside select
- Changed addBookingFormValidators() method. Now successful validation ends with AJAX form submit and modal window asking user to do not panic, sit and wait 
- Changed addBookingFormValidators(), added validator for passport exp. date
- Changed initAdditionalBookingFields(), added hide/show passport exp. date action

10.06.2013
- Added has3d() method and used in updateDeal and updateFFPCard methods
- Changed method addRefineSearchListeners - change of any input on search form does not submit query, but shows only button to submit

09.06.2013:
- in method updateNewResultsListeners added line $(scope).foundation()
- in accountInit() method added execution of initNewProgramLabel(), which is new method
- changed last lines of initNewProgramSections (cards section initialization)
- changed method validateSearchForm in order to support correct validation of form while return date input is hidden (one-way)
- in method showLoaderOverlay() changed executed function from "prepend" to "append"
- added this.addBrowserClass() method while initialize of every page, added addBrowserClass method 

08.06.2013:
- in method frontPageInit added line with this.initFrontPageAnimation();
- added methods: initFrontPageAnimation, updateDeal ,updateFFPCard, initClouds ,updateClouds, initScreens, updateScreens
 
 */

function _30k(searchUrl, searchResultUrl){
	return {	
		/**
		 * Set URL that search request is sent 
		 */
		searchUrl: searchUrl,
		searchResultUrl: searchResultUrl,
		
		/**
		 * Sample data for autocomplete fields
		 */
		sampleAirports: [
		    'Shanghai SHA',
		    'Shanghai PVG',
		    'London LHT',
		    'London LON'
		],
		
		/**
		 * Sample data for autocomplete fields for user profile
		 */
		sampleProgramsAirlines: [
		    'Mileage Plus United',
		    'Flying Blue',
		    'Air China',
		    'Lufthansa',
		    'British Airways',
		    'Finnair'
		],
		
		/**
		 * Regex 
		 */
		
		emailPattern: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/,
		datePattern: /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/,
		phonePattern: /^[0-9][0-9 ]{4,}$/,
		dateMask: '99/99/9999',
		
		/*
		 * Delay between two search requests to server 
		 */
		searchRequestTimeout: 1000,
		ALERT_DURATION: 10000,
		
		/**
		 * Labels, alerts etc. in order to future translations
		 */
		NO_DEPARTURE_AIRPORT: 'Select departure airport.',
		NO_RETURN_AIRPORT: 'Select destination airport.',
		DEPARTURE_LATER_THAN_RETURN: 'We do not support time travel in current version. Return date is set before departure.',
		DEPARTURE_BEFORE_TODAY: 'We do not support time travel in current version. Departure date you have choosen had passed.',
		NO_PASSANGER: 'You have to select at least one adult passenger.',
		WAIT_TO_APPLY_FILTERS: 'Filtering results. Please wait&hellip;',
		LOADING_PROGRAM_CHANGE: 'Loading miles for selected program&hellip;',
		LOADING_PROGRAM_MILES: 'Retriving miles data. Please wait&hellip;',
		LOADING_SAVE_PROGRAM: 'Saving program. Please wait&hellip;',
		LOADING_SWITCH_PROGRAM: 'Switching program. Please wait&hellip;',
		LOADING_EDIT_PROGRAM: 'Loading program to edit. Please wait&hellip;',
		LOADING_DELETE_PROGRAM: 'Deleting program. Please wait&hellip;',
		INVALID_EMAIL_ALERT: 'Is it correct e-mail address?',
		PASSWORDS_DONT_MATCH: 'Passwords don\'t match',
		EMPTY_PASSWORD: 'Field is required',
		EMPTY_FIELD: 'This field is mandatory',
		INVALID_PHONE_NUMBER: 'Number is invalid. Use only digits and spaces.',
		TOO_LONG_ALERT: 'Too many characters. Maximum: ',
		INVALID_NUMBER: 'Use only digits',
		CHECKBOX_ALERT: 'This field is mandatory',
		INVALID_DATE_ALERT: 'Correct date to dd/mm/yyyy format',
		SAVING_DESTINATION: 'Saving dream destination. Please wait&hellip;', 
		PASS_RECOVER_ALERT: 'Check your e-mail. We have sent you new password to your account.',
		DEPARTURE_RETURN_THE_SAME: 'Departure and arrival airport are the same.',
		SEARCH_UPDATED_WITH_PROGRAM: 'Search updated with ',
		SUBSCRIBED_ALERT: 'Success! You will receive e-mail notification when your new program will be activated.',
		
		/**
		 * Where all magic begins... index.html
		 */
		frontPageInit: function(){
			this.addBrowserClass();
			this.initRadioButtons();
			this.initSpinners();
			this.initAirportInputs();
			this.initAirportShortcuts();
			this.initDatepickers();
			this.initSearchFormValidation();
			this.addLogInListeners('#login-section');
			this.addLogInListeners('#signup-section');
			this.addLogInValidation();
			this.addForgetPassListener();
			this.initAutocompleteFields();
			
			this.initFrontPageAnimation();
		},
		
		/**
		 * Where all magic begins... search.html
		 */
		searchPageInit: function(){
			setTimeout($.proxy(this.searchRequest, this), this.searchRequestTimeout); //delete this - only in prototype
			//this.searchRequest(); //uncomment this - production version
			this.addBrowserClass();
			this.newSearchInit();
			this.initRadioButtons();
			this.addLogInListeners('#login-section');
			this.addLogInListeners('#signup-section');
			this.addLogInValidation();
			this.addNewSearchTabButtonsListeners();
			this.addNewSearchListener();
			this.addAlert1Listeners();
			this.addAlert2Listeners();
			this.addForgetPassListener();
			this.initAutocompleteFields();
			
			this.initTwitterSideBlock();
		},
		
		/**
		 * Where all magic begins... account.html
		 */
		accountInit: function(){
			this.addBrowserClass();
			this.addLogInListeners('#login-section');
			this.addLogInListeners('#signup-section');
			this.addLogInValidation();
			this.initProgramInput();
			this.addProgramListeners();
			this.addForgetPassListener();
			this.initNewProgramLabel();
		},
		
		/**
		 * Where all magic begins... account_profile.html
		 */
		accountProfileInit: function(){
			this.addBrowserClass();
			this.addLogInListeners('#login-section');
			this.addLogInListeners('#signup-section');
			this.addLogInValidation();
			this.initAccountProfileForm();
			this.addForgetPassListener();
		},
		
		/**
		 * Where all magic begins... account_pass.html
		 */
		accountPasswordInit: function(){
			this.addBrowserClass();
			this.addLogInListeners('#login-section');
			this.addLogInListeners('#signup-section');
			this.addLogInValidation();
			this.initAccountPasswordForm();
			this.addForgetPassListener();
		},
		
		/**
		 * Where all magic begins... booking.html
		 */
		bookingInit: function(){
			this.addBrowserClass();
			this.addLogInListeners('#login-section');
			this.addLogInListeners('#signup-section');
			this.addLogInValidation();
			this.initRadioButtons();
			this.initAdditionalBookingFields();
			this.initBookingCreditCards();
			this.initExtraLineFields();
			this.addDateFieldMask();
			this.addBookingFormValidators();
			this.initBookingChangeProgram();
			this.initChangeProgramMiles();
			this.initProgressBars();
			this.initBookingBg();
			
			$('#booking-modal').foundation('reveal', 'open'); 
			setTimeout($.proxy(function(){
				$('#booking-fare-modal').foundation('reveal', 'open'); 
			}, this), 3000);
		},
		
		/**
		 * Starting point for booking_success.html 
		 */
		bookingConfirmationInit: function(){	
			this.addBrowserClass();		
			this.addLogInListeners('#login-section');
			this.addLogInListeners('#signup-section');
			this.addLogInValidation();
			this.initProgressBars();
		},
		
		/**
		 * Add program listeners to program block in user account page 
		 */
		addProgramListeners: function(scope){
			if(!scope) scope = $('body');
			
			this.initProgramSwitch(scope);
			this.initProgramEdit(scope);
		},
		
		/**
		 * Add all listeners in new program form loaded in user account page 
		 */
		addNewProgramListeners: function(scope){
			if(!scope) scope = $('body');
			
			this.initRadioButtons(scope);
			this.initNewProgramSections(scope);
			this.initGetMiles(scope);
			this.initNewProgramButtons(scope);
		},
		
		/**
		 * Add listeners after new search tab is shown
		 */
		newSearchInit: function(scope){
			if(!scope) scope = $('body');
			
			this.initSpinners(scope);
			this.initAirportInputs(scope);
			this.initAirportShortcuts(scope);
			this.initDatepickers(scope);
			this.addEditPassengersListener(scope);
			this.addRefineSearchListeners(scope);
			this.addCloseTabListener(scope);
		},
		
		/**
		 * Add all listeners to new or updated search results including filters
		 */
		updateFiltersListeners: function(scope){
			if(!scope) scope = $('body');
			
			this.initRadioButtons(scope);
			this.initSliders(scope);
			this.addMoreResultsListener(scope);
			this.addMoreFiltersListener(scope);
			this.initFilterCheckboxes(scope);
			this.initFilterSubmit(scope);
			
			this.updateAllResultsListeners(scope);
		},
		
		/**
		 * Add all listeners to new set of results (without filters)
		 */
		updateAllResultsListeners: function(scope){
			if(!scope) scope = $('body');
			
			this.addMoreSlotResultsListeners(scope);
			this.updateNewResultsListeners(scope);
		},
		
		/**
		 * Add all listeners to new search results 
		 */
		updateNewResultsListeners: function(scope){
			if(!scope) scope = $('body');
			
			$(scope).foundation();
			this.addResultListener(scope);
			this.initProgressBars(scope);
			this.setResultsRowHeight(scope);
		},
		
		/**
		 * Add all listeners to loaded flight element within comparison column
		 * @param scope - should be .flight element
		 */
		addFlightListeners: function(scope){
			if(!scope) scope = $('body');
			
			$(scope).foundation(); //init custom form elements
			this.hideLongerStopResults(scope);
			this.addSelectReturnListener(scope);
			this.addCloseFlightListener(scope);
			this.initFlightDetails(scope);
			this.initFlightDetailsHovers(scope);
			this.addShowClassesListener(scope);
			this.addClassChangeListener(scope);
			this.addProgramChangeListener(scope);
			this.initProgressBars(scope);
			this.initChangeProgramMiles(scope);
			this.initClassChange(scope);
			this.addFlightOptionsSelectListener(scope);
		},
		
		/* Adds listeners to all links, which should open login/signup modal window*/
		addLogInListeners: function(anchor){	
			//Click on Log In / Sign Up
			$('a[href="' + anchor + '"]').each(function(index, el){
				if($(el).parents('#login-modal').size() > 0) return;
				
				$(el).click(function(e){
					e.preventDefault();
					if($(e.currentTarget).hasClass('disabled')) return;
					modal = $('#login-modal');
					modal.find('.panel').removeClass('show');
					var link = $(e.currentTarget);
					var panelId = link.attr('data-panel-id');
					if(panelId){
						modal.find('#' + panelId).addClass('show');
					}
					modal.foundation('reveal', 'open');
					modal.find('a[href="' + anchor + '"]').first().click(); //Select appropriate tab inside modal
				});
			});
			
			//While modal window is opened
			$('#login-modal').on('opened', null, function(){ 
				$(document).foundation(); //Recalculate size of tabs inside modal
			});
		},
		
		/**
		 * Inits spinners with +/- links incrementing/decrementing numeric value in corresponding field
		 */
		initSpinners: function(scope){
			if(!scope) scope = $('body');
			scope.find('.spinner a.plus, .spinner a.minus').each($.proxy(function(index, el){
				$(el).click($.proxy(function(e){
					e.preventDefault();
					var link = $(e.currentTarget); 
					var input = link.closest('.row').find('input[type="text"]');
					var val = input.val();
					var min = input.attr('data-min');
					if(!isNaN(val)){
						if(link.hasClass('minus')) val = val <= min ? min : val - 1;
						else if(link.hasClass('plus')) val++;
					}
					input.val(val);
					input.change();
				}, this));
			}, this));
		},
		
		/**
		 * Inits autocomplete on airport input fields
		 */
		initAirportInputs: function(scope){
			if(!scope) scope = $('body');
			scope.find('input.airport').autocomplete({
				source: this.sampleAirports
			});
		},
		
		/**
		 * Inits animated progress bars 
		 */
		initProgressBars: function(scope){
			if(!scope) scope = $('body');
			scope.find('.meter .before').each($.proxy(function(index, el){
				var meter = $(el).parent();
				meter.animate({
					'padding-right': meter.attr('data-increment') + '%'
				}, 3000);
			}, this));
		},
		
		/**
		 * Inits listeners on links with sample airports below the inputs
		 */
		initAirportShortcuts: function(scope){
			if(!scope) scope = $('body');
			scope.find('a.airport-shortcut').each($.proxy(function(index, el){
				$(el).click($.proxy(function(e){
					e.preventDefault();
					var link = $(e.currentTarget);
					var airports = $(link).closest('.departure,.return');
					var text = airports.find('input.airport');
					var hidden = airports.find('input.airport_id');
					text.val(link.attr('data-airport-name'));
					hidden.val(link.attr('data-airport-id'));
				}, this));
			}, this));
		},
		
		/**
		 * Inits flight date inputs
		 */
		initDatepickers: function(scope){
			if(!scope) scope = $('body');
			scope.find('input[name="departureDate"]').each($.proxy(function(index, el){
				$(el).datepicker({
					numberOfMonths: 2,
					defaultDate: '+1d',
					changeMonth: true,
					onClose: this.onCloseDeparture,
					minDate: '0'
				});
				
				$(el).focus($.proxy(function(e){
					$(e.currentTarget).addClass('departure');
					$('#ui-datepicker-div').addClass('departure');
				}, this));
				$(el).blur($.proxy(function(e){
					$(e.currentTarget).removeClass('departure');
					$('#ui-datepicker-div').removeClass('departure');
				}, this));
			}, this));
			
			scope.find('input[name="returnDate"]').each($.proxy(function(index, el){
				$(el).datepicker({
					numberOfMonths: 2,
					defaultDate: "+2d",
					changeMonth: true,
					onClose: this.onCloseReturn,
					minDate: '0'
				});
				
				$(el).focus($.proxy(function(e){
					$(e.currentTarget).addClass('return');
					$('#ui-datepicker-div').addClass('return');
				}, this));
				$(el).blur($.proxy(function(e){
					$(e.currentTarget).removeClass('return');
					$('#ui-datepicker-div').removeClass('return');
				}, this));
			}, this));
		},
		
		/**
		 * Action after user selects departure date from calendar
		 */
		onCloseDeparture: function(selectedDate){		
			var ret = $(this).closest('.departure').siblings('.return');
			if(ret.size() > 0) ret = ret.find('input[name="returnDate"]');
			ret.datepicker('option', 'minDate', selectedDate);
			ret.focus();
		},
		
		/**
		 * Action after user selects return date from calendar
		 */
		onCloseReturn: function(selectedDate){			
			var dep = $(this).closest('.return').siblings('.departure').find('input[name="departureDate"]');
			dep.datepicker('option', 'maxDate', selectedDate);
		},
		
		validateSearchForm: function(container){
			var alert = container.find('.alert-box');
			var alertText = alert.find('.alert-text'); 
			var depAirport = container.find('input[name="departureAirport"]');
			var retAirport = container.find('input[name="destinationAirport"]');				 
			var depDate = container.find('input[name="departureDate"]');
			var retDate = container.find('input[name="returnDate"]:visible');
			var adults = container.find('input[name="adults"]');
			var alerts = [];
			
			if(!depAirport.val()) alerts.push(this.NO_DEPARTURE_AIRPORT);
			if(!retAirport.val()) alerts.push(this.NO_RETURN_AIRPORT);
			if(depAirport.val() == retAirport.val() && depAirport.val() != '' && retAirport.val() != '') alerts.push(this.DEPARTURE_RETURN_THE_SAME);
			
			var today = new Date();
			
			if(!depDate.val()) depDate.datepicker('setDate', '+1');
			if(retDate.size() > 0 && !retDate.val()) retDate.datepicker('setDate', '+2');
			
			if(retDate.size() > 0 && depDate.datepicker('getDate') > retDate.datepicker('getDate')) alerts.push(this.DEPARTURE_LATER_THAN_RETURN);
			if(depDate.datepicker('getDate') < today || (retDate.size() > 0 && retDate.datepicker('getDate') < today)) alerts.push(this.DEPARTURE_BEFORE_TODAY);
			
			if(isNaN(adults.val()) || adults.val() < 1) alerts.push(this.NO_PASSANGER);
			
			if(alerts.length > 0) {
				alertText.html(alerts.join(' '));
				alert.show();
				return false;
			} else {
				alert.hide();
				return true;
			}
		},
		
		/**
		 * Validates form after user clicks on submit button
		 */
		initSearchFormValidation: function(scope){
			if(!scope) scope = $('body');
			scope.find('.search-flight-btn').each($.proxy(function(index, el){
				$(el).click($.proxy(function(e){
					var container = $(e.currentTarget).closest('form');
					return this.validateSearchForm(container);
				}, this));
			}, this));
		},
		
		/**
		 * Adds listener to link showing inputs for number of passsengers in search form in search page
		 */
		addEditPassengersListener: function(scope){
			if(!scope) scope = $('body');
			scope.find('.refine-search a.edit-passengers').each($.proxy(function(index, el){
				$(el).click($.proxy(function(e){
					e.preventDefault();
					var link = $(e.currentTarget);
					link.parent().hide();
					var container = link.closest('.refine-search').find('.passengers').show();
				}, this));
			}, this));
		},
		
		/**
		 * Sends AJAX flight search request based on GET parameters encoded in URL 
		 */
		searchRequest: function(lastResponseId, resultsSectionId, searchId, params){
			var container = $('.results-section.active');
			
			if(!resultsSectionId){
				/* Adds parameter from which tab request has been sent */
				var id = container.attr('id');
				var matches = id.match(/\d+/);
				if(matches.length > 0) resultsSectionId = matches[0];
				else resultsSectionId = 0;
			}else{
				container = $('.results-section-' + resultsSectionId);
			}
			
			if(!searchId) searchId = this.getSearchId();
			
			container.find('.more-results').css('visibility', 'hidden');
			container.find('.search-progress').show();
			
			if(!lastResponseId) lastResponseId = 0;
			
			if(!params){
				params = location.search;
				if(params.length > 0 && params.charAt(0) == '?') params = params.substr(1);
			}
			
			if(params){				
				params += '&lastResponseId=' + lastResponseId; 
				params += '&resultsSectionId=' + resultsSectionId;
				if(searchId) params += '&searchId=' + searchId;
			}
			
			$.post(
				this.searchUrl + lastResponseId + '.php', //needs to be changed to one constant URL
				params,
				$.proxy(this.searchCallback, this)
			);
		},
		
		/**
		 * Receives chunks of search results 
		 */
		searchCallback: function(json){
			var response = $.parseJSON(json);
			var container = $('#results-section-' + response.resultsSectionId);
			if(!container || container.size() < 1) return;
			
			if(!response.success){
				var alert = container.find('.search-progress .alert-box');
				container.find('.search-progress .loader').removeClass('loader');
				alert.find('.alert-text').html(response.alert);
				alert.show();
			}else{
				if(container.data('responseId') + 1 < response.responseId) return; //another request has been sent, so stop loading elder results
				
				if(container.hasClass('new-search-section')) {
					this.addSearchTab(container, response);
				}else{
					this.updateResults(container, response);
					this.updateFiltersListeners(container);
				}
				
				if(!response.finalResponse){
					//still more results, so wait and send another request
					setTimeout($.proxy(
						this.searchRequest, 
						this, 
						response.responseId, 
						response.resultsSectionId,
						response.searchId
					), this.searchRequestTimeout);
				}else{
					//all results loaded
					container.find('.more-results').css('visibility', 'visible');
					container.find('.search-progress').hide();
				}
			}
		},
		
		/**
		 * Update one tab with search results with new chunk of results
		 */
		updateResults: function(container, response){
			container.data('searchId', response.searchId); //store searchId param in .results-section element
			container.data('responseId', response.responseId); //to prevent multiple search requests running paralel
			
			//update progress bar
			container.find('.search-progress .meter').animate({
				width: response.progress + '%' 
			}, 'slow');
			
			//update number of flight results retrived
			container.find('.flights-number').each($.proxy(function(index, el){
				$(el).html(response.flightsNumber);
			}, this));
			
			//update cheapest price from all results
			container.find('.cheapest-price').each($.proxy(function(index, el){
				$(el).html(response.cheapestPrice);
			}, this));
			
			//update featured results
			container.find('.featured-search-results').html(response.featuredResultsHtml);
			
			//update regular results
			container.find('.regular-search-results').html(response.regularResultsHtml);
			
			//update filters
			container.find('.filters-wrapper').html(response.filters)
		},
		
		/**
		 * Add listeners for show/hide links expanding regular results within time slots
		 */
		addMoreSlotResultsListeners: function(scope){
			if(!scope) scope = $('body');
			var show = scope.find('.more-slot-results .show a');
			var hide = scope.find('.more-slot-results .hide a');
			
			show.click($.proxy(function(e){
				e.preventDefault();
				var link = $(e.currentTarget);
				var container = link.closest('.search-results');
				var results = container.find('.all-slot-results');
				if(results.hasClass('loaded')){
					results.show();
					container.find('.more-slot-results .show').hide();
					container.find('.more-slot-results .hide').show();
				} else this.loadMoreSlotResults(results);
			}, this));
			
			hide.click($.proxy(function(e){
				e.preventDefault();
				var link = $(e.currentTarget);
				var container = link.closest('.search-results');
				container.find('.more-slot-results .show').show();
				container.find('.more-slot-results .hide').hide();
				container.find('.all-slot-results').hide();
			}, this));
		},
		
		/**
		 * Sends AJAX request for more results within one time slot
		 */
		loadMoreSlotResults: function(resultsContainer){
			var timeSlotSection = resultsContainer.closest('.search-results'); 
			var timeSlotId = timeSlotSection.attr('data-slot-id');
			timeSlotSection.find('.more-slot-results .show').addClass('loader');
			
			$.post(
				'json/search_more/search_more.php',{
					resultsSectionId: this.getResultsSectionId(),
					searchId: this.getSearchId(),
					timeSlotId: timeSlotId
				},
				//in production change below to: $.proxy(this.callbackMoreSlotResults, this)
				$.proxy(function(json){
					setTimeout($.proxy(this.callbackMoreSlotResults, this, json), this.searchRequestTimeout);
				}, this)
			);
		},
		
		/**
		 * Callback receiving more search results within expanded on demand time slot
		 */
		callbackMoreSlotResults: function(json){
			var response = $.parseJSON(json);
			var container = $('#results-section-' + response.resultsSectionId);
			var slot = container.find('[data-slot-id="' + response.timeSlotId + '"]');
			var more = slot.find('.more-slot-results');
			
			if(response.success){
				more.find('.show').removeClass('loader').hide();
				more.find('.hide').show();
				slot.find('.all-slot-results').html(response.moreResultsHtml).addClass('loaded').show();
				this.updateNewResultsListeners(slot);
			}else{
				//error handling
				more.find('.show').removeClass('loader');
			}
		},
		
		/**
		 * Get search ID for current tab (results-section) or provided tab ID
		 */
		getSearchId: function(resultsSectionId){
			var container = $('.results-section.active');
			if(resultsSectionId) container = $('#results-section-' + resultsSectionId);
			return container.data('searchId');
		},
		
		/**
		 * Ensure result rows will have equal height on hover
		 */
		setResultsRowHeight: function(scope){
			if(!scope) scope = $('body');
			
			var results = scope.find('a.result article');
			var maxHeight = 0;
			results.each($.proxy(function(index, el){
				var max = 0;
				var rows = $(el).find('.flight,.hover');
				rows.children('.columns').css('height', 'auto');
				rows.each($.proxy(function(idx, row){
					if($(row).height() > max) max = $(row).height();
				}, this));
				rows.children('.columns').css('height', max + 'px');
			}, this));
			
			scope.find('.hover').addClass('hide');
		},
		
		/**
		 * Adds listeners on change event to all inputs in refine search form on search page 
		 */
		addRefineSearchListeners: function(scope){
			if(!scope) scope = $('body');
			var container = scope.find('.refine-search');
			container.find('input[type="date"],input[type="text"],select').change($.proxy(function(e){
				var input = $(e.currentTarget);
				var form = input.closest('form.refine-search');
				form.find('.submit').show();
			}, this));
		},
		
		/**
		 * Adds listener to button showing all results in search page
		 */
		addMoreResultsListener: function(scope){
			if(!scope) scope = $('body');
			scope.find('.show-more-results').each($.proxy(function(index, el){
				$(el).click($.proxy(function(e){
					e.preventDefault();
					var btn = $(e.currentTarget);
					btn.closest('.results-column').find('.more-results-wrapper').show();
					btn.closest('.more-results').hide();
					this.setResultsRowHeight(scope);
				}, this));
			}, this));
		},
		
		/**
		 * Adds listener to link showing more filters
		 */
		addMoreFiltersListener: function(){
			$('.more-filters .more a').each(function(index, el){
				$(el).click(function(e){
					e.preventDefault();
					var link = $(e.currentTarget);
					link.closest('.filters').find('.advanced-filters').show();
					link.closest('.more-filters').find('.more').hide();
					link.closest('.more-filters').find('.less').show();
					$(document).foundation();					
				});
			});
			
			$('.more-filters .less a').each(function(index, el){
				$(el).click(function(e){
					e.preventDefault();
					var link = $(e.currentTarget);
					link.closest('.filters').find('.advanced-filters').hide();
					link.closest('.more-filters').find('.more').show();
					link.closest('.more-filters').find('.less').hide();
				});
			});
		},
		
		/**
		 * Inits selection one of buttons included into button group (a'la radio button)
		 */
		initRadioButtons: function(scope){
			if(!scope) scope = $('body');
			scope.find('.button-group.radio').each(function(index, el){
				var name = $(el).attr('data-name');
				if(name){
					var val = '';
					var btn = $(el).find('.button.disabled');
					if(btn.size() > 0) val = btn.attr('data-value');
					$(el).append('<input type="hidden" name="' + name + '" value="' + val + '" />')
				}
				
				$(el).find('.button').click(function(e){
					e.preventDefault();
					var btn = $(e.currentTarget);
					var group = btn.closest('.button-group');
					var input = group.find('input[name="' + group.attr('data-name') + '"]');
					if(input) input.val(btn.attr('data-value'));
					group.find('.button').removeClass('disabled');
					btn.addClass('disabled');
				});
			});
		},
		
		/**
		 * Callback updating slide label of price filter
		 */
		onPriceSlide: function(event, ui){
			$(ui.handle).find('.value').html(ui.value);
		},
		
		/**
		 * Formates number of minutes into duration 
		 */
		minutesToDuration: function(minutes){
			return Math.floor(minutes / 60) + 'h' + (minutes % 60) + 'min'; 
		},
		
		/**
		 * Callback updating slide label of duration filter
		 */
		onDurationSlide: function(event, ui){
			var slider = $(ui.handle).closest('.slider');
			var minDuration = parseInt(slider.attr('data-duration-min'));
			var maxDuration = parseInt(slider.attr('data-duration-max'));
			var minValue = parseInt(slider.attr('data-min'));
			var maxValue = parseInt(slider.attr('data-max'));
			var ratio = ui.value / (maxValue - minValue);
			var duration = Math.round((maxDuration - minDuration) * ratio) + minDuration;
			$(ui.handle).find('.value').html(this.minutesToDuration(duration));
		},
		
		/**
		 * Returns number of minutes from midnight based on time in 24h format or 12h am/pm
		 */
		timeToMinutes: function(time, format){
			var minutes = 0;
			var hours = 0;
			
			if(format == 12){
				var timeMatches = time.match(/(\d{1,2}):(\d{2})(.)m/);
				hours = parseInt(timeMatches[1]);
				minutes = parseInt(timeMatches[2]);
				var am = timeMatches[3] == 'a';
				
				if(hours == 12 && am) hours -= 12;
				if(hours != 12 && !am) hours += 12;
			}else if(format == 24){
				var timeMatches = time.match(/(\d{1,2}):(\d{2})/);
				hours = parseInt(timeMatches[1]); 
				minutes = parseInt(timeMatches[2]);
			}
			
			return hours * 60 + minutes;
		},
		
		/**
		 * From number of minutes from midnight to time in format 24h or 12am/pm
		 */
		minutesToTime: function(totalMinutes, format){
			var hours = Math.floor(totalMinutes / 60);
			var minutes = totalMinutes % 60;
			var postfix = '';
			
			if(format == 12){
				if(hours == 0) {
					hours = 12;
					postfix = 'am';
				}else if(hours >= 1 && hours < 12){
					postfix = 'am';
				}else if(hours == 12){
					postfix = 'pm';
				}else if(hours > 12){
					hours -= 12;
					postfix = 'pm';
				}
				
				return hours + ':' + (minutes < 10 ? '0' : '') + minutes + postfix;
			}else{
				return hours + ':' + (minutes < 10 ? '0' : '') + minutes;
			}
		},
		
		/**
		 * Change callback of slider displaying time range
		 */
		onTimeSlide: function(event, ui){
			var slider = $(ui.handle).closest('.slider');
			var minMinutes = slider.data('minutes-min');
			var maxMinutes = slider.data('minutes-max');
			var format = parseInt(slider.attr('data-time-format'));
			
			if(!minMinutes || !maxMinutes){
				var min = slider.attr('data-time-min');
				var max = slider.attr('data-time-max');
				
				minMinutes = this.timeToMinutes(min, format);
				maxMinutes = this.timeToMinutes(max, format);
				slider.data('minutes-min', minMinutes);
				slider.data('minutes-max', maxMinutes);
			}
			
			var minValue = slider.attr('data-min');
			var maxValue = slider.attr('data-max');
			var ratio = (ui.value / (maxValue - minValue));
			var totalMinutes = Math.round((maxMinutes - minMinutes) * ratio) + minMinutes;
			var time = this.minutesToTime(totalMinutes, format);
			
			$(ui.handle).find('.value').html(time);
		},
		
		/**
		 * Inits single jQuery UI slider,
		 * adds labels to handlers, adds callback on change
		 */
		initSlider: function(slider, onSlideCallback){
			if(!slider || slider.size() < 0) return;
			
			var min = slider.attr('data-min');
			var max = slider.attr('data-max');
			
			var minLabel = slider.find('.min').detach();
			var maxLabel = slider.find('.max').detach();
			
			slider.slider({
				range: true,
				min: min,
				max: max, 
				values: [min, max],
				slide: onSlideCallback
			});
			
			var handles = slider.find('.ui-slider-handle');
			handles.first().html(minLabel);
			handles.last().html(maxLabel);
			
			onSlideCallback(null, {
				handle: handles.first(),
				value: min
			});
			
			onSlideCallback(null, {
				handle: handles.last(),
				value: max
			});
		},
		
		/**
		 * Inits jQuery UI sliders used in search filters
		 */
		initSliders: function(scope){
			if(!scope) scope = $('body');
			this.initSlider(scope.find('.price-slider'), $.proxy(this.onPriceSlide, this));
			this.initSlider(scope.find('.duration-slider'), $.proxy(this.onDurationSlide, this));
			this.initSlider(scope.find('.departure-slider'), $.proxy(this.onTimeSlide, this));
			this.initSlider(scope.find('.return-slider'), $.proxy(this.onTimeSlide, this));
		},
		
		/**
		 * Select all/deselect all checkbox listener in search filters
		 */
		initFilterCheckboxes: function(scope){
			if(!scope) scope = $('body');
			
			//listener on none/all checkbox
			scope.find('.filters :checkbox[name$="all"]').each(function(index, el){
				$(el).change(function(e){
					var checkbox = $(e.currentTarget);
					var set = checkbox.closest('.filter').find(':checkbox').not('[name$="all"]');
					if(checkbox.is(':checked')) set.prop('checked', true);
					else set.prop('checked', false);
				});
			});
			
			//listener on regular checkbox
			scope.find('.filters :checkbox').not('[name$="all"]').change($.proxy(function(e){
				var checkbox = $(e.currentTarget);
				var filter = checkbox.closest('.filter');
				var set = filter.find(':checkbox').not('[name$="all"]');
				var allNone = filter.find(':checkbox[name$="all"]');
				if(set.size() == set.filter(':checked').size()) allNone.prop('checked', true);
				else allNone.prop('checked', false);
			}, this));
		},
		
		/**
		 * Show overlay over passed container with custom message
		 */
		showLoaderOverlay: function(container, message){
			var overlay = container.find('.loader-overlay');
			if(overlay.size() < 1){
				container.append('<div class="loader-overlay"></div>');
				overlay = container.find('.loader-overlay');
			}
			container.css('position', 'relative');
			overlay.css({
				width: (container.width() -20) + 'px',
				height: (container.height() -20) + 'px'
			});
			overlay.html('<h5 class="silver"><span class="loader">' + message + '</span></h5>');
		},
		
		/**
		 * Hide overlay generated in showLoaderOverlay method
		 */
		hideLoaderOverlay: function(container){
			var overlay = container.find('.loader-overlay');
			if(overlay.size() > 0){
				overlay.detach();
			}
		},
		
		/**
		 * Returns parameters to pass to server from stops filter
		 */
		getStopsFilterParam: function(filter, name){
			var values = filter.find('.button.disabled').attr('data-value');
			return {
				filter: name,
				values: values.split(',')
			};
		},
		
		/**
		 * Returns parameters to pass to server from price filter
		 */
		getPriceFilterParam: function(filter, name){
			var range = filter.find('.slider').slider('values');
			return {
				filter: name,
				range: {
					min: range[0],
					max: range[1]
				}
			};
		},
		
		/**
		 * Returns parameters to pass to server from duration, departure and return filter
		 */
		getSliderFilterParam: function(filter, name){
			var min = filter.find('.slider .min .value').html();
			var max = filter.find('.slider .max .value').html();
			return {
				filter: name,
				range: {
					min: min,
					max: max
				}
			};
		},
		
		/**
		 * Returns parameters to pass to server from checkbox-based filter
		 */
		getCheckboxFilterParam: function(filter, name){
			var checkboxes = filter.find('input:checked').not('[name$="all"]');
			var values = [];
			
			checkboxes.each($.proxy(function(index, el){
				var val = $(el).attr('name');
				var matches = val.match(/\d+/);
				values.push(matches[0]);
			}, this));
			
			return {
				filter: name,
				values: values
			};
		},
		
		/**
		 * Returns numeric id of results section of passed element
		 */
		getResultsSectionId: function(el){
			var section = $('.results-section.active');
			if(el) section = el.closest('.results-section');
			var id = section.attr('id');
			var num = id.match(/\d+/);
			return num[0];
		},
		
		/**
		 * Gather all parameters from all filters and send them in POST request
		 */
		submitFilters: function(e){
			var input = jQuery(e.currentTarget);
			var filters = input.closest('.filters').find('.filter');
			var params = [];
			
			filters.each($.proxy(function(index, filter){
				var name = $(filter).attr('data-filter');
				filter = $(filter);
				
				switch(name){
					case 'stops':
						params.push(this.getStopsFilterParam(filter, name))
						break;
					case 'price':
						params.push(this.getPriceFilterParam(filter, name))
						break;
					case 'duration':
					case 'departure':
					case 'return':
						params.push(this.getSliderFilterParam(filter, name))
						break;
					default:
						params.push(this.getCheckboxFilterParam(filter, name))
						break;
				}
			}, this));
			
			this.showLoaderOverlay(input.closest('.filters'), this.WAIT_TO_APPLY_FILTERS);
			
			$.post(
				'json/search_filter/search_filter.php',{
					resultsSectionId: this.getResultsSectionId(input),
					searchId: this.getSearchId(),
					filters: params
				},
				//in production change followint to: $.proxy(this.callbackApplyFilters, this)
				$.proxy(function(json){
					setTimeout($.proxy(this.callbackApplyFilters, this, json), this.searchRequestTimeout);
				}, this)
			);
		},
		
		/**
		 * Apply filters to search results
		 */
		callbackApplyFilters: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				var container = $('#results-section-' + response.resultsSectionId);
				container.find('.regular-search-results').html(response.regularResultsHtml);
				this.updateAllResultsListeners(container);
			}else{
				//to do
			}
			this.hideLoaderOverlay($('.filters'));
		},
		
		/**
		 * Adds listeners to all filters in order to submit change on any of them
		 */
		initFilterSubmit: function(scope){
			if(!scope) scope = $('body');
			var container = scope.find('.filter');
			container.find('.button').click($.proxy(this.submitFilters, this));
			container.find(':checkbox').change($.proxy(this.submitFilters, this));
			container.find('.slider').on('slidechange', $.proxy(this.submitFilters, this));
		},
		
		/**
		 * Adds listener to single result row 
		 */
		addResultListener: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('a.result').click($.proxy(function(e){
				e.preventDefault();
				var result = $(e.currentTarget);
				if(result.hasClass('selected')) return;
				
				var resultsSectionId = this.getResultsSectionId();
				var flightId = result.attr('data-flight-id');
				var container = $('#results-section-' + resultsSectionId);
				result.addClass('selected');
				
				var comparison = container.find('.comparison-column');
				var tip = comparison.children('.comparison-tip');
				
				var emptyFlightContainer = '<article class="flight loader" data-flight-id="' + flightId + '"></article>';
				var content = result.data('flight') ? result.data('flight') : emptyFlightContainer;
				
				if(tip.size() > 0) {
					tip.first().before(content).detach();
				}else{
					comparison.append(content);
				}
				
				if(result.data('flight')) return;
				
				$.post(
					this.searchResultUrl,{
						searchId: this.getSearchId(),
						resultsSectionId: resultsSectionId,
						flightId: flightId
					},
					//in production change following to: $.proxy(this.callbackAddResult, this)
					$.proxy(function(json){
						setTimeout($.proxy(this.callbackAddResult, this, json), this.searchRequestTimeout);
					}, this)
				);
			}, this));
		},
		
		/**
		 * Callback after user adds result to comparison column
		 */
		callbackAddResult: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				var flight = $('#results-section-' + response.resultsSectionId + ' .comparison-column [data-flight-id="' + response.flightId + '"]');
				flight.removeClass('loader');
				flight.html(response.flightHtml);
				
				this.addFlightListeners(flight);
			}else{
				//add error handling
			}
		},
		
		/**
		 * Hide results with longer stops  
		 */
		hideLongerStopResults: function(scope){
			if(!scope) scope = $('body');
			
			//show rows
			var links = scope.find('.show-longer-overlay');
			links.each($.proxy(function(index, link){
				//hide rows
				var hideRows = $(link).attr('data-collapsed-rows');
				var nextRows = $(link).closest('tr').nextAll();
				for(var i = 0 ; i < hideRows ; i++) nextRows.eq(i).addClass('longer')
				
				//add listener on link showing more return options
				$(link).click($.proxy(function(e){
					e.preventDefault();
					var el = $(e.currentTarget);
					var nextRows = el.closest('tr').nextAll();
					var hideRows = el.attr('data-collapsed-rows');
					for(var i = 0 ; i < hideRows ; i++) nextRows.eq(i).removeClass('longer')
					$(link).hide();
				}, this));
			}, this));
			
			//hide rows
			var links = scope.find('.hide-longer-overlay');
			links.click($.proxy(function(e){
				e.preventDefault();
				var link = $(e.currentTarget);
				var showRows = link.attr('data-collapsed-rows');
				var row = link.closest('tr');
				var input = row.find('input');
				for(var i = 0 ; i < showRows ; i++){
					row.addClass('longer');
					row = row.prev();
				}
				row.find('.show-longer-overlay').show();				
				if(input.is(':checked')) row.find('.custom.radio').click();				
			}, this));
		},
		
		/**
		 * Add deselected class to cells without selected return flight
		 */
		selectReturn: function(radioButton){
			var allRadio = radioButton.closest('tbody').find(':radio');
			allRadio.each($.proxy(function(index, el){
				var td = $(el).closest('td');
				td.addClass('deselected');
				td.nextAll().addClass('deselected');
			}, this));
			var td = radioButton.closest('td');
			td.removeClass('deselected');
			td.nextAll().removeClass('deselected');
			
			this.updateFlightPrice(radioButton)
		},
		
		/**
		 * Update price on flight result top left container after choosing
		 * different return option or class 
		 */
		updateFlightPrice: function(radioButton){
			var radio = radioButton.siblings('.radio').first();
			var price = radio.html();
			var totalPrice = radio.attr('data-total-price');
			var priceContainer = radioButton.closest('article.flight').children('.panel').find('.price');
			if(priceContainer.parent().hasClass('double') && totalPrice){
				priceContainer.children('div').html(price);
				priceContainer.children('.total').html(totalPrice);
			}else{
				priceContainer.html(price);	
			}
		},
		
		/**
		 * Add listener to radio buttons selecting return flight
		 */
		addSelectReturnListener: function(scope){
			if(!scope) scope = $('body');
			
			var radio = scope.find('input[name^="return-"]');
			this.selectReturn(radio.filter(':checked'));
			radio.change($.proxy(function(e){
				this.selectReturn($(e.currentTarget));
			}, this));
		},
		
		/**
		 * Add listener to close icon
		 */
		addCloseFlightListener: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('.close').click($.proxy(function(e){
				e.preventDefault();
				var link = $(e.currentTarget);
				var flight = link.closest('.flight');
				var section = flight.closest('.results-section');
				var result = section.find('.result[data-flight-id="' + flight.attr('data-flight-id') + '"]');
				if(result.size() > 0) result.removeClass('selected');
				result.data('flight', flight.detach());
			}));
		},
	
		
		/**
		 * Leave only checked return flight in table
		 */
		showSelectedReturnOnly: function(flight){
			var tbody = flight.find('.flight-options tbody');
			tbody.find('tr').hide();
			
			var checkedReturn = flight.find('.flight-options input:checked');
			
			//create temporary row with one selected option
			tbody.append('<tr class="temporary"></tr>');
			var temp = tbody.find('.temporary');
			var departureCols = tbody.find('tr:first td').slice(0, 3).clone();
			temp.append(departureCols);
			var returnCols = checkedReturn.closest('tr').find('td').slice(-3).clone();
			returnCols.find(':radio').detach();
			temp.append(returnCols);
		},
		
		/**
		 * Show all return options
		 */
		showAllReturns: function(flight){
			flight.find('.flight-options tbody tr').show();
			flight.find('.flight-options .temporary').detach();
		},
		
		/**
		 * Opens flight details panel
		 */
		showFlightDetails: function(flight){
			//swap show/hide details buttons				
			flight.find('.hide-flight-details').show();
			flight.find('.show-flight-details').hide();
			
			//open details panel
			flight.find('.details-wrapper').show();
			
			//find checked return flight
			var checkedReturn = flight.find('.flight-options input:checked');
			
			this.showSelectedReturnOnly(flight);
			
			//show graph only for checked flight
			flight.find('.return.details').hide();
			flight.find('.return.details[data-return-id="' + checkedReturn.val() + '"]').show();
		},
		
		/**
		 * Hide flight details panel
		 */
		hideFlightDetails: function(flight){
			//swap show/hide details buttons
			flight.find('.show-flight-details').show();
			flight.find('.hide-flight-details').hide();
			
			//close details panel
			flight.find('.details-wrapper').hide();
			
			if(flight.find('.classes-wrapper:visible').size() < 1) this.showAllReturns(flight);
		},
		
		/**
		 * Inits flight details section
		 */
		initFlightDetails: function(scope){
			if(!scope) scope = $('body');
			
			//show details
			scope.find('.show-flight-details').click($.proxy(function(e){
				e.preventDefault();
				var flight = $(e.currentTarget).closest('.flight');
				this.showFlightDetails(flight);
			}, this));
			
			//hide details
			scope.find('.hide-flight-details').click($.proxy(function(e){
				e.preventDefault();
				var flight = $(e.currentTarget).closest('.flight');
				this.hideFlightDetails(flight);
			}, this));
		},
		
		/**
		 * Add hover listener to graph in flight details
		 */
		initFlightDetailsHovers: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('.graph-fragment').hover($.proxy(function(e){
				var fragment = $(e.currentTarget);
				fragment.closest('.graph-wrapper').addClass('fragment-selected');
				var info = fragment.closest('.details').find('.graph-info');
				info.find('.info-default').hide();
				info.find('.info-fragment[data-fragment-id="' + fragment.attr('data-fragment-id') + '"]').show();
			}, this), $.proxy(function(e){
				var fragment = $(e.currentTarget);
				fragment.closest('.graph-wrapper').removeClass('fragment-selected');
				var info = fragment.closest('.details').find('.graph-info');
				info.find('.info-default').show();
				info.find('.info-fragment').hide();
			}, this));
		},
		
		/**
		 * Add deselected class for all rows beside selected one (radio button)
		 */
		highlightClassRow: function(row){
			row.siblings().not('.separator').addClass('deselected');
			row.removeClass('deselected');
		},
		
		/**
		 * Add listener to radio button within cabin class selection panel
		 */
		addClassChangeListener: function(scope){
			if(!scope) scope = $('body');
			scope.find('.classes-wrapper :radio').change($.proxy(function(e){
				e.preventDefault();
				var radio = $(e.currentTarget);
				this.highlightClassRow(radio.closest('tr'));
			}, this));
		},
		
		/**
		 * Adds listener to button in flight showing all cabin classes
		 */
		addShowClassesListener: function(scope){
			this.highlightClassRow(scope.find('.classes tbody .separator + tr').first());
			
			scope.find('.show-classes').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				var flight = btn.closest('.flight');
				flight.find('.classes-wrapper').show();
				
				flight.find('.hide-classes').show();
				flight.find('.actions > .book-ticket').show();
				flight.find('.book-ticket-dropdown').hide();
				
				this.showSelectedReturnOnly(flight);
				btn.hide();
			}, this));
			
			scope.find('.hide-classes').click($.proxy(function(e){
				e.preventDefault();
				var link = $(e.currentTarget);
				var flight = link.closest('.flight');
				flight.find('.classes-wrapper').hide();
				
				link.hide();
				flight.find('.actions > .book-ticket').hide();
				flight.find('.actions > .show-classes').show();
				
				this.hideFlightDetails(flight);
			}, this));
		},
		
		/**
		 * Add listener to combo box with programs appliable to flight result
		 */
		addProgramChangeListener: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('.change-program').change($.proxy(function(e){
				var combo = $(e.currentTarget);
				this.showLoaderOverlay(combo.closest('.flight'), this.LOADING_PROGRAM_CHANGE);
				
				$.post(
					'json/flight_program_change/flight_program_change.php',{
						programId: combo.val(),
						flightId: combo.closest('.flight').attr('data-flight-id'),
						resultsSectionId: this.getResultsSectionId(),
						searchId: this.getSearchId()
					},
					//in production change following to: $.proxy(this.callbackProgramChange, this)
					$.proxy(function(json){
						setTimeout($.proxy(this.callbackProgramChange, this, json), this.searchRequestTimeout);
					}, this)
				);
			}, this));
		},
		
		/**
		 * Callback after change of program within flight result container
		 */
		callbackProgramChange: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				var flight = $('#results-section-' + response.resultsSectionId + ' .comparison-column [data-flight-id="' + response.flightId + '"]');
				this.hideLoaderOverlay(flight);
				flight.html(response.flightHtml);
				flight.find('.change-program').val(response.programId);
				this.addFlightListeners(flight);
			}else{
				//add error handling
			}
		},
		
		/**
		 * Add listeners to new search tab buttons - one way/round trip
		 */
		addNewSearchTabButtonsListeners: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('.round-trip-btn').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				btn.closest('.search-form').find('.dates .return').show();
			}, this));
			
			scope.find('.one-way-btn').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				btn.closest('.search-form').find('.dates .return').hide();
			}, this));
		},
		
		/**
		 * Add listener to search form in new search tab
		 */
		addNewSearchListener: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('.search-flight-btn').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				var container = btn.closest('.results-section');
				var form = container.find('.search-form');
				if(!this.validateSearchForm(form)) return;
				container.find('.search-progress').show();
				this.searchRequest(-1, null, null, form.serialize());
				form.hide();
			}, this));
		},
		
		/**
		 * Add new tab with search results
		 */
		addSearchTab: function(container, response){
			container.attr('id', 'results-section-' + (parseInt(response.resultsSectionId) + 1));
			container.find('.search-progress').hide();
			container.find('.search-form').show();
			container.before(response.searchSectionHtml);
			$(document).foundation();
			var newTab = container.prev();
			newTab.find('.results-tab a').click();
			this.newSearchInit(newTab);
		},
		
		/**
		 * Adds listener to close button for tabbed search results
		 */
		addCloseTabListener: function(scope){
			if(!scope) scope = $('body');
			
			btn = scope.find('.close-tab');
			btn.click($.proxy(function(e){
				e.preventDefault();
				var section = $(e.currentTarget).closest('.results-section');
				section.detach();
				$(document).foundation();
			}, this));
		},
		
		/**
		 * Inits autocomplete for program or airline input
		 */
		initProgramInput: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('#program').change($.proxy(this.addNewProgram, this));
		},
		
		/**
		 * After slide with credit cards is changed in account page,
		 * set correct values of switches, which are static 
		 */
		onCardsSlideChange: function(slide){
			var switchesWrapper = slide.closest('#program-cards').find('.switches-wrapper');
			switchesWrapper.show();
			var switches = switchesWrapper.find('.switch');
			var cards = slide.find('.program-card');
			cards.each($.proxy(function(index, card){
				if(parseInt($(card).attr('data-card-enabled')) == 1) switches.find('[for="card-on-' + index + '"]').click();
				else switches.find('[for="card-off-' + index + '"]').click();
			}, this));
		},
		
		/**
		 * Inits connection between static switches and cards images on orbit scroller
		 * in account page. When switch is clicked, search for cards with same data-card-id
		 * as has overlying card and set every copy new value (enabled/disabled)
 		 * @param {Object} scope
		 */
		initSwitchesOnCards: function(scope){
			if(!scope) scope = $('body');
			
			this.onCardsSlideChange(scope.find('.orbit-container li.active'));
			
			scope.find('.orbit-container').on('orbit:after-slide-change', $.proxy(function(e){
				this.onCardsSlideChange($(e.currentTarget).find('li.active'));
			}, this));
			
			scope.find('.orbit-container').on('orbit:before-slide-change', $.proxy(function(e){
				$(e.currentTarget).closest('#program-cards').find('.switches-wrapper').hide();
			}, this));
			
			var switchesWrapper = scope.find('#program-cards .switches-wrapper');
			switchesWrapper.find(':radio').change($.proxy(function(e){
				var radio = $(e.currentTarget);
				var explodedId = radio.attr('id').split('-');
				var value = 1;
				if(explodedId[1] == 'off') value = 0;
				var container = radio.closest('#program-cards'); 
				var cardOnTop = container.find('li.active .program-card').eq(explodedId[2]);
				var sameCards = container.find('.program-card[data-card-id="' + cardOnTop.attr('data-card-id') + '"]');
				sameCards.attr('data-card-enabled', value);
			}, this));
		},
		
		/**
		 * Event fired after user changes value in text input with airlines/programs
 		 * @param {Object} e
		 */
		addNewProgram: function(e){
			var input = $('#program');
			var prefix = input.closest('form').find('.prefix');
			var loadedForm = $('#new-program-details.loaded');
			if(prefix.hasClass('loader') || (loadedForm.size() > 0 && loadedForm.data('programId') == input.val())) return;
			prefix.addClass('loader');
			this.closeAllEditedPrograms();
			
			$.post(
				'json/account_new_program/account_new_program.php',{
					programId: input.val()
				},
				//in production change following to: $.proxy(this.callbackAddProgram, this)
				$.proxy(function(json){
					setTimeout($.proxy(this.callbackAddProgram, this, json), this.searchRequestTimeout);
				}, this)
			);
		},
		
		/**
		 * Close edit program forms 
		 */
		closeAllEditedPrograms: function(){
			var edit = $('form.edit-program');
			if(edit.size() > 0) edit.each($.proxy(function(index, el){
				var id = $(el).attr('data-program-id');
				$(el).detach();
				var program = $('#program-' + id);
				if(program.size() > 0) program.show();
			}, this));
			var newProgram = $('#new-program-details.loaded');
			if(newProgram.size() > 0) this.closeNewProgram();
		},
		
		/**
		 * Callback after typing new program by user to add in user's account page
 		 * @param {Object} json
		 */
		callbackAddProgram: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				$('#program').closest('form').find('.prefix').removeClass('loader');
				var newProgram = $('#new-program-details');
				newProgram.addClass('loaded').data('programId', response.programId).html(response.programHtml);
				this.addNewProgramListeners(newProgram);
			}else{
				//add error handling
			}
		},
		
		/**
		 * Show/hide sections in new program form on account page
 		 * @param {Object} scope
		 */
		initNewProgramSections: function(scope){
			if(!scope) scope = $('body');
			
			var milesAuto = scope.find('#miles-auto');
			var milesManual = scope.find('#miles-manual');
			var noCards = scope.find('#program-no-cards');
			var hasCards = scope.find('#program-has-cards');
			
			milesAuto.change($.proxy(function(e){
				$('#miles-auto-wrapper').show();
				$('#miles-manual-wrapper').hide();
			}, this));
			
			milesManual.change($.proxy(function(e){
				$('#miles-manual-wrapper').show();
				$('#miles-auto-wrapper').hide();
			}, this));
			if(milesManual.is(':checked')) milesManual.change();
			
			noCards.change($.proxy(function(e){
				$('#program-cards').removeClass('shown');
			}, this));
			
			hasCards.change($.proxy(function(e){
				var el = $('#program-cards');
				if(!el.hasClass('shown')){
					this.initSwitchesOnCards(scope);
					el.addClass('shown');	
				}
			}, this));
			
			$(document).foundation();
			if(noCards.is(':checked')) noCards.change();
			else hasCards.change();
		},
		
		/**
		 * Adds listener to button sending creditentials to frequent
		 * flyer programs 
 		 * @param {Object} scope
		 */
		initGetMiles: function(scope){
			scope.find('.check-miles').click($.proxy(function(e){
				e.preventDefault();
				var login = $('#program-login');
				var pass = $('#program-pass');
				if(login.val() && pass.val()){
					this.showLoaderOverlay($('#miles-auto-wrapper'), this.LOADING_PROGRAM_MILES);
					$.post(
						'json/account_get_miles/account_get_miles.php',{
							programId: $('#new-program-details').data('programId'),
							login: login.val(),
							password: pass.val()
						},
						//in production change following to: $.proxy(this.callbackGetMiles, this)
						$.proxy(function(json){
							setTimeout($.proxy(this.callbackGetMiles, this, json), this.searchRequestTimeout);
						}, this)
					)
				}else{
					//add error handling
				}
			}, this));
		},
		
		/**
		 * Callback after retriving mile values after connection with frequent flyer program
		 * in user account
 		 * @param {Object} json
		 */
		callbackGetMiles: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				var section = $('#miles-auto-wrapper');
				this.hideLoaderOverlay(section);
				var milesResult = $('#miles-result');
				milesResult.html(response.milesHtml);
				section.find('.get-miles').hide();
				section.find('.reconnect').show();
				var buttonsWrapper = $('#program-status');
				var statusButton = buttonsWrapper.find('[data-status-id="' + response.statusId + '"]');
				if(statusButton.size() > 0) statusButton.click();
			}else{
				//add error handling
			}
		},
		
		/**
		 * Prepares data to submit new frequent flyer program in user account 
		 */
		saveNewProgram: function(e){
			e.preventDefault();
			var newProgramContainer = $('#new-program-details');
			var editProgramContainer = $('form.edit-program');
			var edit = true;
			if(newProgramContainer.hasClass('loaded') > 0) edit = false;
			
			var params = {
				programId: edit ? editProgramContainer.attr('data-program-id') : newProgramContainer.data('programId'),
				statusId: $('#program-status .button.disabled').attr('data-status-id'),
				awardMiles: $('#program-award-miles').val(),
				statusMiles: $('#program-status-miles').val(),
				segments: $('#program-segments').val(),
				action: edit ? 'edit' : 'new'
			};
			
			if(edit){
				params.cardNumber = $('#program-card-no').val()
			}
			
			var cardsRadio = $('#program-has-cards');
			
			if(cardsRadio.size() > 0 && cardsRadio.is(':checked')){
				var on = [];
				var off = [];
				$('#program-cards .program-card').each($.proxy(function(index, el){
					var enabled = parseInt($(el).attr('data-card-enabled'));
					var cardId = parseInt($(el).attr('data-card-id'));
					if(enabled == 1 && on.indexOf(cardId) == -1){
						on.push(cardId);
					}
					if(enabled == 0 && off.indexOf(cardId) == -1){
						off.push(cardId);
					}
				}, this));
					
				params.creditCards = [];
					
				$(on).each($.proxy(function(index, val){
					params.creditCards.push({
						cardId: val,
						status: true
					});
				}, this));
					
				$(off).each($.proxy(function(index, val){
					params.creditCards.push({
						cardId: val,
						status: false
					});
				}, this));
			}
			
			if(edit){
				this.showLoaderOverlay(editProgramContainer, this.LOADING_SAVE_PROGRAM);
			}else{
				this.showLoaderOverlay(newProgramContainer, this.LOADING_SAVE_PROGRAM);
			}
			
			
			$.post(
				'json/account_program_save/account_program_save.php',
				params,	
				//in production change following to: $.proxy(this.callbackSaveProgram, this)
				$.proxy(function(json){
					setTimeout($.proxy(this.callbackSaveProgram, this, json), this.searchRequestTimeout);
				}, this)			
			)
		},
		
		/**
		 * Retrives new program to include in user account 
		 */
		callbackSaveProgram: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				if(response.action == 'edit'){
					this.hideLoaderOverlay($('form.edit-program'));
					var program = $('#program-' + response.programId);
					if(program.size() > 0) program.replaceWith(response.programHtml);
				}else{
					this.hideLoaderOverlay($('#new-program-details'));	
					var container = $('.programs-wrapper'); 
					container.append(response.programHtml);
				}
				this.closeAllEditedPrograms();
				this.addProgramListeners($('#program-' + response.programId));
			}else{
				//add error handling
			}
		},
		
		/**
		 * Close new program panel 
		 */
		closeNewProgram: function(e){
			if(e) e.preventDefault();
			$('#new-program-details').html('').removeClass('loaded').data('programId', null);
			$('#program').val(null);
		},
		
		/**
		 * Adds listeners to buttons in account page, add new program section
 		 * @param {Object} scope
		 */
		initNewProgramButtons: function(scope){
			scope.find('.change-new-program').click($.proxy(this.closeNewProgram, this));
			scope.find('#save-program').click($.proxy(this.saveNewProgram, this));
			var deleteProgram = scope.find('#delete-program');
			if(deleteProgram.size() > 0) deleteProgram.click($.proxy(this.deleteProgram, this));
		},
		
		/**
		 * Delete edited program in user account page
		 */
		deleteProgram: function(e){
			e.preventDefault();
			var editForm = $(e.currentTarget).closest('form.edit-program');
			this.showLoaderOverlay(editForm, this.LOADING_DELETE_PROGRAM);
			var programId = editForm.attr('data-program-id');
			
			$.post(
				'json/account_program_delete/account_program_delete.php',{
					programId: programId
				},
				//in production change following to: $.proxy(this.callbackDeleteProgram, this)
				$.proxy(function(json){
					setTimeout($.proxy(this.callbackDeleteProgram, this, json), this.searchRequestTimeout);
				}, this)
			)
		},
		
		/**
		 * Response for delete program action 
		 */
		callbackDeleteProgram: function(json){
			var response = $.parseJSON(json);
			this.hideLoaderOverlay($('form.edit-program'));
			if(response.success){
				$('form.edit-program').detach();
				$('#program-' + response.programId).detach();
			}else{
				//add error handler
			}
		},
		
		/**
		 * Add listener on program switch in account page
 		 * @param {Object} scope
		 */
		initProgramSwitch: function(scope){
			scope.find('.actions :radio').change($.proxy(function(e){
				var radio = $(e.currentTarget);
				var idParts = radio.attr('id').split('-');
				var enabled = idParts[2] == 'on';
				var program = radio.closest('.program');
				var programId = program.attr('id').split('-');
				
				this.showLoaderOverlay(program, this.LOADING_SWITCH_PROGRAM);
				
				$.post(
					'json/account_program_switch/account_program_switch.php',{
						programId: programId[1],
						status: enabled
					},
					//in production change following to: $.proxy(this.callbackSwitchProgram, this)
					$.proxy(function(json){
						setTimeout($.proxy(this.callbackSwitchProgram, this, json), this.searchRequestTimeout);
					}, this)
				);
			}, this));
		},
		
		/**
		 * Retrives from server confirmation about enabling/disabling program in user account  
		 */
		callbackSwitchProgram: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				var program = $('#program-' + response.programId);
				this.hideLoaderOverlay(program);
				if(response.status) program.removeClass('disabled');
				else program.addClass('disabled');
			}else{
				//add error handling
			}
		},
		
		/**
		 * Add listener on program edit link in account page
 		 * @param {Object} scope
		 */
		initProgramEdit: function(scope){
			scope.find('.edit-program').click($.proxy(function(e){
				var link = $(e.currentTarget);
				var program = link.closest('.program');
				var programId = program.attr('id').split('-');
				this.showLoaderOverlay(program, this.LOADING_EDIT_PROGRAM);
				this.closeAllEditedPrograms();
				
				$.post(
					'json/account_program_edit/account_program_edit.php',{
						programId: programId[1]
					},
					//in production change following to: $.proxy(this.callbackEditProgram, this)
					$.proxy(function(json){
						setTimeout($.proxy(this.callbackEditProgram, this, json), this.searchRequestTimeout);
					}, this)
				);
			}, this));
		},
		
		/**
		 * Returns edit form for one selected program in user account
 		 * @param {Object} json
		 */
		callbackEditProgram: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				var program = $('#program-' + response.programId);
				this.hideLoaderOverlay(program);
				program.after(response.programHtml).hide();
				this.addNewProgramListeners($('form.edit-program'));
			}else{
				//add error handler
			}
		},
		
		/**
		 * Adds listener to button connected with address input field
		 * adding new lines to address 
		 */
		initExtraLineFields: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('.add-line').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				var first = btn.closest('.first-line');
				var next = first.next();
				var clone = next.clone();
				clone.show();
				clone.find('.del-line').click(function(e){
					e.preventDefault();
					var btn = $(e.currentTarget);
					var line = btn.closest('.extra-line');
					var parent = line.parent();
					line.detach();
					parent.find('input:visible').each(function(index, el){
						$(el).attr('name', 'street[' + index + ']');
					});
				});
				first.parent().append(clone);
				first.parent().find('input:visible').each(function(index, el){
					$(el).attr('name', 'street[' + index + ']');
				});
			}, this));
		},
		
		/**
		 * Adds listeners to elements on user profile form 
		 */
		initAccountProfileForm: function(json){
			$('.redress,.extra-line').hide();
			
			$('#user-has-redress').change($.proxy(function(e){
				if($(e.currentTarget).is(':checked')){
					$('.redress').show();
				}else{
					$('.redress').hide();
				}
			}, this));
			
			this.addDateFieldMask();
			this.initExtraLineFields();
			
			$('#save-profile').click($.proxy(function(e){
				e.preventDefault();
				var email = $('#user-email');
				var emailVal = email.val();

				if(!emailVal.match(this.emailPattern)){
					this.addInputError(email, this.INVALID_EMAIL_ALERT);
					
				}else{
					this.removeInputError(email);
					$(e.currentTarget).closest('form').submit();
				}
			}, this));
			
			this.initDreamDestinationForm();
		},
		
		/**
		 * Add error message to input 
		 */
		addInputError: function(input, error){
			input.addClass('error');
			var alert = input.next('small.error');
			if(alert.size() > 0) alert.detach();
			if(error && input.width() > 100) input.after('<small class="error">' + error + '</small>');
		},
		
		/**
		 * Remove error mesage from input 
		 */
		removeInputError: function(input){
			input.removeClass('error');
			var alert = input.next('small.error');
			if(alert.size() > 0) alert.detach();
		},
		
		/**
		 * Add submit listener and simple validation in user account page (change password form) 
		 */
		initAccountPasswordForm: function(){
			$('#change-pass').click($.proxy(function(e){
				e.preventDefault();
				var form = $(e.currentTarget).closest('form');
				var first = $('#user-new-pass');
				var second = $('#user-new-pass-check');
				var valid = true;
				var all = $('#user-new-pass,#user-new-pass-check,#user-pass');
				all.each($.proxy(function(index, el){
					if(!$(el).val()){
						this.addInputError($(el), this.EMPTY_PASSWORD);
						valid = false;
					}else{
						this.removeInputError($(el));
					}
				}, this));
				
				if(valid){
					if(first.val() == second.val()){
						this.removeInputError(first);
						this.removeInputError(second);
					}else{
						this.addInputError(first, this.PASSWORDS_DONT_MATCH);
						this.addInputError(second, this.PASSWORDS_DONT_MATCH);
						valid = false;
					}	
				}
				
				if(valid) form.submit();
			}, this));
		},
		
		/**
		 * Inits form where user can input his dream destionation 
		 */
		initDreamDestinationForm: function(){
			$('#user-destination').autocomplete({
				source: this.sampleAirports	
			});
			
			$('#user-home').autocomplete({
				source: this.sampleAirports	
			});
		},
		
		/**
		 * Adds listeners on checkboxes in order to uncover hidden fields
		 */
		initAdditionalBookingFields: function(){
			$('.redress').change($.proxy(function(e){
				var check = $(e.currentTarget);
				var field = check.closest('fieldset').find('.redress-number');
				if(check.is(':checked')) field.show();
				else field.hide();
			}, this));
			
			$('input[name^="document"]').change($.proxy(function(e){
				var radio = $(e.currentTarget);
				if(radio.val() == 'usaid'){
					radio.closest('fieldset').find('.passport-exp-date').hide();
				}else if(radio.val() == 'passport'){
					radio.closest('fieldset').find('.passport-exp-date').show();
				}
			}, this));
			
			$('#save-to-profile').change($.proxy(function(e){
				var check = $(e.currentTarget);
				if(check.is(':checked')) $('#pass').show();
				else $('#pass').hide();
			}, this));
		},
		
		/**
		 * Makes equal height of both cards and add
		 * listener to card number fields to pass focus to next one 
		 */
		initBookingCreditCards: function(){
			var container = $('#booking-payment');
			var cards = container.find('.card');
			cards.eq(1).height(cards.eq(0).height());
			
			var digit4 = $('input[name^="credit-card-number"]');
			digit4.keypress($.proxy(function(e){
				var el = $(e.currentTarget);
				if(!el.is(':focus')) return;
				var digits = el.val();
				if(digits.length != el.attr('maxlength')) return;
				var name = el.attr('name');
				var matches = name.match(/\d+/);
				var index = parseInt(matches[0]) + 1;
				name = name.replace(/\d+/, index);
				var next = $('[name="' + name + '"]');
				if(next.size() > 0) next.focus();
			}, this));
		},
		
		/**
		 * Adds mask to specified fields eg. date inputs 
		 */
		addDateFieldMask: function(scope){
			if(!scope) scope = $('body');
			
			var date = scope.find('input.date-mask');
			date.each($.proxy(function(index, el){
				$(el).mask(this.dateMask);
			}, this));
		},
		
		emptyFieldValidator: function(input){
			if(!input.val()) {
				this.addInputError(input, this.EMPTY_FIELD);
				return false;
			}else{
				this.removeInputError(input);
				return true;
			}
		},
		
		dateFieldValidator: function(input){
			var val = input.val();
			if(!val.match(this.datePattern)) {
				this.addInputError(input, this.INVALID_DATE_ALERT);
				return false;
			}else{
				this.removeInputError(input);
				return true;
			}
		},
		
		phoneFieldValidator: function(input){
			var val = input.val();
			if(!val.match(this.phonePattern)) {
				this.addInputError(input, this.INVALID_PHONE_NUMBER);
				return false;
			}else{
				this.removeInputError(input);
				return true;
			}
		},
		
		emailFieldValidator: function(input){
			var val = input.val();
			if(!val.match(this.emailPattern)) {
				this.addInputError(input, this.INVALID_EMAIL_ALERT);
				return false;
			}else{
				this.removeInputError(input);
				return true;
			}
		},
		
		numberFieldValidator: function(input){
			var val = input.val();
			var limit = input.attr('maxlength');
			if(limit){
				if(val.length != limit){
					this.addInputError(input, this.TOO_LONG_ALERT + limit);
					return false;
				}else{
					this.removeInputError(input);
				}
			}
			if(!val.match(/^[0-9]+$/)) {
				this.addInputError(input, this.INVALID_NUMBER);
				return false;
			}else{
				this.removeInputError(input);
				return true;
			}
		},
		
		checkboxValidator: function(input){
			var label = null;
			var id = input.attr('id');
			if(id){
				label = $('label[for="' + input.attr('id') + '"]').first();
			}
			if(!input.is(':checked')) {
				if(label) this.addInputError(label);
				return false;
			}else{
				if(label) this.removeInputError(label);
				return true;
			}
		},
		
		validateField: function(e){
			var el = $(e.currentTarget);
			var validator = el.data('validator');
			if(typeof validator == 'function') return validator(el);
			return true;
		},
		
		/**
		 * Add validators to input fields in booking page 
		 */
		addBookingFormValidators: function(){
			$('#card-no,[id^="first-name-"],[id^="last-name-"],[id^="document-number-"],#credit-card-holder,#pass')
				.data('validator', $.proxy(this.emptyFieldValidator, this))
				.blur($.proxy(this.validateField, this));
			$('[id^="dob-"],[id^="passport-exp-date"]')
				.data('validator', $.proxy(this.dateFieldValidator, this))
				.blur($.proxy(this.validateField, this));
			$('#phone')
				.data('validator', $.proxy(this.phoneFieldValidator, this))
				.blur($.proxy(this.validateField, this));
			$('#email')
				.data('validator', $.proxy(this.emailFieldValidator, this))
				.blur($.proxy(this.validateField, this));
			$('[name^="credit-card-number"],#credit-card-month,#credit-card-year,#credit-card-security')
				.data('validator', $.proxy(this.numberFieldValidator, this))
				.blur($.proxy(this.validateField, this));
			$('#terms')
				.data('validator', $.proxy(this.checkboxValidator, this))
				.blur($.proxy(this.validateField, this));
				
			$('#submit-booking').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				var form = btn.closest('form');
				var inputs = form.find('input:visible');
				var valid = true;
				inputs.each($.proxy(function(index, el){
					$(el).blur();
					if($(el).hasClass('error')) valid = false;
				}, this));
				if(valid) $('#booking-confirm-modal').foundation('reveal', 'open');
				else{
					var error = $('small.error').first();
					if(error.size() > 0){
						var offset = error.offset();
						window.scroll(0, offset.top - 100);	
					}
				}
			}, this));
		},
		
		/**
		 * Sends AJAX request after frequent flyer program change (buttons) 
		 */
		initBookingChangeProgram: function(){
			$('#booking-ffp .change-program').change($.proxy(function(e){
				var select = $(e.currentTarget);
				this.showLoaderOverlay($('#booking-ffp'), this.LOADING_SWITCH_PROGRAM)
				
				$.post(
					'json/booking_change_program/booking_change_program.php',{
						programId: select.val()
					},
					//in production change following to: $.proxy(this.callbackBookingChangeProgram, this)
					$.proxy(function(json){
						setTimeout($.proxy(this.callbackBookingChangeProgram, this, json), this.searchRequestTimeout);
					}, this)
				);
			}, this));
		},
		
		/**
		 * Callback after changing program by user on booking page 
 		 * @param {Object} json
		 */
		callbackBookingChangeProgram: function(json){
			var response = $.parseJSON(json);
			var container = $('#booking-ffp');
			this.hideLoaderOverlay(container);
			if(response.success){
				container.replaceWith(response.ffpHtml);
				$(document).foundation();
				this.initBookingChangeProgram();
				this.initChangeProgramMiles();
				this.initProgressBars();
			}else{
				//add error handler
			}
		},
		
		/**
		 * Event listener after selecting program in alert1 input 
		 */
		onAlert1SelectProgram: function(e){
			var input = $(e.currentTarget);
			var row = input.closest('.new-program')
			row.addClass('loader');
			row.find('.apply-program').hide();
			row.find('.foundicon-remove').hide();
			
			$.post(
				'json/alert1_program/alert1_program.php',{
					programId: input.val()
				},
				//in production change following to: $.proxy(this.callbackAlert1SelectedProgram, this)
				$.proxy(function(json){
					setTimeout($.proxy(this.callbackAlert1SelectedProgram, this, json), this.searchRequestTimeout);
				}, this)
			);
		},
		
		/**
		 * Callback after selecting program in alert1 to reload status buttons
		 */
		callbackAlert1SelectedProgram: function(json){
			var response = $.parseJSON(json);
			if(response.success){
				var container = $('.alert1');
				var row = container.find('.new-program.loader');
				row.find('apply-program').removeClass('disabled');
				row.removeClass('loader');
				row.find('.apply-program').show();
				row.find('.foundicon-remove').show();
				container.find('.save-programs').removeAttr('disabled');
				var status = row.find('[name="status"]');
				status.next('.dropdown').detach();
				status.replaceWith(response.statusHtml);
				$(document).foundation();
			}else{
				//add error handler
			}
		},
		
		/**
		 * Listener of link action to add another program row in .alert1 
		 */
		addAlert1ProgramRow: function(e){
				e.preventDefault();
				var link = $(e.currentTarget);
				var templateRow = link.closest('.alert1').find('.extra-line');
				var copyRow = templateRow.clone();
				templateRow.before(copyRow);
				copyRow.removeClass('extra-line');
				var allRows = copyRow.parent().find('.new-program:visible');
				copyRow.find('.remove-another-program').click($.proxy(function(e){
					e.preventDefault();
					var delLink = $(e.currentTarget);
					var row = delLink.closest('.new-program');
					var allRows = copyRow.siblings('.new-program:visible');
					row.detach();
					this.searchRequest();
				}, this));
				this.addAlert1Listeners(copyRow);
		},
		
		/**
		 * Adds listeners to .alert1 box on search page with
		 * form to input program name and status 
		 */
		addAlert1Listeners: function(scope){
			if(!scope) scope = $('.alert1');
			var input = scope.find('[name="program"]');
			input.each($.proxy(function(index, el){
				if($(el).closest('.extra-line').size() > 0) return;
				$(el).change($.proxy(this.onAlert1SelectProgram, this));
			}, this));
			scope.find('[name="status"]').change(function(e){
				$(e.currentTarget).closest('.new-program').find('.apply-program').removeClass('disabled');
			});
			scope.find('.apply-program').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				btn.addClass('disabled');
				var programName = btn.closest('.new-program').find('select[name="program"]').next('.dropdown').find('.current').html();
				this.searchRequest();
				this.addTopAlert(this.SEARCH_UPDATED_WITH_PROGRAM + programName, true);
			}, this));
			scope.find('.add-another-program').click($.proxy(this.addAlert1ProgramRow, this));
			
			this.addNoProgramListener();
			
			this.addAlertToggleListener(scope);
		},
		
		/**
		 * Adds listener to links on alert to toggle it's state: expanded/collapsed 
		 */
		addAlertToggleListener: function(scope){
			scope.find('.toggle-alert').click(function(e){
				e.preventDefault();
				var link = $(e.currentTarget);
				link.closest('.alert1,.alert2').toggleClass('collapsed');
			});
		},
		
		/**
		 * Add all listeners to .alert3 box in search_alert2.html
		 * ANIMATION, won't work in real enveiroment
		 */
		/*addAlert3Listeners: function(){
			var container = $('.alert3');
			if(!container || container.size() < 1) return;
			container.find('#user-destination').autocomplete({
				source: this.sampleAirports
			});
			container.find('#user-home').autocomplete({
				source: this.sampleAirports
			});
			
			container.find('.close-alert').click($.proxy(function(e){
				e.preventDefault();
				$(e.currentTarget).closest('.alert3').detach();
				$('.green-alert-icon').detach();
			}, this));
			
			container.find('.save-destination').click($.proxy(function(e){
				e.preventDefault();
				this.showLoaderOverlay($('.alert3'), this.SAVING_DESTINATION);
				setTimeout($.proxy(function(){
				location.href = 'search.html';
				}, this), this.searchRequestTimeout);
			}, this));
		},*/
		
		/**
		 * Add all listeners to .alert2 box in search_alert2.html
		 * ANIMATION, won't work in real enveiroment
		 */
		addAlert2Listeners: function(){
			var container = $('.alert2');
			if(!container || container.size() < 1) return;
			
			container.find('.close-alert').click($.proxy(function(e){
				e.preventDefault();
				$(e.currentTarget).closest('.alert2').detach();
				$('.yellow-alert-icon').detach();
			}, this));
			
			container.find('a.check-miles').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				
				var input = btn.closest('.program-input').find('input[type="text"], input[type="password"]');
				var valid = true;
				input.each($.proxy(function(index, el){
					if($(el).val() == '') {
						$(el).addClass('error');
						valid = false;
					}
				}, this));
				
				if(!valid) return;
				
				btn.parent().addClass('loader');
				btn.css('visibility', 'hidden');
				$.post(
					'json/alert2_program/alert2_program.php',{
						programId: 34
					},
					$.proxy(function(json){
						setTimeout(
							$.proxy(function(json){
								var response = $.parseJSON(json);
								var container = $('.alert2');
								var row = container.find('.loader').closest('.program-input');
								
								if(response.success){
									row.replaceWith(response.milesHtml);
									this.addTopAlert(this.SEARCH_UPDATED_WITH_PROGRAM + response.programName, true);
									this.searchRequest();
								}else{
									var col = row.find('.loader');
									col.removeClass('loader');
									var btn = col.find('.button');
									btn.css('visibility', 'visible');
									var pass = row.find('input[type="password"]');
									this.addInputError(pass, response.alert);
								}
							}, this, json),
							this.searchRequestTimeout
						);
					}, this)
				);
			}, this));
			
			this.addAlertToggleListener(container);
		},
		
		/**
		 * Adds validation to login/signup forms in modal window 
		 */
		addLogInValidation: function(){
			var container = $('#login-modal');
			
			container.find('#login-email,#signup-email')
				.data('validator', $.proxy(this.emailFieldValidator, this))
				.blur($.proxy(this.validateField, this));
			container.find('#login-pass,#signup-pass')
				.data('validator', $.proxy(this.emptyFieldValidator, this))
				.blur($.proxy(this.validateField, this));
			container.find('#signup-pass2')
				.data('validator', $.proxy(function(input){
					if(input.val() != input.closest('#login-modal').find('#signup-pass').val()) {
						this.addInputError(input, this.PASSWORDS_DONT_MATCH);
						return false;
					}else{
						this.removeInputError(input);
						return true;
					}
				}, this))
				.blur($.proxy(this.validateField, this));
				
			container.find('#login-submit,#signup-submit').click($.proxy(function(e){
				e.preventDefault();
				var btn = $(e.currentTarget);
				var form = btn.closest('form');
				var inputs = form.find('input:visible');
				var valid = true;
				inputs.each($.proxy(function(index, el){
					$(el).blur();
					if($(el).hasClass('error')) valid = false;
				}, this));
				if(valid) {
					btn.closest('.submit-col').addClass('loader');
					setTimeout(function(){
						$('#login-modal .submit-col').removeClass('loader');
						$('#login-modal .close-reveal-modal').click();
					}, this.searchRequestTimeout);
				}
				else{
					var error = $('small.error').first();
					if(error.size() > 0){
						var offset = error.offset();
						window.scroll(0, offset.top - 100);	
					}
				}
			}, this));
		},
		
		/**
		 * Add alert on top of page 
		 */
		addTopAlert: function(alertText, success){
			var sampleAlertCls = '.sample-alert';
			if(success) sampleAlertCls += '.alert-success';
			else sampleAlertCls += '.alert-error';
			var sample = $(sampleAlertCls);
			var alert = sample.clone();
			alert.find('.alert-text').html(alertText);
			$('#top-alerts').append(alert);
			alert.removeClass('sample-alert').addClass('row-alert').hide().fadeIn(600);
			alert.find('.close').click(function(e){
				e.preventDefault();
				$(e.currentTarget).closest('.row-alert').detach();
			});
			location.href = '#top';
			if(success){
				setTimeout($.proxy(function(){
					this.fadeOut(600, $.proxy(function(){ this.detach(); }, this));
				}, alert), this.ALERT_DURATION);
			}
		},
		
		/**
		 *  Submit action on form to remind password
		 *  Validation and confirmation alert
		 */
		addForgetPassListener: function(){
			$('#forgot-pass-modal form').submit($.proxy(function(e){
				var form = $(e.currentTarget);
				var email = form.find('input[type="email"]');
				if(this.emailFieldValidator(email)){
					form.closest('#forgot-pass-modal').find('.close-reveal-modal').click();
					this.addTopAlert(this.PASS_RECOVER_ALERT, true);
					return false;
				}
				return false;
			}, this));
		},
		
		/**
		 *  Submit action on form to subscribe for new programs
		 *  Validation and confirmation alert
		 */
		addNoProgramListener: function(){
			$('#no-program form').submit($.proxy(function(e){
				var form = $(e.currentTarget);
				var email = form.find('input[type="email"]');
				if(this.emailFieldValidator(email)){
					form.closest('#no-program').find('.close-reveal-modal').click();
					this.addTopAlert(this.SUBSCRIBED_ALERT, true);
					return false;
				}
				return false;
			}, this));
		},
		
		/**
		 * Adds value of miles to program name in custom select
 		 * 
		 */
		initChangeProgramMiles: function(scope){
			if(!scope) scope = $('body');
			
			var original = scope.find('select.change-program');
			var copy = scope.find('.dropdown.change-program');
			var copyItems = copy.find('li');
			var groups = original.find('optgroup:nth-child(2n)');
			if(groups.size() > 0){
				groups.each($.proxy(function(index, el){
					var options = $(el).find('option');
					options.addClass('odd').first().addClass('first');
				}, this));
			}
			original.find('option').each($.proxy(function(index, el){
				var miles = $(el).attr('data-miles');
				var li = copyItems.eq(index);
				if(miles) li.append('<span>' + miles + '</span>');
				li.attr('class', $(el).attr('class')); 
			}, this));
		},
		
		/**
		 * Add listener to class price radiobuttons to change price in result
		 */
		initClassChange: function(scope){
			if(!scope) scope = $('body');
			
			scope.find('.classes input[type="radio"]').change($.proxy(function(e){
				this.updateFlightPrice($(e.currentTarget));
			}, this));
		},
		
		/**
		 * Selects on focus text in input fields with autocomplete feature 
		 */
		initAutocompleteFields: function(){
			$('.ui-autocomplete-input').focus(function(e){
				this.select();
			});
		},
		
		initBookingBg: function(){
			var bg = $('#booking-bg');
			var marginHeight = $('#top').height();
			var height = $(window).height() - marginHeight - 30;
			maxHeight = height * 1.5;
			minHeight = height * 0.8;
			bg.height(minHeight);
			var width = bg.width();
			var minLeft = -width / 2;
			var maxLeft = $(window).width() - width;
			bg.css({
				bottom: '30px',
				left: minLeft + 'px'
			}).data('minLeft', minLeft).data('maxLeft', maxLeft).data('minHeight', minHeight).data('maxHeight', maxHeight);
			
			$(window).scroll(function(e){
				var bg = $('#booking-bg');
				var maxScroll = bg.parent().height();
				var scroll = $(window).scrollTop();
				if(scroll > maxScroll) scroll = maxScroll;
				var progress = scroll / maxScroll;
				
				bg.css({
					left: (bg.data('minLeft') + (bg.data('maxLeft') - bg.data('minLeft')) * progress) + 'px'	
				}).height(bg.data('minHeight') + (bg.data('maxHeight') - bg.data('minHeight')) * progress);
			});
		},
		
		/**
		 * Opens select when label nex to it is clicked on account page 
		 */
		initNewProgramLabel: function(){
			$('.new-program-prefix').click(function(e){
				var el = $('#program').next('.dropdown').addClass('open');
			});
		},
		
		/* Animation of Front Page */
		initFrontPageAnimation: function(){
			imagesLoaded($('.imgs-loaded'), $.proxy(this.initClouds, this));
			this.initScreens();
			
			$(window).scroll($.proxy(function(e){
				this.updateClouds();
				this.updateFFPCard();
				this.updateScreens();
				this.updateDeal();
			}, this));
		},
		
		/* Animation of Front Page */
		updateDeal: function(){
			if(!this.has3d()) return;
			var scroll = $(window).scrollTop();
			var container = $('#guide-6-5');
			var deal = container.find('hgroup');
			var offset = container.offset();
			var start = offset.top - $(window).height() / 2;
			var end = offset.top;
			var max = 1.5;
			
			if(scroll > start && scroll < end){
				var progress = Math.abs(scroll - start) / Math.abs(end - start);
				var scale = (max - 1) * Math.sin(progress * Math.PI) + 1;
				deal.css('transform', 'scale(' + scale + ')');
			}else{
				deal.css('transform', 'scale(1)');
			}
		},
		
		/* Animation of Front Page */
		updateFFPCard: function(){
			if(!this.has3d()) return;
			var scroll = $(window).scrollTop();
			var container = $('#guide-2');
			var slide2 = container.find('hgroup');
			var slide1 = container.find('figure');
			var offset = $('#guide-2').offset();
			
			var start = offset.top - $(window).height() + slide1.height();
			var end = offset.top - slide1.height() / 2;	
			
			var middle = end - Math.abs(start - end) / 2;
			slide1.css({ transform: 'rotateY(0deg)' });
			slide2.css({ transform: 'rotateY(0deg)' });
			
			if(scroll < middle) {
				slide1.show();
				slide2.hide();
				if(scroll > start){
					var rot = (Math.abs(scroll - start) / Math.abs(middle - start)) * 90;
					slide1.css({
						transform: 'rotateY(' + rot + 'deg)'
					});
				}
			}else{
				slide1.hide();
				slide2.show();
				if(scroll < end){
					var rot = 90 - (Math.abs(scroll - middle) / Math.abs(end - middle)) * 90;
					slide2.css({
						transform: 'rotateY(' + rot + 'deg)'
					});
				}
			}
		},
		
		/* Animation of Front Page */
		initClouds: function(){
			var first = $('.clouds');
			first.after(first.clone());
			
			var width = first.width();
			var baseWidth = width * 1.2;
			var maxWidth = width * 1.8;
			var minWidth = width * 0.1;
			var horizon = first.height() / 2;
			var length = $(window).height();	
			var progress = baseWidth / (maxWidth - minWidth);
			
			$('.clouds').data('baseWidth', baseWidth)
				.data('maxWidth', maxWidth)
				.data('minWidth', minWidth)
				.data('horizon', horizon)
				.data('length', length)
				.data('baseProgress', progress);
				
			this.updateClouds();
		},
		
		/* Animation of Front Page */
		updateClouds: function(){
			var clouds = $('.clouds');
			var scroll = $(window).scrollTop();
			var current = scroll % clouds.data('length');
			var progress = current / clouds.data('length') + clouds.data('baseProgress');
			if(progress > 1) progress -= 1;
			
			var offset = $('#guide-6').offset();			
			var opacityProgress = (scroll > offset.top ? offset.top : scroll) / offset.top;
			
			var antiProgress = progress + 0.3;
			if(antiProgress > 1) antiProgress -= 1;
			var present = clouds.eq(0).width();
			var future = progress * clouds.data('maxWidth'); 
			clouds.eq(0).width(future);
			
			present = clouds.eq(1).width();
			future = antiProgress * clouds.data('maxWidth'); 
			clouds.eq(1).width(future);
						
			clouds.each($.proxy(function(index, cloud){
				var cloud = $(cloud);
				var top = cloud.data('horizon') - cloud.height() / 2;
				var left = $(window).width() / 2 - cloud.width() / 2;
				cloud.css({
					left: left + 'px',
					top: top + 'px',
					opacity: 1 - opacityProgress
				});
			}, this));
		},
		
		/* Animation of Front Page */
		initScreens: function(){
			var container = $('#guide-4');
			var img = container.find('.comp');
			img.after(img.clone());
			img.after(img.clone());
			
			this.updateScreens();
		},
		
		/* Animation of Front Page */
		updateScreens: function(){
			var scroll = $(window).scrollTop();
			var container = $('#guide-4');
			var imgs = container.find('.comp');
			var containerWidth = container.width();
			var imgWidth = imgs.width();
			var middleLeft = (containerWidth - imgWidth) / 2;
			var offset = container.offset();
			var start = offset.top - $(window).height();
			var end = offset.top - $(window).height() / 2;	
			var speed = 4;	
			var text = container.find('.screen');	
			
			if(scroll < start || scroll > end){
				imgs.eq(1).data('initial', middleLeft).addClass('hightlight');
				imgs.eq(0).data('initial', middleLeft - imgWidth);
				imgs.eq(2).data('initial', middleLeft + imgWidth);
				imgs.each($.proxy(function(index, i){
					$(i).css({ 
						left: $(i).data('initial') + 'px',
						opacity:  Math.sin((($(i).data('initial') + imgWidth) / (containerWidth + imgWidth)) * Math.PI)
					});
				}, this));
				text.css('opacity', 1);
			}else{
				var progress = Math.abs(scroll - start) / Math.abs(end - start);
				imgs.each($.proxy(function(index, i){
					$(i).removeClass('hightlight');
					var left = (($(i).data('initial') + progress * speed * containerWidth) % (containerWidth + imgWidth)) - imgWidth;
					$(i).css({ left: left + 'px', opacity: Math.sin(((left + imgWidth) / (containerWidth + imgWidth)) * Math.PI) });
				}, this));
				text.css('opacity', Math.sin(Math.PI * (3 / 2) + progress * (Math.PI / 2)) + 1);
			}
		},
		
		/* Adds browser name as class to body */
		addBrowserClass: function(){
			$('body').addClass(BrowserDetect.browser.toLowerCase());
		},
		
		/**
		 * Checks if CSS transform property is supported 
		 */
		has3d: function(){
			if(this._has3d === undefined){
			   var el, has3d;
			   el = document.createElement('div');
			   el.style['transform'] = 'matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)';
			   document.body.insertBefore(el, document.body.lastChild);
			   has3d = window.getComputedStyle(el).getPropertyValue('transform');
			   if( has3d !== undefined ){
			      this._has3d = has3d !== 'none';
			   } else {
			      this._has3d = false;
			   }
		   }
		   
		   return this._has3d;
		},
		
		/**
		 * Selects options for flight on click of row (fare / departure / return)
		 */
		addFlightOptionsSelectListener: function(scope){
			scope.find('.custom.radio.price').each($.proxy(function(index, el){
				$(el).closest('tr').click($.proxy(function(e){
					var target = $(e.target);
					if(target.is('.custom.radio.price')) return;
					if(!target.is('td')) target = target.closest('td');
					while(target.size() > 0 && !target.hasClass('radio-col')) target = target.prev();
					if(target.size() < 1) return;
					var radio = target.find('.custom.radio.price');
					if(radio.size() > 0) radio.click();
				}, this));
			}, this));
		},
		
		initTwitterSideBlock: function(){
			
			var wrapper = $('#twitter-side-block');
			
			wrapper.find('.close').click(function(){
				$('#twitter-side-block').addClass('collapsed');
			});
			
			wrapper.find('.collapse').click(function(){
				$('#twitter-side-block').removeClass('collapsed');
			});
			
			wrapper.find('button').click(function(){
				$('#twitter-modal').foundation('reveal', 'open'); 
				$('#twitter-side-block').addClass('collapsed');
			});
			
			setTimeout(function(){
				var wrapper = $('#twitter-side-block');
				wrapper.removeClass('collapsed');
				wrapper.animate({
					right: 0
				}, 600);
			}, 3000); 
			
			var modal = $('#twitter-modal');
			modal.find('.highlight button').click(function(event){
				var modal = $('#twitter-modal');
				modal.find('.highlight button').removeClass('selected');
				var btn = $(event.currentTarget);
				var name = btn.attr('class');
				modal.find('.webname').html(name);
				alert('Here twitter button should change apperance according to selected logo: ' + name);
				btn.addClass('selected');
				//to close modal simply invoke:
				//$('#twitter-modal').foundation('reveal', 'close'); 
			});
		}
	};
}