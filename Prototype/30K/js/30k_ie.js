function _30k_ie(){
	return {
		/*
		 * Counter for ids of labels as placeholders
		 */
		placeholderId: 0,
		
		/**
		 * Start of execution 
		 */
		init: function(){
			this.addLabelsInsteadPlaceholders();
		},
		
		/**
		 * IE ver < 10 does not render placeholders.
		 * For fields, that does not have labels, such labels are created to be displayed as placeholders inside field
		 */
		addLabelsInsteadPlaceholders: function(){
			var inputs = $('input[placeholder]');
			inputs.each($.proxy(function(index, el){
				var siblingLabels = $(el).siblings('label');
				var id = $(el).attr('id');
				numOfLabels = 0;
				if(id){
					numOfLabels = $('label[for="' + id + '"]').size();
				}else{
					id = 'input-placeholder-' + this.placeholderId;
					$(el).attr('id', id);
					this.placeholderId++;
				}
				var connectedLabel = $('label[for="' + id + '"]');
				var numOfLabels =+ connectedLabel.size();
				if(numOfLabels > 0) return;
				$(el).wrap('<div class="placeholder-wrapper" />');
				$(el).before('<label class="placeholder" for="' + id + '">' + $(el).attr('placeholder') + '</label>');
				$(el).removeAttr('placeholder');
				var label = $('label.placeholder[for="' + id + '"]');
				label.width($(el).width());
				$(el).focus($.proxy(function(e){
					var id = $(e.currentTarget).attr('id');
					var label = $('label.placeholder[for="' + id + '"]');
					label.hide();
				}, this));
				$(el).blur($.proxy(function(e){
					var id = $(e.currentTarget).attr('id');
					var label = $('label.placeholder[for="' + id + '"]');
					label.show();
				}, this));
			}, this));
		}
	}.init();
}
