<?php die(json_encode(array(

	'success' => true,
	'responseId' => 2,
	'searchId' => 23,
	'finalResponse' => true,
	'progress' => 100,
	'flightsNumber' => 521,
	'cheapestPrice' => '$404',
	'resultsSectionId' => isset($_POST['resultsSectionId']) ? $_POST['resultsSectionId'] : '',
	'featuredResultsHtml' => file_get_contents('featuredSearchResults0.html'),
	'regularResultsHtml' => file_get_contents('../regularSearchResults0.html'),
	'filters' => file_get_contents('../searchResultsFilters0.html')
)));
