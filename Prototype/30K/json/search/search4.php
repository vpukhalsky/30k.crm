<?php die(json_encode(array(

	'success' => true,
	'responseId' => 5,
	'searchId' => 23,
	'finalResponse' => true,
	'progress' => 100,
	'flightsNumber' => 1127,
	'cheapestPrice' => '$322',
	'resultsSectionId' => isset($_POST['resultsSectionId']) ? $_POST['resultsSectionId'] : '',
	'featuredResultsHtml' => file_get_contents('featuredSearchResults0.html'),
	'regularResultsHtml' => file_get_contents('regularSearchResults0.html'),
	'filters' => file_get_contents('searchResultsFilters0.html')
	
)));
