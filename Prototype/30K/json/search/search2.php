<?php die(json_encode(array(

	'success' => true,
	'responseId' => 3,
	'searchId' => 23,
	'finalResponse' => false,
	'progress' => 68,
	'flightsNumber' => 810,
	'cheapestPrice' => '$390',
	'resultsSectionId' => isset($_POST['resultsSectionId']) ? $_POST['resultsSectionId'] : '',
	'featuredResultsHtml' => file_get_contents('featuredSearchResults0.html'),
	'regularResultsHtml' => file_get_contents('regularSearchResults0.html'),
	'filters' => file_get_contents('searchResultsFilters0.html')
)));
