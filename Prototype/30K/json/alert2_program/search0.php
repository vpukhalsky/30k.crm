<?php die(json_encode(array(

	'success' => true,
	'searchId' => 23,
	'responseId' => 1,
	'finalResponse' => true,
	'progress' => 12,
	'flightsNumber' => 343,
	'cheapestPrice' => '$432',
	'resultsSectionId' => isset($_POST['resultsSectionId']) ? $_POST['resultsSectionId'] : '',
	'featuredResultsHtml' => file_get_contents('featuredSearchResults0.html'),
	'regularResultsHtml' => file_get_contents('../search/regularSearchResults0.html'),
	'filters' => file_get_contents('../search/searchResultsFilters0.html')
	
)));
