<?php die(json_encode(array(

	'success' => true,
	'programId' => isset($_POST['programId']) ? $_POST['programId'] : '', 
	'milesHtml' => file_get_contents('miles.html'),
	'programName' => 'Delta Skymiles'
	
)));

/* PASSWORD ERROR EXAMPLE
 
die(json_encode(array(

	'success' => false,
	'programId' => isset($_POST['programId']) ? $_POST['programId'] : '', 
	'alert' => 'Password is incorrect'
	
)));
 
 */