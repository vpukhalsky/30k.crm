// Tips
function smalltips(ele)
{
	$(ele).find('.tips').each(function(){
	  $(this).qtip(
      {
         content: {
            text: false    
         },
         position: {
            corner: {
               target: 'rightMiddle',tooltip: 'leftMiddle'
            },
            adjust: {
               screen: true
            }
         },
         style: {
            tip: {
			 corner: 'topLeft', 
			 color: '#222',
			 size: {
				x: 8, y : 8
			 }
			},
            border: {
               width:1,
			   color: '#555'
            },
            name: 'dark'
         }
      });
	});
}

function wbImageHover(){
	$('.creditcard,.airline').hover(function(){
		if(!$(this).hasClass('actived'))
		{
			$(this).find('img').eq(1).fadeOut();
		}
	},function(){
		if(!$(this).hasClass('actived'))
		{
			$(this).find('img').eq(1).fadeIn();
		}
	});
}

function validation(){	
	$("#form_connect_account").validate({
		errorPlacement: function(error, element) {
			$('#form_connect_account_error').fadeIn().html('');
			errorElement: "div",
			error.appendTo($('#form_connect_account_error'));
		},
		submitHandler: function (form) {
			$('#form_connect_account_error').fadeOut();
			$('#loading_tips').fadeIn();
			setTimeout(function(){
				$('#loading_tips_i1').fadeOut(200);
				$('#loading_tips_i2').delay(200).fadeIn(200);
				var top = ($(window).height()-200)/2;
				var left = ($(window).width()-350)/2;
				$('#loading_tips').animate({'width':350, 'height':200, 'left':left, 'top':top});
			},3000);
		   	return false;
	   	},
		rules: {
			account: "required",
			password: "required"
		},
		messages: {
			account: "Please enter your account",
			password: "Please enter your password"
		}
	});
	
	
	$("#form_simulate").validate({
		errorPlacement: function(error, element) {
			$('#form_simulate_error').fadeIn().html('');
			errorElement: "div",
			error.appendTo($('#form_simulate_error'));
		},
		submitHandler: function (form) {
			$('#form_simulate_error').fadeOut();
			
		   	return false;
	   	},
		rules: {
			award: "required",
			status: "required",
			segments: "required",
			points: "required"
		},
		messages: {
			award: "Please enter your award",
			status: "Please enter your status",
			segments: "Please enter your segments",
			points: "Please enter your points"
		}
	});
	
	$("#form_choosedate").validate({
		errorPlacement: function(error, element) {
			$('#form_choosedate_error').fadeIn().html('');
			errorElement: "div",
			error.appendTo($('#form_choosedate_error'));
		},
		submitHandler: function (form) {
			$('#form_choosedate_error').fadeOut();
			//validation
			$.facebox.settings.closeImage = 'images/closelabel.png';
			$.facebox.settings.loadingImage = 'images/loading.gif';
			jQuery.facebox({ div: '#loading_popup'});
			//jump to search page
			//setTimeout(function(){
			//	window.location.href = "search.html";
			//},2000);
		   	return false;
	   	},
		rules: {
			from_input: "required",
			to_input: "required",
			l_depart_date_val: "required",
			l_return_date_val: "required"
		},
		messages: {
			from_input: "Please enter your departure city",
			to_input: "Please enter your arrive city",
			l_depart_date_val: "Please enter your departure date",
			l_return_date_val: "Please enter your return date"
		}
	});
}