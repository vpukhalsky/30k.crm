$(document).ready(function () {
	$('.progress_bar').each(function(){
		bar_value = $(this).attr('data-rel');
		$(this).progressbar({
            value: parseInt(bar_value)
        });
	});
	
	//edit account
	$('#change_account_info').click(function(){
		$('#account_view_status,#change_account_info').fadeOut(400);
		$('#account_edit_status,#save_account_info').delay(400).fadeIn(400);
	});
	
	$('#save_account_info').click(function(){
		$('#account_edit_status,#save_account_info').fadeOut(400);
		$('#account_view_status,#change_account_info').delay(400).fadeIn(400);
	});
	
	//edit mydream
	$('#change_dream').click(function(){
		$('#dream_view_status,#change_dream').fadeOut(400);
		$('#dream_edit_status,#save_dream').delay(400).fadeIn(400);
	});
	
	$('#save_dream').click(function(){
		$('#dream_edit_status,#save_dream').fadeOut(400);
		$('#dream_view_status,#change_dream').delay(400).fadeIn(400);
	});
	
	// Formate datepicker
	$( "#depart_date" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#return_date" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#return_date" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#depart_date" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	
	//remove credit card
	$('.account_creditcard .close').click(function(){
		$(this).parent().remove();
	});
	
	// Choose program
	$('.home_program li').click(function(){
		//do something
		$('#step1_1').fadeOut(400);
		$('#step1_2').delay(500).fadeIn(400);
	});
	
	// click add program button on step1-2
	$('#btn_adprogram1').click(function(){
		$('#step1_2').fadeOut(400);
		$('#step1_1').delay(500).fadeIn(400);
	});
	
	// click add program button on step1-3
	$('#btn_adprogram2').click(function(){
		$('#step1_3').fadeOut(400);
		$('#step1_1').delay(500).fadeIn(400);
		$('#home_my_program').delay(500).fadeIn(400);
	});
	
	//show add new program
	$('#link_new_program').click(function(){
		$('#account_new_program').fadeIn();
	});
	
	//show add new creditcard
	$('#link_new_creditcard').click(function(){
		$('#account_new_creditcard').fadeIn();
	});
	
	//edit program
	$('#account_program .edit').click(function(){
		$(this).parent().parent().find('.program_detail').fadeOut(200);
		$(this).parent().parent().find('.program_edit').delay(200).fadeIn(200);
	});
	
	//close program
	$('#account_program .close').click(function(){
		$(this).parent().parent().fadeOut();
	});
	
	// scrolling content
	$("#home_creditcard_program_wrap,.home_program").jScrollPane({autoReinitialise:true});
});
