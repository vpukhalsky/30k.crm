$(document).ready(function () {
	// Nice form for radio button
	$( "#routtype" ).buttonset();
	
	// Formate datepicker
	$( "#depart_date" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#return_date" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#return_date" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#depart_date" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	
	var now3 = new Date();
	$('.select_date').DatePicker({
		flat: true,
		format: 'd b, Y',
		date: [new Date(now3)],
		calendars: 2,
		mode: 'range',
		starts: 7,
		mindate: new Date(now3.getFullYear(),now3.getMonth(),now3.getDate()),
		onChange: function(formated) {
			$('#l_depart_date_val').val(formated[0]);
			$('#l_return_date_val').val(formated[1]);
		}
	});
	
	// Choose program
	$('.home_program li').click(function(){
		//do something
		$('#step1_1').fadeOut(400);
		$('#step1_2').delay(500).fadeIn(400);
	});
	
	//$('.connect_account .login').click(function(){
//		//do something
//		$('#step1_2').fadeOut(400);
//		$('#step1_3').delay(500).fadeIn(400);
//	});
	
	//Search Button
	//$('#search_btn input').click(function(){
//		//validation
//		$.facebox.settings.closeImage = 'images/closelabel.png';
//  		$.facebox.settings.loadingImage = 'images/loading.gif';
//		jQuery.facebox({ div: '#loading_popup'});
//		//jump to search page
//		setTimeout(function(){
//			window.location.href = "search.html";
//		},2000);
//		
//	});
	
	// Cancel Complete Profile
	$('#cancel_complete_profile').live('click',function(){
		$('#facebox #complete_profile').fadeOut(400);
		$('#facebox #complete_profile_ad').delay(400).fadeIn();
		
	});
	
	$('#step3_tab_container').easytabs({'updateHash':false});
	
	// Video popup box
	$('#home_video_play').click(function(){
		$.facebox.settings.closeImage = 'images/closelabel.png';
  		$.facebox.settings.loadingImage = 'images/loading.gif';
		jQuery.facebox({ div: '#video_popup'});
	});
	
	
	// click add program button on step1-2
	$('#btn_adprogram1').click(function(){
		$('#step1_2').fadeOut(400);
		$('#step1_1').delay(500).fadeIn(400);
	});
	
	// click add program button on step1-3
	$('#btn_adprogram2').click(function(){
		$('#step1_3').fadeOut(400);
		$('#step1_1').delay(500).fadeIn(400);
		$('#home_my_program').delay(500).fadeIn(400);
	});
	
	// scrolling content
	$("#home_creditcard_program_wrap,.home_program").jScrollPane({autoReinitialise:true});
	
	
	//Credit card selected
	$('.home_creditcard .creditcard').click(function(){
		$(this).find('img').eq(1).fadeOut();
		$(this).addClass('actived');
	});
	
	
	var availableTags = [
		"Shanghai",
		"Beijing"		
	];
	$('input[name="from_input"],input[name="to_input"]').autocomplete({
		source: availableTags
	});
	
	
	//remove credit card
	$('.account_creditcard .close').click(function(){
		$(this).parent().remove();
	});
	
	// Login popup box
	$('#facebox #signin_login').live('click',function(){
		$.facebox.settings.closeImage = 'images/closelabel.png';
  		$.facebox.settings.loadingImage = 'images/loading.gif';
		jQuery.facebox({ div: '#saveprogram_popup',settings: {closeImage:'images/close.png',loadingImage:'images/loading.gif'}});
	});
	
	
});

function show_reason2join(){
	$('#facebox #video_popup_box').fadeOut(400);
	$('#facebox #reason2join').delay(400).fadeIn(400);
}