$(document).ready(function () {
	
	
	// Airport detail tips
	$('.ticket_col1,.aireport_detail_tip').each(function(){
		$(this).qtip(
      {
         content: {
            // Set the text to an image HTML string with the correct src URL to the loading image you want to use
            text: $('#ticket_col1_ajax').html()
            //url: $(this).attr('rel'), // Use the rel attribute of each element for the url to load
            
         },
         position: {
            corner: {
               target: 'rightMiddle', // Position the tooltip above the link
               tooltip: 'leftMiddle'
            },
            adjust: {
               screen: true // Keep the tooltip on-screen at all times
            }
         },
         show: { 
            when: 'mouseenter', 
            solo: true // Only show one tooltip at a time
         },
         hide: {
			 fixed: true,
			 when: 'mouseleave'
		 },
         style: {
            tip: { // Now an object instead of a string
			 corner: 'topLeft', // We declare our corner within the object using the corner sub-option
			 color: '#ccc',
			 size: {
				x: 8, // Be careful that the x and y values refer to coordinates on screen, not height or width.
				y : 8 // Depending on which corner your tooltip is at, x and y could mean either height or width!
			 }
			},
            border: {
               width: 1,
               radius: 2,
			   color: '#ccc'
            },
            name: 'light', // Use the default light style
            width: 530 // Set the tooltip width
         }
      });
	});
	
	// Price detail tips
	$('.ticket_table .price_box').each(function(){
	  $(this).qtip(
      {
         content: {
            text: $('#price_detail_ajax').html()           
         },
         position: {
            corner: {
               target: 'rightMiddle',tooltip: 'leftMiddle'
            },
            adjust: {
               screen: true
            }
         },
         show: { 
            when: 'mouseenter',solo: true
         },
         hide: {
			 fixed: true,when: 'mouseleave'
		 },
         style: {
            tip: {
			 corner: 'topLeft', 
			 color: '#ccc',
			 size: {
				x: 8, y : 8
			 }
			},
            border: {
               width: 1,radius: 2,color: '#ccc'
            },
            name: 'light', width: 400
         }
      });
	});
	
	
	// Miles detail tips
	$('.ticket_table .miles_box, .miles_detail_tip').each(function(){
	  $(this).qtip(
      {
         content: {
            text: $('#miles_detail_ajax').html()           
         },
         position: {
            corner: {
               target: 'rightMiddle',tooltip: 'leftMiddle'
            },
            adjust: {
               screen: true
            }
         },
         show: { 
            when: 'mouseenter',solo: true
         },
         hide: {
			 fixed: true,when: 'mouseleave'
		 },
         style: {
            tip: {
			 corner: 'topLeft', 
			 color: '#ccc',
			 size: {
				x: 8, y : 8
			 }
			},
            border: {
               width: 1,radius: 2,color: '#ccc'
            },
            name: 'light', width: 325
         }
      });
	});
	
	chooseTicket();
});

// Choose ticket
function chooseTicket(){
	$('#departure_ticket .item').click(function(){
		$('#departure_ticket .selected').remove();
		$('#depart_selected_info').fadeIn();
		$(this).append('<div class="selected"><strong>Selected</strong></div>');
		var className = $(this).parent().attr('class');
		var a = className.match(/\d/g).join("");
		$('#ticket_detail .class'+a).animate({'opacity':1});
	});
	
	$('#return_ticket .item').click(function(){
		$('#return_ticket .selected').remove();
		$('#return_selected_info').fadeIn();
		$(this).append('<div class="selected"><strong>Selected</strong></div>');
	});

	$('#select_depart_btn').click(function(){
		$('#departure_ticket').fadeOut(400);
		$('#return_ticket').delay(400).fadeIn(400);
		$('#departure_timeline').fadeIn();
		var paddingTop = $('#search_result_wrap').css('padding-top');
		var height = $('#ticket_detail').height();
		$('#search_result_wrap').animate({'padding-top':parseInt(paddingTop)+38});
		$('#ticket_detail').animate({'height':parseInt(height)+38});
	});
	
	$('#select_return_btn').click(function(){
		$('#return_ticket').fadeOut(400);
		$('#timeline_info').delay(400).fadeIn(400);
		$('#return_timeline').fadeIn();
		$('#search_result_wrap').animate({'padding-top':335});
		$('#ticket_detail').animate({'height':335});
	});
	
	$('#departure_timeline_change').click(function(){
		$('#return_ticket').fadeOut(400);
		$('#timeline_info').fadeOut(400);
		$('#departure_ticket').delay(400).fadeIn(400);
	});
	
	$('#return_timeline_change').click(function(){
		$('#departure_ticket').fadeOut(400);
		$('#timeline_info').fadeOut(400);
		$('#return_ticket').delay(400).fadeIn(400);
	});
	
	$('.add_compare_btn').click(function(){
		$('#ticket_detail').effect("transfer", { to: $("#tab_compare") }, 1000);
		$.scrollTo( 0, 800 );
	});
}