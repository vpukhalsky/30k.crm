$(document).ready(function () {
	
	// Price range slider
	$( "#price_range" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 0, 500 ],
		slide: function( event, ui ) {
			$( "#l_price_range" ).html( "$" + ui.values[ 0 ] + " <span>-</span> $" + ui.values[ 1 ] );
		}
	});
	$( "#l_price_range" ).html( "$" + $( "#price_range" ).slider( "values", 0 ) + " <span>-</span> $" + $( "#price_range" ).slider( "values", 1 ) );
	
	// Miles range slider
	$( "#miles_range" ).slider({
		range: true,
		min: 0,
		max: 5000,
		values: [ 0, 5000 ],
		slide: function( event, ui ) {
			$( "#l_miles_range" ).html( ui.values[ 0 ] + "mi <span>-</span> " + ui.values[ 1 ] + "mi");
		}
	});
	$( "#l_miles_range" ).html( $( "#miles_range" ).slider( "values", 0 ) + "mi <span>-</span> " + $( "#miles_range" ).slider( "values", 1 ) + "mi") ;
	
	// Duration slider
	$( "#duration_range" ).slider({
		range: true,
		min: 0,
		max: 48,
		values: [ 0, 48 ],
		slide: function( event, ui ) {
			$( "#l_duration_range" ).html( ui.values[ 0 ] + "h <span>-</span> " + ui.values[ 1 ] + "h");
		}
	});
	$( "#l_duration_range" ).html( $( "#duration_range" ).slider( "values", 0 ) + "h <span>-</span> " + $( "#duration_range" ).slider( "values", 1 ) + "h") ;
	
	// Time slider		
	$(".time_range").slider({
        range: true,
        min: 0,
        max: 1440,
		values: [ 0, 1440 ],
        step: 15,
        slide: function(e, ui) {
            var hours = Math.floor(ui.values[0] / 60);
            var minutes = ui.values[0] - (hours * 60);
            if(hours.toString().length == 1) hours = '0' + hours;
            if(minutes.toString().length == 1) minutes = '0' + minutes;			
			var hours2 = Math.floor(ui.values[1] / 60);
            var minutes2 = ui.values[1] - (hours2 * 60);
            if(hours2.toString().length == 1) hours2 = '0' + hours2;
            if(minutes2.toString().length == 1) minutes2 = '0' + minutes2;
            $(this).next().html(hours+':'+minutes + "<span>-</span>" + hours2 + ':'+ minutes2);
        }
    });
	
	// Airport detail tips
	$('.ticket_col1,.aireport_detail_tip').each(function(){
		$(this).qtip(
      {
         content: {
            // Set the text to an image HTML string with the correct src URL to the loading image you want to use
            text: $('#ticket_col1_ajax').html()
            //url: $(this).attr('rel'), // Use the rel attribute of each element for the url to load
            
         },
         position: {
            corner: {
               target: 'rightMiddle', // Position the tooltip above the link
               tooltip: 'leftMiddle'
            },
            adjust: {
               screen: true // Keep the tooltip on-screen at all times
            }
         },
         show: { 
            when: 'mouseenter', 
            solo: true // Only show one tooltip at a time
         },
         hide: {
			 fixed: true,
			 when: 'mouseleave'
		 },
         style: {
            tip: { // Now an object instead of a string
			 corner: 'topLeft', // We declare our corner within the object using the corner sub-option
			 color: '#ccc',
			 size: {
				x: 8, // Be careful that the x and y values refer to coordinates on screen, not height or width.
				y : 8 // Depending on which corner your tooltip is at, x and y could mean either height or width!
			 }
			},
            border: {
               width: 1,
               radius: 2,
			   color: '#ccc'
            },
            name: 'light', // Use the default light style
            width: 700 // Set the tooltip width
         },
		api: {
			onContentUpdate:function() {
			   smalltips('.ticket_timeline');
			}
		}
      });
	});
	
	// Price detail tips
	$('.ticket_table .price_box').each(function(){
	  $(this).qtip(
      {
         content: {
            text: $('#price_detail_ajax').html()        
         },
         position: {
            corner: {
               target: 'rightMiddle',tooltip: 'leftMiddle'
            },
            adjust: {
               screen: true
            }
         },
         show: { 
            when: 'mouseenter',solo: true
         },
         hide: {
			 fixed: true,when: 'mouseleave'
		 },
         style: {
            tip: {
			 corner: 'topLeft', 
			 color: '#ccc',
			 size: {
				x: 8, y : 8
			 }
			},
            border: {
               width: 1,radius: 2,color: '#ccc'
            },
            name: 'light', width: 400
         },
		 api: {
			onContentUpdate:function() {
			   smalltips('.ticket_data');
			}
		}
      });
	});
	
	
	// Miles detail tips
	$('.ticket_table .miles_box, .miles_detail_tip').each(function(){
	  $(this).qtip(
      {
         content: {
            text: $('#miles_detail_ajax').html()          
         },
         position: {
            corner: {
               target: 'rightMiddle',tooltip: 'leftMiddle'
            },
            adjust: {
               screen: true
            }
         },
         show: { 
            when: 'mouseenter',solo: true
         },
         hide: {
			 fixed: true,when: 'mouseleave'
		 },
         style: {
            tip: {
			 corner: 'topLeft', 
			 color: '#ccc',
			 size: {
				x: 8, y : 8
			 }
			},
            border: {
               width: 1,radius: 2,color: '#ccc'
            },
            name: 'light', width: 325
         },
		 api: {
			onContentUpdate:function() {
			   smalltips('.miles_detail');
			}
		}
      });
	});
	
	// Miles detail tips - 2012-11-9
	$('#departure_ticket .ticket_table .item').each(function(){
	  $(this).qtip(
      {
		 
         content: {
            text: $('#better_fare_ajax').html(),
			title: {
				text: '&nbsp;',
				button: 'Close'
         	},        
         },
         position: {
			target: $('#departure_ticket .ticket_table .item').eq(1), // Specify the number of the item
            corner: {
               target: 'topMiddle',tooltip: 'bottomMiddle'
            },
            adjust: {
               screen: true
            }
         },
         show: { 
            when: 'click',solo: true
         },
		 hide: {
			when: {
				event: 'click'
			}	 
		 },
         style: {
            tip: {
			 corner: 'topLeft', 
			 color: '#ccc',
			 size: {
				x: 8, y : 8
			 }
			},
            border: {
               width: 1,radius: 2,color: '#ccc'
            },
            name: 'light', width: 225
         }
      });
	});
	
	$('#return_ticket .ticket_table .item').each(function(){
	  $(this).qtip(
      {
		 
         content: {
            text: $('#better_fare_ajax').html(),
			title: {
				text: '&nbsp;',
				button: 'Close'
         	},        
         },
         position: {
			target: $('#return_ticket .ticket_table .item').eq(1), // Specify the number of the item
            corner: {
               target: 'topMiddle',tooltip: 'bottomMiddle'
            },
            adjust: {
               screen: true
            }
         },
         show: { 
            when: 'click',solo: true
         },
		 hide: {
			when: {
				event: 'click'
			}	 
		 },
         style: {
            tip: {
			 corner: 'topLeft', 
			 color: '#ccc',
			 size: {
				x: 8, y : 8
			 }
			},
            border: {
               width: 1,radius: 2,color: '#ccc'
            },
            name: 'light', width: 225
         }
      });
	});
	
	// Formate datepicker
	$( "#depart_date" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#return_date" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#return_date" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#depart_date" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	
	// Open search item
	openSearchItem();
	
	// Close search item
	closeSearchItem();
	
	chooseTicket();
	
	niceForm();
	
	// Filter Class
	filterClass();
	
	// Show more
	showMoreFilter();
	
	// Click new search btn
	$('#btn_new_search').click(function(){
		$.facebox.settings.closeImage = 'images/closelabel.png';
  		$.facebox.settings.loadingImage = 'images/loading.gif';
		jQuery.facebox({ ajax: 'ajax/newsearch.html'});
	});
	
	
	
});

var currentPos;
var rowcount = 2;
var departTimelinesCount = 2;
var returnTimelinesCount = 1;
function openSearchItem(){
	$('#search_bestdeal').click(function(){
		$(this).fadeOut();
		$('#ad250x250_search').fadeOut(function(){
			$('#search_result_wrap').animate({'padding-top':350});
			$('#ticket_detail').css({'opacity':0,'display':'block'});
			$('#ticket_detail').addClass('red_box');
			$('#ticket_detail').animate({'opacity':1,'width':776,'height':336}); // Ticket table need load using ajax, the height need calculate by row count
			currentPos = -1;	
		});
	});
	
	$('#search_result .item_box').bind('click',_openItem);
}

function _openItem(){
	$('#search_result .item_box').unbind('click');
	var wrap = $('#search_result');
	var currentitem = $(this);
	var temp_item = $(this).clone().addClass('temp_item');
	$('#ticket_detail').removeClass('red_box');
	if(currentPos != undefined)
	{
		if(currentPos == -1)
		{
			$('#search_bestdeal').fadeIn();
			$('#ad250x250_search').fadeIn();
		}
		var current_top = $('#search_result .item_box').eq(currentPos)[0].offsetTop;
		var current_left = $('#search_result .item_box').eq(currentPos)[0].offsetLeft;
		$('#ticket_detail').animate({'opacity':0,'width':236,'height':200});
		$('#search_result .temp_item').animate({'opacity':1,'top':current_top,'left':current_left});
		$('#search_result .item_box').eq(currentPos).css({'opacity':0}).animate({'width':236,'margin-right':34},function(){
			$(this).css('opacity',1);
			$('#search_result .temp_item').remove();
			temp_item.appendTo(wrap);
			var left = currentitem[0].offsetLeft;
			var top = currentitem[0].offsetTop;
			temp_item.css({'position':'absolute','left':left,'top':top});
			loadTableData();
			$('#search_result_wrap').animate({'padding-top':263+(90*rowcount)}); // Ticket table need load using ajax, the height need calculate by row count
			temp_item.animate({'top':0});
			temp_item.queue();
			temp_item.animate({'left':0},function(){
				currentitem.animate({'width':0,'margin-right':0});
				temp_item.animate({'opacity':0});
				$('#ticket_detail').css({'opacity':0,'display':'block'});
				$('#ticket_detail').animate({'opacity':1,'width':776,'height':243+(90*rowcount)}); // Ticket table need load using ajax, the height need calculate by row count
				$.scrollTo( '#ticket_detail', 800 );
				if($.browser.chrome)
				{
					window.location.href = "#ticket_detail";
				}
				$('#search_result .item_box').bind('click',_openItem);
			});	
		});
	}
	else
	{
		var left = $(this)[0].offsetLeft;
		var top = $(this)[0].offsetTop;
		temp_item.appendTo(wrap);
		temp_item.css({'position':'absolute','left':left,'top':top});
		$(this).css({'opacity':0});
		loadTableData();
		$('#search_result_wrap').animate({'padding-top':263+(90*rowcount)});
		temp_item.animate({'top':0});
		temp_item.queue();
		temp_item.animate({'left':0},function(){
			currentitem.animate({'width':0,'margin-right':0});
			temp_item.animate({'opacity':0});
			$('#ticket_detail').css({'opacity':0,'display':'block'});
			$('#ticket_detail').animate({'opacity':1,'width':776,'height':243+(90*rowcount)});// Ticket table need load using ajax, the height need calculate by row count
			$.scrollTo( '#ticket_detail', 800 );
			if($.browser.chrome)
			{
				window.location.href = "#ticket_detail";
			}
			$('#search_result .item_box').bind('click',_openItem);
		});	
	}
	currentPos = $.inArray(this,$('#search_result .item_box'));
}

function closeSearchItem(){
	$('#ticket_detail .close').click(function(){
		if(currentPos == -1)
		{
			$('#search_bestdeal').fadeIn();
			$('#ad250x250_search').fadeIn();
			$('#ticket_detail').removeClass('red_box');
		}
		$('#ticket_detail').fadeOut(function(){
			$('#search_result_wrap').animate({'padding-top':0});
			$('#search_result .item_box').eq(currentPos).animate({'width':236,'margin-right':34,'opacity':1}).fadeIn();
			currentPos = undefined;
			$('#search_result .temp_item').remove();
		});
		
	});
}

// Choose ticket
function chooseTicket(){
	$('#departure_ticket .item').click(function(){
		filterClassInit();
		$('#departure_ticket .selected').remove();
		$('#depart_selected_info').fadeIn();
		$(this).append('<div class="selected"><strong>Selected</strong></div>');
		$(this).parent().animate({'opacity':1});
	});
	
	$('#return_ticket .item').click(function(){
		$('#return_ticket .selected').remove();
		$('#return_selected_info').fadeIn();
		$(this).append('<div class="selected"><strong>Selected</strong></div>');
	});

	$('#select_depart_btn').click(function(){
		$('#departure_ticket').fadeOut(400);
		$('#return_ticket').delay(400).fadeIn(400);
		$('#departure_timeline').fadeIn();
		var paddingTop = $('#search_result_wrap').css('padding-top');
		var height = $('#ticket_detail').height();
		$('#search_result_wrap').animate({'padding-top':parseInt(paddingTop)+40});
		$('#ticket_detail').animate({'height':parseInt(height)+40});
		$('.qtip').hide(); // close all the tooltips 2012-11-13
	});
	
	$('#select_return_btn').click(function(){
		$('#return_ticket').fadeOut(400);
		$('#timeline_info').delay(400).fadeIn(400);
		$('#return_timeline').fadeIn();
		$('#search_result_wrap').animate({'padding-top':263 + 40*departTimelinesCount + 40*returnTimelinesCount});// Ticket table need load using ajax, the height need calculate by row count
		$('#ticket_detail').animate({'height':243 + 40*departTimelinesCount + 40*returnTimelinesCount});// Ticket table need load using ajax, the height need calculate by row count
		
		$('.qtip').hide(); // close all the tooltips 2012-11-13
	});
	
	$('#departure_timeline_change').click(function(){
		$('#timeline_info,#departure_timeline,#return_ticket,#return_timeline').fadeOut(400);
		$('#departure_ticket').delay(400).fadeIn(400);
		$('#departure_timeline').fadeOut();
		var paddingTop = 263+(90*rowcount);// Ticket table need load using ajax, the height need calculate by row count
		var height = 243+(90*rowcount);// Ticket table need load using ajax, the height need calculate by row count
		$('#search_result_wrap').animate({'padding-top':parseInt(paddingTop)});
		$('#ticket_detail').animate({'height':parseInt(height)});
	});
	
	$('#return_timeline_change').click(function(){
		$('#timeline_info,#return_timeline,#departure_ticket').fadeOut(400);
		$('#return_ticket,#departure_timeline').delay(400).fadeIn(400);
		var paddingTop = 263+(90*rowcount);// Ticket table need load using ajax, the height need calculate by row count
		var height = 243+(90*rowcount);// Ticket table need load using ajax, the height need calculate by row count
		$('#search_result_wrap').animate({'padding-top':parseInt(paddingTop)+40});
		$('#ticket_detail').animate({'height':parseInt(height)+40});
	});
	
	$('.add_compare_btn').click(function(){
		$('#ticket_detail').effect("transfer", { to: $("#tab_compare") }, 1000);
		$.scrollTo( 0, 800 );
		if($.browser.chrome)
		{
			window.location.href = "#header";
		}
	});
}

// Load table data
function loadTableData(){
	
	// this is only for html demo, should be loaded ajax content replace the old table
	$('#departure_timeline').hide();
	$('#return_timeline').hide();
	$('#return_ticket').hide();
	$('#timeline_info').hide();
	$('#departure_ticket').show();
	
}

// Nice form
function niceForm(){
	$( "#consider_type" ).buttonset();
	$( "#bonusmiles_type" ).buttonset();
}

// Filter Class,  set opacity for table col when uncheck the filter class
function filterClass(){
	
	// when click
	$('input[name="filter_class"]').click(function(){
		var val = $(this).val();
		var checked = $(this).attr('checked');
		if(checked)
		{
			$('#ticket_detail .class'+val).animate({'opacity':1});
		}
		else
		{
			$('#ticket_detail .class'+val).animate({'opacity':0.5});
		}
	});
	
	filterClassInit();
}

function filterClassInit(){
	// inital
	$('input[name="filter_class"]').each(function(){
		var val = $(this).val();
		var checked = $(this).attr('checked');
		if(checked)
		{
			$('#ticket_detail .class'+val).animate({'opacity':1});
		}
		else
		{
			$('#ticket_detail .class'+val).animate({'opacity':0.5});
		}
	});
}

function showMoreFilter(){
	$('.showmore').click(function(){
		$(this).parent().find('.hidden').fadeIn();
		$(this).hide();
	});
	
	$('#filter_program').click(function(){
		$('#filter_program_box').fadeIn(function(){
			$('body').click(function(e){
				if(e.target.parentElement.id !== 'filter_program_box')
				{
					$('#filter_program_box').fadeOut();
					$('#filter_program').removeClass('actived');
					$('body').unbind('click');
				}
			});
		});
		$(this).addClass('actived');
		$('#filter_creditcard_box').fadeOut();
		$('#filter_creditcard').removeClass('actived');
	});
	
	$('#filter_creditcard').click(function(){
		$('#filter_creditcard_box').fadeIn(function(){
			$('body').click(function(e){
			if(e.target.parentElement.id !== 'filter_creditcard_box')
				{
					$('#filter_creditcard_box').fadeOut();
					$('#filter_creditcard').removeClass('actived');
					$('body').unbind('click');
				}
			});
		});
		$(this).addClass('actived');
		$('#filter_program_box').fadeOut();
		$('#filter_program').removeClass('actived');
	});
	
	$('#filter_program_apply').click(function(){
		$('#filter_program_box').fadeOut();
		$('#filter_program').removeClass('actived');
	});
	
	$('#filter_creditcard_apply').click(function(){
		$('#filter_creditcard_box').fadeOut();
		$('#filter_creditcard').removeClass('actived');
	});
	
	// filter type selector 2012-11-8 Tony
	$('#search_filter .filter_type a').click(function(){
		$(this).addClass('actived');
		$(this).siblings().removeClass('actived');
	});
}