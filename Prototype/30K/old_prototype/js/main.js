$(document).ready(function () {
	init();
});

function init(){	
	// Nice form
	if(!/msie|MSIE 6/.test(navigator.userAgent)){
		$("input[type='radio'], input[type='checkbox'], textarea").uniform();
    }
	
	$("select").select2();
	
	function format(state) {
		return '<img src="images/icon_'+state.text.replace(' ','').toLowerCase() + '.gif" />' + state.text;
	}
	$("#form_simulate select").select2({
		formatResult: format,
		formatSelection: format
	});
	
	// Login popup box
	$('.signin').click(function(){
		$.facebox.settings.closeImage = 'images/closelabel.png';
  		$.facebox.settings.loadingImage = 'images/loading.gif';
		jQuery.facebox({ div: '#signin_popup',settings: {closeImage:'images/close.png',loadingImage:'images/loading.gif'}});
	});
	
	// FAQ Accordion
	$( "#faq_wrap" ).accordion({
		heightStyle: "content"
	});
	
	// Tips
	smalltips('body');
	
	// Creditcard hover
	wbImageHover();
	
	
	// Login Loading
	$('#step1_login').click(function(){
		$('#loading_tips').fadeIn();
		setTimeout(function(){
			$('#loading_tips_i1').fadeOut(200);
			$('#loading_tips_i2').delay(200).fadeIn(200);
			var top = ($(window).height()-200)/2;
			var left = ($(window).width()-350)/2;
			$('#loading_tips').animate({'width':350, 'height':200, 'left':left, 'top':top});
		},3000);
	});
	
	$('#update_btn_no').click(function(){
		$('#loading_tips_i2').fadeOut(200);
		$('#loading_tips_i3').delay(200).fadeIn(200);
		$('#loading_tips').animate({'width':160, 'height':65, 'left':5, 'top':5});
	});
	$('#update_btn_yes').click(function(){
		$('#loading_tips').fadeOut(200);
		$('#sigin_tips').fadeIn();
	});
	$('#sigin_tips .close').click(function(){
		$('#sigin_tips').fadeOut();
	});
	
	//form validation
	validation();
}