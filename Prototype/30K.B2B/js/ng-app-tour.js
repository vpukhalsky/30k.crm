'use strict';

// Declare app level module which depends on filters, and services
angular.module('30k.tour', [
  'mm.foundation',
  'ngSanitize'
])

.filter('price', ['$filter', function($filter){
    return function(i){
        return '$' + $filter('number')(i, 0);
    };
}])

.filter('duration', function(){
    return function(d){
        var min = (d % 60);
        return Math.floor(d / 60) + 'h ' + (min < 10 ? '0' : '') + min + 'm';
    };
})

.filter('dt', ['dateFilter', function(dateFilter){
    return function(d, f){
        if(!angular.isObject(d)) d = new Date(d);
        var n = new Date();
        var o = n.getTimezoneOffset();
        d.setMinutes(d.getMinutes() + o);
        return dateFilter(d, f);
    };
}])

.directive('kkTour', ['$q', '$http', '$templateCache', '$timeout', '$interval', function($q, $http, $templateCache, $timeout, $interval){

    return{
        scope: {
            step: '=',
            steps: '=',
            templateUrl: '@?'
        },
        templateUrl: 'partials/tour.html',
        controller: function($scope, $element){
            
            var tipSize = 8;
            var scrollOffset = 25;
            var optionalSize = 48;
            
            var frameDuration = 100;
            var frames = 8;
            var frameSize = 48;
            
            var prevNextPromise = null;
            
            $scope.overlay = false;
            
            $scope.initialized = false;
            
            $(window).on('scroll', onScroll);
            
            var currentTip = null;
            
            $scope.getClass = function(tip){
                
                var cls = { modal: !tip.attachTo, visible: tip.show && !tip.bubble, smooth: tip.smoothScroll };
                if(angular.isString(tip.cls)) cls[tip.cls] = true;
                return cls;
                
            };
            
            $scope.getPct = function(index){
                return Math.round((index / $scope.steps.length) * 100) + '%';
            };
            
            function onScroll(){
                
                if(!currentTip || currentTip.show !== true || !currentTip.attachTo) return;
                
                $scope.$apply(function(){
                    currentTip.smoothScroll = true;
                    if(!isBubble(currentTip)) setupPos(currentTip);
                    else setupOptionalPos(currentTip);
                });
                
            }
            
            $scope.$watch('step', function(newValue, oldValue){
                if(newValue >= 0 && (oldValue < 0 || oldValue === undefined || oldValue === null)) start();
            });
            
            $scope.next = function(){
                next();
            };
            
            $scope.prev = function(){
                prev();
            };
            
            function start(){
                
                if($scope.steps.length > 0){
                    
                    var id = 0;
                    
                    $scope.steps.forEach(function(st){ 
                        
                        st.id = id++;
                        st.show = false; 
                        
                    });
                    
                    $scope.initialized = true;
                    show($scope.steps[$scope.step]);
                    
                }
                
            }
            
            $scope.showOptionalTip = function(opt){
                
                opt.selected = true;
                show(opt);

            };
            
            function setupOptionalPos(opt){
                
                var wrapper = angular.element(opt.attachTo);
                var offset = wrapper.offset();
                var left = 0;
                var top = 0;
                var position = 'top';
                if(opt.position) position = opt.position;
                
                switch(position){
                    case 'top':
                        top = offset.top - optionalSize;
                        left = offset.left + (wrapper.outerWidth() - optionalSize) / 2;
                        break;
                    case 'bottom':
                        top = offset.top + wrapper.outerHeight();
                        left = offset.left + (wrapper.outerWidth() - optionalSize) / 2;
                        break;
                    case 'left':
                        top = offset.top + (wrapper.outerHeight() - optionalSize) / 2;
                        left = offset.left - optionalSize;
                        break;
                    case 'right':
                        top = offset.top + (wrapper.outerHeight() - optionalSize) / 2;
                        left = offset.left + wrapper.outerWidth();
                        break;
                }
                
                if(!angular.isObject(opt.style)) opt.style = {};
                
                opt.style.top = top + 'px';
                opt.style.left = left + 'px';
                opt.style.width = optionalSize + 'px';
                opt.style.height = optionalSize + 'px';
                opt.style.lineHeight = optionalSize + 'px';
                
                var rect = {
                    top: top,
                    left: left,
                    width: optionalSize,
                    height: optionalSize
                };
                
                return rect;
            }
            
            function isBubble(config){
                return config.bubble === true;
            }
            
            function delay(config, func){
                
                var del = 0;
                if(angular.isNumber(config.delay)) del = 1000 * config.delay; 
                
                var promise = $timeout(func, del);
                
                return promise;
                
            }
            
            function startAnimation(config){
                
                if(!config.interval){
                    config.interval = $interval(function(){
                        
                        if(!config.show) return;
                        if(!config.frame) config.frame = 0;
                        config.frame = (config.frame + 1) % frames;
                        var left = config.frame * frameSize;
                        config.style.backgroundPosition = -left + 'px 0';
                        
                    }, frameDuration);
                }
                
            }
            
            function stopAnimation(config){
                
                if(config.interval){
                    
                    $interval.cancel(config.interval);
                    config.frame = 0;
                    config.interval = false;
                    
                }
                
            }
            
            function showBubble(opt){
                
                var promise =delay(opt, function(){
                
                    opt.show = true;
                    var rect = setupOptionalPos(opt);
                    scroll(opt, rect);
                    startAnimation(opt);
                    
                });
                
                return promise;
                
            }
            
            function showRegular(config){
                
                var promise = $timeout(function(){});
                
                config.show = true;
                if(!config.style) config.style = {};
                
                if(!config.attachTo){
                    if(config.top) config.style.top = config.top;
                }else{
                    promise = delay(config, function(){
                        var rect = setupPos(config);
                        scroll(config, rect);
                    });
                }
                
                return promise;
                
            }
            
            function show(config){
                
                var promise = prevNextPromise;
                if(!promise) promise = $timeout(function(){});
                
                promise.then(function(){

                    var afterPromise = null;
                    
                    if(isBubble(config)){
                        afterPromise = showBubble(config);
                    }else{
                        afterPromise = showRegular(config);
                    }
                    
                    afterPromise.then(function(){
                    
                        afterShow(config);
                        
                    });
                    
                    prevNextPromise = null;
                    currentTip = config; 
                    
                });
                
            }
            
            function afterShow(config){
                    
                var tip = config;
                
                if(tip.overlay){
                    var onTop = $(tip.overlay);
                    onTop.addClass('stay-on-top');
                    if(onTop.css('position') == 'static') onTop.addClass('rel');
                    $scope.overlay = true;
                }
                
                if(angular.isString(tip.highlight)){
                    angular.element(tip.highlight).addClass('highlight');
                }
                
                if(tip.action != 'button' && angular.isString(tip.target)){
                    angular.element(tip.target).on(tip.action, next);
                }
                
            }
            
            function scroll(config, stepRect){
                
                var el = angular.element(config.attachTo);
                var targetOffset = el.offset();
                var targetHeight = el.outerHeight();
                targetOffset.bottom = targetOffset.top + targetHeight;
                
                stepRect.bottom = stepRect.top + stepRect.height;
                
                var windowHeight = $(window).height() - 2 * scrollOffset;
                var windowTop = $(window).scrollTop() + scrollOffset;
                var windowBottom = windowTop + windowHeight;
                
                var top = stepRect.top;
                var bottom = stepRect.bottom;
                
                if(targetHeight < windowHeight){
                    top = targetOffset.top < stepRect.top ? targetOffset.top : stepRect.top;
                    bottom = targetOffset.bottom > stepRect.bottom ? targetOffset.bottom : stepRect.bottom;
                }
                
                var move = 0;
                
                if(top < windowTop || top > windowBottom){
                    move = -windowTop + top;
                }else if(bottom > windowBottom){
                    move = windowBottom - bottom;
                    if(windowTop - move < top) move = top - windowTop + move;
                }
                
                var scrollTop = windowTop + move;

                $('html,body').animate({ scrollTop: scrollTop + 'px' }, 500);
                
            }
            
            function setupPos(config){
                
                var el = angular.element(config.attachTo);
                var targetOffset = el.offset();
                var targetHeight = el.outerHeight();
                var targetWidth = el.outerWidth();
                
                var step = getStep(config);
                var stepHeight = step.outerHeight();
                var stepWidth = step.outerWidth();
                if(!config.position) config.position = 'top';
                
                var left = 0;
                var top  = 0;
                
                switch(config.position){
                    case 'top':
                        top = targetOffset.top - stepHeight - tipSize;
                        left = targetOffset.left;
                        break;
                    case 'bottom':
                        top = targetOffset.top + targetHeight + tipSize;
                        left = targetOffset.left;
                        break;
                    case 'left':
                        top = targetOffset.top;
                        left = targetOffset.left - stepWidth - tipSize;
                        break;
                    case 'right':
                        top = targetOffset.top;
                        left = targetOffset.left + targetWidth + tipSize;
                        break;
                }
                
                config.style.top = top + 'px';
                config.style.left = left + 'px';
                
                return {
                    top: top,
                    left: left,
                    width: stepWidth,
                    height: stepHeight
                };
            }
            
            function getStep(config){
                return angular.element('#tour-step-' + config.id);
            }
            
            function cancelPrevNextPromise(){
                
                if(prevNextPromise){
                    prevNextPromise.reject('cancel');
                    prevNextPromise = null;
                }
                
            }
            
            function next(){
                
                hide($scope.steps[$scope.step]);
                
                if(angular.isFunction($scope.steps[$scope.step].onNext)){
                    cancelPrevNextPromise();
                    prevNextPromise = $scope.steps[$scope.step].onNext();
                }
                
                if($scope.step < $scope.steps.length - 1){
                    $scope.step++;
                    show($scope.steps[$scope.step]);
                }
                
            }
            
            function prev(){
                
                if($scope.step > 0){
                    hide($scope.steps[$scope.step]);
                    
                    if(angular.isFunction($scope.steps[$scope.step].onPrevious)) {
                        cancelPrevNextPromise();
                        prevNextPromise = $scope.steps[$scope.step].onPrevious();
                    }
                    
                    do{
                        $scope.step--;
                    }while($scope.step > 0 && $scope.steps[$scope.step].bubble);
                    
                    show($scope.steps[$scope.step]);
                }
                
            }
            
            function hide(config){
                
                config.show = false;
                
                var tip = config;
                
                tip.show = false;
                tip.smoothScroll = false;
                if(!tip.style) tip.style = {};
                if(!tip.attachTo){
                    if(tip.top) tip.style.top = '100%';
                }else{
                    tip.style.top = '-1000px';
                }
                
                if(angular.isString(tip.highlight)){
                    angular.element(tip.highlight).removeClass('highlight');
                }
                
                if(tip.action != 'button' && angular.isString(tip.target)){
                    angular.element(tip.target).off(tip.action, next);
                }
                
                if(tip.overlay){
                    $(tip.overlay).removeClass('stay-on-top').removeClass('rel');
                }
                
                if(isBubble(tip)) stopAnimation(tip);
                
                $scope.overlay = false;
                currentTip = null;
                
            }
            
        }
       
    };
    
}])

.filter('username', function(){
    return function(name, placeholder){
        
        if(!name) return placeholder;
        return name.replace(' ', '_').toLowerCase();
        
    };
})

.controller('LastStepController', ['$scope', '$interval', '$timeout', function($scope, $interval, $timeout){
    
    $scope.selected = null;
    
    $scope.onMouseOver = function(airline){
        airline.hover = true;  
    };
    
    $scope.onMouseLeave = function(airline){
        airline.hover = false;
    };
    
    $scope.getAirlineBgPos = function(airline){
        
        if((!$scope.selected || airline.id != $scope.selected.id) && !airline.hover){
            return airline.bgPos;
        }else{
            var parts = airline.bgPos.split(' ');
            return parts[0] + ' 0';
        }
        
    };
    
    $scope.select = function(airline){
        
        $scope.selected = airline;
        
        $timeout(function(){
            initTwitter(airline);    
        });
        
    };
    
    function initTwitter(selected) {

        var placeholder = $('#twPlaceholder');

        // Remove existing iframe
        $('iframe#twitter-mention-button').remove();

        //var text = "You should add this feature: @30kdotcom add-on let's you see how many miles each flight earns! Check www.30k.com";
        var text = "Add frequent flyer miles to your flight results. There are "
            + "45646"
            + " frequent flyers like me around the world who can benefit. Check @30kdotcom and www.30k.com";

        // Generate new markup
        var tweetBtn = $('<a></a>')
            .addClass('twitter-mention-button')
            .attr('href', 'https://twitter.com/intent/tweet?screen_name=' + selected.id)
            .attr('id', 'twitter-mention-button')
            .attr('data-lang', 'en')
            .attr('data-size', 'large')
            .attr('data-text', text);

        placeholder.html(tweetBtn);

        if (twttr && twttr.widgets) {
            twttr.widgets.load();
        }

    }
    
    $interval(function(){
        if(angular.isNumber($scope.tip.counter)) $scope.tip.counter++;
    }, 2000);
    
}])

.controller('TourController', ['$scope', '$interval', '$timeout', '$modal', '$templateCache', '$http', '$q', '$compile', 'usernameFilter', function($scope, $interval, $timeout, $modal, $templateCache, $http, $q, $compile, usernameFilter){
    
    $scope.config = {
        counter: GLOBALS.counter,
        twitterAirlines: [{
            id: 'KAYAK',
            bgPos: '-12px -36px'
        },{
            id: 'Skyscanner',
            bgPos: '-179px -36px'
        },{
            id: 'priceline',
            bgPos: '-851px -36px'
        },{
            id: 'Orbitz',
            bgPos: '-1366px -36px'
        },{
            id: 'Expedia',
            bgPos: '-515px -36px' 
        },{
            id: 'Googletravel',
            bgPos: '-1021px -36px'
        },{
            id: 'tripit',
            bgPos: '-1523px -36px'
        },{
            id: 'concur',
            bgPos: '-691px -36px'
        },{
            id: 'travel.jp',
            bgPos: '-1187px -36px'
        },{
            id: 'Momondo',
            bgPos: '-348px -36px'
        }],
        login: {
            user: GLOBALS.user,
            username: usernameFilter(GLOBALS.user, 'john_excel'),
            password: '123dsdsa73',
            ffpLogo: '../img/tour/united.png',
            ffpName: 'Mileage Plus'
        },
        search: {
            from: {
                city: "Berlin",
                code: "BER",
                date: "2015-03-07T21:05:00Z"
            },
            to: {
                city: "Singapore",
                code: "SIN",
                date: "2015-03-14T21:05:00Z"
            }
        },
        stops: [{
            name: '1 stop',
            price: 1813,
        },{
            name: '2+ stops',
            price: 2589
        }],
        ffps: [{
            name: 'Mileage Plus',
            activeStatus: 1,
            statuses: [{
                name: 'Premier Silver'
            },{
                name: 'Premier Gold'
            },{
                name: 'Premier Platinum'
            }],
            user: {
                name: (GLOBALS.user ? GLOBALS.user : 'John Excel'),
                status: 'Premier Silver',
                nextStatus: 'Premier Gold',
                miles: {
                    award: 43604,
                    status: 38167,
                    remainingAward: 16396,
                    remainingStatus: 11833
                }
            }
        },{
            name: 'Flying Blue'
        },{
            name: 'Miles & More'
        }],
        takeoff: {
            departure: { from: "11:50 am", to: "6:50 pm" },
            arrival: { from: "00:15 am", to: "11:55 pm" }
        },
        price: {
            from: 1666,
            to: 4163
        },
        best: 1,
        results: [{
            price: 1099,
            airline: { name: "Quatar Airways", logo: "qatar-color.png" },
            flights: [{
                departure: {
                    date: '2015-03-07T21:05:00Z',
                    airport: { city: 'Berlin', code: "TXL" }
                },
                arrival: {
                    date: '2015-03-07T19:35:00Z',
                    airport: { city: 'Singapore', code: "SIN" }
                },
                stops: { num: 1, duration: 136 }
            },{
                departure: {
                    date: '2015-03-14T21:05:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                arrival: {
                    date: '2015-03-14T12:55:00Z',
                    airport: { city: 'Berlin', code: 'TXL'}
                },
                stops: { num: 1, duration: 245 }
            }],
            ffp: "Privilege Club",
            miles: {
                award: 6598
            }
        },{
            price: 1197,
            airline: { name: 'Lufthansa', logo: 'lufthansa-color.png' },
            flights: [{
                departure: {
                    date: '2015-03-07T18:45:00Z',
                    airport: { city: 'Berlin', code: 'TXL' }
                },
                arrival: {
                    date: '2015-03-07T16:45:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                stops: { num: 1, duration: 140 }
            },{
                departure: {
                    date: '2015-03-14T23:55:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                arrival: {
                    date: '2015-03-14T08:55:00Z',
                    airport: { city: 'Berlin', code: 'TXL'}
                },
                stops: { num: 1, duration: 145 }
            }],
            ffp: "Miles & More",
            miles: {
                award: 13782,
                exactAward: 20673,
                exactStatus: 13782
            },
            awards: {
                status: {
                    success: true,
                    threshold: 50000,
                    name: 'Gold Status'
                },
                flight: {
                    success: true,
                    threshold: 60000,
                    name: 'Free intra-Europe round trip'
                }
            }
        },{
            price: 1235,
            airline: { name: 'Etihad Airways', logo: 'etihad-color.png' },
            flights: [{
                departure: {
                    date: '2015-03-07T12:55:00Z',
                    airport: { city: 'Berlin', code: 'TXL' }
                },
                arrival: {
                    date: '2015-03-07T22:10:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                stops: { num: 2, duration: 325 }
            },{
                departure: {
                    date: '2015-03-14T19:45:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                arrival: {
                    date: '2015-03-14T06:35:00Z',
                    airport: { city: 'Berlin', code: 'TXL'}
                },
                stops: { num: 1, duration: 215 }
            }],
            ffp: "Etihad Guest",
            miles: {
                award: 6558
            }
        },{
            price: 1307,
            airline: { name: 'Turkish Airlines', logo: 'turkish-color.png' },
            flights: [{
                departure: {
                    date: '2015-03-07T19:00:00Z',
                    airport: { city: 'Berlin', code: 'TXL' }
                },
                arrival: {
                    date: '2015-03-07T17:20:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                stops: { num: 1, duration: 115 }
            },{
                departure: {
                    date: '2015-03-14T00:30:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                arrival: {
                    date: '2015-03-14T10:25:00Z',
                    airport: { city: 'Berlin', code: 'TXL'}
                },
                stops: { num: 1, duration: 145 }
            }],
            ffp: "Miles and Smiles",
            miles: {
                award: 13522,
                exactAward: 12940,
                exactStatus: 12940
            },
            smartDeal: {
                price: 1392,
                fare: 'Full fare economy',
                miles: {
                    exactStatus: 16902
                }
            },
            awards: {
                status: {
                    success: true,
                    threshold: 50000,
                    name: 'Gold Status'
                },
                flight: {
                    success: false,
                    threshold: 60000,
                    name: 'Europe-domestic round trip'
                }
            }
        },{
            price: 1520,
            airline: { name: 'British Airways', logo: 'british-color.png' },
            flights: [{
                departure: {
                    date: '2015-03-07T14:05:00Z',
                    airport: { city: 'Berlin', code: 'TXL' }
                },
                arrival: {
                    date: '2015-03-07T15:05:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                stops: { num: 1, duration: 85 }
            },{
                departure: {
                    date: '2015-03-14T23:10:00Z',
                    airport: { city: 'Singapore', code: 'SIN' }
                },
                arrival: {
                    date: '2015-03-14T23:25:00Z',
                    airport: { city: 'Berlin', code: 'TXL'}
                },
                stops: { num: 1, duration: 215 }
            }],
            ffp: "Executive Club",
            miles: {
                award: 14710
            }
        }]
    };
    
    $scope.step = -1;
    
    $scope.steps = [{
        // 0
        title: 'Welcome' + ($scope.config.login.user ? (' ' + $scope.config.login.user) : '')  + '!',
        body: 'Running through our step by step demo is really easy, just click on the flashing icons appearing on screen, sit back and relax.',
        action: 'button',
        top: '60%',
        nextButton: 'Got it'
    },{
        // 1
        attachTo: '.sidebar .takeoff',
        position: 'right',
        bubble: true
    },{
        // 2
        title: 'Your ordinary flight search website',
        body: 'This is what a regular flight comparison website looks like: a long list of flight results, sorted by price. Looks familiar?',
        position: 'bottom',
        action: 'button',
        nextButton: 'Next',
        attachTo: '.sidebar .takeoff'
    },{
        // 3
        attachTo: '#mileage-0',
        position: 'bottom',
        bubble: true
    },{
        // 4
        title: 'Miles for all flights',
        body: 'Our technology allows miles or points (both award and elite qualifying) to be shown for any flight, covering the important frequent flyer programs in the world. We are turning ordinary flight comparison websites into extraordinary ones.',
        position: 'bottom',
        action: 'button',
        nextButton: 'Next',
        overlay: '#mileage-0',
        attachTo: '#mileage-0'
    },{
        // 5
        attachTo: '#join-0',
        position: 'bottom',
        bubble: true
    },{
        //6
        attachTo: '#join-0',
        title: 'Join program',
        body: 'Don’t have a frequent flyer membership or want to join a new one? Sign up directly on the website without leaving your current search page.',
        img: '../img/tour/qatar-signup.jpg',
        position: 'left',
        overlay: '#mileage-0',
        action: 'button',
        nextButton: 'Next',
        cls: 'large'
    },{
        // 7
        attachTo: '#ffp-0 .status',
        position: 'right',
        bubble: true
    },{
        // 8
        attachTo: '#ffp-0',
        title: 'Personalize your search',
        body: 'Selecting your memberships and status will further refine your search. With our technology you can select as many as you want.',
        position: 'right',
        overlay: '.sidebar',
        action: 'button',
        nextButton: 'Next'
    },{
        // 9
        attachTo: '#result-1',
        position: 'bottom',
        bubble: true
    },{
        // 10
        title: 'Results that matter to you',
        body: 'As you further personalize your search by selecting your frequent flyer programs and statuses only flights earning to your memberships will now display miles.',
        action: 'button',
        attachTo: '#result-1 .flights',
        position: 'top',
        nextButton: 'Next',
        overlay: '#result-1'
    },{
        // 11
        attachTo: '.result-best .price',
        position: 'top',
        bubble: true
    },{
        // 12
        title: 'Best deal',
        body: 'Did you notice there was a Lufthansa flight earning you 20,673 more miles than the cheapest fare for only $98 more? Our technology is smart enough to calculate a ‘best deal’ taking into account flight and loyalty-related information to display an ideal result for you.',
        action: 'button',
        attachTo: '.result-best .mileage-block.status',
        position: 'bottom',
        nextButton: 'Next',
        overlay: '.result-best'
    },{
        // 13
        attachTo: '#ffp-login',
        position: 'left',
        bubble: true,
        onNext: login
    },{
        // 14
        title: 'Connect your accounts',
        body: 'Connect your frequent flyer accounts directly on the website and store it under your user profile so you can save it for your next visit.',
        attachTo: '.reveal-modal.tiny.in',
        position: 'right',
        action: 'button',
        nextButton: 'Next',
        onPrevious: closeLogin,
        onNext: closeLogin,
        delay: 1
    },{
        // 15
        attachTo: '.ffps .balance',
        position: 'right',
        bubble: true,
        delay: 0.5
    },{
        // 16
        title: 'Keep track of your memberships',
        body: 'It’s not magic – connecting your account allows us to retrieve information – your status, miles balances and expiration dates – and use that to further customize your searches.',
        attachTo: '.ffps .balance',
        position: 'right',
        action: 'button',
        nextButton: 'Next',
        overlay: '.sidebar',
        onNext: openProgressTooltip,
        onPrevious: login
        
    },{
        // 17
        title: 'Progress towards status and rewards',
        body: 'Now that we know how many miles you already have it’s easy to show you which flights get you to gold status or award flights faster.',
        attachTo: '.result-best .mileage-block.award',
        position: 'left',
        action: 'button',
        nextButton: 'Next',
        onPrevious: closeProgressTooltip,
        onNext: closeProgressTooltip,
        overlay: '#mileage-best',
        delay: 0.5
    },{
        // 18
        attachTo: '#smart-deal-link-3',
        position: 'top',
        bubble: true,
        delay: 0.5
    },{
        // 19
        attachTo: '#result-3 .smart-deal .mileage',
        positon: 'bottom',
        overlay: '#result-3 .smart-deal .mileage',
        title: 'Smarter deals',
        nextButton: 'Finish',
        body: 'We will notify you when there’s a different fare within the same flight earning a lot more miles for a small price incremental.',
        action: 'button',
        delay: 0.5,
        onPrevious: openProgressTooltip,
        onNext: scrollTop
    },{
        // 20
        top: '15%',
        title: 'Thank you!',
        templateUrl: 'partials/last-step.html',
        airlines: $scope.config.twitterAirlines,
        counter: $scope.config.counter,
        nextButton: false
    }];
    
    $scope.best = $scope.config.results[$scope.config.best];
    
    $scope.userMiles = $scope.config.ffps[0].user.miles;
    $scope.totalAward = $scope.userMiles.award + $scope.userMiles.remainingAward;
    $scope.totalStatus = $scope.userMiles.status + $scope.userMiles.remainingStatus;
    
    function scrollTop(){
        jQuery('body,html').animate({ scrollTop: 0 }, 500);
    }
    
    function openProgressTooltip(){
        
        var tip = jQuery('.kk-tooltip');
        if(tip.size() <= 0 || tip.css('visibility') == 'hidden') jQuery('#mileage-best .progress.paward').click();
        
    }
    
    function closeProgressTooltip(){
        
        var tip = jQuery('.kk-tooltip');
        if(tip.size() > 0 && tip.css('visibility') == 'visible') jQuery('#mileage-best .progress.paward').click();
        
    }
    
    $scope.getAwardMeter = function(miles){
        
        var width = ((miles + $scope.userMiles.award) / $scope.totalAward) * 100;
        if(width > 100) width = 100;
        return { width: width + '%' };
        
    };
    
    $scope.getStatusMeter = function(miles){
        
        var width = ((miles + $scope.userMiles.status) / $scope.totalStatus) * 100;
        if(width > 100) width = 100;
        return { width: width + '%' };
        
    };
    
    $scope.getCurrentAwardMeter = function(){
        
        var width = ($scope.userMiles.award / $scope.totalAward) * 100;
        return { width: width + '%' };
        
    };
    
    $scope.getCurrentStatusMeter = function(){
        
        var width = ($scope.userMiles.status / $scope.totalStatus) * 100;
        return { width: width + '%' };
        
    };
    
    $scope.loadingProgress = 0;
    
    var loginModal = null;
    
    var intervalHandle = $interval(function(){
        $scope.loadingProgress += Math.random() * 20 + 1;
        
        if($scope.loadingProgress >= 100){
            
            $scope.loadingProgress = 100;
            $interval.cancel(intervalHandle);
            $timeout(function(){
                $scope.startButtonVisible = true;
            }, 1000);
        }
        
    }, 300);
    
    $scope.start = function(){
        $scope.step = 0;
    };
    
    $scope.selectStatus = function($index){
        $scope.config.ffps[0].activeStatus = $index;
    };
    
    function closeLogin(){
        if(loginModal){
            var promise = loginModal.dismiss('cancel');
            loginModal = null;
            return promise;
        }
    }
    
    function login(){
        
        loginModal = $modal.open({
           templateUrl: 'partials/login.html',
           windowClass: 'tiny',
           backdrop: 'static',
           resolve: {
               config: function(){ return $scope.config.login; }
           },
           controller: function($scope, $modalInstance, config){
               
               $scope.config = config;
               
               $scope.cancel = function(){
                   $modalInstance.dismiss('cancel');
               };
               
               $scope.login = function(form){
                   if(form.$valid) $modalInstance.close(true);
               };
           }
        });
        
        loginModal.result.then(function(result){
            
        }, function(){
            loginModal = null;
        });
        
        return loginModal.opened;
        
    }
    
    function loadTemplate(url){
                
        var tpl = $templateCache.get(url);
            
        if(!tpl){
            
            return $http.get(url).then(function(response){
                
                tpl = response.data;
                $templateCache.put(url, tpl);
                return tpl;
                
            });
            
        }else{
            return $q.defer().resolve(tpl);
        }
        
    }
    
}])

.directive('kkFixedBlock', ['$http', '$templateCache', '$compile', '$timeout', function($http, $templateCache, $compile, $timeout){
    
    return{
        
        scope: {
            attachTo: '@?'
        },
        transclude: true,
        template: '<div ng-transclude></div>',
        
        controller: function($scope, $element){
            
            var height = 0;
            var width = 0;
            
            function init(){
                
                angular.element(window).on('scroll', onScroll);
                angular.element(window).on('resize', onResize);
                
                height = $element.children().eq(0).height();
                width = $element.width();
                
            }
            
            function onScroll(e){
                update();
            }
            
            function onResize(e){
                
                height = $element.children().eq(0).height();
                width = $element.width();
                
                update();
            }
            
            function update(){
                
                var bottom = Number.MAX_VALUE;
                var scrollTop = angular.element(window).scrollTop();
                
                if($scope.attachTo){
                    var wrapper = angular.element($scope.attachTo);
                    bottom = wrapper.offset().top + wrapper.height();
                }
                
                var top = $element.offset().top;
                
                $element.children().eq(0).children().eq(0).width(width);
                
                if(scrollTop > top && scrollTop < bottom - height){
                    fix();
                }else{
                    unfix();
                }
                
            }
            
            function fix(){
                if(!$element.hasClass('fixed-block')){
                    $element.addClass('fixed-block');
                    height = $element.children().eq(0).height();
                    $element.height(height);
                }
            }
            
            function unfix(){
                $element.removeClass('fixed-block');
            }
            
            $scope.$on('$destroy', function(){
                angular.element(window).off('scroll', onScroll);
                angular.element(window).on('resize', onResize);
            });
            
            init();
            
        }
    };
    
}])

.directive('kkError', [function(){
    
    return {
        
        scope: {
            field: '=',
            type: '@?'
        },
        
        template: '<small class="error kk-error" ng-show="show()" ng-transclude></small>',
        replace: true,
        restrict: 'AE',
        transclude: true,
        
        controller: function($scope, $element){
            
            var submited = false;
            
            $scope.show = function(){
                
                if($scope.field.$invalid && ($scope.field.$dirty || submited)){
                    
                    if(angular.isString($scope.type) && !$scope.field.$error[$scope.type]){
                        return false;
                    }
                    
                    return true;
                }
                
                return false;
            };
            
            function onFormSubmit(){
                submited = true;
            }
            
            var form = $element.closest('form');
            form.on('submit', onFormSubmit);
            
            $scope.$on('$destroy', function(){
                form.off('submit', onFormSubmit);
            });
            
            $scope.$on('kk-error', function(event){
                onFormSubmit();
            });
        }
    };
}])

.directive('kkTooltip', ['$compile', '$timeout', '$templateCache', function($compile, $timeout, $templateCache){
  
  return {
    restrict: 'A',
    scope: {
      kkTooltip: '@',
      tooltipData: '=?',
      tooltipIf: '=?',
      tooltipToggle: '=?'
    },
    controller: function($scope, $element){
      
      var idPrefix = 'kk-tooltip-';
      var counter = getCounter();
      var tip = null;
      var template = '<div ng-attr-id="{{id}}" class="kk-tooltip" ng-class="{ down: down }" ng-style="style"><span class="tip" ng-style="tipStyle"></span><span ng-include="template"></span></div>';
      var defaultContentTemplate = '<span ng-bind-html="kkTooltip"></span>';
      var tipOffset = {
        top: -14,
        left: 5
      };
      var globalOffset = {
        x: 0,
        y: 7
      };
      var timeout = false;
      var delay = 100;
      var toggleOpened = false;
      
      $scope.$watch('tooltipIf', function(newValue){
        
        if(newValue) angular.element($element).addClass('has-tooltip');
        else angular.element($element).removeClass('has-tooltip');
        
      });
      
      $scope.data = $scope.tooltipData;
      $scope.template = 'kk-tooltip-default-content-template.html';
      $scope.id = null;
      
      if(!angular.isDefined($scope.data)){
        
        $templateCache.put($scope.template, defaultContentTemplate);
        if(!angular.isDefined($scope.tooltipIf)) $scope.tooltipIf = $scope.kkTooltip;
        
      }else{
        
        if(!angular.isDefined($scope.tooltipIf)) $scope.tooltipIf = $scope.data;
        $scope.template = $scope.kkTooltip;
        
      }
      
      $scope.style = {
        top: 0,
        left: 0,
        visibility: 'hidden'
      };
      
      $scope.tipStyle = {
        top: tipOffset.top + 'px',
        left: tipOffset.left + 'px'
      };
      
      $scope.down = false;
      
      function getCounter(){
        var body = $('body');
        return body.data('kk-tooltip-counter');
      }
      
      function setCounter(val){
        var body = $('body');
        return body.data('kk-tooltip-counter', val);
      }
      
      function createTooltip(){
        var el = angular.element(template);
        tip = $compile(el)($scope, function(tip, $scope){
          $('body').append(tip);
        });
      }
      
      function show(){
        
        if(timeout) $timeout.cancel(timeout);
        
        timeout = $timeout(function(){
          
          var wnd = $(window);
          var maxHeight = wnd.height() + wnd.scrollTop();
          var maxWidth = wnd.width();
          
          var width = tip.outerWidth();
          var height = tip.outerHeight();
          var offset = $element.offset();
          var tipHeight = Math.abs(tipOffset.top / 2);
          var elHeight = $element.outerHeight();

          var left = 0;
          var top = 0;
          var tipLeft = 0;
          var tipTop = 0;
          
          if(offset.left + width > maxWidth){
            
            left = maxWidth - width - globalOffset.x;
            tipLeft = offset.left - left + tipOffset.left;
            
          }else{
            
            left = offset.left + globalOffset.x;
            tipLeft = tipOffset.left;
            
          }
          
          if(offset.top + elHeight + height +  tipHeight > maxHeight){
            
            top = offset.top - height - tipHeight - globalOffset.y;
            tipTop = height;
            $scope.down = true;
            
          }else{
            
            top = offset.top + elHeight + tipHeight + globalOffset.y;
            tipTop = tipOffset.top;
            $scope.down = false;
            
          }
          
          $scope.style.left = left + 'px';
          $scope.style.top = top + 'px';
          $scope.style.visibility = 'visible';
          $scope.style.display = 'inline-block';
          $scope.tipStyle.left = tipLeft + 'px';
          $scope.tipStyle.top = tipTop + 'px';
          
          timeout = false;
          
        }, delay);
        
        $('body').on('mousemove', onMouseMove);
        
      }
      
      function hide(){
        
        if(timeout) $timeout.cancel(timeout);
        
        timeout = $timeout(function(){
          $scope.style.visibility = 'hidden';
          timeout = false;
        }, delay);
        
        $('body').off('mousemove', onMouseMove);
      }
      
      function createAndShow(){
        
        if($scope.tooltipIf){
          if(!tip) createTooltip();
          show();
        }
        
      }
      
      function onMouseEnter(e){ 
        
        if(!toggleOpened) createAndShow();
        
      }
      
      function onMouseLeave(e){
        
        if(!toggleOpened) hide();
        
      }
      
      function onMouseMove(e){
        
        if(toggleOpened) return;
        
        var x = e.pageX;
        var y = e.pageY;
        var offset = $element.offset();
        var width = $element.outerWidth();
        var height = $element.outerHeight();
        
        if((x < offset.left || x > offset.left + width) && (y < offset.top || y > offset.top + height)){
        
          hide();
        }
      }
      
      function onClick(e){
        
        if(!$scope.tooltipToggle) return false;
        
        if(!toggleOpened) createAndShow();
        else hide();
        
        toggleOpened = !toggleOpened;
        
      }
      
      if(counter === null || counter === undefined){
        counter = 0;
      }else{
        counter++;
      }
      
      setCounter(counter);
      
      $scope.id = idPrefix + counter;
      
      if(angular.isArray($scope.kkTooltip)){
        $scope.kkTooltip = $scope.kkTooltip.join('<br />');
      }
      
      $element.on('mouseenter', onMouseEnter);
      $element.on('mouseleave', onMouseLeave);
      $element.on('click', onClick);
      
      $scope.$on('$destroy', function(){
        $element.off('mouseenter', onMouseEnter);
        $element.off('mouseleave', onMouseLeave);
        $element.off('click', onClick);
      });
      
    }
    
  };
  
}]);
