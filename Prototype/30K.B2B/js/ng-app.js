'use strict';

// Declare app level module which depends on filters, and services
angular.module('30k_ca', [
  'mm.foundation',
  'datePicker',
  'googlechart'
])

.value('config', {
    stats: null
})

.filter('dateToStr', function(){
    return function(date){
        return (date.getDate() < 10 ? '0' : '') + date.getDate() + '-' 
                + (date.getMonth() < 9 ? '0' : '') +  (date.getMonth() + 1) + '-' + date.getFullYear();  
    };
})

.service('kkService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){
    
    var jsonPath = 'ajax/';
    
    function _handleError(response){
        
        $rootScope.loading--;
        var message = 'An unknown server error occurred.';
 
        if (!response.data.message){
            
            _showAlert('An unknown server error occured. Please try again or refresh page.');
            
        }else{
            
            _showAlert(response.data.message);
            message = response.data.message;
        }
        
        return $q.reject(message);

    }
         
    function _handleSuccess(response){
        
        $rootScope.loading--;
        
        if(!angular.isObject(response.data) || !response.data.success){
            
            if(response.data.message){
                _showAlert(response.data.message);
            }
            
        }else{
            if(response.data.message){
                _showAlert(response.data.message, 'success');
            }
        }
        
        return response.data.result;
        
    }
    
    function _showAlert(msg, type){
        
        if(type === undefined) type = 'alert';

        $rootScope.$emit('kk-alert', { alert: { type: type, msg: msg }});
    }
    
    function sendPost(urlSlug, data){
        
        $rootScope.loading++;
        
        var request = $http({
            method: 'post',
            url: jsonPath + urlSlug,
            data: data
        });
    
        return request.then(_handleSuccess, _handleError);
        
    }
    
    function _prepareGetUrl(url, params){
        
        if(params && angular.isObject(params)){
            for(var i in params){
                url = url.replace(':' + i, params[i]);
            }
        }
        
        return url;
    }
    
    function sendGet(urlSlug, params){
        
        $rootScope.loading++;
        
        var url = _prepareGetUrl(urlSlug, params);
        
        var request = $http({
            method: 'get',
            url: jsonPath + url
        });
        
        return request.then(_handleSuccess, _handleError);
    }
    
    return {
        sendPost: sendPost,
        sendGet: sendGet
    };
    
}])

.service('calcService', ['kkService', '$filter', function(kkService, $filter){
    
    function getConfig(){
        
        return kkService.sendGet('calc/config.json').then(function(response){
            return response;
        });
        
    }
    
    function getAirports(input){
        
        return kkService.sendPost('calc/airports.json', {
            input: input
        }, false).then(function(response){
            return response.airports;
        });
        
    }
    
    function search(params){
        
        return kkService.sendPost('calc/search.json', params).then(function(response){
            return response;
        });
        
    }
    
    return {
        getConfig: getConfig,
        getAirports: getAirports,
        search: search
    };
    
}])

.service('customerService', ['kkService', '$q', 'config', '$filter', function(kkService, $q, config, $filter){
    
    function getConfig(){
        
        if(angular.isObject(config.stats)){
            var deferred = $q.defer();
            deferred.resolve(config.stats);
            return deferred.promise;
        }else{
            return kkService.sendGet('stats/config.json').then(function(response){
                config.stats = response;
                return config.stats;
            });
        }
        
    }
    
    function getStats(period){
        
        return kkService.sendPost('stats/stats.json',{
            start: period.start,
            end: period.end
        }).then(function(response){
            
            return _getStats(response);
            
        }, function(){
            //TODO
        }); 
        
    }
    
    function _prepareGraphsData(data){
        
        for(var i = 0 ; i < data.length ; i++){
            var date = new Date(data[i].c[0].v);
            var parts = date.toDateString().split(' ');
            data[i].c[0].v = parts[1]; // month only
            //data[i].c[0].v = parts[1] + ' ' + parts[3]; //month with year
        }
        return data;
        
    }
    
    function _getStats(response){
        
        for(var i in response.data.graphs){
            response.data.graphs[i] = _prepareGraphsData(response.data.graphs[i]);
        }
        
        return response.data;

    }
    
    function getInvoices(period, itemsPerPage, page){
        
        var params = {
            itemsPerPage: itemsPerPage,
            page: page
        };
        
        if(period.start.getFullYear() != period.end.getFullYear() 
            || period.start.getMonth() != period.end.getMonth()
            || period.start.getDate() != period.end.getDate()
        ){
            params.start = $filter('dateToStr')(period.start);
            params.end = $filter('dateToStr')(period.end);
        }
        
        return kkService.sendPost('invoices/invoices.json', params).then(function(response){
            
            return response;
            
        }, function(){
            //TODO
        }); 
        
    }
    
    return {
        getStats: getStats,
        getConfig: getConfig,
        getInvoices: getInvoices
    };
}])

.directive('kkAlerts', ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll){
    
    return{
        restrict: 'A',
        
        template: '<alert ng-repeat="alert in alerts" type="alert.type" close="closeAlert($index)">{{alert.msg}}</alert>',
        
        controller: function($scope, $element){
            
            $scope.alerts = [];
            
            $scope.$watchCollection('alerts', function(newValue, oldValue){
              
              if(newValue.length > oldValue.length){
                $location.hash('window-top');
                $anchorScroll();
              }
              
            });
            
            function addAlert(alert){
                $scope.alerts.push(alert);
            }
            
            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };
            
            function onAlert(event, args){
                if(angular.isObject(args.alert)){
                    addAlert(args.alert);
                }
            }
            
            $rootScope.$on('kk-alert', onAlert);
        }
    };
    
}])

.directive('kkError', [function(){
    
    return {
        
        scope: {
            field: '=',
            type: '@?',
            visible: '=?'
        },
        
        template: '<small class="error kk-error" ng-show="show()" ng-transclude></small>',
        replace: true,
        restrict: 'AE',
        transclude: true,
        
        controller: function($scope, $element){
            
            var submited = false;
            
            $scope.show = function(){
                
                if($scope.field.$invalid && ($scope.field.$dirty || submited)){
                    
                    if(angular.isString($scope.type) && !$scope.field.$error[$scope.type]){
                        $scope.visible = false;
                        return false;
                    }
                    
                    $scope.visible = true;
                    return true;
                }
                
                $scope.visible = false;
                return false;
            };
            
            function onFormSubmit(){
                submited = true;
            }
            
            var form = $element.closest('form');
            form.on('submit', onFormSubmit);
            
            $scope.$on('$destroy', function(){
                form.off('submit', onFormSubmit);
            });
            
            $scope.$on('kk-error', function(event){
                onFormSubmit();
            });
        }
    };
}]);