'use strict';

angular.module('30k_ca')

.controller('CustomerStatsController', ['$scope', 'kkService', 'customerService', '$timeout', '$window', function($scope, kkService, customerService, $timeout, $window) {
  
  $scope.config = null;
  $scope.totals = null;
  $scope.loading = false;
  
  $scope.periodPickerClick = function($event){
    
    $timeout(function(){
      
      var picker = document.getElementById('period-picker');
      var matchLeft = picker.style.left.match(/\d+\.?\d*/);
      if(!angular.isArray(matchLeft) || matchLeft.length < 1) return;
      var left = parseFloat(matchLeft[0]);
      var width = picker.offsetWidth;
      var maxWidth = $window.innerWidth;
      var offset = 30;
      var diff = left + width - maxWidth + offset;
      
      if(diff > 0){
        left -= diff;
        angular.element(picker).css('left', left + 'px');
      }
      
    }, 200);
    
  };
  
  $scope.statsPeriod = {
    start: new Date(),
    end: new Date(),
    range: 0
  };
  
  $scope.monthRange = 0;
  $scope.yearRange = 0;
  
  var revenueChartLabel = 'Revenue ({{CODE}})';
  
  $scope.$watch('config.currency.code', function(newValue, oldValue){
    $scope.revenueChart.data.cols[1].label = revenueChartLabel.replace('{{CODE}}', newValue);
  });
  
  $scope.$watch('config.chart.revenue.maxValue', function(newValue, oldValue){
     $scope.revenueChart.options.vAxis.maxValue = newValue; 
  });
  
  $scope.$watch('config.chart.searches.maxValue', function(newValue, oldValue){
     $scope.searchesChart.options.vAxis.maxValue = newValue; 
  });
  
  $scope.revenueChart = {
    type: 'AreaChart',
    options: {
      colors: ['#f6e800'],
      hAxis: {
          textStyle: {
              color: '#999999'
          }
      },
      vAxis: {
          textStyle: {
              color: '#999999'
          }
      },
      legend: {
          position: 'none'
      },
      chartArea: {
          left: '15%',
          top: '5%',
          width: '90%',
          height: '80%'
      }
    },
    data: {
      cols: [{
        id: 'd', label: 'Date', type: 'string'
      },{
        id: 's', label: revenueChartLabel, type: 'number'
      }],
      rows: []
    }
  };
  
  $scope.searchesChart = {
    type: 'AreaChart',
    options: {
      colors: ['#00e587'],
      hAxis: {
          textStyle: {
              color: '#999999'
          }
      },
      vAxis: {
          textStyle: {
              color: '#999999'
          }
      },
      legend: {
          position: 'none'
      },
      chartArea: {
          left: '15%',
          top: '5%',
          width: '90%',
          height: '80%'
      }
    },
    data: {
      cols: [{
        id: 'd', label: 'Date', type: 'string'
      },{
        id: 's', label: 'Searches', type: 'number'
      }],
      rows: []
    }
  };
  
  var statsTimeoutPromise = false;

  function loadStats(){
    
    if(statsTimeoutPromise){
      $timeout.cancel(statsTimeoutPromise);
      statsTimeoutPromise = null;
    }
    
    statsTimeoutPromise = $timeout(function(){
      
      var period = angular.copy($scope.statsPeriod);
      period.start.setDate(period.start.getDate() + 1);
      period.end.setDate(period.end.getDate() + 1);
      
      $scope.loading = true;
      
      customerService.getStats(period).then(function(stats){
        
        $scope.searchesChart.data.rows = stats.graphs.searches;
        $scope.revenueChart.data.rows = stats.graphs.revenue;
        $scope.totals = stats.totals;
        $scope.loading = false;
        
      }, function(){
          $scope.loading = false;
      });
      
    }, 500);
  }
  
  function loadConfig(){
    
    customerService.getConfig().then(function(config){
      $scope.config = angular.copy(config);
    });
  }
  
  function getMonthPeriod(){
    var now = new Date();
    var end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
    var start = new Date(end.getFullYear(), end.getMonth() - 1, end.getDate(), 0, 0, 0);
    end.setMilliseconds(0);
    start.setMilliseconds(0);
    
    return {
      start: start,
      end: end,
      range: end.valueOf() - start.valueOf()
    };
    
  }
  
  $scope.setMonthPeriod = function(){
    $scope.statsPeriod = getMonthPeriod();
  };
  
  $scope.setYearPeriod = function(){
    $scope.statsPeriod = getYearPeriod();
  };
  
  function closePicker(){

    $timeout(function(){
      if($('#period-picker:visible').size() > 0) $('a[dropdown-toggle="#period-picker"]').click();
    });
    
  }
  
  $scope.$watch('statsPeriod.start', function(newValue, oldValue){
    $scope.statsPeriod.range = $scope.statsPeriod.end.valueOf() - newValue.valueOf();
    loadStats();
  });
  
  $scope.$watch('statsPeriod.end', function(newValue, oldValue){
    $scope.statsPeriod.range = newValue.valueOf() - $scope.statsPeriod.start.valueOf();
    if($scope.statsPeriod.range > 0) {
      closePicker();
    }
    loadStats();
  });
  
  $scope.isMonthSelected = function(){
    return $scope.statsPeriod.range == $scope.monthRange;
  };
  
  $scope.isYearSelected = function(){
    return $scope.statsPeriod.range == $scope.yearRange;
  };
  
  $scope.isOtherPeriodSelected = function(){
    return !$scope.isMonthSelected() && !$scope.isYearSelected();
  };
  
  $scope.preventDefault = function($event){
    $event.preventDefault(); 
    $event.stopPropagation();
  };
  
  function getYearPeriod(){
    var now = new Date();
    var end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
    var start = new Date(end.getFullYear() - 1, end.getMonth(), end.getDate(), 0, 0, 0);
    end.setMilliseconds(0);
    start.setMilliseconds(0);
    
    return {
      start: start,
      end: end,
      range: end.valueOf() - start.valueOf()
    };
    
  }
  
  function init(){
    
    $scope.statsPeriod = getMonthPeriod();
    $scope.monthRange = getMonthPeriod().range;
    $scope.yearRange = getYearPeriod().range;
    
    loadConfig();
    loadStats();
    
  }
  
  init();
  
}]);