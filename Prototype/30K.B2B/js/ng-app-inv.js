'use strict';

angular.module('30k_ca')

.controller('CustomerInvoicesController', ['$scope', 'customerService', '$timeout', '$window', function($scope, customerService, $timeout, $window) {
    
    $scope.loading = false;
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 12;
    $scope.invoices = [];
    
    var timeoutPromise = false;
    
    $scope.periodPickerClick = function($event){
    
        $timeout(function(){
            
            var picker = document.getElementById('period-picker');
            var matchLeft = picker.style.left.match(/\d+\.?\d*/);
            if(!angular.isArray(matchLeft) || matchLeft.length < 1) return;
            var left = parseFloat(matchLeft[0]);
            var width = picker.offsetWidth;
            var maxWidth = $window.innerWidth;
            var offset = 30;
            var diff = left + width - maxWidth + offset;
            
            if(diff > 0){
                left -= diff;
                angular.element(picker).css('left', left + 'px');
            }
        
        }, 200);
        
    };
    
    var defaultPeriod = {
        start: new Date(),
        end: new Date(),
        range: 0
    };
    
    $scope.period = angular.copy(defaultPeriod);
    
    $scope.isPeriodStartDirty = function(){
        return $scope.period.start.valueOf() != defaultPeriod.start.valueOf();
    };
    
    $scope.isPeriodEndDirty = function(){
        return $scope.period.end.valueOf() != defaultPeriod.end.valueOf();
    };
    
    $scope.selectPage = function(page){
        $scope.page = page;
        loadInvoices();
    };
    
    function loadInvoices(){
        
        if(timeoutPromise){
            $timeout.cancel(timeoutPromise);
            timeoutPromise = null;
        }
    
        timeoutPromise = $timeout(function(){
            
            var period = angular.copy($scope.period);
            period.start.setDate(period.start.getDate() + 1);
            period.end.setDate(period.end.getDate() + 1);
          
            $scope.loading = true;
            
            customerService.getInvoices(period, $scope.itemsPerPage, $scope.currentPage).then(function(result){
            
                $scope.totalItems = result.totalItems;
                $scope.currentPage = result.page;
                $scope.itemsPerPage = result.itemsPerPage;
                $scope.invoices = result.items;
                $scope.loading = false;
            
            }, function(){
                $scope.loading = false;
            });
            
        }, 500);
        
    }
    
    function closePicker(){
    
        $timeout(function(){
          if($('#period-picker:visible').size() > 0) $('footer').click();
        });
        
    }
    
    $scope.filter = function(){
        
        if($scope.isPeriodStartDirty() && $scope.isPeriodEndDirty()){
            $scope.page = 1;
            loadInvoices();    
        }
        
    };
    
    $scope.$watch('period.start', function(newValue, oldValue){
        $scope.period.range = $scope.period.end.valueOf() - newValue.valueOf();
    });
  
    $scope.$watch('period.end', function(newValue, oldValue){
        
        $scope.period.range = newValue.valueOf() - $scope.period.start.valueOf();
        
        if($scope.period.range > 0) {
            closePicker();
        }
        
    });
    
    $scope.preventDefault = function($event){
        $event.preventDefault(); 
        $event.stopPropagation();
    };
    
    loadInvoices();
    
}]);