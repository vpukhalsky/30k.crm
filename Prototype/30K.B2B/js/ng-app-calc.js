'use strict';

angular.module('30k_ca')

.controller('EarningCalculatorController', ['$scope', 'calcService', '$anchorScroll', '$location', '$filter', function($scope, calcService, $anchorScroll, $location, $filter){
    
    $scope.params = {
        segments: [],
        showOptional: false
    };
    
    var defaultSegment = {
        
    };
    
    $scope.result = null;
    $scope.loading = false;
    $scope.selectedAccrualIndex = 0;
    
    $scope.selectAccrual = function(index){
        
        if(index >= 0 && index < $scope.result.accruals.length){
            $scope.selectedAccrualIndex = index;
        }
        
    };
    
    $scope.getFlightNoPrefix = function(segment){
        
        var code = '';
        
        if(angular.isObject(segment.mkAirline)){
            code = segment.mkAirline.code;
        }
        
        return code;
        
    };
    
    $scope.preventDefault = function($event){
        
        $event.stopPropagation();
        
    };
    
    $scope.initSegmentForm = function(scope){
        
        scope.$watch('segment.mkAirline', function(newValue, oldValue){
            
            if(angular.isObject(newValue) && !scope.segment.opAirline){
                scope.segment.opAirline = angular.copy(newValue);
                scope.segmentForm.opAirline.$setDirty(true);
            }
            
        });
          
    };
    
    $scope.addSegment = function(){
        
        var extend = {};
        
        if($scope.params.segments.length > 0){
            var last = $scope.params.segments[$scope.params.segments.length - 1];
            extend.depAirport = angular.copy(last.arrAirport);
        }
        $scope.params.segments.push(angular.extend(angular.copy(defaultSegment), extend));
    };
    
    $scope.removeSegment = function(index){
        $scope.params.segments.splice(index, 1);
    };
    
    $scope.toggleOptional = function(segment){
        
        if(segment){
            segment.showOptional = !segment.showOptional;
        }else{
            $scope.params.showOptional = !$scope.params.showOptional;
        }
        
    };
    
    $scope.getAirports = function(match){
        
        return calcService.getAirports(match);
        
    };
    
    $scope.initPurchaseDate = function(scope){
        scope.$on('setDate', function(e, date, view){
            $scope.params.purDate = date;
            
            if(view == 'date') $('input[name="pdate"]').blur();
        });
    };
    
    $scope.initDepartureDate = function(scope){
        scope.$on('setDate', function(e, date, view){
            scope.segment.depDate = date;
            
            if(view == 'date') $('input[name="ddate"]').blur();
        });
    };
    
    $scope.selectBookingClass = function(segment, code){
        segment.bookingClass = code;  
    };
    
    $scope.selectProgram = function(program){
        $scope.params.program = program;
    };
    
    $scope.selectCurrency = function(code){
        $scope.params.currency = code;
    };
    
    function getParams(){
        
        var params = {
            program: $scope.params.program.id,
            segments: []
        };
        
        if($scope.params.program.showFare){
            params.fare = $scope.params.fare;
            params.currency = $scope.params.currency;
        }
        
        if($scope.params.showOptional){
            params.purchaseDate = angular.isObject($scope.params.purDate) ? $filter('dateToStr')($scope.params.purDate) : null;
            params.flightNo = $scope.params.flightNo;
        }
        
        for(var i = 0 ; i < $scope.params.segments.length ; i++){
            
            var segment = $scope.params.segments[i];
            
            var segmentParams = {
                departureAirport: segment.depAirport.code,
                arrivalAirport: segment.arrAirport.code,
                marketingCarrier: segment.mkAirline.code,
                bookingClass: segment.bookingClass
            };
            
            if($scope.params.showOptional){
                segmentParams.operatingCarrier = angular.isObject(segment.opAirline) ? segment.opAirline.code : null;
                segmentParams.departureDate = angular.isObject(segment.depDate) ? $filter('dateToStr')(segment.depDate) : null;
            }
            
            params.segments.push(segmentParams);
        }
        
        return params;
    }
    
    function prepareResult(result){
        
        result.basicStatus = $filter('filter')(result.statuses, { isElite: false }).pop();
        result.totalDistance = 0;
        
        for(var i = 0 ; i < result.segments.length ; i++){
            result.totalDistance += result.segments[i].distance;
        }
        
        for(var i = 0 ; i < result.statuses.length ; i++){
            
            var status = result.statuses[i];
            
            var totals = {
                rdm: 0, eqm: 0, eqp: 0
            };
            
            for(var j = 0 ; j < result.segments.length ; j++){
                
                var segment = $filter('filter')(result.segments[j].earnings, { statusId: status.id }).pop();
                
                totals.rdm += segment.rdm;
                totals.eqm += segment.eqm;
                totals.eqp += segment.eqp;
                
            }
            
            status.totals = totals;
        }
        
        return result;
    }
    
    $scope.submit = function(form){
        
        if(form.$invalid) return;
        
        var params = getParams();
        
        $scope.loading = true;
         
        calcService.search(params).then(function(response){
            
            $scope.result = prepareResult(response);
            $scope.loading = false;
            
            $location.hash('calc-result');
            $anchorScroll();  
            
        }, function(){
            
            $scope.loading = false;
            
        });
        
    };
    
    $scope.initResultSegment = function(segment){
        segment.basicEarnings = $filter('filter')(segment.earnings, { statusId: $scope.result.basicStatus.id }).pop();
        segment.expanded = false;
    };
    
    $scope.toggleSegment = function(segment){
        segment.expanded = !segment.expanded;
    };
    
    $scope.addSegment();
    
    calcService.getConfig().then(function(response){
        $scope.config = response;
    });
    
}]);