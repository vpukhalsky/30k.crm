$(document).ready(function(){
    
    return {
        
        dashboardStatsUrl: 'ajax/dashboard.html',
        
        foundersFadeDuration: 200,
        foundersBannerFadeDuration: 600,
        
        scrollOffset: 60,
        scrollVelocity: 2,
        
        slideDuration: 400,
        
        counterInterval: 2000,
        
        featuresHeight: 1300,
        featuresTransition: 0.12,
        featuresDelay: 0,
        featuresZoomTransition: 0.15,
        featuresZoomDelay: 0.46,
        
        smallScreenWidth: 640,
        mediumScreenWidth: 1024,
        largeScreenWidth: 1440,
        
        scrollerLargeVisible: 4,
        scollerMediumVisible: 3,
        scrollerSmallVisible: 1,
        
        sampleAirports: [
            'London Metropolitan Area, London, United Kingdom (LON)',
            'British Rail Terminal, London, United Kingdom (ZLX)',
            'Corbin-london, London, United States (LOZ)',
            'Deadmans Cay, Long Island, Bahamas (LGI)',
            'Dew Station, Lonely, United States (LNI)',
            'Heliport, Long Beach, United States (JLB)',
            'Helmuth Baungartem, Lontras, Brazil (LOI)',
            'Lawas, Long Semado, Malaysia (LSM)',
            'Liancheng, Longyan, China (LCX)',
            'Loncopue, Loncopue, Argentina (LCP)',
            'Londolozi, Londolozi, South Africa (LDZ)',
            'London - Kings Cross, London, (QQK)',
            'London Ashford Internati...d, United Kingdom (LYX)'
        ],
        
        init: function(){
            this.initSmoothScroll();
            this.initFrontPageFeaturesSection();
            $(document).foundation();
            this.initCounter();
            this.initDropdownSelect();
            
            this.initScrollers();
            this.initDashboardStats();
            this.initAboutUsFounders();
            this.initProfileValidation();
            this.initFAQAccordion();
            
            return this;
        },
        
        initFAQAccordion: function(){
            $('#faq ul.accordion').on('toggled', $.proxy(function (event, accordion) {
                if(accordion.hasClass('active')) {
                    this.scrollTo(accordion.closest('.accordion-navigation'));
                }
            }, this));  
        },
        
        initProfileValidation: function(){
            
            $('#profile form').submit($.proxy(function(e){
                
                var form = $(e.currentTarget);
                var val = form.find('#hidden-country').val();
                var btn = form.find('#country-btn');
                
                if(val){
                    btn.parent().removeClass('error');
                    $(window).resize();
                    return true;
                }
                
                btn.parent().addClass('error');
                e.preventDefault();
                $(window).resize();
                return false;
                
            }, this));
            
        },
        
        onImageLoad: function(img, callback){
            img.each(function(){
                if (this.complete || /*for IE 10-*/ $(this).height() > 0) {
                    callback.apply(this);
                }
                else {
                    $(this).on('load', function(){
                        callback.apply(this);
                    });
                }
            });
        },
        
        initAboutUsFounders: function(){
            
            if($('body.about').size() <= 0) return;
            
            var height = $('#about-banner article.initial').height();
            $('#about-banner').height(height);
            
            this.onImageLoad($('#about-founders').find('.person img'), function(){
               $(this).closest('figure').height($(this).height());
            });
            
            $(window).resize(function(){
                
                $('#about-banner').css({height: 'auto'});
                var height = $('#about-banner article.initial').height();
                $('#about-banner').height(height);
                
                $('#about-founders').find('.person figure').css({height: 'auto'});
                height = $('#about-founders').find('.person img').height();
                $('#about-founders').find('.person figure').height(height);
                
            });
            
            $('#about-founders').find('.person figure').hover($.proxy(function(e){
                
                var figure = $(e.currentTarget);
                var img = figure.find('img');
                var ffp = figure.find('.ffp');
                ffp.stop();
                img.stop().stop().fadeOut(this.foundersFadeDuration, $.proxy(function(){
                    ffp.fadeIn(this.foundersFadeDuration);
                }, this));
                
            }, this), $.proxy(function(e){
                
                var figure = $(e.currentTarget);
                var img = figure.find('img');
                var ffp = figure.find('.ffp');
                img.stop();
                ffp.stop();
                ffp.fadeOut(this.foundersFadeDuration, $.proxy(function(){
                    img.fadeIn(this.foundersFadeDuration);
                }, this));
                
            }, this));
            
            $('#about-founders').find('a[data-target]').click($.proxy(function(e){
                
                e.preventDefault();
                
                var btn = $(e.currentTarget);
                var targetId = '#' + btn.attr('data-target');
                var target = $(targetId);
                /*var person = btn.closest('.person').find('figure');
                var people = btn.closest('#about-founders').find('.person figure').not(person);
                
                people.find('img').stop();
                people.find('.ffp').stop();
                people.each($.proxy(function(index, one){
                    
                    var one = $(one);
                    
                    one.find('.ffp').fadeOut(this.foundersFadeDuration, $.proxy(function(){
                        one.find('img').fadeIn(this.foundersFadeDuration);
                    }, this));
                    
                }, this));
                
                person.find('.ffp').stop();
                person.find('img').stop().fadeOut(this.foundersFadeDuration, $.proxy(function(){
                    person.find('.ffp').fadeIn(this.foundersFadeDuration);
                }, this));*/
                
                $('#about-banner article:visible').not(targetId).stop().fadeOut(this.foundersBannerFadeDuration, $.proxy(function(){
                    
                    target.fadeIn(this.foundersBannerFadeDuration);
                    this.scrollTo(target);
                    
                }, this));
                
            }, this));
              
        },
        
        initDashboardStats: function(){
            
            var dash = $('#dashboard');
            if(dash.size() < 1) return;
            
            dash.find('#hidden-period').on('change', $.proxy(function(e){
                
                dash.find('.loader').addClass('active');
                
                dash.find('.stats').load(this.dashboardStatsUrl, $.proxy(function(){
                    
                    dash.find('.loader').removeClass('active');
                    
                }, this));
                
            }, this));
            
        },
        
        initScrollers: function(){
            
            $(window).resize($.proxy(function(){
                this.initScrollersOnResize();
            }, this));
            
            this.initScrollersOnResize();
            
            $('.scroller-wrapper').each($.proxy(function(index, el){
                
                var scroller = $(el);
                scroller.find('.nav').click($.proxy(function(e){
                    
                    e.preventDefault();
                    
                    var a = $(e.currentTarget);
                    var scroller = a.closest('.scroller-wrapper');
                    
                    if(scroller.data('animated')){
                        scroller.find('.scroller').stop();
                    }
                    
                    var position = scroller.data('position');
                    if(a.hasClass('prev')) position--;
                    else if(a.hasClass('next')) position++;
                    scroller.data('position', position);
                    
                    var items = scroller.data('items');
                    var size = scroller.data('size');
                    
                    if(position <= 0) scroller.find('.nav.prev').hide();
                    else scroller.find('.nav.prev').show();
                    if(position + size >= items) scroller.find('.nav.next').hide();
                    else scroller.find('.nav.next').show();
                    
                    scroller.data('animated', true);
                    var leftOffset = this.getScrollerLeftOffset(scroller);
                    
                    scroller.find('.scroller').animate({
                        left: leftOffset + 'px'
                    }, 'fast', 'swing', function(){
                        $(this).parent().data('animated', false);
                    });
                    
                }, this));
                
            }, this));
            
        },
        
        initScrollersOnResize: function(){
            
            $('.scroller-wrapper').each($.proxy(function(index, el){
                
                var scroller = $(el);
                var size = this.scrollerSmallVisible;
                
                var windowWidth = $(window).width();
                
                if(windowWidth > this.smallScreenWidth) size = this.scollerMediumVisible;
                if(windowWidth > this.mediumScreenWidth) size = this.scrollerLargeVisible;
                
                scroller.data('size', size);
                
                var rowWidth = $('.row').width();
                var colWidth = rowWidth / size;
                scroller.data('column-width', colWidth);
                var marginSize = Math.ceil(((windowWidth - rowWidth) / 2) / colWidth);
                scroller.data('margin-size', marginSize);
                 
                var offset = (windowWidth - ((size + 2 * marginSize) * colWidth)) / 2;
                scroller.data('offset', offset);
                
                var position = scroller.data('position') ? parseInt(scroller.data('position')) : 0;
                scroller.data('position', position);
                
                var items = scroller.find('article').size();
                scroller.data('items', items);
                
                var leftOffset = this.getScrollerLeftOffset(scroller);
                scroller.find('.scroller').css({
                    left: leftOffset + 'px'
                });
                
                scroller.find('article').css({
                    width: colWidth + 'px'
                });
                
                var prev = scroller.find('.nav.prev');
                var next = scroller.find('.nav.next');
                
                if(position == 0) prev.hide();
                else prev.show();
                
                if(position + size >= items) next.hide();
                else next.show();
                
                var navOffset = 10;
                
                if(windowWidth > this.smallScreenWidth) navOffset = 0;
                
                prev.css({
                    left: ((windowWidth - rowWidth) / 2 + navOffset) + 'px'
                });
                
                next.css({
                    right: ((windowWidth - rowWidth) / 2 + navOffset) + 'px'
                });
                
                scroller.find('.blending').width((windowWidth - rowWidth) / 2);
                
                var vrLeft = scroller.find('.vr-left');
                var vrRight = scroller.find('.vr-right');
                   
                switch(size){ 
                    case 4: 
                        var vrLeftOffset = (windowWidth - rowWidth) / 2 + colWidth;
                        var vrRightOffset = vrLeftOffset + 2 * colWidth;
                        vrLeft.css({ left: vrLeftOffset + 'px' }).show();
                        vrRight.css({ left: vrRightOffset + 'px' }).show();
                        break;
                    case 3:
                        var vrLeftOffset = (windowWidth - rowWidth) / 2 + colWidth;
                        var vrRightOffset = vrLeftOffset + colWidth;
                        vrLeft.css({ left: vrLeftOffset + 'px' }).show();
                        vrRight.css({ left: vrRightOffset + 'px' }).show();
                        break;
                    case 1:
                        vrLeft.hide();
                        vrRight.hide(); 
                        break; 
                }
                
                var height = scroller.find('.scroller').height();
                scroller.height(height);
                
            }, this));
            
        },
        
        getScrollerLeftOffset: function(scroller){
            
            var position = scroller.data('position');
            var offset = scroller.data('offset');
            var size = scroller.data('size');
            var marginSize = scroller.data('margin-size');
            var colWidth = scroller.data('column-width');
            var left = 0;
            var items = scroller.find('article').size();
            var itemOffset = 0;
            
            if(items < size && items > 0){
                itemOffset = Math.floor((size - items) / 2);
            }
            
            left = colWidth * (marginSize - position + itemOffset) + offset;
            return left;
        },
        
        initDropdownSelect: function(){
            
            $('[data-dropdown-input]').each($.proxy(function(index, el){
                
                var btn = $(el);
                var dd = $('#' + btn.attr('data-dropdown'));
                dd.find('li a').click($.proxy(this.onDropdownSelect, btn));
                
            }, this));
            
        },
        
        onDropdownSelect: function(e){
            
            var a = $(e.currentTarget);
            var input = $('#' + this.attr('data-dropdown-input'));
            var beforeVal = input.val();
            input.val(a.attr('data-value'));
            
            if(beforeVal != a.attr('data-value')){
                input.trigger('change', {
                    target: input
                });
            }
            
            this.html(a.html());
            
        },
        
        initCounter: function(){
            
            $('.counter').each($.proxy(function(index, el){
                 
                 var counter = $(el);
                 var text = counter.html();
                 var number = parseInt(text, 10);
                 counter.data('counter-value', number);
                 $.proxy(this.incCounter, counter)();
                 setInterval($.proxy(this.incCounter, counter), this.counterInterval);

            }, this));
              
        },
        
        incCounter: function(){
            
            var number = this.data('counter-value');
            var text = number.toString();
            this.data('counter-value', number + 1);
            
            var html = '';
            
            while(text.length > 0){
                var char = text.substring(0, 1);
                text = text.substring(1);
                html += '<span>' + char + '</span>';
            }
            
            this.html(html);
        },
        
        initFrontPageFeaturesSection: function(){
            
            this.proxyOnFrontPageSmallFeatureClick = $.proxy(this.onFrontPageSmallFeatureClick, this);
            this.proxyOnFrontPageMediumUpFeatureClick = $.proxy(this.onFrontPageMediumUpFeatureClick, this);
            
            this.onFrontPageResize();
            
            $(window).resize($.proxy(function(e){
                
                $('.screen .slide.active').each($.proxy(function(index, el){
                    
                    var slide = $(el); 
                    var parent = slide.offsetParent();
                    var pos = slide.position();
                    parent.css({ left: -pos.left });
                    
                }));
                
                this.onFrontPageResize();
                
            }, this));
            
            $(window).scroll($.proxy(function(e){
                
                var wrapper = $('.ipad-wrapper');
                if(wrapper.size() <= 0 || Modernizr.touch) return;
                
                var offset = wrapper.offset();
                var scrollTop = $(window).scrollTop();
                var ipad = $('.ipad');
                
                var sectionHeight = this.featuresHeight / wrapper.find('dt').size();
                var pos = 0;
                
                if(offset.top <= scrollTop){
                    
                    var margin = scrollTop - offset.top;
                    if(margin > this.featuresHeight) margin = this.featuresHeight;
                    
                    var top = scrollTop - (offset.top + this.featuresHeight);
                    if(top < 0) top = 0;
                    
                    ipad.css({
                        position: 'fixed',
                        top: -top + 'px',
                        left: 0,
                        width: '100%'
                    });
                    
                    wrapper.css({
                        height: ipad.height() + this.featuresHeight//margin
                    });
                    
                    pos = margin;
                    
                }else{
                    ipad.css({
                        position: 'static',
                        width: 'auto'
                    });
                    wrapper.css({
                        height: 'auto'
                    });
                }
                
                var sections = $('#features dl dt');
                var currentSection = Math.floor(pos / sectionHeight);
                var posInSection = (pos - (sectionHeight * currentSection)) / sectionHeight;
                var dt = sections.last();
                
                if(currentSection < sections.size()){
                    dt = sections.eq(currentSection);    
                }
                
                var dd = dt.next('dd');
                sections.removeClass('active').each(function(index, el){
                    var d = $(el).next('dd');
                    d.css({
                        overflow: 'auto',
                        height: 'auto',
                        display: 'none'
                    });
                });
                dt.addClass('active');
                var ddMaxHeight = dd.css({
                    height: 'auto',
                    display: 'block'
                }).height();
                var zooms = $('.screen .zoom');
                zooms.hide();
                
                if(currentSection < sections.size()){
                    
                    var screenId = dt.find('a').attr('data-screen');
                    var screen = $('#' + screenId);
                    var zoom = $('.zoom[data-screen="' + screenId + '"]');
                    var screenPos = screen.position();
                    var screenParent = screen.offsetParent();
                    
                    var innerPos = 1;
                    
                    if(posInSection < this.featuresTransition && currentSection > 0){
                        
                        innerPos = posInSection / this.featuresTransition;
                        
                        dd.css({
                            overflow: 'hidden',
                            height: (innerPos * ddMaxHeight) + 'px',
                            display: 'block'
                        });
                        
                    }else{
                        
                        dd.css({
                            overflow: 'auto',
                            height: 'auto',
                            display: 'block'
                        });
                    
                    }
                    
                    innerPos = 0;
                    
                    if(posInSection > 1 - this.featuresTransition && currentSection < sections.size() - 1){
                        
                        innerPos = (posInSection - (1 - this.featuresTransition)) / this.featuresTransition;
                        
                        dd.css({
                            overflow: 'hidden',
                            height: ((1 - innerPos) * ddMaxHeight) + 'px',
                            display: 'block' 
                        });
                        
                    }
                    
                    screenParent.css({
                        left: (-screenPos.left - (screen.width() * innerPos)) + 'px'
                    });
                    
                    var screenEl = zoom.closest('.screen');
                    var zoomMaxWidth = screenEl.width() * parseFloat(zoom.attr('data-size'));
                    var zoomLeft = parseFloat(zoom.attr('data-left')) * screenEl.width();
                    var zoomTop = parseFloat(zoom.attr('data-top')) * screenEl.height();
                    
                    zoom.css({
                        backgroundImage: 'url(' + screen.attr('src') + ')'
                    });
                    
                    if(posInSection > this.featuresTransition + this.featuresDelay 
                        && posInSection <= this.featuresTransition + this.featuresDelay + this.featuresZoomTransition + this.featuresZoomDelay
                    ){
                        innerPos = (posInSection - (this.featuresTransition + this.featuresDelay)) / this.featuresZoomTransition;
                        if(innerPos > 1) innerPos = 1;
                        
                    }else if(posInSection > this.featuresTransition + this.featuresDelay + this.featuresZoomTransition + this.featuresZoomDelay 
                        && posInSection <= this.featuresTransition + this.featuresDelay + 2 * this.featuresZoomTransition + this.featuresZoomDelay
                    ){
                        innerPos = 1 - ((posInSection - (this.featuresTransition + this.featuresDelay + this.featuresZoomTransition + this.featuresZoomDelay)) / this.featuresZoomTransition);
                        if(innerPos < 0) innerPos = 0;
                    }else{
                        innerPos = 0;
                    }
                    
                    var size = innerPos * zoomMaxWidth;
                    
                    zoom.css({
                        top: (zoomTop - size / 2) + 'px',
                        left: (zoomLeft - size / 2) + 'px',
                        width: size + 'px',
                        height: size + 'px',
                        opacity: 0.3 + 0.7 * innerPos
                    }).show();
                    
                    if(innerPos <= 0) zoom.hide();
                    
                }
            }, this));
            
        },
        
        onFrontPageResize: function(){
            
            var a = $('#features dl dt a');
            
            if(Modernizr.touch){
                a.off('click', this.proxyOnFrontPageMediumUpFeatureClick);
                a.on('click', this.proxyOnFrontPageSmallFeatureClick);
            }else{
                a.off('click', this.proxyOnFrontPageSmallFeatureClick);
                a.on('click', this.proxyOnFrontPageMediumUpFeatureClick);
            }
            
        },
        
        onFrontPageMediumUpFeatureClick: function(e){
            
            e.preventDefault();
            
            var a = $(e.currentTarget);
            var section = a.parent('dt').prevAll('dt').size();
            var size = a.closest('dl').find('dt').size();
            var wrapper = $('.ipad-wrapper');
            var top = wrapper.offset().top;
            var sectionHeight = this.featuresHeight / size;
            
            top += section * sectionHeight;
            top += (this.featuresDelay + this.featuresZoomTransition + this.featuresZoomDelay) * sectionHeight;
            var distance = Math.abs($(window).scrollTop() - top);
            var duration = distance / this.scrollVelocity;
            
            $('html,body').animate({ scrollTop: top + 'px' }, duration);
            
        },
        
        onFrontPageSmallFeatureClick: function(e){
        
            e.preventDefault();
            
            var a = $(e.currentTarget);
            if(a.parent().hasClass('active')) return;
            a.data('in-progress', true);
            
            a.closest('dl').find('dt.active + dd').slideUp(this.slideDuration, function(){
                $(this).prev('dt').removeClass('active');
            });
            
            var dd = a.parent().next('dd');
            dd.slideDown(this.slideDuration, $.proxy(function(dd){
                var dt = $(dd).prev('dt');
                dt.addClass('active');
                dt.find('a').data('in-progress', false);
                if($(window).width() < this.smallScreenWidth) this.scrollTo(dd, null, 400);
            }, this, dd));
            
            var slide = $('#' + a.attr('data-screen'));
            var parent = slide.offsetParent();
            var pos = slide.position();
            
            parent.animate({
                left: -pos.left
            }, this.slideDuration, $.proxy(function(){
                slide.parent().find('.slide.active').removeClass('active');
                slide.addClass('active');
            }, this));
            
        },
        
        initSmoothScroll: function(){
            
            $('a[href^="#"]').click($.proxy(function(e){
                
                var a = $(e.currentTarget);
                if(a.parent().is('.accordion-navigation')) return;
                var target = $(a.attr('href'));
                this.scrollTo(target, e);
                
            }, this));
            
        },
        
        scrollTo: function(target, event, duration){
            var offset = target.offset();
            if(!offset) return; 
            
            var top = offset.top - this.scrollOffset;
            
            if(top < 0) top = 0;
            var distance = Math.abs(top - $(window).scrollTop());
            if(distance <= 0) return;
            
            if(event) event.preventDefault();
            
            var featuresSection = $('#features');
            
            if(featuresSection.size() > 0 && !Modernizr.touch){
                
                var fromSection = $(event.currentTarget).closest('body > section, body > header, body > footer');
                var toSection = target.closest('body > section, body > header, body > footer');    
                var fromTop = fromSection.offset().top;
                var toTop = toSection.offset().top;
                
                if(fromTop < toTop && fromSection.nextAll('#features').size() > 0 && toSection.prevAll('#features').size() > 0){
                    top += this.featuresHeight;
                }else if(fromTop > toTop && fromSection.prevAll('#features').size() > 0 && toSection.nextAll('#features').size() > 0){
                    top -= this.featuresHeight;
                }else if(target.is('#benefits-more') && fromSection.nextAll('#features').size() > 0){
                    top += this.featuresHeight;
                }
            }
            
            if(!duration){
                duration = distance / this.scrollVelocity;    
            }
            
            $('html,body').animate({ scrollTop: top + 'px' }, duration);
        }
        
    }.init();
    
});