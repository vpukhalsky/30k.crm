private LoyaltyProgram GetProgram()
        {
            return new LoyaltyProgram
                {
                    Enabled = true,
                    Id = 1,
                    Name = "United MileagePlus",
                    OwningAirline = "UA",
                    WebSiteUrl = "http://www.united.com/web/en-us/content/mileageplus/default.aspx",
                    HelpUrl = "http://www.united.com/web/en-US/apps/account/settings/password/passwordResolution.aspx",
                    ExternalLoginEnabled = true,
                    Alliance = Alliance.OneWorld,
                    Tiers = new List<LoyaltyProgramTier>
                        {
                            new LoyaltyProgramTier
                                {
                                    Id = 5,
                                    TierAlias = TierAlias.Bronze,
                                    Name = "Member",
                                    IsElite = false,
                                },
                            new LoyaltyProgramTier
                                {
                                    Id = 6,
                                    TierAlias = TierAlias.Silver,
                                    Name = "Premier Silver",
                                    IsElite = true,
                                    Bonus = 0.25
                                },
                            new LoyaltyProgramTier
                                {
                                    Id = 7,
                                    TierAlias = TierAlias.Gold,
                                    Name = "Premier Gold",
                                    IsElite = true,
                                    Bonus = 0.5
                                },
                            new LoyaltyProgramTier
                                {
                                    Id = 8,
                                    TierAlias = TierAlias.Platinum,
                                    Name = "Premier Platinum",
                                    IsElite = true,
                                    Bonus = 0.75
                                },
                            new LoyaltyProgramTier
                                {
                                    Id = 9,
                                    TierAlias = TierAlias.ExecutivePlatinum,
                                    Name = "Premier 1K",
                                    IsElite = true,
                                    Bonus = 1
                                },
                        },
                    ConfiguredAccruals = new List<LoyaltyProgramAccrual>
                        {
                            new LoyaltyProgramAccrual
                                {
                                    Id = 1,
                                    AccrualType = AccrualType.Rdm,
                                    Name = "Redeemable Miles"
                                },
                            new LoyaltyProgramAccrual
                                {
                                    Id = 2,
                                    AccrualType = AccrualType.Eqm,
                                    Name = "Status Miles"
                                },
                            new LoyaltyProgramAccrual
                                {
                                    Id = 3,
                                    AccrualType = AccrualType.Eqs,
                                    Name = "Status segments"
                                },
                        }
                };
        }