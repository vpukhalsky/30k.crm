1.
[GET]
/TestRules/

The response (described by Response.PageData.json) first of all it will contain properties �inherited�  from the Response.Default.json and will also contain the Value property containing existing testing rules (every of those described by the TestRule.json) as well as pages infrastructure required data (like programs configuration).

2.
[PUT]
/TestRules/

Sends testing rule model (described with TestRule.json) using PUT http verb to the server. If Id of the rule isn�t yet set the rule considered to be a new one, otherwise existing record will be matched by given Id and updated.
The response (described by Response.Default.json) provides information about performed action on the server � whether or not it succeeded, and message to be displayed to the client.

3.
[GET]
/TestRules/Clone/{Id}

Forces server to create a clone of the existing testing rule referenced by {Id} parameter.
The response (described by Response.CloneRule.json) will contain properties �inherited�  from the Response.Default.json and also contain the Value property containing cloned testing rule (described by the TestRule.json) if action succeeded on the server.

4.
[DELETE]
/TestRules/{Id}

Forces server to delete testing rule with given {Id}.
The response (described by Response.Default.json) provides information about performed action on the server � whether or not it succeeded, and message to be displayed to the client.

1. validation
2. 
    MK carrier of segment #1 (LH) the moment I input it. We’ll edit it if necessary
    OP Carrier box (UA) for segment #1 should be automa2cally prepopulated with same carrier as MK carrier of segment #1 (LH) the moment I input it. We’ll edit it if necessary
    Fare code for segm#2 ( L ) should be automa2cally prepopulated with same fare code as segment #1 ( K ) the moment I input it. We’ll edit it if necessary
3. in the test rule details page calculated results blocks should have borders for the cells -either red or green depending on whether expected results matcch calculated
4. one thing I should have also clarified - for the flight numbers which are optional for every segment if there is no value populated the model should still contain ""
Vitaly Pukhalsky: so if there are 2 segments configured and flight number is only set for the second line
the model and its FlightNumbers array should be [ ''", "987" ]
probably that's how it currtntly works - just in case