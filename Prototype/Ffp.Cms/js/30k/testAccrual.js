/**
 * Test Accrual class
 * @author Artur Polkowski
 */ 

_3.TestAccrual = KClass.extend({
    
    observed: {
        
        /**
         * Accrual code eg. Rdm
         * @type string
         */ 
        AccrualType: '',
        
        /**
         * Expected value in test
         * @type int
         */ 
        Expected: 0,
        
        /**
         * Calculated value from fare
         */ 
        TotalFromFare: null,
        
        /**
         * Test passed?
         */ 
        Matched: false,
        
        /**
         * Calculated values per segments
         * @type int
         */ 
        Segments: []
    },
    
    computed: {
        isValid: function(){
            var expected = this.Expected();
            if(expected === undefined || expected === null || expected === false) return false;
            return expected.toString().match(/^[0-9\.]+$/) !== null;
        },
        
        isCalculated: function(){
            return this.TotalFromFare() !== null;
        }
    },
    
    reset: function(){
        this.TotalFromFare(null);
        this.Matched(this.observed.Matched);
        var s = this.Segments();
        for(var i = 0 ; i < s.length ; i++) s[i] = null;
    }
});