/**
 * Key-Value class
 * @author Artur Polkowski
 */ 

_3.KeyValue = KClass.extend({
    Id: null,
    Name: ''
});
