/**
 * Test class
 * @author Artur Polkowski
 * 
 */ 
_3.Test = KClass.extend({
    
    /**
     * Test unique id
     * @type int
     */ 
    Id: null,
    
    /**
     * Has been edited after being loaded?
     */ 
    edited: false,
    
    /**
     * Hash based on editable values.
     * Used to check if test has been changed after it has been loaded from server
     * @type string
     */ 
    lastSaveHash: '',
    
    /**
     * Action name requested prior save
     * @type string 'save' | 'run'
     */ 
    requestedAction: '',
    
    observed: {
        
        /**
         * True if Test has never been submited to server
         */ 
        isNew: false,
        
        /**
         * Ffp Code
         * @type string
         */ 
        FfpCode: '',
        
        /**
         * Array of airport codes
         * @type string
         */ 
        Itinerary: [],
        
        /**
         * Array of airline codes, one per segment
         * @type string
         */ 
        MkAirlines: [],
        
        /**
         * Array of airline codes, one per segment
         * @type string
         */ 
        OpAirlines: [],
        
        /**
         * Array of fare codes, one per segment
         * @type string
         */ 
        FareCodes: [],
        
        /**
         * Array of flight numbers, one per segment
         * @type string
         */ 
        FlightNums: [],
        
        /**
         * Date in format: mm/dd/yyyy
         * @type string
         */ 
        FlightDate: '',
        
        /**
         * Date in format: mm/dd/yyyy
         * @type string
         */ 
        PurchDate: '',
        
        /**
         * Test objective
         * @type string
         */ 
        TestObj: '',
        
        /**
         * Array of test blocks object
         * @type Object
         */ 
        CalcBlocks: [],
        
        /**
         * Array of test values grouped by accrual type
         * @type TestAccrual
         */ 
        BasicAccruals: [],
        
        /**
         * Array of test values grouped by accrual type
         * @type TestAccrual
         */ 
        MinimumAccruals: [],
        
        /**
         * Array of computed test values for specific status
         * @type Object
         */ 
        BasicStatuses: [],
        
        /**
         * Array of computed test values for specific status
         * @type Object
         */ 
        MinimumStatuses: [],
        
        /**
         * Comments for Test, optional
         * @type string
         */ 
        Comments: '',
        
        /**
         * Program Configured Accruals
         * @type string
         */ 
        FfpAccruals: [],
        
        /**
         * Initial departure airport code
         * @type string
         */ 
        DepartureAirport: '',
        
        /**
         * Itinerary segments to organize UI
         * @type TestSegment
         */ 
        Segments: [],
        
        /**
         * Open section with minimum miles test?
         */
        hasMinimumMilesTest: false,
        
        /**
         * Expand rows with status calculations in test edit view?
         */ 
        areStatusBasicRowsExpanded: false,
        areStatusMinimumRowsExpanded: false
    },
    
    computed: {
        
        isDepartureAirportValid: function(){
            return this.DepartureAirport().length == 3;  
        },
        
        isBasicTestValid: function(){
            var ba = this.BasicAccruals();
            var fa = this.FfpAccruals();
            
            for(var i = 0 ; i < ba.length ; i++){
                if(fa.indexOf(ba[i].AccrualType()) != -1 && !ba[i].isValid()) return false;
            }
            
            return true;
        },
        
        isMinimumTestValid: function(){
            if(!this.hasMinimumMilesTest()) return true;
            
            var ma = this.MinimumAccruals();
            var fa = this.FfpAccruals();
        
            for(var i = 0 ; i < ma.length ; i++){
                if(fa.indexOf(ma[i].AccrualType()) != -1 && !ma[i].isValid()) return false;
            }
            
            return true;
        },
        
        /**
         * Is Test valid to save?
         */ 
        isValid: function(){
            var segments = this.Segments();
            for(var i = 0 ; i < segments.length ; i++){
                if(!segments[i].isValid()) return false;
            }
            
            var valid = 
                this.isDepartureAirportValid()
                && this.isBasicTestValid()
                && this.isMinimumTestValid();
            
            return valid;
        },
        
        /**
         * Do expected values match computed?
         */ 
        Matched: function(){
            var ba = this.BasicAccruals();
            var ma = this.MinimumAccruals();
            
            for(var i = 0 ; i < ba.length ; i++){ if(!ba[i].Matched()) return false; }
            for(var i = 0 ; i < ma.length ; i++){ if(!ma[i].Matched()) return false; }
            
            return true;
        },
        
        /**
         * Has basic test been calculated already?
         */ 
        isBasicCalculated: function(){
            var ba = this.BasicAccruals();
            var a = this.FfpAccruals();
            for(var i = 0 ; i < ba.length ; i++){ if(a.indexOf(ba[i].AccrualType()) != -1 && !ba[i].isCalculated()) return false; }
            return true;
        },
        
        /**
         * Has minimum test been calculated already?
         */ 
        isMinimumCalculated: function(){
            var ma = this.MinimumAccruals();
            var a = this.FfpAccruals();
            for(var i = 0 ; i < ma.length ; i++){ if(a.indexOf(ma[i].AccrualType()) != -1 && !ma[i].isCalculated()) return false; }
            return true;
        },
        
        hasBasicSegmentsCalculated: function() {
            var ba = this.BasicAccruals();
            for (var i = 0; i < ba.length; i++) {
                if (ba[i].Segments() && ba[i].Segments().length > 0) {
                    return true;
                };
            }
            return false;
        },
        
        hasBasicStatusesCalculated: function () {
            var statuses = this.BasicStatuses();
            
            for (var i = 0; i < statuses.length; i++) {
                if (statuses[i].Status) return true;
            }

            return false;
        },
        
        hasMinSegmentsCalculated: function () {
            var ma = this.MinimumAccruals();
            for (var i = 0; i < ma.length; i++) {
                if (ma[i].Segments() && ma[i].Segments().length > 0) {
                    return true;
                };
            }
            return false;
        },
        
        hasMinStatusesCalculated: function () {
            var statuses = this.MinimumStatuses();

            for (var i = 0; i < statuses.length; i++) {
                if (statuses[i].Status) return true;
            }

            return false;
        }
    },
    
    mapped: {
        BasicAccruals: _3.TestAccrual,
        MinimumAccruals: _3.TestAccrual,
        Segments: _3.TestSegments
    },
    
    /**
     * Properties not included in save Test JSON
     */ 
    filtered: [
        'observed',
        'computed',
        'mapped',
        'filtered',
        'IsValid',
        'BasicAccruals',
        'MinimumAccruals',
        'FfpAccruals',
        'Matched',
        'Segments',
        'areStatusBasicRowsExpanded',
        'areStatusMinimumRowsExpanded',
        'DepartureAirport',
        'BasicStatuses',
        'MinimumStatuses',
        'isBasicCalculated',
        'isMinimumCalculated',
        'hasMinimumMilesTest',
        'lastSaveHash',
        'isDepartureAirportValid',
        'isBasicTestValid',
        'isMinimumTestValid',
        'isValid',
        'edited',
        'requestedAction'
    ],
    
    /**
     * Inits knockout.js subscribers on observed properties
     * @see KClass
     */ 
    initSubscribed: function(){
        
        //if FFP is changed...
        this.FfpCode.subscribe($.proxy(this.updateFfpAccruals, this));

        this.hasMinimumMilesTest.subscribe($.proxy(this.updateHasMinMilesTest, this));
    },
    
    /**
     * Pull Test data from server
     */ 
    load: function(callback){
        _3.view.isLoading(true);
        
        $.get(_3.utils.getJsonUrl('test-one.json'), {
           Id: this.Id
        }, $.proxy(function(response){
            
            if(response.Success){
                
                //overwrite properties with new data
                this.ext(response.Value.Test);
                
                if($.isFunction(callback)) callback();
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);   
        }, this));  
    },
    
    /**
     * Save corresponding FFP's accrual types
     */ 
    updateFfpAccruals: function(){
        var ffp = this.getFfp();
        if(!ffp) return;
        
        var code = this.FfpCode();
        
        if(code.length > 0){
            this.FfpAccruals([]);
            
            var ffp = this.getFfp();
            
            var ca = ffp.ConfiguredAccruals();
            var accruals = _3.view.accruals();
            
            var temp = [];
            for(var i = 0 ; i < ca.length ; i++){
                temp.push(ca[i].AccrualType());
            }
            
            //keep order the same as in ffpViewModel.accruals
            for(var i = 0 ; i < accruals.length ; i++){
                if(temp.indexOf(accruals[i].AccrualType()) != -1){
                    this.FfpAccruals.push(accruals[i].AccrualType());
                }
            }
        }
    },
    
    /**
    * Add/Remove minimumMiles block
    **/
    updateHasMinMilesTest: function () {
        var cb = this.CalcBlocks();
        
        var minimumBlockIndex = -1;

        for (var i = 0 ; i < cb.length ; i++) {
            if (cb[i].Distance == _3.view.MINIMUM_TEST_DISTANCE) {
                minimumBlockIndex = i;
            }
        }

        if (this.hasMinimumMilesTest() && minimumBlockIndex === -1) {
            cb.push({
                Distance: _3.view.MINIMUM_TEST_DISTANCE,
                Expected: {}
            });
        } else if(!this.hasMinimumMilesTest() && minimumBlockIndex >= 0){
            cb.splice(minimumBlockIndex, 1);
            this.CalcBlocks(cb);
        }
    },
    
    /**
     * Edit test
     */ 
    edit: function(){
        this.edited = true;
        this.updateFfpAccruals();
        _3.view.editTest(this);
    },
    
    /**
     * Shorthand method
     */ 
    getFfp: function(){
        return _3.view.getFfpByCode(this.FfpCode());
    },

    /**
     * Cancel program edition
     */ 
    cancelEdit: function(){
        
        //reload previous state
        //if(!this.isNew()) this.load($.proxy(this.onCancelEditSuccess, this));
        //else this.onCancelEditSuccess();
        
        this.onCancelEditSuccess();
    },
    
    onCancelEditSuccess: function(){
        //switch to another view
        _3.view.cancelTestEdit();   
    },

    /**
     * Push Test data to server
     */ 
    save: function(callback){
        if(!this.isValid()){
            _3.view.validate(true);
            return;
        }
        
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('test-add.json');
        
        this.beforeSave();
        var data = ko.toJSON(this, this.jsonFilter);
        
        //using REST change following line to:
        //_3.utils.sendPut(url + (this.isNew() ? '' : '/' + this.Id), data, $.proxy(function(response){
        _3.utils.sendPost(url, data, $.proxy(function(response){
            if(response.Success){

                this.isNew(false);
                
                _3.view.saveTest();
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);
        }, this));  
    },
    
    /**
     * Create hash value based on editable fields
     * @return String hash
     */ 
    toHash: function(){
        var data = this.hasMinimumMilesTest() + this.DepartureAirport() 
            + this.Comments() + this.TestObj() + this.PurchDate() + this.FlightDate() + this.FfpCode();
        
        var ba = this.BasicAccruals();
        var ma = this.MinimumAccruals();
        var s = this.Segments();
        
        for(var i = 0 ; i < ba.length ; i++) data += ba[i].Expected();
        for(var i = 0 ; i < ma.length ; i++) data += ma[i].Expected();
        
        for(var i = 0 ; i < s.length ; i++) data += s[i].Airport() + s[i].MkAirline() + s[i].OpAirline() + s[i].FareCode() + s[i].FlightNum();
        
        return _3.utils.strToHash(data);
    },
    
    /**
     * Extend test with @param obj
     */ 
    ext: function(obj){
        this.parent(obj);
        
        this.setupAccruals();
        this.setupSegments();
        
        this.lastSaveHash = this.toHash();
    },
    
    /**
     * Move values from object models to simple arrays and CalcBlocks object
     */ 
    beforeSave: function(){
        var segments = this.Segments();
        this.Itinerary([this.DepartureAirport().toUpperCase()]);
        this.MkAirlines([]);
        this.OpAirlines([]);
        this.FareCodes([]);
        this.FlightNums([]);
        
        for(var i = 0 ; i < segments.length ; i++){
            var s = segments[i];
            this.Itinerary.push(s.Airport().toUpperCase());
            this.MkAirlines.push(s.MkAirline().toUpperCase());
            this.OpAirlines.push(s.OpAirline().toUpperCase());
            this.FareCodes.push(s.FareCode().toUpperCase());
            this.FlightNums.push(s.FlightNum() ? s.FlightNum() : '');
        }
        
        var cb = this.CalcBlocks();
        var ba = this.BasicAccruals();
        var ma = this.MinimumAccruals();
        var basicBlock = null;
        var minimumBlock = null;
        var minimumBlockIndex = -1;
        
        for(var i = 0 ; i < cb.length ; i++){
            var block = cb[i];
            if(block.Distance == _3.view.BASIC_TEST_DISTANCE){
                basicBlock = block;
            }else if(block.Distance == _3.view.MINIMUM_TEST_DISTANCE){
                minimumBlock = block;
                minimumBlockIndex = i;
            }
        }
        
        for(var i = 0 ; i < ba.length ; i++){
            var accrual = ba[i];
            basicBlock.Expected[accrual.AccrualType()] = accrual.Expected();
            accrual.reset();
        }
        
        if (minimumBlock) {
            for (var i = 0 ; i < ma.length ; i++) {
                var accrual = ma[i];
                if (this.hasMinimumMilesTest()) {
                    minimumBlock.Expected[accrual.AccrualType()] = accrual.Expected();
                } else {
                    minimumBlock.Expected[accrual.AccrualType()] = null;
                }
                accrual.reset();
            }

            if (minimumBlockIndex >= 0 && !this.hasMinimumMilesTest()) {
                cb.splice(minimumBlockIndex, 1);
                this.CalcBlocks(cb);
            }
        }
    },
    
    /**
     * Save data from CalcBlocks as separate objects
     * grouped by accrual type.
     * @see TestAccrual
     */ 
    setupAccruals: function(){
        
        if(!_3.view) return;
        
        var cb = this.CalcBlocks();
        var types = _3.view.accruals();
        this.BasicAccruals([]);
        this.MinimumAccruals([]);
        
        if(this.isNew()){
            
            for(var i = 0 ; i < types.length ; i++){
                var type = types[i].AccrualType();
                
                this.BasicAccruals.push(new _3.TestAccrual({
                    AccrualType: type
                }));
                
                this.MinimumAccruals.push(new _3.TestAccrual({
                    AccrualType: type
                }));
            }
            
            this.CalcBlocks([{
                Distance: _3.view.BASIC_TEST_DISTANCE,
                Expected: {}
            },{
                Distance: _3.view.MINIMUM_TEST_DISTANCE,
                Expected: {}
            }]);
        }else{
        
            for(var i = 0 ; i < cb.length ; i++){
                
                var block = cb[i];
                
                for(var j = 0 ; j < types.length ; j++){
                    var type = types[j].AccrualType();
                    var segments = [];
                    var statuses = [];
                    
                    var accrualConfig = {
                        AccrualType: type,
                        Expected: block.Expected[type],
                    };
                    
                    if(block.Calculated){
                        
                        var ps = block.Calculated.FromFare.PerSegment;
                        for(var k = 0 ; k < ps.length ; k++) segments.push(ps[k][type]);
                        var blockType = block.Calculated.FromFare.Total[type];   
                        
                        accrualConfig.TotalFromFare = (blockType !== null && blockType !== undefined) ? blockType.Value : null;
                        accrualConfig.Matched = (blockType !== null && blockType !== undefined) ? blockType.Matched : true;
                        accrualConfig.Segments = segments;
                    
                    }
    
                    var accrual = new _3.TestAccrual(accrualConfig);
                    
                    if(block.Distance == _3.view.BASIC_TEST_DISTANCE){
                        this.BasicAccruals.push(accrual);
                    }else if(block.Distance == _3.view.MINIMUM_TEST_DISTANCE){
                        this.MinimumAccruals.push(accrual);
                    }
                }
                
                if(block.Calculated){
                    
                    //add statuses
                    var fs = block.Calculated.FromStatus;
                    
                    for(var k = 0 ; k < fs.length ; k++) statuses.push(fs[k]);
                    
                    if(block.Distance == _3.view.BASIC_TEST_DISTANCE){
                        this.BasicStatuses(statuses);
                    }else if(block.Distance == _3.view.MINIMUM_TEST_DISTANCE){
                        this.MinimumStatuses(statuses);
                    }   
                    
                }
                
            }
            
            if(this.MinimumAccruals().length < 1){
                
                for(var i = 0 ; i < types.length ; i++){
                    var type = types[i].AccrualType();
                    
                    this.MinimumAccruals.push(new _3.TestAccrual({
                        AccrualType: type
                    }));
                }
                
            }
        }
        
    },
    
    /**
     * Save values in form easy to read
     * by knockout (grid with computed values)
     */ 
    setupSegments: function(){
        var it = this.Itinerary();
        
        if(it.length < 1){
            this.Segments([new _3.TestSegment()]);
        }else{
            var ma = this.MkAirlines();
            var oa = this.OpAirlines();
            var fc = this.FareCodes();
            var fn = this.FlightNums();
            
            this.Segments([]);
            
            this.DepartureAirport(it[0]);
            
            for(var i = 0 ; i < it.length - 1 ; i++){
                
                this.Segments.push(
                    new _3.TestSegment({
                        Airport: it[i+1],
                        MkAirline: ma[i],
                        OpAirline: oa[i],
                        FareCode: fc[i],
                        FlightNum: fn[i]
                    })
                );
                
            }   
        }
    },
    
    /**
     * Run test, but first save if neccessary
     */ 
    run: function(){
        if(this.lastSaveHash != this.toHash()){
            this.requestedAction = 'run';
            $('#save-test-modal').foundation('reveal', 'open');
        }else {
            this.runSaved();
        }
    },
    
    /**
     * Clone test, but first save if neccessary
     */ 
    copy: function(){
        if(this.lastSaveHash != this.toHash()){
            this.requestedAction = 'copy';
            $('#save-test-modal').foundation('reveal', 'open');
        }else{
            this.copySaved();  
        }
    },
    
    /**
     * User confirms save prior other action (run / clone)
     */
    confirmSave: function(){
        switch(this.requestedAction){
            case 'run':
                this.save($.proxy(this.runSaved, this));
                break;
            case 'copy':
                this.save($.proxy(this.copySaved, this));
                break;
        }
        
        this.requestedAction = null;
    },
    
    /**
     * User cancel save and following action (run / clone)
     */ 
    cancelSave: function(){
        this.requestedAction = null;
    },
    
    /**
     * Clone test
     */ 
    copySaved: function(callback){
        
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('test-clone.json');
        
        //Uncoment for REST method with id in url:
        //url += '/' + this.Id;
        
        _3.utils.sendGet(url, $.proxy(function(response){
            if(response.Success){
                
                _3.view.cloneTest(response.Value.Test);
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);
        }, this));
        
    },
    
    /**
     * Run saved test by sending request to server
     */ 
    runSaved: function(callback){
        
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('test-run.json');
        var data = { Id: this.Id };
        
        _3.utils.sendPost(url, data, $.proxy(function(response){
            if(response.Success){
                
                //overwrite properties with new data
                this.ext(response.Value.Test);
                
                _3.view.runTest();
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);
        }, this)); 
        
    },
    
    /**
     * Delete segment from collection
     */ 
    removeSegment: function(segment){
        this.Segments.remove(segment);
    },
    
    /**
     * Add segment to collection
     */ 
    addSegment: function(){
        var fareCode = '';
        var segments = this.Segments();
        
        if(segments.length > 0){
            fareCode = segments[segments.length - 1].FareCode();
        }
        
        this.Segments.push(new _3.TestSegment({
            FareCode: fareCode
        }));
    },
    
    /**
     * Fare code in segment changed
     */
    onFareCodeChange: function(segment){
        if(!this.isNew()) return;
        
        var segments = this.Segments();
        var found = false;
        
        for(var i = 0 ; i < segments.length ; i++){
            if(!found && segments[i] == segment){
                found = true;
                continue;
            }else if(found && segments[i].FareCode() == segments[i].observed.FareCode){
                segments[i].FareCode(segment.FareCode());
            }
        }
    }
});
