/**
 * Test Segment class
 * @author Artur Polkowski
 */ 

_3.TestSegment = KClass.extend({
    
    observed: {
        Airport: '',
        MkAirline: '',
        OpAirline: '',
        FareCode: '',
        FlightNum: ''
    },
    
    computed: {
        isAirportValid: function(){
            return this.Airport().length == 3;
        },
        
        areOptionsValid: function(){
            return this.MkAirline().length == 2 && this.OpAirline().length == 2 && this.FareCode().length == 1;
        },    
    
        isValid: function(){
            return this.isAirportValid() && this.areOptionsValid();
        }
    },
    
    /**
     * Inits knockout.js subscribers on observed properties
     * @see KClass
     */ 
    initSubscribed: function(){
        
        this.MkAirline.subscribe($.proxy(this.onMkAirlineChange, this));
        
        this.FareCode.subscribe($.proxy(this.onFareCodeChange, this));
    },
    
    onMkAirlineChange: function(newValue){
        if(this.OpAirline() == this.observed.OpAirline) this.OpAirline(newValue);
    },
    
    onFareCodeChange: function(newValue){
        if(!_3.view) return;
        var test = _3.view.editedTest();
        
        if(test && test.isNew()){
            test.onFareCodeChange(this, newValue);
        }
    }
});