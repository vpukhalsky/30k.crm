/**
 * FFP Rule Accrual class
 * @author Artur Polkowski
 */ 

_3.RuleAccrual = KClass.extend({
    
    /**
     * Unique rule accrual id
     * @type int
     */
    Id: null,
    
    observed: {
        Factor: 0,
        Fixed: 0,
        FixedBonus: 0,
        Minimum: 0,
        ForLeg: false,
        EliteBonus: false,
        EliteMinimum: false,
        RangeFrom: 0,
        RangeTo: 0
    },
    
    computed: {
        isFactorValid: function(){
            return $.isNumeric(this.Factor())/* && this.Factor() != this.observed.Factor*/;
        },
        
        isFixedValid: function(){
            return $.isNumeric(this.Fixed())/*  && this.Fixed() != this.observed.Fixed*/;
        },
        
        isValid: function(){
            return this.isFactorValid() || this.isFixedValid();
        },
    },
    
    filtered: [
        'isFactorValid',
        'isFixedValid',
        'isValid'
    ],
    
    canAdd: function(index){
        return index() == 0;
    },
    
    canSubtract: function(index){
        return index() > 0;
    }
});
