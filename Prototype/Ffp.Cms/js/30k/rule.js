/**
 * FFP Rule class
 * @author Artur Polkowski
 */ 

_3.Rule = KClass.extend({
    
	/**
	 * Rule Id
	 * @required
	 * @type int
	 */ 
	Id: 0,
    
    /*persists last value for relationship*/
    lastRelationship: null,
	
    observed: {
        
        /**
         * FFP id
         * @required
         * @type int
         */ 
        ProgramId: 0,
        
        /**
         * Is fully updated with data pulled from server?
         */ 
        isLoaded: false,
               
		/**
         * Rule Id Generated
         * 
         * Pattern:
         * 'F' + FFP.Code + AirlineCode + NumericId + 'A' * times_cloned
         * 
         * @type string
         */ 
        RuleId: '',
		
        /**
         * Is rule enabled?
         */ 
        Enabled: true,
        
        /**
         * Two char code
         * @type string
         */ 
        AirlineCode: '',
        
        /**
         * Relationship Id
         * @see FFPViewModel.relationships
         * @type int
         */ 
        Relationship: null,
        
        /**
         * Array of airline codes
         * @type string
         */ 
        Subsidiaries: [],
        
        /**
         * Fare codes as single string
         * @type string
         */ 
        RDBs: '',
        
        /**
         * Fare family id
         * @see FFPViewModel.fareFamilies
         */
        FareFamily: 0,
        
        /**
         * Fare Family corresponding name
         */ 
        FareFamilyName: '',
        
        /**
         * Array of ids
         * @type int
         * @see FFPViewModel.operationalRules
         */ 
        MarketingRules: [],
        
        /**
         * Array of ids
         * @type int
         * @see FFPViewModel.operationalRules
         */ 
        OperatingRules: [],
        
        /**
         * Array of tags avaliable to edit
         * when "Specific" option for MarketingRules is selected
         * @type string
         */ 
        MarketingSpecificAirlines: [],
        
        /**
         * Array of tags avaliable to edit
         * when "Specific" option for MarketingRules is selected
         * @type string
         */ 
        OperatingSpecificAirlines: [],
        
        /**
         * Array of tags
         * @type string
         */ 
        FlightNoLimit: [],
        
        /**
         * Array of tags
         * @type string
         */
        FlightNoExcl: [],
        
        /**
         * Array of tags
         * @type string
         */ 
        GeoFilters: [],
        
        /**
         * Array of tags
         * @type string
         */ 
        CodedConditions: [],
        
        IsInternational: false,
        
        /**
         * Date in format: mm/dd/yyyy
         * @type string 
         */ 
        ValidityFrom: '',
        
        /**
         * Date in format: mm/dd/yyyy
         * @type string 
         */ 
        ValidityUntil: '',
        
        /**
         * Date in format: mm/dd/yyyy
         * @type string 
         */ 
        ValidityFlightFrom: '',
        
        /**
         * Date in format: mm/dd/yyyy
         * @type string 
         */ 
        ValidityFlightUntil: '',
        
        /**
         * Comments for entire rule
         * @type string
         */ 
        Comments: '',
        
        /**
         * Accrual groups containing accrual sub-rules for every accrual type
         * @see FFPAccrual, RuleAccrual
         * @type RuleAccrualGroup
         */ 
        AccrualGroups: [],
       
        MinimumMiles: 0,
        
        /**
         * Shown in rules grid
         * @type boolean
         */ 
        BonusAwardMiles: false,
        BonusStatusMiles: false,
        
        /**
         * Tag input objects
         */ 
        subsidiariesInput: null,
        geoInput: null,
        codedConditionsInput: null,
        mSpecificAirlinesInput: null,
        oSpecificAirlinesInput: null,
        flightNoExclInput: null,
        flightNoLimitInput: null,
        
        /**
         * True for rule, that hasn't been pushed to server yet
         */ 
        isNew: false,
        
        /**
         * Is rule edited in-row?
         */ 
        isInQuickEditMode: false,
        
        /**
         * Visibility of sections within rules edit view
         */ 
        isOperationalRulesVisible: false,
        isGeographicalRulesVisible: false,
        isValidityRulesVisible: false,
        isCommentRulesVisible: false,
        isStatusTiersVisible: false,
        
        /**
         * Currently edited tier object
         * @type FFPTier
         */ 
        editedTier: null,
        
        /**
         * Array of FFP tiers
         * @type FFPTier
         */
        Tiers: []
    },
    
    computed: {
        
        hasLimitedValidity: function(){
            if(this.ValidityFrom().length > 0 
                || this.ValidityUntil().length > 0 
                || this.ValidityFlightFrom().length > 0
                || this.ValidityFlightUntil().length > 0
            ){
                return true;
            }
            
            return false;
        },
        
        /**
         * @return array of fare codes (chars)
         */ 
        FareCodes: function(){
            return _3.utils.toUpperCase(this.RDBs()).split('');
        },
        
        /**
         * Shown in rules grid
         * @type string
         */ 
        GeoRulesDesc: function(){
            return this.GeoFilters().join('<br />');
        },
        
        areOperatingRulesValid: function(){
            //if(this.MarketingRules().indexOf('2') >= 0){
            //    var o = this.OperatingRules();
            //    for(var i = 0 ; i < o.length ; i++){
            //        if(o[i] != '2') return false;
            //    }
            //}
            
            return true;
        },
        
        areMarketingRulesValid: function(){
            //if(this.OperatingRules().indexOf('2') >= 0){
            //    var m = this.MarketingRules();
            //    for(var i = 0 ; i < m.length ; i++){
            //        if(m[i] != '2' && m[i] != '1') return false;
            //    }
            //}
            
            return true;
        },
        
        isAirlineCodeValid: function(){
            return this.AirlineCode().match(/^[a-zA-Z0-9]{2}$/) !== null;
        },
        
        isRDBValid: function(){
            return this.RDBs().match(/^[a-zA-Z]+$/) !== null;
        },
        
        isMinimumMilesValid: function(){
            return $.isNumeric(this.MinimumMiles());
        },
        
        isQuickEditValid: function(){
            
            var ag = this.AccrualGroups();
            for(var i = 0 ; i < ag.length ; i++){
                if(!$.isNumeric(ag[i].Value())) return false;
            }
            
            var valid = this.isAirlineCodeValid()
                && this.isRDBValid()
                && this.isMinimumMilesValid();
            return valid;
        },
        
        areAccrualGroupsValid: function(){
            if(!_3.view) return true;
            var ffp = this.getFfp();
            if(!ffp) return true;
            var ag = this.AccrualGroups();
            
            for(var i = 0 ; i < ag.length; i++){
                var rules = ag[i].Rules();
                
                for(var j = 0 ; j < rules.length ; j++){
                    if(!rules[j].isValid()){
                        var accruals = ffp.ConfiguredAccruals();
                        
                        for(var k = 0 ; k < accruals.length ; k++){
                            if(ag[i].AccrualType() == accruals[k].AccrualType() && ag[i].EarningBase() == accruals[k].EarningBase()){
                                return false;
                            }
                        }
                        
                        break;
                    }
                }
            }
            
            return true;
        },
        
        isFullEditValid: function(){
            return this.areAccrualGroupsValid() 
                && this.isAirlineCodeValid() 
                && this.isRDBValid() 
                && this.areOperatingRulesValid() 
                && this.areMarketingRulesValid();
        },
        
        isValid: function(){
            return (this.isInQuickEditMode() && this.isQuickEditValid()) || this.CloneOf !== undefined || this.isFullEditValid();
        },
        
        /**
         * Description used in grid view onmouseover
         */ 
        operationalRulesDesc: function(){
            if(!_3.view) return '';
            
            function bold(text){
                return '<strong>' + text + '</strong>';
            }
            
            var mr = this.MarketingRules();
            var or = this.OperatingRules();
            var rules = _3.view.operationalRules();
            
            var desc = '';
            var mdesc = bold('Marketing: ');
            var odesc = bold('Operating: ');
            var fnlDesc = bold('Flight No. Limit.: ');
            var fneDesc = bold('Flight No. Excl.: ');
            var mc = 0;
            var oc = 0;
            
            for(var i = 0 ; i < rules.length ; i++){
                
                if(mr.indexOf(rules[i].Id.toString()) != -1){
                    if(mc > 0) mdesc += ', ';
                    mdesc += rules[i].Name;
                    
                    if(rules[i].Id == 6 && this.MarketingSpecificAirlines().length > 0){
                        mdesc += ' (' + this.MarketingSpecificAirlines().join(', ') + ')';
                    }
                    
                    mc++;
                }
                
                if(or.indexOf(rules[i].Id.toString()) != -1){
                    if(oc > 0) odesc += ', ';
                    odesc += rules[i].Name;
                    
                    if(rules[i].Id == 6 && this.OperatingSpecificAirlines().length > 0){
                        odesc += ' (' + this.OperatingSpecificAirlines().join(', ') + ')';
                    }
                    
                    oc++;
                }
            }
            
            desc = (mc > 0 ? mdesc : '') + ((mc > 0 && oc > 0) ? '<br />' : '') + (oc > 0 ? odesc : '');
            
            if(desc != '' && this.FlightNoLimit().length > 0){
                desc += '<br />' + fnlDesc + this.FlightNoLimit().join(', ');
            }
            
            if(desc != '' && this.FlightNoExcl().length > 0){
                desc += '<br />' + fneDesc + this.FlightNoExcl().join(', ');
            }
            
            if(desc == '') desc = 'No operational rules selected';
            
            return desc;
        },
        
        areTiersValid: function(){
            //return this.Tiers().length > 0;
            return true;
        }
    },
    
    mapped: {
        AccrualGroups: _3.RuleAccrualGroup,
        
        subsidiariesInput: _3.TagInput,
        geoInput: _3.TagInput,
        codedConditionsInput: _3.TagInput,
        mSpecificAirlinesInput: _3.TagInput,
        oSpecificAirlinesInput: _3.TagInput,
        flightNoExclInput: _3.TagInput,
        flightNoLimitInput: _3.TagInput,
        
        editedTier: _3.FFPTier,
        Tiers: _3.FFPTier
    },
    
    /**
     * Which properties should be synchronized in 'apply to all' mode
     */ 
    trackInApplyToAll: [
        'Enabled',
        'AirlineCode',
        'Relationship',
        'Subsidiaries',
        'RDBs',
        'FareFamily',
        'MarketingRules',
        'OperatingRules',
        'MarketingSpecificAirlines',
        'OperatingSpecificAirlines',
        'FlightNoLimit',
        'FlightNoExcl',
        'GeoFilters',
        'CodedConditions',
        'IsInternational',
        'ValidityFrom',
        'ValidityUntil',
        'ValidityFlightFrom',
        'ValidityFlightUntil',
        'Comments',
        'MinimumMiles',
        'BonusAwardMiles',
        'BonusStatusMiles',
        'Tiers'
    ],
    
    filtered: [
        'trackInApplyToAll',
        'flightNoExclInput',
        'flightNoLimitInput',
        'isLoaded',
        'FareFamilyName',
        'subsidiariesInput',
        'geoInput',
        'isInQuickEditMode',
        'isQuickEditValid',
        'isAirlineCodeValid',
        'isRDBValid',
        'isAwardMilesValid',
        'isAwardPointsValid',
        'isStatusMilesValid',
        'isStatusSegmentsValid',
        'isMinimumMilesValid',
        'areAccrualGroupsValid',
        'isFullEditValid',
        'isValid',
        'codedConditionsInput',
        'mSpecificAirlinesInput',
        'oSpecificAirlinesInput',
        'isOperationalRulesVisible',
        'isGeographicalRulesVisible',
        'isValidityRulesVisible',
        'isCommentRulesVisible',
        'areOperatingRulesValid',
        'areMarketingRulesValid',
        'isStatusTiersVisible',
        'editedTier',
        'lastRelationship',
        'hasLimitedValidity'
    ],
    
    /**
     * Extend object with properties
     * @param Object data
     */ 
    ext: function(data){
        
        var ffp = this.getFfp();
        
        if(ffp && ffp.applyToAllMode()) ffp.applyToAllInProgress = true;
        
        this.parent(data);
        
        //setup tag inputs
        this.initTagInputs();
        
        //setup deault set of accruals
        this.initAccruals();
        
        //show or hide sections in rules view
        this.initSectionVisibility();
        
        if(ffp && ffp.applyToAllMode()) ffp.applyToAllInProgress = false;
    },
    /*
    hasRanges: function(type){
        
        var groups = this.AccrualGroups();
        
        for(var i = 0 ; i < groups.length; i++){
            
            var ranges = groups[i].Rules();
            
            if(groups[i].AccrualType() == type && ranges.length > 0 && ranges[0].Id && ranges[0].RangeFrom() != ranges[0].RangeTo()){
                
                return true;
                
            }
            
        }
        
        return false;
    },
    
    getRangesTooltip: function(type){
        
        var groups = this.AccrualGroups();
        var tip = '';
        
        for(var i = 0 ; i < groups.length; i++){
            
            var ranges = groups[i].Rules();
            
            if(groups[i].AccrualType() == type && ranges.length > 0){
                
                for(var j = 0 ; j < ranges.length ; j++){
                    
                    if(tip != '') tip += '<br />';
                    tip += '<strong>' + ranges[j].RangeFrom() + '&ndash;' + ranges[j].RangeTo() + '</strong>: ' + ranges[j].Factor();
                    
                }
                
                break;
            }
            
        }
        
        
        if(tip == ''){
            tip = 'No ranges specified';
        }
        
        return tip;
    },*/
    
    /**
     * Updates visibility of sections in rules view
     */ 
    initSectionVisibility: function(){
        
        this.isOperationalRulesVisible(
            this.MarketingRules().length > 0
            || this.OperatingRules().length > 0
            || this.FlightNoLimit() != this.observed.FlightNoLimit
            || this.FlightNoExcl() != this.observed.FlightNoExcl
        );
        
        this.isGeographicalRulesVisible(
            this.GeoFilters().length > 0
            || this.CodedConditions().length > 0
        );
        
        this.isValidityRulesVisible(
            this.ValidityFrom() != this.observed.ValidityFrom
            || this.ValidityUntil() != this.observed.ValidityUntil
            || this.ValidityFlightFrom() != this.observed.ValidityFlightFrom
            || this.ValidityFlightUntil() != this.observed.ValidityFlightUntil
        );
        
        this.isCommentRulesVisible(
            this.Comments() != this.observed.Comments
        );
        
        this.isStatusTiersVisible(
            this.Tiers().length > 0
        );
    },
    
    /**
     * Update properties together with observed
     */ 
    initSubscribed: function(){
        this.FareFamily.subscribe($.proxy(this.updateFareFamilyName, this));
        
        //if empty value, set 0
        this.MinimumMiles.subscribe(this.setDefaultIfEmpty, this.MinimumMiles);
        
        this.Relationship.subscribe(this.onRelationshipUpdated, this);
        
        this.OperatingRules.subscribe(this.onOperatingRulesUpdated, this);
        this.MarketingRules.subscribe(this.onMarketingRulesUpdated, this);
        
        this.initApplyToAllSubscribed();
    },
    
    /**
     * Add subscribers to all properties tracked in 'apply all mode'
     */ 
    initApplyToAllSubscribed: function(){
        for(var i = 0 ; i < this.trackInApplyToAll.length ; i++){
            var prop = this[this.trackInApplyToAll[i]];
            if (!prop) {
                continue;
            }
            prop.subscribe($.proxy(this.applyToAllSubscriber, this, this.trackInApplyToAll[i]));
        }
    },
    
    /**
     * Fired when tracked property in 'apply all mode' changed its value
     * @param newValue - new value of property
     * @param propName - name of property
     */ 
    applyToAllSubscriber: function(propName, newValue){
        var ffp = this.getFfp();
        if(!ffp || !ffp.applyToAllMode() || ffp.applyToAllInProgress) return false;
        
        ffp.applyToAll(this, propName, newValue);
    },
    
    updateAccrualGroupValue: function(accrualGroup){
        
        var ffp = this.getFfp();
        if(!ffp || !ffp.applyToAllMode() || ffp.applyToAllInProgress) return false;
        
        ffp.applyToAll(this, 'AccrualGroups', accrualGroup);
    },
    
    /**
         * True if operating and marketing rules have default values
         */
    areOperationalRulesModified: function (relationship) {
        if (!_3.view) return false;

        var ffp = this.getFfp();

        if (!ffp) return false;

        var codeshare = parseInt(ffp.Codeshare());
        var rel = parseInt(relationship ? relationship : this.Relationship());

        var mr = this.MarketingRules();
        var or = this.OperatingRules();

        var preset =
            (
                codeshare == 1
                &&
                (
                    (rel == 1 && mr.length == 3 && mr.indexOf('1') != -1 && mr.indexOf('3') != -1 && mr.indexOf('4') != -1)
                    ||
                    (rel == 2 && mr.length == 2 && mr.indexOf('1') != -1 && mr.indexOf('3'))
                )
                &&
                (or.length == 2 && or.indexOf('1') != -1 && or.indexOf('2') != -1)
            )
            ||
            (
                codeshare == 2
                &&
                (
                    (rel == 1 && or.length == 4 && or.indexOf('1') != -1 && or.indexOf('2') != -1 && or.indexOf('3') != -1 && or.indexOf('4') != -1)
                    ||
                    (rel == 2 && or.length == 3 && or.indexOf('1') != -1 && or.indexOf('2') != -1 && or.indexOf('3') != -1)
                )
                &&
                (mr.length == 1 && mr.indexOf('1') != -1)
            );

        return !preset;
    },

    /**
     * If any of checkboxes of operational rules are changed
     */ 
    onOperatingRulesUpdated: function(){
        var field = $('#rule-operating-specific');
        
        if(!_3.view || field.size() < 1) return;
        
        if(this.OperatingRules.indexOf(_3.view.MO_RULE_SPECIFIC_ID) >= 0){
            setTimeout(function(){ $('#rule-operating-specific').focus(); }, 50);
        }
    },
    
    /**
     * If any of checkboxes of marketing rules are changed
     */ 
    onMarketingRulesUpdated: function(){
        var field = $('#rule-marketing-specific');
        
        if(!_3.view || field.size() < 1) return;
        
        if(this.MarketingRules.indexOf(_3.view.MO_RULE_SPECIFIC_ID) >= 0){
            setTimeout(function(){ $('#rule-marketing-specific').focus(); }, 50);
        }
    },
    
    /**
     * Prepopulate MarketingRules and OperatingRules
     */ 
    onRelationshipUpdated: function(rel){
        
        var ffp = this.getFfp();
        if(!ffp) return;
        
        var codeshare = parseInt(ffp.Codeshare());
        rel = parseInt(rel);

        var codeshareModified = this.areOperationalRulesModified(this.lastRelationship);
        
        if (this.isNew() || !codeshareModified) {
            
            //preset marketing rules if empty
            
            switch(codeshare){
                case 1: //operating
                
                    switch(rel){
                        case 1: //alliance
                            this.MarketingRules(['1', '3', '4']); //current, owning, alliance
                            break;
                            
                        case 2: //private
                            this.MarketingRules(['1', '3']); //current, owning
                            break;
                    }
                    break;
                    
                case 2: //marketing
                    this.MarketingRules(['1']); //current
                    break;
            }
            
            //preset operating rules if empty
            
            switch(codeshare){
                case 1: //operating
                    this.OperatingRules(['1', '2']); //current, subsidiary
                    break;
                    
                case 2: //marketing
                    switch(rel){
                        case 1: //alliance
                            this.OperatingRules(['1', '2', '3', '4']); //current, subsidiary, owning, alliance
                            break;
                            
                        case 2: //private
                            this.OperatingRules(['1', '2', '3']); //current, subsidiary, owning
                            break;
                    }
                    break;
            }

            this.lastRelationship = this.Relationship();
        }
        
        this.isOperationalRulesVisible(
            this.MarketingRules().length > 0
            || this.OperatingRules().length > 0
            || this.FlightNoLimit() != this.observed.FlightNoLimit
            || this.FlightNoExcl() != this.observed.FlightNoExcl
        );
    },
    
    /**
     * Soft validation - set default values for empty strings
     */ 
    setDefaultIfEmpty: function(newValue){
        if(newValue == null || newValue == '') newValue = 0;
        this(newValue);
    },
    
    /**
     * Inits tag inputs components in rule edit view
     */ 
    initTagInputs: function(){
        this.update('subsidiariesInput', {
            beforeAdd: _3.utils.toUpperCase,
            tags: this.Subsidiaries
        });
        
        this.update('geoInput', {
            beforeAdd: _3.utils.toUpperCase,
            tags: this.GeoFilters,
            stopKeys: ['comma', 'enter'],
            validationUrl: _3.utils.getJsonUrl('geo-rules-validation.json')
        });  
        
        this.update('codedConditionsInput', {
            beforeAdd: _3.utils.toUpperCase,
            tags: this.CodedConditions,
            stopKeys: ['comma', 'enter']
        }); 
        
        this.update('mSpecificAirlinesInput', {
            beforeAdd: _3.utils.toUpperCase,
            tags: this.MarketingSpecificAirlines,
            stopKeys: ['comma', 'enter'],
        });
        
        this.update('oSpecificAirlinesInput', {
            beforeAdd: _3.utils.toUpperCase,
            tags: this.OperatingSpecificAirlines,
            stopKeys: ['comma', 'enter'],
        });
        
        this.update('flightNoExclInput', {
            beforeAdd: _3.utils.toUpperCase,
            tags: this.FlightNoExcl,
            stopKeys: ['comma', 'enter']
        }); 
        
        this.update('flightNoLimitInput', {
            beforeAdd: _3.utils.toUpperCase,
            tags: this.FlightNoLimit,
            stopKeys: ['comma', 'enter']
        }); 

    },
    
    /**
     * Remove rule
     */
    remove: function(){
        this.getFfp().removeRule(this);
    },
    
    /**
     * Update fare family name according to id set by user in rule edit view
     */ 
    updateFareFamilyName: function(){
        var ff = _3.view.fareFamilies();
        
        for(var i = 0 ; i < ff.length ; i++){
            if(ff[i].Id == this.FareFamily()){
                this.FareFamilyName(ff[i].Name);
                break;
            }
        }
    },
    
    /**
     * Edit rule
     */ 
    edit: function(){
        if(!this.isLoaded()) this.load($.proxy(this.onEditSuccess, this));
        else this.onEditSuccess();
    },
    
    onEditSuccess: function(){
        this.getFfp().editRule(this);
    },
    
    /**
     * In-row edit rule
     */ 
    quickEdit: function(){
        this.isInQuickEditMode(true);
        this.getFfp().quickEditRule(this);
    },
    
    /**
     * Add accrual groups for types, that rule hasn't yet
     */ 
    initAccruals: function(){
        if(!_3.view) return;
        if(!_3.view.editedFfp()) return;
        
        var ffp = _3.view.editedFfp();
        
        //default set of accrual types
        var accruals = ffp.ConfiguredAccruals();
        var ag = this.AccrualGroups();
        
        var toAdd = [];
        
        for(var i = 0 ; i < accruals.length ; i++){
            var found = false;
            
            for(var j = 0 ; j < ag.length ; j++){
                if(ag[j].AccrualType() == accruals[i].AccrualType() && ag[j].EarningBase() == accruals[i].EarningBase()){
                    found = true;
                    break;
                }
            }
            if(!found) toAdd.push({ type: accruals[i].AccrualType(), base: accruals[i].EarningBase()});
        }
        
        for(var i = 0 ; i < toAdd.length ; i++){
            this.AccrualGroups.push(new _3.RuleAccrualGroup({ 
                AccrualType: toAdd[i].type,
                EarningBase: toAdd[i].base
            }));
        }
    },
    
    /**
     * Save on ENTER pressed
     */ 
    saveOnKeyPress: function(data, event){
        if(event.keyCode == _3.view.ENTER_KEY || event.charCode == _3.view.ENTER_KEY){
            
            var ffp = this.getFfp();
            
            if(ffp && ffp.applyToAllMode()) ffp.saveRules();
            else this.save();
            
        } 
        
        return true;
    },
    
    /**
     * Push rule data to server
     */ 
    save: function(callback){
        
        if(!this.isValid()){
            _3.view.validate(true);
            return;
        }
        
        _3.view.isLoading(true);
        
        //default - update whole rule data
        var file = 'rule-update.json';
        
        if(this.isNew()){
            
            //for new rule, that has been added
            file = 'rule-add.json';
            
        }else if(this.isInQuickEditMode()){
            
            //for rule in quick-edit mode
            file = 'rule-update-quick.json';
        
        }
        
        var url = _3.utils.getJsonUrl(file);
        var data = ko.toJSON(this, this.jsonFilter);
        
        $.post(url, data, $.proxy(function(response){
            if(response.Success){
                
                this.onSaveSuccess(response.Rule); 
                this.getFfp().saveRule(); 
                
                if($.isFunction(callback)) callback();
            
            }else{
                _3.view.addAlert(response.Message);
            }			
            
            _3.view.isLoading(false); 
        }, this));  
    },
    
    /**
     * After rule has been successfully saved
     */ 
    onSaveSuccess: function(data){
        var ffp = this.getFfp();
        
        //update rule with data from server
        if(data) this.ext(data);
        
        this.isInQuickEditMode(false);
        
        this.isNew(false);
                
        if(this.CloneOf) delete this.CloneOf;
        
    },
    
    /**
     * Cancel rule edit
     */ 
    cancelEdit: function(){
        
        //reload previous state
        if(!this.isNew()) this.load($.proxy(this.onCancelEditSuccess, this));
        else this.onCancelEditSuccess();
    },
    
    onCancelEditSuccess: function(){
        var ffp = this.getFfp();
        if(ffp) ffp.cancelRuleEdit();   
    },
    
    /**
     * Shorthand method
     */ 
    getFfp: function(){
        if(!_3.view) return false;
        return _3.view.getFfpById(this.ProgramId());
    },
    
    /**
     * Pulls data from server
     */ 
    load: function(callback){
        _3.view.isLoading(true);
        
        //default - load full data for rule
        var file = 'rule-one.json';
        
        if(this.isInQuickEditMode() && !this.getFfp().applyToAllMode()){
            //load only subset of whole data for rule (grid view)
            file = 'rule-one-quick.json';
        }
        
        var url = _3.utils.getJsonUrl(file);
        
        $.post(url, {
           Id: this.Id
        }, $.proxy(function(response){
            
            if(response.Success){
                
                this.ext(response.Rule);

                this.lastRelationship = response.Rule.Relationship ? response.Rule.Relationship : 1;

                this.isLoaded(true);

                if (!this.isInQuickEditMode()) {
                    //make sure after data is updated, datepickers are binded again
                    _3.view.initDatepickers();
                }
                
                var ffp = this.getFfp();
                this.isInQuickEditMode(false);
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);
        }, this));  
    },
    
    /**
     * Switches edited rule to next one (and cancel changes)
     */
    editNext: function(){
        //reload previous state
        if(!this.isNew()) this.load($.proxy(this.onCancelEditNext, this));
        else this.onCancelEditNext();
    },
    
    /**
     * Opens next rule to edit
     */ 
    onCancelEditNext: function(){
        var ffp = this.getFfp();
        if(ffp) ffp.nextRule();
    },
    
    /**
     * Switches edited rule to previous one (and cancel changes)
     */
    editPrev: function(){
        //reload previous state
        if(!this.isNew()) this.load($.proxy(this.onCancelEditPrev, this));
        else this.onCancelEditPrev();
    },
    
    /**
     * Opens previous rule to edit
     */ 
    onCancelEditPrev: function(){
        var ffp = this.getFfp();
        if(ffp) ffp.prevRule();
    },
    
    /**
     * Add FFP tier
     * @param FFPTier tier
     */ 
    addTier: function(tier){
        this.Tiers.push(tier);
        this.editedTier(new _3.FFPTier());
    },
    
    /**
     * Remove FFP tier
     * @param FFPTier tier
     */ 
    removeTier: function(tier){
        this.Tiers.remove(tier);
    },
    
    /**
     * Edit FFP tier
     * @param FFPTier tier
     */ 
    editTier: function(tier){
        this.editedTier(tier);  
        this.removeTier(tier);
    }
});
