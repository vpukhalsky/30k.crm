/**
 * FFP Accrual class
 * @author Artur Polkowski
 */ 

_3.FFPAccrual = KClass.extend({
    
    Id: 0,
    
    observed: {
        
        /**
         * eg. 'Rdm' - works as an unique identifier for any type of miles
         * @required
         */
        AccrualType: '',
        
        /**
         * User-defined alias of accrual specific for particuar FFP
         * eg. 'Cool Status Miles'
         */ 
        Alias: '',
        
        /**
         * Defined in FFP edit view
         * @type int
         */ 
        EarningBase: null,
        
        /**
         * Common name for accrual type
         */ 
        Name: '',
    },
    
    computed: {

        /**
         * FFP form edit validation helper
         */ 
        isValid: function(){
            return this.Alias().length > 0 && this.EarningBase() && this.AccrualType();
        }
    },
    
    filtered: [
        'isValid'
    ],
    
    init: function(data){
        this.parent(data);
    }
});