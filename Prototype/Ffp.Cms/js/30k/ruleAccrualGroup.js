/**
 * FFP Rule Accrual Group class
 * @author Artur Polkowski
 */
 
_3.RuleAccrualGroup = KClass.extend({
    
    observed: {
        
        /**
         * Type of accrual eg. 'Rdm'
         * @see FFPAccrual
         * @type string
         */ 
        AccrualType: '',
        
        EarningBase: 1,
        
        Value: null,
        
        /**
         * Array of FFP rule sub-rules with accrual data
         * @type RuleAccrual
         */
        Rules: []
    },
    
    computed: {
        
        hasRanges: function(){
            var ranges = this.Rules();
            return ranges.length > 0 && ranges[0].Id && ranges[0].RangeFrom() != ranges[0].RangeTo();
        },
        
        getRangesTooltip: function(){
            
            var ranges = this.Rules();
            var tip = '';
            
            if(ranges.length > 0){
                
                for(var j = 0 ; j < ranges.length ; j++){
                    
                    if(tip != '') tip += '<br />';
                    tip += '<strong>' + ranges[j].RangeFrom() + '&ndash;' + ranges[j].RangeTo() + '</strong>: ' + ranges[j].Factor();
                    
                }
            
            }
            
            return tip;
        },
        
        isValueValid: function(){
            return $.isNumeric(this.Value());
        }
    },
    
    filtered: [
        'hasRanges',
        'getRangesTooltip',
        'isValueValid'
    ],
    
    mapped: {
        Rules: _3.RuleAccrual
    },
    
    init: function(data){
        this.parent(data);
        
        if(this.Rules().length < 1){
            
            //always keep at least one rule to edit
            this.addRule();
        }
    },
    
    addRule: function(){
        this.Rules.push(new _3.RuleAccrual());
    },
    
    removeRule: function(rule){
        this.Rules.remove(rule);
    },
    
    initSubscribed: function(){
        this.Value.subscribe($.proxy(this.updateValue, this));
    },
    
    updateValue: function(newValue){

        if(!_3.view || !_3.view.editedFfp() || !_3.view.editedFfp().editedRule()) return;
        
        var rule = _3.view.editedFfp().editedRule();
        rule.updateAccrualGroupValue(this);
        
    }
});