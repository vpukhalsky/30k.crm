/**
 * FFP View Model class
 * Main class of FPP CMS
 * @author Artur Polkowski
 * 
 */ 
_3.FFPViewModel = KClass.extend({
    
    /**
     * Knockout.js data-view-view-data binding enabled
     */ 
    isViewModel: true,
    
    /**
     * Constant value - id of "Specific" option
     * available for marketing and operational rules (rule edit view)
     * @see json/ffp-all.json
     */ 
    MO_RULE_SPECIFIC_ID: '6',
    
    /**
     * Constant values, 
     * Tests JSON CalcBlocks are distinguished based on them
     * @see json/text-all.json
     */ 
    BASIC_TEST_DISTANCE: 1000,
    MINIMUM_TEST_DISTANCE: 100,
    
    /**
     * Enter key code (to save rule in quick mode)
     */ 
    ENTER_KEY: 13,
    
    /**
     * Scroll to specified object eg. when new added
     */ 
    scrollTo: null,
    
    /**
     * Object containing last scroll position for all the views
     */ 
    scrollPos: {},
    
    observed: {
        
        /**
         * Which section is currently visible for user?
         * @type string
         */ 
        selectedMainNav: 'ffp-all',
        
        /**
         * Is initial data retrived from server?
         * @see ffp-all.json
         */ 
        isLoaded: false,
        
        /**
         * Is any pending AJAX request enabled?
         */ 
        isLoading: false,
        
        /**
         * Array of FFPs
         * @see ffp.js
         * @type FFP
         */ 
        ffps: [],
        
        //ffp view
        
        /**
         * Array of alliances
         * @see ffp-all.json
         * @type KeyValue
         */ 
        alliances: [],
        
        /**
         * Array of FFP tiers aliases names'
         * @see ffp-all.json
         * @type string
         */ 
        tierAliases: [],
        
        /**
         * Array of available accruals to set in FFPs
         * @see ffp-all.json
         * @type KeyValue
         */ 
        accruals: [],
        
        /**
         * Array of accrual earining bases
         * @see ffp-all.json
         * @type KeyValue
         */ 
        earningBases: [],
        
        /**
         * Currently edited FPP, also set if it's rule is edited, otherwise - null
         * @type FFP
         */ 
        editedFfp: null,
        
        //rule view
        
        /**
         * Array of FFP rule relationship types
         * @see ffp-all.json
         * @type KeyValue
         */ 
        relationships: [],
        
        /**
         * Array of FFP rule fare families
         * @see ffp-all.json
         * @type KeyValue
         */ 
        fareFamilies: [],
        
        /**
         * Array of FFP rule operational and marketing sub-rules
         * @see ffp-all.json
         * @type KeyValue
         */ 
        operationalRules: [],
        
        //test view
        
        /**
         * Array of Tests
         * @see test.js
         * @type Test
         */ 
        tests: [],
        
        /**
         * Array of FFPs
         * @see ffp.js
         * @type FFP
         */ 
        testFfps: [],
        
        /**
         * Currently edited Test
         * @type Test
         */ 
        editedTest: null,
        
        /**
         * Have tests been loaded?
         * @type bool
         */ 
        areTestsLoaded: false,
        
        /**
         * Selected tests in grid view
         */ 
        selectedTests: [],
        
        /**
         * Array of alerts displayed on top of screen eg. in case of server errors
         * @see alert.js
         * @type Alert
         */ 
        alerts: [],
        
        showValidation: false
    },
    
    computed: {
        
        /**
         * Show alert background?
         */ 
        isInAlertMode: function(){
            var ffp = this.editedFfp();
            return ffp && ffp.applyToAllMode();
        }
    },
    
    mapped: {
        editedFfp: _3.FFP,
        ffps: _3.FFP,
        alliances: _3.KeyValue,
        accruals: _3.FFPAccrual,
        earningBases: _3.KeyValue,
        relationships: _3.KeyValue,
        fareFamilies: _3.KeyValue,
        operationalRules: _3.KeyValue,
        alerts: _3.Alert,
        editedTest: _3.Test,
        tests: _3.Test,
        testFfps: _3.FFP
    },
    
    /**
     * Static array of view objects used to display main menu
     * @type Object
     */ 
    mainNav: [],
    
    /**
     * Constructor
     * @param data - config object overwritting default class properties
     */ 
    init: function(data){
        
        this.initFoundation();
        
        //build navigation array
        this.initNav();
        
        this.parent(data);
        
        //no edited FFP
        this.setEditedFfp(null);
        
        //no edited Test
        this.setEditedTest(null);
        
        $(window).on('scroll', $.proxy(this.onScroll, this));
        
        //pull data from server
        this.load();
    },
    
    /**
     * Init Foundation scripts 
     */ 
    initFoundation: function(){
        $(document).foundation();  
    },
    
    /**
     * Set array of main menu items
     */ 
    initNav: function(){
        this.mainNav = [
            { 
                //menu label
                name: 'All Programs', 
                
                //id of an document section
                id: 'ffp-all', 
                
                //is position visible in menu?
                visible: true, 
                
                //callback action after user selects that menu item
                callback: $.proxy(this.showFfps, this) 
            },
            //{ name: 'Add Program', id: 'ffp-edit', visible: true, callback: $.proxy(this.addFfp, this) },
            //{ name: 'All Rules', id: 'rule-all', visible: false, callback: $.proxy(this.showRules, this) },
            //{ name: 'Edit Rule', id: 'rule-edit', visible: false, callback: $.proxy(this.editRule, this) },
            { name: 'All Tests', id: 'test-all', visible: true, callback: $.proxy(this.showTests, this) }//,
            //{ name: 'Add Test', id: 'test-edit', visible: true, callback: $.proxy(this.addTest, this) }
        ];
    },
    
    /**
     * Sets single Test in 'edit' mode
     * @param test
     * @see tests
     */ 
    setEditedTest: function(test){
        if(test) this.editedTest(test); //set defined current Test
        else if(test === null || test === false) this.editedTest(null); //no Test is edited
        else {
            //create new Test and open to edit
            
            this.update('editedTest', { isNew: true });
        }
    },
    
    /**
     * Clone test object after successful clone request
     * @param clonedProperties - Object to save as new test
     */ 
    cloneTest: function(clonedProperties){
        
        //create new test
        this.setEditedTest();
        this.editedTest().ext($.extend(clonedProperties, { isNew: false }));
        
        //add to collection
        this.tests.push(this.editedTest());    
        
        this.addNotification('Test has been successfully cloned.');
    },
    
    addTest: function(){
        if(!this.areTestsLoaded()) this.loadTests($.proxy(this.addTestCallback, this));
        else this.addTestCallback();
    },
    
    /**
     * Creates new Test object and opens edit section
     */ 
    addTestCallback: function(){
        this.setEditedTest();
        this.selectedMainNav('test-edit');
    },
    
    /**
     * Opens Test grid section and drops edited Test
     */ 
    cancelTestEdit: function(){
        this.showTests();
        this.setEditedTest(null);
    },
    
    /**
     * Adds currently edited Test to tests array, finishes edit mode
     */ 
    saveTest: function(){
        if(this.areTestsLoaded()) this.onSaveTestSuccess();
        else this.loadTests($.proxy(this.onSaveTestSuccess, this));
    },
    
    /**
     * Add test to collection and show confirmation
     */ 
    onSaveTestSuccess: function(){
        if(this.tests.indexOf(this.editedTest()) == -1){
            this.tests.push(this.editedTest());    
        }
        this.addNotification('Test has been successfully saved.');
    },
    
    /**
     * Opens Test edit section for existing Test
     */ 
    editTest: function(test){
        this.setEditedTest(test);
        this.selectedMainNav('test-edit');
    },
    
    /**
     * Opens section with Tests grid
     */ 
    showTests: function(){
        if(!this.areTestsLoaded()) this.loadTests($.proxy(this.onShowTestSuccess, this));
        else this.onShowTestSuccess();
    },
    
    /**
     * Callback fired after tests have ben successfully fetched from server
     */ 
    onShowTestSuccess: function(){
        this.selectedMainNav('test-all');
    },
    
    /**
     * Pull tests data from server to display test grid
     */ 
    loadTests: function(callback){
        _3.view.isLoading(true);
        
        _3.utils.sendGet(_3.utils.getJsonUrl('test-all.json'), $.proxy(function(response){
            
            if(response.Success){
                var ffps = response.Value.FFPs;
                
                for(var i = 0 ; i < ffps.length ; i++){
                    for(var j = 0 ; j < ffps[i].ConfiguredAccruals.length ; j++){
                        ffps[i].ConfiguredAccruals[j].Enabled = true;
                    }
                }
                
                this.update('testFfps', response.Value.FFPs);
                this.update('tests', response.Value.Tests);
                
                this.areTestsLoaded(true);
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false); 
            
        }, this));
    },
    
    /**
     * Add subscribers to knockout.js observabled properties
     */ 
    initSubscribed: function(){
        
        //makes sure after AJAX request is finished to init datepickers
        this.isLoading.subscribe($.proxy(this.onLoadingFinished, this));
        
        this.selectedMainNav.subscribe($.proxy(this.onViewChange, this));
    },
    
    /**
     * When initial data has been loaded successfully
     */ 
    onLoadSuccess: function(response){
        //update 'alliances' array with mapped objects
        this.update('alliances', response.Alliances);
                
        this.update('accruals', response.Accruals);
        this.update('ffps', response.FFPs);
        this.update('earningBases', response.EarningBases);
        this.update('relationships', response.Relationships);
        this.update('fareFamilies', response.FareFamilies);
        this.update('operationalRules', response.OperationalRules);
                
        this.tierAliases(response.TierAliases);
                
        this.isLoaded(true);
    },
    
    /**
     * Pulls initial data from server
     * @see ffp-all.json
     */ 
    load: function(){
        this.isLoading(true);
        
        $.getJSON(_3.utils.getJsonUrl('ffp-all.json'), $.proxy(function(response){
            
            if(response.Success){
                
                this.onLoadSuccess(response);
                
            }else{
                
                this.addAlert(response.Message);
            
            }
            
            this.isLoading(false);  
        }, this));  
    },
    
    /**
     * Sets single FFP in 'edit' mode
     * @param ffp
     * @see ffps
     */ 
    setEditedFfp: function(ffp){
        if(ffp) this.editedFfp(ffp); //set defined current FFP
        else if(ffp === null || ffp === false) this.editedFfp(null); //no FFP is edited
        else {
            //create new FFP and open to edit
            
            this.update('editedFfp');
            this.editedFfp().isNew(true);
        }
    },
    
    /**
     * Creates new FFP object and opens edit section
     */ 
    addFfp: function(){
        this.setEditedFfp();
        this.selectedMainNav('ffp-edit');
    },
    
    /**
     * Opens FFP grid section and drops edited FFP
     */ 
    cancelFfpEdit: function(){
        this.selectedMainNav('ffp-all');
        this.setEditedFfp(null);
    },
    
    /**
     * Adds currently edited FFP to ffps array, finishes edit mode
     */ 
    saveFfp: function(){
        if(this.ffps.indexOf(this.editedFfp()) == -1){
            this.ffps.push(this.editedFfp());    
        }
        this.cancelFfpEdit();
    },
    
    /**
     * Opens FFP edit section for existing FFP
     */ 
    editFfp: function(ffp){
        this.setEditedFfp(ffp);
        this.selectedMainNav('ffp-edit');
    },
    
    /**
     * Opens section with all rules for edited FFP
     */ 
    showRules: function(ffp){ 
        
        if(!(ffp instanceof _3.FFP)) ffp = this.editedFfp();
        if(!ffp) return;
        
        this.setEditedFfp(ffp);
        
        this.selectedMainNav('rule-all');
        
    },
    
    /**
     * Opens edit FFP rule section 
     */ 
    editRule: function(rule){
        this.selectedMainNav('rule-edit');
    },
    
    /**
     * Opens section with FFPs grid
     */ 
    showFfps: function(){
        this.selectedMainNav('ffp-all');
    },
    
    /**
     * Helper method used in FFP rule to retrive corresponding FFP
     * @param int ffpId
     * @return FFP
     */
    getFfpById: function(ffpId){
        var ffps = this.ffps();
        for(var i = 0 ; i < ffps.length ; i++){
            if(ffps[i].Id == ffpId) return ffps[i];
        }
        return null;
    },
    
    /**
     * Helper method used in FFP rule to retrive corresponding FFP
     * @param string ffpCode
     * @return FFP
     */
    getFfpByCode: function(ffpCode){
        if(typeof ffpCode != 'string') return null;
        
        var ffps = this.testFfps();
        for(var i = 0 ; i < ffps.length ; i++){
            if(ffps[i].Code().toLowerCase() == ffpCode.toLowerCase()) return ffps[i];
        }
        return null;
    },
    
    /**
     * Re-initialize datapickers if needed
     */ 
    onLoadingFinished: function(){
        if(this.isLoading() == false){
            this.initDatepickers()
        }
    },
    
    /**
     * Inits datepicker widget
     * @see foundation-datepicker.js
     */
    initDatepickers: function(){
        var view = this.selectedMainNav();
        if(view == 'rule-edit' || view == 'test-edit'){
            
            //init only when FFP rule edit or test edit section is opened
            
            $('.date-input').fdatepicker({
                format: 'mm/dd/yyyy',
                weekStart: 1
            });   
        }
    },
    
    /**
     * After HTML <section> containing view has been changed
     */ 
    onViewChange: function(){
        this.showValidation(false);  
        this.initDatepickers();
        this.scroll();
    },
    
    /**
     * Prompts user to confirm he wants to delete FFP rule
     */ 
    removeRule: function(){
        $('#remove-rule-modal').foundation('reveal', 'open');
    },
    
    /**
     * Prompts user to confirm he wants to delete selected FFP rules
     */ 
    removeRules: function(){
        $('#remove-rules-modal').foundation('reveal', 'open');
    },
    
    /**
     * Closes remove FFP rule modal window
     */ 
    cancelRemoveRule: function(){
        $('#remove-rule-modal').foundation('reveal', 'close');
    },
    
    /**
     * Closes remove selected FFP rules modal window
     */ 
    cancelRemoveRules: function(){
        $('#remove-rules-modal').foundation('reveal', 'close');
    },
    
    /**
     * Prompts user to confirm he wants to delete Test
     */ 
    removeTest: function(test){
        $('#remove-test-modal').foundation('reveal', 'open');
        this.editedTest(test);
    },
    
    /**
     * Closes remove Test modal window
     */ 
    cancelRemoveTest: function(){
        $('#remove-test-modal').foundation('reveal', 'close');
    },
    
    /**
     * User confirms test save prior other action (run / clone)
     */ 
    confirmSaveTest: function(){
        $('#save-test-modal').foundation('reveal', 'close');
        this.editedTest().confirmSave();
    },
    
    /**
     * Closes save Test modal window
     */ 
    cancelSaveTest: function(){
        $('#save-test-modal').foundation('reveal', 'close');
        this.editedTest().cancelSave();
    },
    
    /**
     * Remove test from set
     */ 
    confirmRemoveTest: function(callback){
        
        //close modal alert window
        this.cancelRemoveTest();
        
        this.isLoading(true);
        
        var test = this.editedTest();
        var url = _3.utils.getJsonUrl('test-delete.json');
        
        //Uncomment for REST url param
        //url += '/' + test.Id;
        //_3.utils.sendDelete(url, $.proxy(function(response){
        
        _3.utils.sendGet(url, $.proxy(function(response){
            
            if(response.Success){
                
                //Confirmed? Remove from set
                this.tests.remove(test);
                if($.isFunction(callback)) callback();
                
                this.addNotification('Test has been sucessfully deleted.');
            }else{
                this.addAlert(response.Message);
            }
            
            this.isLoading(false); 
        }, this));
        
    },
    
    /**
     * Run multiple tests at once
     */ 
    runTests: function(callback){
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('test-run-multiple.json');
        var data = { Ids: this.selectedTests() };
        
        _3.utils.sendPost(url, data, $.proxy(function(response){
            if(response.Success){
                
                var tests = response.Value.Tests;
                
                for(var i = 0 ; i < tests.length ; i++){
                    var id = tests[i].Id;
                    
                    //find corresponding test in collection
                    for(var j = 0 ; j < this.tests().length ; j++){
                        var test = this.tests()[j];
                        
                        if(test.Id == id){
                            //save new values
                            test.ext(tests[i]);
                            break;
                        }
                    }
                }
                
                this.addNotification('Tests has been successfully executed.');
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);
        }, this)); 
    },
    
    /**
     * Confirm test has been successfully ran
     */ 
    runTest: function(){
        this.addNotification('Test has been successfully executed.');
    },
    
    /**
     * Data-binding helper method
     * @see programs.html
     * @param FFPAccrual accrual
     * @return string
     */ 
    getAccrualName: function(type){
        /*var accrual = this.getAccrualFromType(type);
        if(accrual) return accrual.Name();
        return '';*/
        var name = '';
        var a = this.accruals();
        for(var i = 0 ; i < a.length ; i++){
            if(type == a[i].AccrualType()){
                name = a[i].Name();
                break;
            }
        }
        return name;
    },
    
    /**
     * Data-binding helper method
     * @see programs.html
     * @param FFPAccrual accrual
     * @return string
     */
    getAccrualNameForHeader: function(accrual) {
        return accrual.Alias();
    },
    
    /**
     * Data-binding helper method
     * @see programs.html
     * @param string type
     * @return string
     */ 
    getAccrualAlias: function(type){
        var accrual = this.getAccrualFromType(type);
        if(accrual) return accrual.Alias();
        return '';
    },
    
    /**
     * Data-binding helper method
     * @see programs.html
     * @param string type
     * @return boolean
     */ 
    getAccrualEnabled: function(type){
        var accrual = this.getAccrualFromType(type);
        if(accrual) return accrual.Enabled();
        return '';
    },
    
    /**
     * Returns currently edited FFP's configured accrual of specified type
     * @param string type
     * @return FFPAccrual
     */ 
    getAccrualFromType: function(type){
        var ca = this.editedFfp().ConfiguredAccruals();
        
        for(var i = 0 ; i < ca.length ; i++){
            if(ca[i].AccrualType() == type){
                return ca[i];
            }
        }
        
        return null;
    },
    
    /**
     * Displays error alert on top of window
     */ 
    addAlert: function(message){
        this.alerts.push(new _3.Alert({ message: message }));
    },
    
    /**
     * Displays green notification on top of window
     */ 
    addNotification: function(message){
        this.alerts.push(new _3.Alert({ message: message, css: 'success' }));
    },
    
    /**
     * User action to close alert
     */ 
    removeAlert: function(alert){
        this.alerts.remove(alert);
    },
    
    /**
     * Show validation alerts and focus on first invalid field
     */ 
    validate: function(){
        this.showValidation(true);
        
        var invalid = $('.validated-field.error:visible').first();
        if(invalid && invalid.size() > 0) invalid.find('input, textarea, select').first().focus();
    },
    
    /**
    * Updates tabindex attributes fields included in rule edit view
    */
    updateRuleTabIndex: function(){
        var first = $('#rule-fare-family');
        if(first.size() <= 0) return;
        
        var ti = parseInt(first.attr('tabindex'));
        
        var gridRows = $('#rule-creator-grid tbody tr');
        var mr = $('#marketing-rules').find('input[type="checkbox"]');
        var or = $('#operating-rules').find('input[type="checkbox"]');
        
        //vertical order
        gridRows.each($.proxy(function(rowIndex, row){
            var inputs = $(row).find('input[type="text"],input[type="checkbox"]');
            inputs.each($.proxy(function(inputIndex, input){
                var index = ti + inputIndex * gridRows.size() + rowIndex;
                $(input).attr('tabindex', index);
            }, this));
        }, this));
        
        var gridInputs = $('#rule-creator-grid').find('input[type="text"],input[type="checkbox"]');
        ti += gridInputs.size();
        
        //horizontal order
        //ti = this.incrementTabIndex(gridInputs, ti);
        
        ti = this.incrementTabIndex(mr, ti);
        $('#rule-marketing-specific').attr('tabindex', ti++);
        
        ti = this.incrementTabIndex(or, ti);
        $('#rule-operating-specific').attr('tabindex', ti++);
        
        $('#rule-flight-limitations').attr('tabindex', ti++);
        $('#rule-flight-exclusions').attr('tabindex', ti++);
        
        $('#rule-geo').attr('tabindex', ti++);
        $('#rule-geo-cc').attr('tabindex', ti++);
        
        $('#rule-valid-from').attr('tabindex', ti++);
        $('#rule-valid-until').attr('tabindex', ti++);
        
        $('#rule-flight-from').attr('tabindex', ti++);
        $('#rule-flight-until').attr('tabindex', ti++);
        
        ti = this.incrementTabIndex($('#rule-status-tiers').find('input, select, button'), ti);
        
        $('#rule-comments').attr('tabindex', ti++);
        
        $('#submit-edited-rule').attr('tabindex', ti++);
        $('#cancel-edited-rule').attr('tabindex', ti++);
    },
    
    /**
     * Increment tabindex attribute of elements starting with tabIndex value
     * @param elements - jQueryinput elements
     * @param tabIndex - @type int, starting index
     * @return last used tabIndex + 1
     */ 
    incrementTabIndex: function(elements, tabIndex){
        for(var i = 0 ; i < elements.size() ; i++){
            elements.eq(i).attr('tabindex', tabIndex++);
        }
        return tabIndex;
    },
    
    /**
     * Scroll to object set in @scrollTo
     */ 
    scroll: function(){
        
        var el = null;
        var pos = -1;
        
        if(this.scrollTo instanceof _3.Rule){
            el = $('[data-rule-id="' + this.scrollTo.RuleId() + '"]');
        }
        
        var view = this.selectedMainNav();
        
        if(el && el.size() > 0) pos = parseInt(el.offset().top);
        else if(this.scrollPos[view] && view == 'rule-all'){
            pos = this.scrollPos[this.selectedMainNav()]; 
        }else{
            pos = 0;
        }
            
        if(pos >= 0){
            $('html, body').animate({
                scrollTop: pos
            }, 50);   
        }
        
        this.scrollTo = null;
    },
    
    /**
     * Responses to scroll event,
     * saves scroll position according to the current view
     */ 
    onScroll: function(e){
        if(this.isLoaded() && !this.isLoading()){
            this.scrollPos[this.selectedMainNav()] = $(window).scrollTop();
        }
    }
});

//set as globally accessable
_3.view = new _3.FFPViewModel();
