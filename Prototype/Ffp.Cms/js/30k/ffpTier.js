/**
 * FFP Tier class
 * @author Artur Polkowski
 * 
 */ 

_3.FFPTier = KClass.extend({
    
    /**
     * Unique tier id
     * @type int
     */ 
    Id: null,
    
    observed: {
        
        /**
         * Tier name, common for every programs
         * @type string
         */ 
        Name : '',
        
        /**
         * User-defined name for tier
         * @type string
         */ 
        TierAlias: null,

        IsElite: false,
        
        Bonus: 0
    },
    
    computed: {
        
        /**
         * Bonus property validation
         */ 
        isBonusIncorrect: function(){
            if(!this.Bonus()) return false;
            
            return !this.Bonus().toString().match(/^[0-9\.]+$/);
        }
    },
    
    filtered: [
        'isBonusIncorrect'
    ]
});