/**
 * FFP class
 * @author Artur Polkowski
 * 
 */ 
_3.FFP = KClass.extend({
    
    /**
     * Program unique id
     * @type int
     */ 
    Id: null,
    
    /*original codeshare received from the server*/
    originalCodeshare: null,
    
    /**
     * Flag to protect infinite recursion
     * while updating rules in 'apply to all' mode
     */ 
    applyToAllInProgress: false,
    
    skipRulesSorting: false,
    
    observed: {
        
        /**
         * True if FFP has neever been submited to server
         */ 
        isNew: false,
        
        Name: '',
        
        Code: '',
        
        Enabled: true,
        
        HelpUrl: '',
        
        WebSiteUrl: '',
        
        SignUpUrl: '',
        
        /**
         * Alliance id
         * @see FFPViewModel.alliances
         * @type int
         */
        Alliance: null,
        
        /**
         * Array of FFP tiers
         * @type FFPTier
         */
        Tiers: [],
        
        /**
         * Array of accruals both enabled and disabled for this FFP
         * @type FFPAccrual
         */ 
        ConfiguredAccruals: [],
        
        /**
         * Earning base id
         * @see FFPViewModel.earningBases
         * @type string (knockout.js does not work with int!)
         */ 
        Codeshare: '1',
        
        /**
         * Elite bonus type id
         * @type string (knockout.js does not work with int!)
         */
        EliteBonusType: '1',
        
        /**
         * Base distance unit
         * @type string 'mi' || 'km'
         */ 
        DistanceUnit: 'mi',
        
        /**
         * Comments for FFP, optional
         * @type string
         */ 
        Comments: '',
        
        /**
         * FFP airlines
         * @type string
         */ 
        OwningAirlines: [],
        
        /**
         * Currently edited tier object
         * @type FFPTier
         */ 
        editedTier: null,
        
        /**
         * Is FFP fully updated with data pulled from server?
         */ 
        isLoaded: false,
        
        /**
         * Tag input object for airlines
         * @see tagInput.js
         */ 
        airlinesInput: null,
        
        /**
         * Array of FFP rules
         * @type FFPRule
         */
        rules: [],
        
        /**
         * Does FFP have all rules pulled from server? 
         */
        hasRulesLoaded: false,
        
        /**
         * Currently edited FFP rule
         * @type FFPRule
         */
        editedRule: null,
        
        /**
         * Rule Ids of selected rules in grid view
         * @type int
         */ 
        selectedRules: [],
        
        /**
         * Are all rules selected via top checkbox in rule grid?
         */ 
        allRulesSelected: false,
        
        /**
         * Is 'apply to all' mode on? 
         */ 
        applyToAllMode: false
    },
    
    computed: {
        
        /**
         * @return HTML element class
         */ 
        EnabledClass: function(){
            return this.Enabled() ?  'success' : 'alert';
        },
        
        /**
         * @return HTML caption in FFP grid
         */ 
        EnabledText: function(){
            return this.Enabled() ? 'Enabled' : 'Disabled';
        },
        
        isNameValid: function(){
            return this.Name().length > 0;  
        },
        
        isCodeValid: function(){
            return this.Code().length > 0;  
        },
        
        areOwningAirlinesValid: function(){
            return this.OwningAirlines().length > 0;
        },
        
        areTiersValid: function(){
            return this.Tiers().length > 0;
        },
        
        areConfiguredAccrualsValid: function(){

            for(var i = 0 ; i < this.ConfiguredAccruals().length ; i++){
                var ca = this.ConfiguredAccruals()[i];
                if(!ca.isValid()) return false;
            }
            
            return true;
        },
        
        /**
         * Is FFP valid to save?
         */ 
        isValid: function(){
            return this.isNameValid() && this.isCodeValid() && this.areOwningAirlinesValid() 
                && this.areTiersValid() && this.areConfiguredAccrualsValid();
        },
        
        /**
         * Has currently edited rule next one in collection?
         */ 
        hasNextRule: function(){
            var index = this.rules.indexOf(this.editedRule());
            
            if(this.applyToAllMode()){
                
                var rules = this.rules();
                for(var i = index + 1 ; i < rules.length ; i++){
                    if(this.selectedRules.indexOf(rules[i].RuleId()) >= 0) return true;
                }
                return false;
                
            }else{
                
                if(index < 0 || index > this.rules().length - 2) return false;  
                return true;
                
            }
        },
                
        /**
         * Has currently edited rule previous one in collection?
         */ 
        hasPrevRule: function(){
            var index = this.rules.indexOf(this.editedRule());
            
            if(this.applyToAllMode()){
                
                var rules = this.rules();
                for(var i = index - 1 ; i > 0 ; i--){
                    if(this.selectedRules.indexOf(rules[i].RuleId()) >= 0) return true;
                }
                return false;
                
            }else{
            
                if(index <= 0 || index > this.rules().length - 1) return false;
                return true;
            }
        },
        
        /**
         * Are all selected rules editable?
         */ 
        canEditRules: function(){
            var ids = this.selectedRules();
            var rules = this.rules();
            if(ids.length < 1) return false;
            
            for(var i = 0 ; i < rules.length ; i++){
                var rule = rules[i];
                
                if(ids.indexOf(rule.RuleId()) >= 0){
                    if(!rule.isInQuickEditMode()) return true;
                }
            }
            
            return false;
        },
        
        /**
         * Are all selected rules allowed to be cloned?
         */ 
        canCloneRules: function(){
            var ids = this.selectedRules();
            var rules = this.rules();
            if(ids.length < 1 || this.applyToAllMode()) return false;
            
            for(var i = 0 ; i < rules.length ; i++){
                var rule = rules[i];
                
                if(ids.indexOf(rule.RuleId()) >= 0){
                    if(rule.isInQuickEditMode()) return false;
                }
            }
            
            return true;
        },
        
        /**
         * Is any rule within selected, which is edited?
         */ 
        canSaveRules: function(){
            var ids = this.selectedRules();
            var rules = this.rules();
            if(ids.length < 1) return false;
            
            for(var i = 0 ; i < rules.length ; i++){
                var rule = rules[i];
                
                if(ids.indexOf(rule.RuleId()) >= 0){
                    if(rule.isInQuickEditMode()) return true;
                }
            }
            
            return false;
        },
        
        /**
         * Is any rule to delete?
         */ 
        canDeleteRules: function(){
            return this.selectedRules().length > 0 && !this.applyToAllMode();
        },
        
        /**
         * Is any rule within selected, which is edited?
         */ 
        canApplyToAllRules: function(){
            return this.selectedRules().length > 1;
        }
    },
    
    mapped: {
        
        //property -> class
        
        ConfiguredAccruals: _3.FFPAccrual,
        editedTier: _3.FFPTier,
        Tiers: _3.FFPTier,
        rules: _3.Rule,
        editedRule: _3.Rule,
        airlinesInput: _3.TagInput
    },
    
    /**
     * Properties not included in save FPP JSON
     */ 
    filtered: [
        'observed',
        'computed',
        'mapped',
        'filtered',
        'editedTier',
        'isLoaded',
        'airlinesInput',
        'editedRule',
        'ruleNumericId',
        'EnabledClass',
        'EnabledText',
        'IsValid',
        'isNameValid',
        'isCodeValid',
        'areOwningAirlinesValid',
        'areTiersValid',
        'areConfiguredAccrualsValid',
		'rules',
		'canDeleteRules',
		'canSaveRules',
		'canEditRules',
		'canCloneRules',
		'selectedRules',
		'allRulesSelected',
        'skipRulesSorting'
    ],

    /**
     * Edit program
     */ 
    edit: function(){
        if(!this.isLoaded()) this.load($.proxy(this.onEditSuccess, this));
        else this.onEditSuccess();
    },
    
    onEditSuccess: function(){
        _3.view.editFfp(this);
    },
    
    /**
     * Cancel program edition
     */ 
    cancelEdit: function(){
        
        //reload previous state
        if(!this.isNew()) this.load($.proxy(this.onCancelEditSuccess, this));
        else this.onCancelEditSuccess();

    },
    
    cancelAndShowRules: function(){
        if(!this.isNew()) this.load(function(){});
        this.showRules();
    },
    
    onCancelEditSuccess: function(){
        //switch to another view
        _3.view.cancelFfpEdit();   
    },

    /**
     * Push FFP data to server
     */ 
    save: function(callback){ 
        if(!this.isValid()){
            _3.view.validate(true);
            return;
        }
        
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl(
            this.isNew() ? 'ffp-add.json' : 'ffp-update.json'
        );
        
        var data = ko.toJSON(this, this.jsonFilter);
        
        $.post(url, data, $.proxy(function(response){
            if(response.Success){
                
                this.isLoaded(true);
                this.isNew(false);
                /*if codeshare has been changed then erase rules */
                if (this.originalCodeshare != this.Codeshare()) {
                    this.update('rules', []);
                    this.hasRulesLoaded(false);
                }

                this.originalCodeshare = this.Codeshare();

                
                _3.view.saveFfp();
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);
        }, this));  
    },
    
    /**
     * Pull FFP data from server
     */ 
    load: function(callback){
        _3.view.isLoading(true);
        
        $.get(_3.utils.getJsonUrl('ffp-one.json'), {
           Id: this.Id
        }, $.proxy(function(response){
            
            if(response.Success){
                
                //overwrite properties with new data
                this.ext(response.FFP);

                this.originalCodeshare = response.FFP.Codeshare ? response.FFP.Codeshare : '1';

                
                this.isLoaded(true);
                
                if($.isFunction(callback)) callback();
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false);   
        }, this));  
    },
    
    /**
     * Inits knockout.js subscribers on observed properties
     * @see KClass
     */ 
    initSubscribed: function(){
        
        this.allRulesSelected.subscribe($.proxy(this.toggleRulesSelection, this));
        
        this.applyToAllMode.subscribe($.proxy(function(newValue){
            if(!newValue) this.applyToAllInProgress = false;
        }, this));
        
    },

    /**
     * Overwrite FFP properties with new ones, create according objects as set in mapped{}
     * @param Object data
     * @see KClass 
     */ 
    ext: function(data){
        this.parent(data);
        
        //add all accruals set as default in FFPViewModel
        //@see ffp-all.json
        this.addDefaultAccruals();
        
        //update tag input with airlines
        this.update('airlinesInput', {
            beforeAdd: _3.utils.toUpperCase,
            tags: this.OwningAirlines
        });
    },
    
    /**
     * Add FFP rule
     */ 
    addRule: function(){
        
        //make sure this FFP is in 'edit' mode
        if(_3.view.editedFfp() != this) _3.view.setEditedFfp(this);
        
        //pull FFP data from server, bacause it's needed to setup new rule
        if(!this.isLoaded()) this.load();
        
        //create new rule
        this.setEditedRule();
        
        //open rule edit section
        _3.view.editRule();
    },
    
    /**
     * Sets rule in 'edit' mode
     * @param FFPRule rule
     */ 
    setEditedRule: function(rule){
        
        if(rule) this.editedRule(rule); //set existing rule to edit
        else if(rule === null || rule === false) this.editedRule(null); //no rule edited
        else {
            //create new rule and edit
            
            this.editedRule(new _3.Rule({
                isNew: true,
                ProgramId: this.Id,
                Subsidiaries: [],
                GeoFilters: [],
                CodedConditions: [],
                MarketingSpecificAirlines: [],
                OperatingSpecificAirlines: [],
                Tiers: []
            }));
        }
    },
    
    /**
     * Show FFP rules
     */ 
    showRules: function(){
        if(!this.isLoaded()) this.load();
        if(!this.hasRulesLoaded()) this.loadRules($.proxy(this.onShowRulesSuccess, this));
        else this.onShowRulesSuccess();
    },
    
    onShowRulesSuccess: function(){
        
        if (this.skipRulesSorting) {
            this.skipRulesSorting = false;
        } else {
            this.sortRules();
        }

        _3.view.showRules(this);

        if(this.applyToAllMode()) this.quickEditRules();
        
    },
    
    /**
     * Pull rules data from server to display rules grid
     */ 
    loadRules: function(callback){
        _3.view.isLoading(true);
        
        $.post(_3.utils.getJsonUrl('rule-all.json'), {
           Id: this.Id
        }, $.proxy(function(response){
            
            if(response.Success){
                
                this.update('rules', response.Rules);
                this.hasRulesLoaded(true);
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false); 
            
        }, this));
    },
    
    /**
     * Update FFP codeshare name
     * @see FFPViewModel.earningBases
     */ 
    updateCodeshareName: function(){
        var eb = _3.view.earningBases();
        
        for(var i = 0 ; i < eb.length ; i++){
            if(eb[i].Id == this.Codeshare()){
                this.CodeshareName(eb[i].Name);
                break;
            }
        }
    },
    
    /**
     * Cancel rule edit, go back to rules grid
     */ 
    cancelRuleEdit: function(){
        this.setEditedRule(null);
        this.showRules();
    },

    /**
     * Add new rule to rules array and close rule edit mode
     */ 
    saveRule: function(){
    
        if(this.rules.indexOf(this.editedRule()) == -1){
            _3.view.scrollTo = this.editedRule();
            this.rules.push(this.editedRule());
        }
        //this.sortRules();
        this.skipRulesSorting = true;
        this.cancelRuleEdit();
    },
    
    sortRules: function(){
        this.rules.sort(this.cmpRules);
    },

    /**
     * Edit FFP rule
     * @param FFPRule rule
		*/ 
    editRule: function(rule){
        if(!this.isLoaded()) this.load($.proxy(this.onEditRuleSuccess, this, rule));
        else this.onEditRuleSuccess(rule);
    },
    
    onEditRuleSuccess: function(rule){
        this.setEditedRule(rule);
        _3.view.editRule();
    },
    
    /**
     * Edit FFP rule in in-row mode
     */ 
    quickEditRule: function(rule){
        this.setEditedRule(rule);
    },
    
    /**
     * Open slected rules in quick edit mode
     */ 
    quickEditRules: function(){
        this.foreachSelectedRule($.proxy(function(rule){
            rule.quickEdit();
        }, this));
    },
    
    /**
     * Save all selected rules
     */ 
    saveRules: function(){
        var data = '';
        
        var valid = this.foreachSelectedRule($.proxy(function(rule){
            if(rule.isInQuickEditMode() && !rule.isValid()) return false;
            data += (data == '' ? '' : ',') + ko.toJSON(rule, rule.jsonFilter);
        }, this));
        
        if(!valid){
            _3.view.validate(true);
            return;
        }
            
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('rules-update-quick.json');
        data = '{"Rules":[' + data + ']}';
        
        $.post(url, data, $.proxy(function(response){
            
            if(response.Success){
                
                this.foreachSelectedRule($.proxy(function(rule){
                    
                    var rulesData = response.Rules;
                    
                    for(var i = 0 ; i < rulesData.length ; i++){
                        
                        if(rulesData[i].RuleId == rule.RuleId()){
                            
                            rule.onSaveSuccess(rulesData[i]);
                            
                            break;
                            
                        }
                    }
                }, this));
                
                this.applyToAllMode(false);
                
            }else{
                _3.view.addAlert(response.Message);
            }			
                
            _3.view.isLoading(false); 
            
        }, this));  
    },
    
    /**
     * Drops changes made in selected rules and gets
     * original data from server
     */ 
    cancelEditRules: function(callback){
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('rule-all.json');
        
        var data = {
            Ids: this.selectedRules()
        };
        
        $.post(url, data, $.proxy(function(response){
            
            if(response.Success){
                
                var rules = response.Rules;
                
                this.foreachSelectedRule($.proxy(function(rule){
                    
                    for(var i = 0 ; i < rules.length ; i++){
                        
                        if(rules[i].RuleId == rule.RuleId()){
                            rule.ext(rules[i]);
                            rule.isInQuickEditMode(false);
                            rules.splice(i, 1);
                            break;
                        }
                        
                    }
                    
                }, this));
                
                if($.isFunction(callback)) callback();
                
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false); 
            
        }, this));
    },
    
    /**
     * Clone all selected rules
     */ 
    cloneRules: function(){
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('rules-clone.json');
        
        var data = {
            Ids: this.selectedRules()
        };
        
        $.post(url, data, $.proxy(function(response){
            
            if(response.Success){
                var rulesData = response.Rules;
                
                for(var i = 0 ; i < rulesData.length ; i++){
                    $.apply(rulesData[i], { isNew: false });
                    this.setEditedRule(new _3.Rule(rulesData[i]));
                    this.saveRule();
                }
            }else{
                _3.view.addAlert(response.Message);
            }			
                
            _3.view.isLoading(false); 
            
        }, this)); 
    },
    
    /**
     * Delete selected rules
     */ 
    removeRules: function(){
        //prompt user if he is sure what is he doing...
        _3.view.removeRules();
    },
    
    confirmRemoveRules: function(){
        this.cancelRemoveRules();
        
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('rules-delete.json');
        
        var data = {
            Ids: this.selectedRules()
        };
        
        $.post(url, data, $.proxy(function(response){
            
            if(response.Success){
                var rules = [];
                
                this.foreachSelectedRule($.proxy(function(rule){
                    rules.push(rule);
                }, this));
                
                for(var i  = 0 ; i < rules.length ; i++) this.rules.remove(rules[i]);
                
                this.selectedRules([]);
                
            }else{
                _3.view.addAlert(response.Message);
            }			
                
            _3.view.isLoading(false); 
            
        }, this)); 
        
    },
    
    /**
     * Invoke function on each selected rule object
     * @param func - custom function
     */ 
    foreachSelectedRule: function(func){
        var ids = this.selectedRules();
        var rules = this.rules();
        for(var i = 0 ; i < rules.length ; i++){
            if(ids.indexOf(rules[i].RuleId()) >= 0){
                if(false === func(rules[i])) return false;
            }
        }
        return true;
    },
    
    /**
     * Remove rule
     * @param FFPRule rule
     */
    removeRule: function(rule){
        
        this.setEditedRule(rule);
        
        //prompt user if he is sure what is he doing...
        _3.view.removeRule();
    },

    /**
     * Cancel removing rule
     */ 
    cancelRemoveRule: function(){
        this.setEditedRule(null);
        _3.view.cancelRemoveRule();
    },
    
    /**
     * Cancel removing rules
     */ 
    cancelRemoveRules: function(){
        _3.view.cancelRemoveRules();
    },
    
    /**
     * Delete FFP rule entirely
     */
    confirmRemoveRule: function(callback){
        
        //close modal alert window
        _3.view.cancelRemoveRule();
        
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('rule-delete.json');
        
        $.post(url, {
            //deleted FFP rule id
            Id: this.editedRule().Id()
        }, $.proxy(function(response){
            
            if(response.Success){
                
                //Confirmed? Remove from FFP
                this.rules.remove(this.editedRule());
                if($.isFunction(callback)) callback();
            }else{
                _3.view.addAlert(response.Message);
            }
            
            _3.view.isLoading(false); 
        }, this));
    },
    
    /**
     * Clone FFP rule
     * @param FFPRule rule to clone
     */ 
    cloneRule: function(rule){
        
        _3.view.isLoading(true);
        
        var url = _3.utils.getJsonUrl('rule-add.json');
        
        var data = {
            Ids: [rule.Id]
        };
        
        $.post(url, data, $.proxy(function(response){
            
            if(response.Success){
                var ruleData = response.Rule;
                
                $.apply(ruleData, { isNew: false });
                this.setEditedRule(new _3.Rule(ruleData));
                this.saveRule();
                
            }else{
                _3.view.addAlert(response.Message);
            }			
                
            _3.view.isLoading(false); 
            
        }, this)); 
        
        
    },

    /**
     * Makes sure all available accrual types are included in FFP, 
     * even if not all of them are enabled for that FFP
     * 
     * Necessary to preserve models consistency in knockout.js bindings.
     * 
     * @see FFPViewModel.accruals
     */
    addDefaultAccruals: function(){
        if(!_3.view || _3.view.accruals().length < 1) return;
        
        var accruals = _3.view.accruals();
        
        var ca = this.ConfiguredAccruals();
        /*var existingTypes = [];
        
        for(var i = 0 ; i < ca.length ; i++){
            existingTypes.push(ca[i].AccrualType());
        }
        
        for(var i = 0 ; i < accruals.length ; i++){
            if(existingTypes.indexOf(accruals[i].AccrualType()) == -1) this.ConfiguredAccruals.push($.extend({}, accruals[i]));
        }*/
        if(ca.length < 1){
            this.addAccrual();
        }
    },
    
    addAccrual: function(){
        this.ConfiguredAccruals.push(new _3.FFPAccrual());  
    },
    
    removeAccrual: function(accrual){
        this.ConfiguredAccruals.remove(accrual);
    },
    
    /**
     * Add FFP tier
     * @param FFPTier tier
     */ 
    addTier: function(tier){
        this.Tiers.push(tier);
        this.editedTier(new _3.FFPTier());
    },
    
    /**
     * Remove FFP tier
     * @param FFPTier tier
     */ 
    removeTier: function(tier){
        this.Tiers.remove(tier);
    },
    
    /**
     * Edit FFP tier
     * @param FFPTier tier
     */ 
    editTier: function(tier){
        this.editedTier(tier);  
        this.removeTier(tier);
    },
    
    /**
     * Edit previous rule to currently edited
     */ 
    prevRule: function(){
        if(!this.hasPrevRule()) this.cancelRuleEdit();
        else{
            
            var index = this.rules.indexOf(this.editedRule());
            
            if(this.applyToAllMode()){
                
                var rules = this.rules();
                for(var i = index - 1 ; i > 0 ; i--){
                    if(this.selectedRules.indexOf(rules[i].RuleId()) >= 0){
                        rules[i].edit();
                        break;
                    }
                }
                
            }else{
                this.rules()[index - 1].edit();
            }

        }
    },
    
    /**
     * Edit next rule to currently edited
     */ 
    nextRule: function(){
        if(!this.hasNextRule()) this.cancelRuleEdit();
        else{
            var index = this.rules.indexOf(this.editedRule());
            
            if(this.applyToAllMode()){
                
                var rules = this.rules();
                for(var i = index + 1 ; i < rules.length ; i++){
                    if(this.selectedRules.indexOf(rules[i].RuleId()) >= 0){
                        rules[i].edit();
                        break;
                    }
                }
                
            }else{
                this.rules()[index + 1].edit();   
            }
        }
    },
    
    /**
     * Select / deselect all rules 
     */ 
    toggleRulesSelection: function(newValue){
        var checks = null;
        
        if(newValue){
            checks = $('#rules-grid input[name="rule-selected"]').not(':checked');
        }else{
            checks = $('#rules-grid input[name="rule-selected"]:checked');
        }
        
        checks.click();
    },
    
    /**
     * Compare two rules for sorting purpose
     */ 
    cmpRules: function(a, b){
        if(a.AirlineCode() < b.AirlineCode()) return -1;
        else if(a.AirlineCode() > b.AirlineCode()) return 1;
        
        return a.RuleId() == b.RuleId() ? 0 : (a.RuleId() < b.RuleId() ? -1 : 1);
    },
    
    /**
     * Separate groups of rules
     */ 
    separateVisually: function(){

        $('#rules-grid tbody tr').each($.proxy(function(index, el){
            
            var row = $(el);
            
            if(index < 1){
                row.addClass('even').removeClass('odd');
                return true;
            }
            
            var prevRow = row.prev();
            var prev = prevRow.hasClass('even');
            
            if(row.find('.airline-code').html() != prevRow.find('.airline-code').html()){
                row.removeClass(prev ? 'even' : 'odd').addClass(prev ? 'odd' : 'even').addClass('first');
            }else{
                row.removeClass(prev ? 'odd' : 'even').addClass(prev ? 'even' : 'odd');    
            }
            
        }, this));
        
    },
    
    /**
     * Toggle 'apply to all' rules mode
     */ 
    toggleApplyToAll: function(){
        
        if(this.applyToAllMode()){
            
            this.cancelEditRules($.proxy(function(){
                this.applyToAllMode(false);
            }, this));
            
        }else{
            
            this.quickEditRules();
            this.applyToAllMode(true);
            
        }
    },
    
    /**
     * Apply rule modified value to all selected ones
     */ 
    applyToAll: function(rule, propName, newValue){
        
        this.applyToAllInProgress = true;
        
        var rules = this.rules();
        
        for(var i = 0 ; i < rules.length ; i++){
            
            var r = rules[i];
            
            if(r.RuleId() == rule.RuleId() || this.selectedRules.indexOf(r.RuleId()) < 0) continue;
            
            if(propName != 'AccrualGroups') r[propName](newValue);
            else this.updateRuleAccrualGroup(rule, newValue);
            
        }
        
        this.applyToAllInProgress = false;
    },
    
    updateRuleAccrualGroup: function(rule, accrualGroup){
        
        var ag = rule.AccrualGroups();
        
        for(var i = 0 ; i < ag.length; i++){
            if(ag[i].AccrualType() == accrualGroup.AccrualType() && ag[i].EarningBase() == accrualGroup.EarningBase()){
                ag[i].Value(accrualGroup.Value());
            }
        }
        
    }
});
